#ifndef Management_h__
#define Management_h__

#include "Engine_Include.h"
#include "Renderer.h"
#include "Scene.h"

BEGIN(Engine)

class ENGINE_DLL CManagement
{
	DECLARE_SINGLETON(CManagement)

private:
	CManagement(void);
	~CManagement(void);

public:
	const VTXTEX* GetTerrainVertex(const WORD& LayerID, const wstring& wstrObjKey);

public:
	HRESULT		Initialize(LPDIRECT3DDEVICE9 pGraphicDev);
	void		Update(void);
	void		Render(float fTime);
	CRenderer* GetRender() { return m_pRenderer; }

public:
	template<typename T>
	HRESULT	SceneChange(T& Functor);

private:
	void		Release(void);

private:
	LPDIRECT3DDEVICE9		m_pGraphicDev	= nullptr;
	CScene*					m_pScene		= nullptr;
	CRenderer*				m_pRenderer		= nullptr;
};

template<typename T>
HRESULT Engine::CManagement::SceneChange(T& Functor)
{
	if (nullptr != m_pScene)
		Engine::Safe_Delete(m_pScene);

	FAILED_CHECK(Functor(&m_pScene, m_pGraphicDev));

	m_pRenderer->SetScene(m_pScene);

	return S_OK;
}

END
#endif // Management_h__
