#ifndef GameObject_h__
#define GameObject_h__

#include "Component.h"

BEGIN(Engine)

class CInfoSubject;
class CManagement;

class CTransform;
class CResourcesMgr;
class ENGINE_DLL CGameObject
{
protected:
	explicit CGameObject(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CGameObject(void);

public:
	virtual const VTXTEX* GetTerrainVertex(void) { return nullptr; }
	const CComponent*	GetComponent(const wstring& wstrComponentKey);

public:
	virtual HRESULT		Initialize(void);
	virtual void		Update(void);
	virtual void		Render(void);
	virtual void		Release(void);

private:

protected:
	LPDIRECT3DDEVICE9			m_pGraphicDev;
	CTransform*					m_pInfo = nullptr;
	CResourcesMgr*				m_pResourcesMgr = nullptr;
	CManagement*				m_pManagement = nullptr;
	CInfoSubject*				m_pInfoSubject = nullptr;
	float m_fSize = 1.f;

	DIRECTION m_DIR[END_COMPARE];

	map<wstring, CComponent*>	m_mapComponent;
};

END

#endif // GameObject_h__
