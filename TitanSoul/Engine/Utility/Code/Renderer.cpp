#include "Renderer.h"

USING(Engine)

Engine::CRenderer::CRenderer(LPDIRECT3DDEVICE9 pGraphicDev)
	: m_pGraphicDev(pGraphicDev)
{
	ZeroMemory(m_szFPS, sizeof(m_szFPS));
}

Engine::CRenderer::~CRenderer(void)
{
	Safe_Release(m_pD3DXFont);
	Safe_Release(m_pFont);
}

void Engine::CRenderer::SetScene(CScene* pScene)
{
	m_pScene = pScene;
}

void Engine::CRenderer::Render(float fTime)
{
	//RECT	rc{ 100, 100, 200, 200 };

	++m_wFrameCnt;
	m_fTime += fTime;

	if (1.f < m_fTime)
	{
		//wsprintf(m_szFPS, L"FPS : %d", m_wFrameCnt);
		m_fTime = 0.f;
		m_wFrameCnt = 0;
	}


	m_pGraphicDev->Clear(0, nullptr, D3DCLEAR_STENCIL | D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_ARGB(255, 0, 0, 0), 1.f, 0);
	m_pGraphicDev->BeginScene();

	//m_pD3DXFont->DrawText(NULL, m_szFPS, lstrlen(m_szFPS), &rc, DT_NOCLIP, D3DCOLOR_ARGB(255, 0, 255, 0));

	if (nullptr != m_pScene)
		m_pScene->Render();

	m_pGraphicDev->EndScene();
	m_pGraphicDev->Present(nullptr, nullptr, nullptr, nullptr);
}

Engine::CRenderer* Engine::CRenderer::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CRenderer*	pRenderer = new CRenderer(pGraphicDev);

	if (FAILED(pRenderer->Initialize()))
		Engine::Safe_Delete(pRenderer);

	return pRenderer;
}

HRESULT Engine::CRenderer::Initialize(void)
{
	D3DXFONT_DESC		hFont;

	ZeroMemory(&hFont, sizeof(D3DXFONT_DESC));

	hFont.Weight = FW_HEAVY;
	hFont.Width = 15;
	hFont.Height = 20;
	lstrcpy(hFont.FaceName, L"�ü�");
	hFont.CharSet = HANGEUL_CHARSET;

	FAILED_CHECK(D3DXCreateFontIndirect(m_pGraphicDev, &hFont, &m_pD3DXFont));

	D3DXFONT_DESCW tFontInfo;
	ZeroMemory(&tFontInfo, sizeof(D3DXFONT_DESCW));

	tFontInfo.Height = 20;	// ����
	tFontInfo.Width = 15;	// �ʺ�
	tFontInfo.Weight = FW_BOLD;	// �β�
	tFontInfo.CharSet = HANGEUL_CHARSET; // �ѱ�
	lstrcpy(tFontInfo.FaceName, L"��������"); // �۾�ü	

	if (FAILED(D3DXCreateFontIndirect(m_pGraphicDev, &tFontInfo, &m_pFont)))
	{
		MSG_BOX("CreateFont Failed");
		return E_FAIL;
	}

	return S_OK;
}

