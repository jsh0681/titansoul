#include "Scene.h"

USING(Engine)

Engine::CScene::CScene(LPDIRECT3DDEVICE9 pGraphicDev)
	: m_pGraphicDev(pGraphicDev)
{

}

Engine::CScene::~CScene(void)
{
	Release();
}

HRESULT Engine::CScene::Initialize(void)
{
	return S_OK;
}

void Engine::CScene::Update(void)
{
	for (auto& iter : m_mapLayer)
		iter.second->Update();
}

void Engine::CScene::Render(void)
{
	for (auto& it : m_mapLayer)
		it.second->Render();
}

void Engine::CScene::Release(void)
{

	for (int i = 0; i < RENDER_END; ++i)
	{
		for (auto& it : m_mapRenderLayer[i])
			it.second->AllClearList();

		for_each(m_mapRenderLayer[i].begin(), m_mapRenderLayer[i].end(), CDeleteMap());
		m_mapRenderLayer[i].clear();
	}

	for_each(m_mapLayer.begin(), m_mapLayer.end(), CDeleteMap());
	m_mapLayer.clear();

}
void CScene::ObjListClear(WORD _wLayerKey, wstring _ObjListKey)
{
	m_mapLayer[_wLayerKey]->SafeDeleteObjList(_ObjListKey);
}
void CScene::ObjListAdd(WORD _wLayerKey, wstring _ObjListKey, CGameObject* _pObject)
{
	m_mapLayer[_wLayerKey]->AddObject(_ObjListKey, _pObject);

}
void CScene::BloodMode()
{
}
const VTXTEX* Engine::CScene::GetTerrainVertex(const WORD& LayerID, const wstring& wstrObjKey)
{
	auto iter = m_mapLayer.find(LayerID);

	if (iter == m_mapLayer.end())
		return nullptr;

	return iter->second->GetTerrainVertex(wstrObjKey);

}
