#include "GameObject.h"

USING(Engine)

Engine::CGameObject::CGameObject(LPDIRECT3DDEVICE9 pGraphicDev)
	: m_pGraphicDev(pGraphicDev)
{
	m_DIR[CUR] = DD;
	m_DIR[PRE] = DD;
}

Engine::CGameObject::~CGameObject(void)
{
	Release();
}

HRESULT Engine::CGameObject::Initialize(void)
{
	return S_OK;
}

void Engine::CGameObject::Update(void)
{
	for (auto& iter : m_mapComponent)
		iter.second->Update();
}

void Engine::CGameObject::Render(void)
{

}

void Engine::CGameObject::Release(void)
{
	for_each(m_mapComponent.begin(), m_mapComponent.end(), CDeleteMap());
	m_mapComponent.clear();
}

const Engine::CComponent* Engine::CGameObject::GetComponent(const wstring& wstrComponentKey)
{
	auto		iter = m_mapComponent.find(wstrComponentKey);

	if (iter == m_mapComponent.end())
		return nullptr;
	
	return iter->second;
}

