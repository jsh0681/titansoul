#include "InfoSubject.h"

USING(Engine)
IMPLEMENT_SINGLETON(CInfoSubject)

Engine::CInfoSubject::CInfoSubject(void)
{

}

Engine::CInfoSubject::~CInfoSubject(void)
{
	Release();
}

void Engine::CInfoSubject::AddData(int iFlag, void* pData)
{
	if (nullptr != pData)
	{
		auto	iter = m_MapDataList.find(iFlag);

		if (iter == m_MapDataList.end())
		{
			m_MapDataList[iFlag] = DATALIST();
		}

		m_MapDataList[iFlag].push_back(pData);
		Notify(iFlag);
	}
}

void Engine::CInfoSubject::RemoveData(int iFlag, void* pData)
{
	auto		iter = m_MapDataList.find(iFlag);

	if (iter != m_MapDataList.end())
	{
		auto	iterList = iter->second.begin();
		auto	iterList_End = iter->second.end();

		for (; iterList != iterList_End; ++iterList)
		{
			if ((*iterList) == pData)
			{
				iter->second.erase(iterList);
				return;
			}
		}
	}

}

void Engine::CInfoSubject::Release(void)
{
	for (auto& iter : m_MapDataList)
		iter.second.clear();

	m_MapDataList.clear();
}

DATALIST* Engine::CInfoSubject::GetDataList(int iFlag)
{
	auto	iter = m_MapDataList.find(iFlag);

	if (iter == m_MapDataList.end())
		return nullptr;

	return &iter->second;
}

