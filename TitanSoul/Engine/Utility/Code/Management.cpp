#include "Management.h"

USING(Engine)
IMPLEMENT_SINGLETON(CManagement)

Engine::CManagement::CManagement(void)
{

}

Engine::CManagement::~CManagement(void)
{
	Release();
}

HRESULT Engine::CManagement::Initialize(LPDIRECT3DDEVICE9 pGraphicDev)
{
	m_pGraphicDev = pGraphicDev;

	m_pRenderer = CRenderer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN_MSG(m_pRenderer, E_FAIL, L"Renderer Create Failed");

	

	return S_OK;
}

void Engine::CManagement::Update(void)
{
	if (nullptr != m_pScene)
		m_pScene->Update();
}

void Engine::CManagement::Render(float fTime)
{
	if (nullptr != m_pRenderer)
		m_pRenderer->Render(fTime);
}

void Engine::CManagement::Release(void)
{
	Engine::Safe_Delete(m_pScene);
	Engine::Safe_Delete(m_pRenderer);
}

const Engine::VTXTEX* Engine::CManagement::GetTerrainVertex(const WORD& LayerID, const wstring& wstrObjKey)
{
	return m_pScene->GetTerrainVertex(LayerID, wstrObjKey);
}

