#include "MathMgr.h"

USING(Engine)

CMathMgr::CMathMgr(void)
{

}

CMathMgr::~CMathMgr(void)
{

}

void CMathMgr::MyRotationX(D3DXVECTOR3* pOut, D3DXVECTOR3* pIn, float fAngle)
{
	/*1	0	0	0
    0   c   s   0
	0   -s  c   0
	0	0	0	1*/

	D3DXVECTOR3	vTemp = *pIn;

	pOut->x = vTemp.x;
	pOut->y = vTemp.y * cosf(fAngle) - vTemp.z * sinf(fAngle);
	pOut->z = vTemp.y * sinf(fAngle) + vTemp.z * cosf(fAngle);

 }

void CMathMgr::MyRotationY(D3DXVECTOR3* pOut, D3DXVECTOR3* pIn, float fAngle)
{
	/*c	0  -s	0
	0   0   0   0
	s   0   c   0
	0	0	0	1*/

	D3DXVECTOR3	vTemp = *pIn;

	pOut->x = vTemp.x * cosf(fAngle) + vTemp.z * sinf(fAngle);
	pOut->y = vTemp.y;
	pOut->z = vTemp.x * -sinf(fAngle) + vTemp.z * cosf(fAngle);
}

void CMathMgr::MyRotationZ(D3DXVECTOR3* pOut, D3DXVECTOR3* pIn, float fAngle)
{
	/*c	s  0	0
	-s  c   0   0
	0   0   1   0
	0	0	0	1*/

	D3DXVECTOR3	vTemp = *pIn;

	pOut->x = vTemp.x * cosf(fAngle) - vTemp.y * sinf(fAngle);
	pOut->y = vTemp.x * sinf(fAngle) + vTemp.y * cosf(fAngle);
	pOut->z = vTemp.z ;
}

float CMathMgr::MyVec3Dot(D3DXVECTOR3 * _pV1, D3DXVECTOR3 * _pV2)
{
	float fResult = (_pV1->x * _pV2->x + _pV1->y * _pV2->y + _pV1->z * _pV2->z);

	return fResult;
}

void CMathMgr::MyVec3Cross(D3DXVECTOR3 * pVOutput, D3DXVECTOR3 * _pV1, D3DXVECTOR3 * _pV2)
{
	//*pVOutput = { _pV1->y * _pV2->z - _pV1->z * _pV2->y,
	//	_pV1->z * _pV2->x - _pV1->x * _pV2->z,
	//	_pV1->x * _pV2->y - _pV1->y * _pV2->x };

	D3DXVECTOR3 v;

		v.x = _pV1->y * _pV2->z - _pV1->z * _pV2->y;
		v.y = _pV1->z * _pV2->x - _pV1->x * _pV2->z;
		v.z = _pV1->x * _pV2->y - _pV1->y * _pV2->x;

		*pVOutput = v;
}

float CMathMgr::MyVec3Length(D3DXVECTOR3 * _pInput)
{
	//벡터의 길이를 구하는 함수임.
	float fResult = (*_pInput).x * (*_pInput).x + (*_pInput).y * (*_pInput).y + (*_pInput).z * (*_pInput).z;
	return sqrtf(fResult);
}

void CMathMgr::MyVec3Normalize(D3DXVECTOR3 * _pOutput, D3DXVECTOR3 * _pInput)
{
	D3DXVECTOR3 vTemp = D3DXVECTOR3(*_pInput);

	float fLength = MyVec3Length(_pInput);

	if (vTemp.x != 0.f)
		vTemp.x /= fLength;

	if (vTemp.y != 0.f)
		vTemp.y /= fLength;

	if (vTemp.z != 0.f)
		vTemp.z /= fLength;

	(*_pOutput) = { vTemp.x, vTemp.y, vTemp.z };

	return;
}


DWORD CMathMgr::FtoDw(float f)
{
	return *((DWORD*)&f);
}

float CMathMgr::GetRandomFloat(float lowBound, float highBound)
{
	if (lowBound >= highBound) // bad input
		return lowBound;

	// get random float in [0, 1] interval
	float f = (rand() % 10000) * 0.0001f;

	// return float in [lowBound, highBound] interval. 
	return (f * (highBound - lowBound)) + lowBound;
}

void CMathMgr::GetRandomVector(D3DXVECTOR3* out, D3DXVECTOR3* min, D3DXVECTOR3* max)
{
	out->x = GetRandomFloat(min->x, max->x);
	out->y = GetRandomFloat(min->y, max->y);
	out->z = GetRandomFloat(min->z, max->z);
}
