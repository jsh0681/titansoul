#include <cstdlib>
#include "ParticleSystem.h"
#include"Export_Function.h"
#include"Engine_Include.h"

#include "../../Client/Headers/enum.h"

CCameraObserver::CCameraObserver(void)
	: m_pInfoSubject(Engine::Get_InfoSubject())
{

}

CCameraObserver::~CCameraObserver(void)
{

}

void CCameraObserver::Update(int iFlag)
{
	list<void*>*		pDataList = m_pInfoSubject->GetDataList(iFlag);
	NULL_CHECK(pDataList);

	switch (iFlag)
	{
	case CAMERAOPTION::VIEW:
		m_matView = *((D3DXMATRIX*)pDataList->front());
		break;

	case CAMERAOPTION::PROJECTION:
		m_matProj = *((D3DXMATRIX*)pDataList->front());
		break;

	case CAMERAOPTION::ORTHO:
		m_matOrtho = *((D3DXMATRIX*)pDataList->front());
		break;
	}
}

CCameraObserver* CCameraObserver::Create(void)
{
	return new CCameraObserver;
}


USING(Engine)

ParticleSystem::ParticleSystem() :
	m_pInfoSubject(Engine::Get_InfoSubject())
{
	m_pDevice = 0;
	m_pVB     = 0;
	m_pTexture    = 0;
}

ParticleSystem::~ParticleSystem()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Safe_Delete(m_pCameraObserver);

	 Safe_Release(m_pVB);
	 Safe_Release(m_pTexture);
}

bool ParticleSystem::Initialize(IDirect3DDevice9* device, wchar_t* texFileName)
{
	//	정점 버퍼의 크기는 우리 시스템의 입자 수와 같지 않습니다.
	//	정점 버퍼를 사용하여 한 번에 입자의 일부분을 그립니다.
	//	정점 버퍼에 대해 선택하는 임의의 크기는 m_pVBSize 변수로 지정됩니다.

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pDevice = device; // save a ptr to the device

	HRESULT hr = 0;

	hr = device->CreateVertexBuffer(
		m_pVBSize * sizeof(tagParticle),
		D3DUSAGE_DYNAMIC | D3DUSAGE_POINTS | D3DUSAGE_WRITEONLY,
		 FVF_PARTICLE,
		D3DPOOL_DEFAULT, // D3DPOOL_MANAGED랑 D3DUSAGE_DYNAMIC은 같이사용할수없다 뻑내버림 호환성이 없음
		&m_pVB,
		0);
	
	if(FAILED(hr))
	{
		::MessageBox(0, L"CreateVertexBuffer() - FAILED", L"ParticleSystem", 0);
		return false;
	}

	hr = D3DXCreateTextureFromFile(
		device,
		texFileName ,
		&m_pTexture);

	if(FAILED(hr))
	{
		::MessageBox(0, L"D3DXCreateTextureFromFile() - FAILED", L"ParticleSystem", 0);
		return false;
	}

	return true;
}

void ParticleSystem::Reset()
{
	std::list<tagAttribute>::iterator i;
	for(i = _particles.begin(); i != _particles.end(); i++)
	{
		ResetParticle( &(*i) );
	}
}

void ParticleSystem::AddParticle()
{
	tagAttribute attribute;

	ResetParticle(&attribute);

	_particles.push_back(attribute);
}

void ParticleSystem::PreRender()
{
	//m_pDevice->SetRenderState(D3DRS_LIGHTING, false);//조명 끔
	m_pDevice->SetRenderState(D3DRS_POINTSPRITEENABLE, true);// True는 현재 지정된 전체 텍스처를 포인트 스프라이트의 텍스처 매핑에 이용할 것임을 의미한다
	m_pDevice->SetRenderState(D3DRS_POINTSCALEENABLE, true); //포인트들이 다 동일한 크기가 아니라 카메라 행렬에의해서 원근감있게 나옴
	m_pDevice->SetRenderState(D3DRS_POINTSIZE, CMathMgr::FtoDw(m_fSize)); //포인트 스프라이트의 크기를 지정한다 
	m_pDevice->SetRenderState(D3DRS_POINTSIZE_MIN, CMathMgr::FtoDw(0.0f)); //포인트 스프라이트의 최소크기 지정


	// 거리에 따른 파티클의 크기를 제어한다.
	m_pDevice->SetRenderState(D3DRS_POINTSCALE_A, CMathMgr::FtoDw(0.0f));
	m_pDevice->SetRenderState(D3DRS_POINTSCALE_B, CMathMgr::FtoDw(0.0f));
	m_pDevice->SetRenderState(D3DRS_POINTSCALE_C, CMathMgr::FtoDw(1.0f));
		
	//텍스쳐 알파를 이용한다.
	m_pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

	m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pDevice->SetRenderState(D3DRS_ALPHAREF, 0x00000088);
	m_pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

}

void ParticleSystem::PostRender()
{
	m_pDevice->SetRenderState(D3DRS_POINTSPRITEENABLE, false);
	m_pDevice->SetRenderState(D3DRS_POINTSCALEENABLE,  false);
	m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	m_pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
}

void ParticleSystem::Render()
{
	/*
	Render 메서드는 정점 버퍼의 섹션을 데이터로 채우고, 그 부분을 렌더링합니다.
	해당 섹션이 렌더링되는 동안 새 섹션을 잠그고 해당 섹션을 채우기 시작합니다.
	일단 해당 섹션이 채워지면 렌더링됩니다.
	이 과정은 모든 파티클이 그려 질 때까지 계속됩니다.
	이 방법의 장점은 비디오 카드와 CPU를 계속 사용하는 것입니다.*/

	if( !_particles.empty() )
	{
		// 렌더 상태를 지정한다.
		PreRender();
		
		m_pDevice->SetTexture(0, m_pTexture);
		m_pDevice->SetFVF( FVF_PARTICLE);
		m_pDevice->SetStreamSource(0, m_pVB, 0, sizeof( tagParticle));

		// 버텍스 버떠를 벗어날 정우 처음부터 시작한다. 
		if(m_pVBOffset >= m_pVBSize)
			m_pVBOffset = 0;

		 tagParticle* v = 0;

		m_pVB->Lock(
			m_pVBOffset    * sizeof( tagParticle),
			m_pVBBatchSize * sizeof( tagParticle),
			(void**)&v,
			m_pVBOffset ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);//세그먼트 잠금
		// 플래그는 버텍스 버퍼의 일부가 렌더링될 때 렌더링되지 않는 일부 버퍼를 잠글 수 있도록 해준다

		DWORD numParticlesInBatch = 0;
	
		//const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
		//NULL_CHECK(pmatView);

		//const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
		//NULL_CHECK(pmatProj);

		//Engine::CPipeline::MyTransformCoord(&i->_position, &i->_position, &I);

		// 모든 파티클이 렌더링 될 때까지 
		std::list< tagAttribute>::iterator i;
		for(i = _particles.begin(); i != _particles.end(); i++)
		{


			if( i->_isAlive )
			{
				// 한 단계의 생존한 파티클을 다읍
				// 버텍스 버퍼 세그먼트로 복사한다 
				v->_position = i->_position;
				v->_color    = (D3DCOLOR)i->_color;

				//Engine::CPipeline::MyTransformCoord(&v->_position, &v->_position, pmatView);
				//Engine::CPipeline::MyTransformCoord(&v->_position, &v->_position, pmatProj);

				v++; // next element;


				numParticlesInBatch++;// 단계 카운터를 증가 시킨다. 

				//// 현재 단계가 모두 채워져 있는가? 
				if(numParticlesInBatch == m_pVBBatchSize) 
				{
					// 버텍스 버퍼에 복사된 입자의 마지막 단계의 파티클을 그립니다.
					m_pVB->Unlock();

					m_pDevice->DrawPrimitive(
						D3DPT_POINTLIST,
						m_pVBOffset,
						m_pVBBatchSize);

					
					/*단계가 그려지는 동안, 다음 단계를 파티클로 채운다.*/

					// 다음 배치의 시작 부분으로 오프셋 이동
					m_pVBOffset += m_pVBBatchSize; 

					// 버택스 버퍼의 경계를 넘는 메모리로 오프셋을 정하지 않는다.
					// 경계를 넘을 경우 처음부터 시작한다. 
					if(m_pVBOffset >= m_pVBSize) 
						m_pVBOffset = 0;       

					m_pVB->Lock(
						m_pVBOffset    * sizeof( tagParticle ),
						m_pVBBatchSize * sizeof(tagParticle),
						(void**)&v,
						m_pVBOffset ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);

					numParticlesInBatch = 0; // 다음 단계를 위한 리셋 
				}	
			}
		}

		m_pVB->Unlock();

		// 만족되지 못하여 마지막 단계가 렌더령되지 않는 경우가
		// 발생할 수 았다. 일부만 채워진 단계는
		// 바로 이곳에서 렌더링된다. 
		
		if( numParticlesInBatch )
		{
			m_pDevice->DrawPrimitive(
				D3DPT_POINTLIST,
				m_pVBOffset,
				numParticlesInBatch);
		}

		// 다음 블록 
		m_pVBOffset += m_pVBBatchSize; 

		PostRender();
	}
}

bool ParticleSystem::isEmpty()
{
	return _particles.empty();
}

bool ParticleSystem::isDead()
{
	std::list< tagAttribute>::iterator i;
	for(i = _particles.begin(); i != _particles.end(); i++)
	{
		// is there at least one living particle?  If yes,
		// the system is not dead.
		if( i->_isAlive )
			return false;
	}
	// no living particles found, the system must be dead.
	return true;
}

void ParticleSystem::removeDeadParticles()
{
	std::list< tagAttribute>::iterator i;

	i = _particles.begin();

	while( i != _particles.end() )
	{
		if( i->_isAlive == false )
		{
			// erase returns the next iterator, so no need to
		    // incrememnt to the next one ourselves.
			i = _particles.erase(i); 
		}
		else
		{
			i++; // next in list
		}
	}
}
