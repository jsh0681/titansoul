#include "Collision.h"

USING(Engine)

Engine::CCollision::CCollision(void)
	: m_pwRefCnt(new WORD(0))
{

}

Engine::CCollision::~CCollision(void)
{
	Release();
}


void Engine::CCollision::Release(void)
{
	if (0 == (*m_pwRefCnt))
		Engine::Safe_Delete(m_pwRefCnt);

	else
		(*m_pwRefCnt)--;
}

