#include "Layer.h"

USING(Engine)

Engine::CLayer::CLayer(LPDIRECT3DDEVICE9 pGraphicDev)
	: m_pGraphicDev(pGraphicDev)
{

}

Engine::CLayer::~CLayer(void)
{
	Release();
}

void Engine::CLayer::Update(void)
{
	for (auto& iter : m_mapObjList)
	{
		for (auto& ListIter : iter.second)
		{
			ListIter->Update();
		}
	}
}

void Engine::CLayer::Render(void)
{
	for (auto& iter : m_mapObjList)
	{
		for (auto& ListIter : iter.second)
		{
			ListIter->Render();
		}
	}
}

void CLayer::SafeDeleteObjList(wstring _wstrObjlistKey)
{
	auto& iter_find = m_mapObjList.find(_wstrObjlistKey);

	if (iter_find == m_mapObjList.end())
	{
		return;
	}

	//안에 내용 지우고
	for (auto& pObject : iter_find->second)
	{
		Safe_Delete(pObject);
	}


	//맵에서 삭제
	m_mapObjList.erase(iter_find);
}

void Engine::CLayer::Release(void)
{
	for (auto& iter : m_mapObjList)
	{
		for_each(iter.second.begin(), iter.second.end(), CDeleteObj());
		iter.second.clear();
	}
	m_mapObjList.clear();
}

Engine::CLayer* Engine::CLayer::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	return new CLayer(pGraphicDev);
}

HRESULT Engine::CLayer::AddObject(const wstring& wstrObjKey, 
								CGameObject* pGameObject)
{
	if (nullptr != pGameObject)
	{
		auto		iter = m_mapObjList.find(wstrObjKey);
		
		if (iter == m_mapObjList.end())
		{
			m_mapObjList[wstrObjKey] = list<CGameObject*>();
		}
		
		m_mapObjList[wstrObjKey].push_back(pGameObject);
	}

	return S_OK;
}

void CLayer::ClearList(const wstring& wstrObjKey)
{
	auto& it_find = m_mapObjList.find(wstrObjKey);

	if (it_find == m_mapObjList.end())
		return;

	it_find->second.clear();
}

void CLayer::AllClearList()
{
	for (auto& it : m_mapObjList)
		it.second.clear();
}

const Engine::CComponent* Engine::CLayer::GetComponent(const wstring& wstrObjKey, 
														const wstring& wstrComponentKey)
{
	auto		mapiter = m_mapObjList.find(wstrObjKey);

	if (mapiter == m_mapObjList.end())
		return nullptr;

	for (auto& iter : mapiter->second)
	{
		const CComponent*	pComponent = iter->GetComponent(wstrComponentKey);
		if (nullptr != pComponent)
			return pComponent;
	}
	return nullptr;
}

list<CGameObject*>* CLayer::GetObjList(const wstring& wstrObjkey)
{
	auto& it_find = m_mapObjList.find(wstrObjkey);

	if (it_find == m_mapObjList.end())
		return nullptr;

	return &it_find->second;
}

const Engine::VTXTEX* Engine::CLayer::GetTerrainVertex(const wstring& wstrObjKey)
{
	auto	iter = m_mapObjList.find(wstrObjKey);

	if (iter == m_mapObjList.end())
		return nullptr;
	
	return iter->second.front()->GetTerrainVertex();
}

