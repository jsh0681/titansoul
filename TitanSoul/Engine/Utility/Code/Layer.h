#ifndef Layer_h__
#define Layer_h__

#include "GameObject.h"

BEGIN(Engine)

class ENGINE_DLL CLayer
{
private:
	explicit	CLayer(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CLayer(void);

public:
	const VTXTEX*			GetTerrainVertex(const wstring& wstrObjKey);
	const CComponent*		GetComponent(const wstring& wstrObjKey,
									const wstring& wstrComponentKey);
	list<CGameObject*>*		GetObjList(const wstring& wstrObjkey);

public:
	HRESULT		AddObject(const wstring& wstrObjKey, CGameObject* pGameObject);
	void		ClearList(const wstring& wstrObjKey);
	void		AllClearList();
	void		Update(void);
	void		Render(void);


	void		SafeDeleteObjList(wstring _wstrObjlistKey);
	
private:
	void		Release(void);

private:
	unordered_map<wstring, list<CGameObject*>>		m_mapObjList;
	LPDIRECT3DDEVICE9						m_pGraphicDev;

public:
	static CLayer*		Create(LPDIRECT3DDEVICE9 pGraphicDev);

};

END
#endif // Layer_h__
