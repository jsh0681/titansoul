#ifndef Subject_h__
#define Subject_h__

#include "Observer.h"

BEGIN(Engine)

class ENGINE_DLL CSubject
{
protected:
	CSubject(void);
public:
	virtual ~CSubject(void);

public:
	virtual void Subscribe(CObserver* pObserver);
	virtual void UnSubscribe(CObserver* pObserver);
	virtual void Notify(int iFlag);

	virtual int GetSize() { return m_ObserverList.size(); }

private:
	void	Release(void);

protected:
	list<CObserver*>			m_ObserverList;

};

END
#endif // Subject_h__
