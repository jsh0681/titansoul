#include "TimeMgr.h"

USING(Engine)
IMPLEMENT_SINGLETON(CTimeMgr)

CTimeMgr::CTimeMgr(void)
{
	ZeroMemory(&m_FrameTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_FixTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_LastTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_CpuTick, sizeof(LARGE_INTEGER));
}

CTimeMgr::~CTimeMgr(void)
{
}

void CTimeMgr::InitTime(void)
{
	QueryPerformanceCounter(&m_FrameTime);
	QueryPerformanceCounter(&m_FixTime);
	QueryPerformanceCounter(&m_LastTime);

	QueryPerformanceFrequency(&m_CpuTick);
}

void CTimeMgr::SetTime(void)
{
	QueryPerformanceCounter(&m_FrameTime);

	if(m_FrameTime.QuadPart - m_LastTime.QuadPart > m_CpuTick.QuadPart)
	{
		QueryPerformanceFrequency(&m_CpuTick);
		m_LastTime.QuadPart = m_FrameTime.QuadPart;
	}
	
	m_fTime = float(m_FrameTime.QuadPart - m_FixTime.QuadPart) / m_CpuTick.QuadPart;
	if (m_bSlowMode)
	{
		m_fSlowTurm -= m_fTime;
		if (m_fSlowTurm < 0.f)
		{
			m_bSlowMode = FALSE;
		}
		m_fTime *= m_fSlowMotion;
	}
	m_FixTime = m_FrameTime;
}

void CTimeMgr::SlowModeStart(const float& fTime, const float& fSpd)
{
	m_bSlowMode = TRUE;
	m_fSlowTurm = fTime;
	m_fSlowMotion = fSpd;
}


float CTimeMgr::GetTime(void)
{
	return m_fTime;
}
