#include "ResourcesMgr.h"

USING(Engine)
IMPLEMENT_SINGLETON(CResourcesMgr)

Engine::CResourcesMgr::CResourcesMgr(void)
{

}

Engine::CResourcesMgr::~CResourcesMgr(void)
{
	Release();
}

void CResourcesMgr::ClientToEngine(RESOURCETYPE eResourceType, const wstring & wstrResourceKey, void * pVertex)
{
	auto	iter = m_mapResources[eResourceType].find(wstrResourceKey);

	if (iter == m_mapResources[eResourceType].end())
	{
		TAGMSG_BOX(wstrResourceKey.c_str(), L"버텍스 못 찾음");
		return;
	}

	dynamic_cast<CVIBuffer*>(iter->second)->ClientToEngine(pVertex);

}

void CResourcesMgr::EngineToClient(RESOURCETYPE eResourceType, const wstring & wstrResourceKey, void * pVertex)
{
	auto	iter = m_mapResources[eResourceType].find(wstrResourceKey);

	if (iter == m_mapResources[eResourceType].end())
	{
		TAGMSG_BOX(wstrResourceKey.c_str(), L"버텍스 못 찾음");
		return;
	}

	dynamic_cast<CVIBuffer*>(iter->second)->EngineToClient(pVertex);

}

HRESULT Engine::CResourcesMgr::AddBuffer(LPDIRECT3DDEVICE9 pGraphicDev, RESOURCETYPE eResourceType, BUFFERTYPE eBufferType, const wstring& wstrResourceKey, const WORD& wCntX /*= 0*/, const WORD& wCntZ /*= 0*/, const WORD& wItv /*= 1*/)
{
	auto	iter = m_mapResources[eResourceType].find(wstrResourceKey);

	if (iter != m_mapResources[eResourceType].end())
	{
		TAGMSG_BOX(wstrResourceKey.c_str(), L"중복된 리소스");
		return E_FAIL;
	}

	CResources*		pResources = nullptr;

	switch (eBufferType)
	{
	case BUFFER_TRICOL:
		pResources = CTriCol::Create(pGraphicDev);
		break;

	case BUFFER_RCCOL:
		pResources = CRcCol::Create(pGraphicDev);
		break;

	case BUFFER_RCTEX:
		if (wCntX != 0)
			pResources = CRcTex::Create(pGraphicDev, wCntX, wCntZ);
		else
			pResources = CRcTex::Create(pGraphicDev);
		break;

	case BUFFER_TERRAINCOL:
		pResources = CTerrainCol::Create(pGraphicDev, wCntX, wCntZ, wItv);
		break;

	case BUFFER_TERRAINTEX:
		pResources = CTerrainTex::Create(pGraphicDev, wCntX, wCntZ, wItv);
		break;

	case BUFFER_CUBETEX:
		if (wCntX == 1 && wCntZ == 1 && wItv == 1)
			pResources = CCubeTex::Create(pGraphicDev, wItv);
		else if (wCntX != 0)
			pResources = CCubeTex::Create(pGraphicDev, wCntX, wCntZ);
		else
			pResources = CCubeTex::Create(pGraphicDev);
		break;
		
	case BUFFER_RCTEX_Z:
		if (wCntX != 0)
			pResources = CRcTexZ::Create(pGraphicDev, wCntX, wCntZ);
		else
			pResources = CRcTexZ::Create(pGraphicDev);
		break;

	case BUFFER_CROSSTEX:
		if (wCntX != 0)
			pResources = CCrossTex::Create(pGraphicDev, wCntX, wCntZ);
		else
			pResources = CCrossTex::Create(pGraphicDev);
		break;

	}
	NULL_CHECK_RETURN(pResources, E_FAIL);

	m_mapResources[eResourceType].emplace(wstrResourceKey, pResources);
	
	return S_OK;
}

void Engine::CResourcesMgr::Render(const wstring& wstrResourceKey)
{
	auto		iter = m_mapResources[0].find(wstrResourceKey);

	if (iter == m_mapResources[0].end())
		return;


	iter->second->Render();
}

void Engine::CResourcesMgr::Render(const wstring& wstrResourceKey, const DWORD& iIndex)
{
	auto		iter = m_mapResources[0].find(wstrResourceKey);
	if (iter == m_mapResources[0].end())
		return;

	dynamic_cast<CTexture*>(iter->second)->Render(iIndex);
}

void Engine::CResourcesMgr::Release(void)
{
	for (size_t i = 0; i < RESOURCE_END; ++i)
	{
		for_each(m_mapResources[i].begin(), m_mapResources[i].end(), CDeleteMap());
		m_mapResources[i].clear();
	}
}

HRESULT Engine::CResourcesMgr::AddTexture(LPDIRECT3DDEVICE9 pGraphicDev, 
											RESOURCETYPE eResourceType, 
											TEXTYPE eTexType, 
											const wstring& wstrResourceKey, 
											const wstring& wstrFilePath, 
											const WORD& wCnt)
{
	auto		iter = m_mapResources[eResourceType].find(wstrResourceKey);

	if (iter != m_mapResources[eResourceType].end())
	{
		TAGMSG_BOX(wstrResourceKey.c_str(), L"중복된 리소스");
		return E_FAIL;
	}

	CResources*		pResources = CTexture::Create(pGraphicDev, eTexType, wstrFilePath, wCnt);
	NULL_CHECK_RETURN(pResources, E_FAIL);

	m_mapResources[eResourceType].emplace(wstrResourceKey, pResources);

	return S_OK;
}

void Engine::CResourcesMgr::ResetDynamic(void)
{
	for_each(m_mapResources[RESOURCE_DYNAMIC].begin(), m_mapResources[RESOURCE_DYNAMIC].end(), CDeleteMap());
	m_mapResources[RESOURCE_DYNAMIC].clear();
}

Engine::CComponent* Engine::CResourcesMgr::Clone(RESOURCETYPE eResourceType, 
												const wstring& wstrResourceKey)
{
	auto	iter = m_mapResources[eResourceType].find(wstrResourceKey);

	if (iter == m_mapResources[eResourceType].end())
	{
		TAGMSG_BOX(wstrResourceKey.c_str(), L"복사 실패");
		return nullptr;
	}

	return iter->second->Clone();

}

