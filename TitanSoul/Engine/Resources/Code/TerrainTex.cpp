#include "TerrainTex.h"

USING(Engine)

Engine::CTerrainTex::CTerrainTex(LPDIRECT3DDEVICE9 pGraphicDev)
	:CVIBuffer(pGraphicDev)
{

}

Engine::CTerrainTex::~CTerrainTex(void)
{
	Release();
}

void Engine::CTerrainTex::Render(void)
{
	CVIBuffer::Render();
}

HRESULT Engine::CTerrainTex::CreateBuffer(const WORD& wCntX, const WORD& wCntZ, const WORD& wItv)
{
	m_dwVtxSize = sizeof(VTXTEX);
	m_dwVtxCnt = wCntX * wCntZ;
	m_dwVtxFVF = VTXFVF_TEX;
	m_dwTriCnt = (wCntX - 1) * (wCntZ - 1) * 2;

	m_dwIdxSize = sizeof(INDEX32);
	m_IdxFmt    = D3DFMT_INDEX32;

	FAILED_CHECK_RETURN(CVIBuffer::CreateBuffer(), E_FAIL);

	VTXTEX*		pVtxTex = nullptr;


	m_pVB->Lock(0, 0, (void**)&pVtxTex, 0);

	int	iIndex = 0;

	for (int z = 0; z < wCntZ; ++z)
	{
		for (int x = 0; x < wCntX; ++x)
		{
			iIndex = z * wCntZ + x;

			pVtxTex[iIndex].vPos = D3DXVECTOR3(float(x) * wItv , 
												0.f, 
												float(z) * wItv);
			pVtxTex[iIndex].vTex = D3DXVECTOR2(x / (wCntX - 1.f), z / (wCntZ - 1.f));
		}
	}	

	m_pVB->Unlock();

	INDEX32*		pIndex = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndex, 0);

	iIndex = 0;
	int	iTriCnt = 0;

	for (int z = 0; z < wCntZ - 1; ++z)
	{
		for (int x = 0; x < wCntX - 1; ++x)
		{
			iIndex = z * wCntZ + x;
			
			// 오른쪽 위
			pIndex[iTriCnt]._0 = iIndex + wCntX;
			pIndex[iTriCnt]._1 = iIndex + wCntX + 1;
			pIndex[iTriCnt]._2 = iIndex + 1;
			iTriCnt++;

			// 왼쪽 아래
			pIndex[iTriCnt]._0 = iIndex + wCntX;
			pIndex[iTriCnt]._1 = iIndex + 1;
			pIndex[iTriCnt]._2 = iIndex;
			iTriCnt++;
		}
	}

	m_pIB->Unlock();

	return S_OK;
}

Engine::CTerrainTex* Engine::CTerrainTex::Create(LPDIRECT3DDEVICE9 pGraphicDev,
	const WORD& wCntX, const WORD& wCntZ, const WORD& wItv)
{
	CTerrainTex*	pInstance = new CTerrainTex(pGraphicDev);

	if (FAILED(pInstance->CreateBuffer(wCntX, wCntZ, wItv)))
		Engine::Safe_Delete(pInstance);

	return pInstance;

}

void Engine::CTerrainTex::Release(void)
{
	
}
