#include "Resources.h"

USING(Engine)

Engine::CResources::CResources(LPDIRECT3DDEVICE9 pGraphicDev)
	: m_pGraphicDev(pGraphicDev)
	, m_pRefCnt(new WORD(0))
{

}


Engine::CResources::~CResources(void)
{
	
}

void Engine::CResources::Release(void)
{
	if (0 == (*m_pRefCnt))
		Engine::Safe_Delete(m_pRefCnt);
	else
		(*m_pRefCnt)--;
}

