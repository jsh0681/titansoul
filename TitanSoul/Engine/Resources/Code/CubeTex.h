#ifndef CubeTex_h__
#define CubeTex_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CCubeTex : public CVIBuffer
{
private:
	explicit CCubeTex(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CCubeTex(void);

public:
	virtual void	Release(void) override;
	virtual void	Render(void);
	HRESULT			CreateBuffer(void);
	HRESULT			CreateBuffer(const WORD& wItv);
	HRESULT			CreateBuffer(const WORD& wCntX, const WORD& wCntZ);

public:
	static CCubeTex*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
	static CCubeTex*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const WORD & wItv);
	static CCubeTex*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const WORD& wCntX, const WORD& wCntZ);
};


END
#endif // CubeTex_h__
