#include "CrossTex.h"

USING(Engine)

Engine::CCrossTex::CCrossTex(LPDIRECT3DDEVICE9 pGraphicDev)
	:CVIBuffer(pGraphicDev)
{

}

Engine::CCrossTex::~CCrossTex(void)
{
	Release();
}

void Engine::CCrossTex::Render(void)
{
	CVIBuffer::Render();
}

HRESULT Engine::CCrossTex::CreateBuffer(void)
{
	m_dwVtxSize = sizeof(VTXTEX);
	m_dwVtxCnt = 8;
	m_dwVtxFVF = VTXFVF_TEX;
	m_dwTriCnt = 4;

	m_dwIdxSize = sizeof(INDEX32);
	m_IdxFmt = D3DFMT_INDEX32;

	FAILED_CHECK_RETURN(CVIBuffer::CreateBuffer(), E_FAIL);

	VTXTEX*		pVtxTex = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVtxTex, 0);

	pVtxTex[0].vPos = D3DXVECTOR3(-1.f, 1.f, 0.f);
	pVtxTex[0].vTex = D3DXVECTOR2(0, 0);

	pVtxTex[1].vPos = D3DXVECTOR3(1.f, 1.f, 0.f);
	pVtxTex[1].vTex = D3DXVECTOR2(1, 0);

	pVtxTex[2].vPos = D3DXVECTOR3(1.f, -1.f, 0.f);
	pVtxTex[2].vTex = D3DXVECTOR2(1, 1);

	pVtxTex[3].vPos = D3DXVECTOR3(-1.f, -1.f, 0.f);
	pVtxTex[3].vTex = D3DXVECTOR2(0, 1);

	m_pVB->Unlock();

	INDEX32*		pIndex = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndex, 0);

	pIndex[0]._0 = 0;
	pIndex[0]._1 = 1;
	pIndex[0]._2 = 2;

	pIndex[1]._0 = 0;
	pIndex[1]._1 = 2;
	pIndex[1]._2 = 3;

	m_pIB->Unlock();

	return S_OK;
}

HRESULT Engine::CCrossTex::CreateBuffer(const WORD& wCX, const WORD& wCZ)
{

	float fCX = (float)wCX/10.f;
	float fCZ = (float)wCZ/10.f;

	m_dwVtxSize = sizeof(VTXTEX);
	m_dwVtxCnt = 8;
	m_dwVtxFVF = VTXFVF_TEX;
	m_dwTriCnt = 4;

	m_dwIdxSize = sizeof(INDEX32);
	m_IdxFmt = D3DFMT_INDEX32;

	FAILED_CHECK_RETURN(CVIBuffer::CreateBuffer(), E_FAIL);

	VTXTEX*		pVtxTex = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVtxTex, 0);

	pVtxTex[0].vPos = D3DXVECTOR3(-fCX, 0.f, fCZ);
	pVtxTex[0].vTex = D3DXVECTOR2(0, 0);

	pVtxTex[1].vPos = D3DXVECTOR3(fCX, 0.f, fCZ);
	pVtxTex[1].vTex = D3DXVECTOR2(1, 0);

	pVtxTex[2].vPos = D3DXVECTOR3(fCX, 0.f, -fCZ);
	pVtxTex[2].vTex = D3DXVECTOR2(1, 1);

	pVtxTex[3].vPos = D3DXVECTOR3(-fCX, 0.f, -fCZ);
	pVtxTex[3].vTex = D3DXVECTOR2(0, 1);

	pVtxTex[4].vPos = D3DXVECTOR3(0.f, fCX, fCZ);
	pVtxTex[4].vTex = D3DXVECTOR2(0, 0);

	pVtxTex[5].vPos = D3DXVECTOR3(0.f, -fCX, fCZ);
	pVtxTex[5].vTex = D3DXVECTOR2(1, 0);

	pVtxTex[6].vPos = D3DXVECTOR3(0.f, -fCX, -fCZ);
	pVtxTex[6].vTex = D3DXVECTOR2(1, 1);

	pVtxTex[7].vPos = D3DXVECTOR3(0.f, fCX, -fCZ);
	pVtxTex[7].vTex = D3DXVECTOR2(0, 1);

	m_pVB->Unlock();

	INDEX32*		pIndex = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndex, 0);

	pIndex[0]._0 = 0;
	pIndex[0]._1 = 1;
	pIndex[0]._2 = 2;

	pIndex[1]._0 = 0;
	pIndex[1]._1 = 2;
	pIndex[1]._2 = 3;

	pIndex[2]._0 = 4;
	pIndex[2]._1 = 5;
	pIndex[2]._2 = 6;

	pIndex[3]._0 = 4;
	pIndex[3]._1 = 6;
	pIndex[3]._2 = 7;
	m_pIB->Unlock();

	return S_OK;
}

Engine::CCrossTex* Engine::CCrossTex::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CCrossTex*	pInstance = new CCrossTex(pGraphicDev);

	if (FAILED(pInstance->CreateBuffer()))
		Engine::Safe_Delete(pInstance);

	return pInstance;

}


//************************************
// Method:    Create
// 작성자:	  정성호
// Date:      2019/01/05
// Message:   택스쳐 이미지는 버퍼의 사이즈에 딱 맞게 출력이 되는데 만약 이미지의 가로 세로 비율이 1:1이 아닐 경우 출력이 될때 찌그러져서 나오게 된다. 
//				따라서 가로 세로 길이의 비율을 인자로 받아 그 비율에 맞게 버퍼를 생성해주는 함수.
// Parameter: wCX 가로 비율*10한 값, wCZ 세로 비율*10한 값 -> 나중에 사용할때 10으로 나눠서 float 형으로 변환해서 사용할 것
//************************************
Engine::CCrossTex* Engine::CCrossTex::Create(LPDIRECT3DDEVICE9 pGraphicDev, const WORD& wCX ,const WORD& wCZ )
{
	CCrossTex*	pInstance = new CCrossTex(pGraphicDev);

	if (FAILED(pInstance->CreateBuffer(wCX, wCZ)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}


void Engine::CCrossTex::Release(void)
{

}