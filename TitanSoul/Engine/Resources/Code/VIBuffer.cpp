#include "VIBuffer.h"

USING(Engine)

Engine::CVIBuffer::CVIBuffer(LPDIRECT3DDEVICE9 pGraphicDev)
	: CResources(pGraphicDev)
	, m_dwVtxSize(0)
	, m_dwVtxCnt(0)
	, m_dwVtxFVF(0)
	, m_dwTriCnt(0)
	, m_dwIdxSize(0)
{

}

Engine::CVIBuffer::~CVIBuffer(void)
{
	Release();
}

void CVIBuffer::ClientToEngine(void * pVertex)
{
	void*		pOriVertex = nullptr;

	m_pVB->Lock(0, 0, &pOriVertex, 0);

	memcpy(pOriVertex, pVertex, m_dwVtxCnt * m_dwVtxSize);

	m_pVB->Unlock();

}

void CVIBuffer::EngineToClient(void * pVertex)
{
	void*		pOriVertex = nullptr;

	m_pVB->Lock(0, 0, &pOriVertex, 0);

	memcpy(pVertex, pOriVertex, m_dwVtxCnt * m_dwVtxSize);

	m_pVB->Unlock();
}

HRESULT Engine::CVIBuffer::CreateBuffer(void)
{
	m_pGraphicDev->CreateVertexBuffer(m_dwVtxSize * m_dwVtxCnt, // 정점 버퍼의 사이즈
									0,					  // 정점 사용방식 : 정적 버퍼 or 동적 버퍼, 값이 0인 경우 정적 버퍼를 사용
									m_dwVtxFVF,			  // 사용하는 버퍼의 옵션(symantic)	
									D3DPOOL_MANAGED,	  // 메모리 풀의 옵션
									&m_pVB, 
									nullptr);				// 버퍼 만들 때 필요한 예약상태

	
	m_pGraphicDev->CreateIndexBuffer(m_dwIdxSize * m_dwTriCnt,
									0, 
									m_IdxFmt, 
									D3DPOOL_MANAGED, 
									&m_pIB, 
									nullptr);

	return S_OK;
}

void Engine::CVIBuffer::Render(void)
{
	m_pGraphicDev->SetStreamSource(0, m_pVB, 0, m_dwVtxSize);
	// 장치에게 현재 내가 그리려는 버퍼를 연결해주는 함수
	// 1. 몇 번 슬롯에 보관할 것인가
	// 2. 어떤 것을 넘겨줄 것인다
	// 3. 어디서부터 그릴 것인가, 0인 경우 버퍼의 최초 값부러 그려라
	// 4. 어떤 단위로 그릴 것인가

	m_pGraphicDev->SetFVF(m_dwVtxFVF);

	// 인덱스 버퍼 활성화
	m_pGraphicDev->SetIndices(m_pIB);

	//m_pGraphicDev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, m_dwTriCnt);
	//// 2인자 : 몇번째 버텍스부터 그릴 것인가
	//// 3인자 : 삼각형의 개수

	m_pGraphicDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_dwVtxCnt, 0, m_dwTriCnt);
	// 2인자 : 버텍스 중 몇 번째 인덱스부터 그릴 것인가
	// 3인자 : 버텍스 버퍼에 들어가있는 순서 중 몇 번째부터 그릴 것인가
	// 4인자 : 사용하고자 하는 버텍스의 개수
	// 5인자 : 인덱스 버퍼에 들어가 있는 순서 중 몇 번째부터 그릴 것인가
	// 6인자 : 출력하고자 하는 삼각형의 개수


}

void Engine::CVIBuffer::Release(void)
{
	if (0 == (*m_pRefCnt))
	{
		Engine::Safe_Release(m_pVB);
		Engine::Safe_Release(m_pIB);

		CResources::Release();
	}

	else
	{
		(*m_pRefCnt)--;
	}

}

Engine::CResources* Engine::CVIBuffer::Clone(void)
{
	(*m_pRefCnt)++;

	return new CVIBuffer(*this);
}

