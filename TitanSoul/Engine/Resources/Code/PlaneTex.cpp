#include "PlaneTex.h"

USING(Engine)

float Engine::CPlaneTex::m_f0X;
float Engine::CPlaneTex::m_f0Z;
float Engine::CPlaneTex::m_f1X;
float Engine::CPlaneTex::m_f1Z;
float Engine::CPlaneTex::m_f2X;
float Engine::CPlaneTex::m_f2Z;
float Engine::CPlaneTex::m_f3X;
float Engine::CPlaneTex::m_f3Z;
int Engine::CPlaneTex::m_iOption;

Engine::CPlaneTex::CPlaneTex(LPDIRECT3DDEVICE9 pGraphicDev)
	:CVIBuffer(pGraphicDev)
{

}

Engine::CPlaneTex::~CPlaneTex(void)
{
	Release();
}

void Engine::CPlaneTex::Render(void)
{
	CVIBuffer::Render();
}

HRESULT CPlaneTex::CreateBuffer()
{
	m_iOpt = m_iOption;
	m_dwVtxSize = sizeof(VTXTEX);
	m_dwVtxCnt = 4;
	m_dwVtxFVF = VTXFVF_TEX;
	m_dwTriCnt = 2;

	m_dwIdxSize = sizeof(INDEX32);
	m_IdxFmt = D3DFMT_INDEX32;

	FAILED_CHECK_RETURN(CVIBuffer::CreateBuffer(), E_FAIL);

	VTXTEX*		pVtxTex = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVtxTex, 0);

	pVtxTex[0].vPos = D3DXVECTOR3(m_f0X, 1.f, m_f0Z);
	pVtxTex[0].vTex = D3DXVECTOR2(0, 0);

	pVtxTex[1].vPos = D3DXVECTOR3(m_f1X, 1.f, m_f1Z);
	pVtxTex[1].vTex = D3DXVECTOR2(1, 0);

	pVtxTex[2].vPos = D3DXVECTOR3(m_f2X, 1.f, m_f2Z);
	pVtxTex[2].vTex = D3DXVECTOR2(1, 1);

	pVtxTex[3].vPos = D3DXVECTOR3(m_f3X, 1.f, m_f3Z);
	pVtxTex[3].vTex = D3DXVECTOR2(0, 1);

	m_pVB->Unlock();

	INDEX32*		pIndex = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndex, 0);

	pIndex[0]._0 = 0;
	pIndex[0]._1 = 1;
	pIndex[0]._2 = 2;

	pIndex[1]._0 = 0;
	pIndex[1]._1 = 2;
	pIndex[1]._2 = 3;

	m_pIB->Unlock();

	return S_OK;
}



CPlaneTex * CPlaneTex::Create(LPDIRECT3DDEVICE9 pGraphicDev, float _f0X, float _f0Z, float _f1X, float _f1Z,
															 float _f2X, float _f2Z, float _f3X, float _f3Z, const int iOption)
{
	CPlaneTex*	pInstance = new CPlaneTex(pGraphicDev);

	pInstance->m_f0X = _f0X;
	pInstance->m_f0Z = _f0Z;
	pInstance->m_f1X = _f1X;
	pInstance->m_f1Z = _f1Z;
	pInstance->m_f2X = _f2X;
	pInstance->m_f2Z = _f2Z;
	pInstance->m_f3X = _f3X;
	pInstance->m_f3Z = _f3Z;
	pInstance->m_iOption = iOption;

	if (FAILED(pInstance->CreateBuffer()))
		Engine::Safe_Delete(pInstance);

	return pInstance;

}

void Engine::CPlaneTex::Release(void)
{

}