#include "Texture.h"

USING(Engine)

Engine::CTexture::CTexture(LPDIRECT3DDEVICE9 pGraphicDev)
	: CResources(pGraphicDev)
{

}


Engine::CTexture::~CTexture(void)
{
	Release();
}

HRESULT Engine::CTexture::LoadTexture(TEXTYPE eTexType, 
										const wstring& wstrFilePath, 
										const WORD& wCnt)
{
	IDirect3DBaseTexture9*		pTexture = nullptr;
	m_vecTexture.reserve(wCnt);

	TCHAR	szFullPath[MAX_PATH] = L"";

	for (size_t i = 0; i < wCnt; ++i)
	{
		wsprintf(szFullPath, wstrFilePath.c_str(), i);
		HRESULT	 hr = NULL;

		switch (eTexType)
		{
		case TEX_NORMAL:
			hr = D3DXCreateTextureFromFile(m_pGraphicDev, szFullPath, (LPDIRECT3DTEXTURE9*)&pTexture);
			break;

		case TEX_CUBE:
			hr = D3DXCreateCubeTextureFromFile(m_pGraphicDev, szFullPath, (LPDIRECT3DCUBETEXTURE9*)&pTexture);
			break;
		}
		FAILED_CHECK_MSG(hr, szFullPath);

		m_vecTexture.push_back(pTexture);
	}

	m_dwContainerSize = m_vecTexture.size();

	return S_OK;
}

void Engine::CTexture::Render(const DWORD& iIndex)
{
	if (iIndex >= m_dwContainerSize)
		return;

	m_pGraphicDev->SetTexture(0, m_vecTexture[iIndex]);
}

Engine::CTexture* Engine::CTexture::Create(LPDIRECT3DDEVICE9 pGraphicDev, TEXTYPE eTexType, const wstring& wstsrFilePath, const WORD& wCnt)
{
	CTexture*		pInstance = new CTexture(pGraphicDev);

	if (FAILED(pInstance->LoadTexture(eTexType, wstsrFilePath, wCnt)))
		Engine::Safe_Delete(pInstance);
	
	return pInstance;

}

void Engine::CTexture::Release(void)
{
	if (0 == (*m_pRefCnt))
	{
		for (size_t i = 0; i < m_vecTexture.size(); ++i)
			Engine::Safe_Release(m_vecTexture[i]);

		m_vecTexture.clear();
	}

	else
		(*m_pRefCnt)--;
}

Engine::CResources* Engine::CTexture::Clone(void)
{
	++(*m_pRefCnt);

	return new CTexture(*this);
}

