#ifndef VIBuffer_h__
#define VIBuffer_h__

#include "Resources.h"

BEGIN(Engine)

class ENGINE_DLL CVIBuffer : public CResources
{

protected:
	explicit CVIBuffer(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CVIBuffer(void);

public:
	void	ClientToEngine(void* pVertex); // input
	void	EngineToClient(void* pVertex); // output
	
public:
	HRESULT	CreateBuffer(void);
	virtual void	Render(void);
public:
	virtual void	Release(void);

protected:
	LPDIRECT3DVERTEXBUFFER9		m_pVB;
	LPDIRECT3DINDEXBUFFER9		m_pIB;

	DWORD						m_dwVtxSize;
	DWORD						m_dwVtxCnt;
	DWORD						m_dwVtxFVF;
	DWORD						m_dwTriCnt;

	DWORD						m_dwIdxSize;
	D3DFORMAT					m_IdxFmt;

public:
	virtual	CResources*		Clone(void);
		
};

END
#endif // VIBuffer_h__
