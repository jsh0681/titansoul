#include "RcTexZ.h"

USING(Engine)

CRcTexZ::CRcTexZ(LPDIRECT3DDEVICE9 pGraphicDev) :
	CVIBuffer(pGraphicDev)
{
}

CRcTexZ::~CRcTexZ()
{
	Release();
}

void CRcTexZ::Release()
{
}

void CRcTexZ::Render()
{
	CVIBuffer::Render();
}

HRESULT CRcTexZ::CreateBuffer()
{
	m_dwVtxSize = sizeof(VTXTEX);
	m_dwVtxCnt = 4;
	m_dwVtxFVF = VTXFVF_TEX;
	m_dwTriCnt = 2;

	m_dwIdxSize = sizeof(INDEX32);
	m_IdxFmt = D3DFMT_INDEX32;

	FAILED_CHECK_RETURN(CVIBuffer::CreateBuffer(), E_FAIL);

	VTXTEX*		pVtxTex = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVtxTex, 0);
	
	pVtxTex[0].vPos = { -1.f, 0.f, 1.f };
	pVtxTex[0].vTex = { 0, 0 };

	pVtxTex[1].vPos = { 1.f, 0.f, 1.f };
	pVtxTex[1].vTex = { 1, 0 };

	pVtxTex[2].vPos = { 1.f, 0.f, -1.f };
	pVtxTex[2].vTex = { 1, 1 };

	pVtxTex[3].vPos = { -1.f, 0.f, -1.f };
	pVtxTex[3].vTex = { 0, 1 };

	m_pVB->Unlock();

	INDEX32*	pIndex = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndex, 0);

	pIndex[0]._0 = 0;
	pIndex[0]._1 = 1;
	pIndex[0]._2 = 2;

	pIndex[1]._0 = 0;
	pIndex[1]._1 = 2;
	pIndex[1]._2 = 3;

	m_pIB->Unlock();

	return S_OK;
}

HRESULT CRcTexZ::CreateBuffer(const WORD& wCntX, const WORD& wCntZ)
{
	m_dwVtxSize = sizeof(VTXTEX);
	m_dwVtxCnt = 4;
	m_dwVtxFVF = VTXFVF_TEX;
	m_dwTriCnt = 2;

	m_dwIdxSize = sizeof(INDEX32);
	m_IdxFmt = D3DFMT_INDEX32;

	FAILED_CHECK_RETURN(CVIBuffer::CreateBuffer(), E_FAIL);

	VTXTEX*		pVtxTex = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVtxTex, 0);

	float fSizeX = float(wCntX)/10;
	float fSizeZ = float(wCntZ)/10;

	pVtxTex[0].vPos = { -fSizeX, 0.f, fSizeZ };
	pVtxTex[0].vTex = { 0, 0 };

	pVtxTex[1].vPos = { fSizeX, 0.f, fSizeZ };
	pVtxTex[1].vTex = { 1, 0 };

	pVtxTex[2].vPos = { fSizeX, 0.f, -fSizeZ };
	pVtxTex[2].vTex = { 1, 1 };

	pVtxTex[3].vPos = { -fSizeX, 0.f, -fSizeZ };
	pVtxTex[3].vTex = { 0, 1 };

	m_pVB->Unlock();

	INDEX32*	pIndex = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndex, 0);

	pIndex[0]._0 = 0;
	pIndex[0]._1 = 1;
	pIndex[0]._2 = 2;

	pIndex[1]._0 = 0;
	pIndex[1]._1 = 2;
	pIndex[1]._2 = 3;

	m_pIB->Unlock();

	return S_OK;
}

CRcTexZ* CRcTexZ::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CRcTexZ* pInstance = new CRcTexZ(pGraphicDev);

	if (FAILED(pInstance->CreateBuffer()))
		Safe_Delete(pInstance);

	return pInstance;
}

CRcTexZ* CRcTexZ::Create(LPDIRECT3DDEVICE9 pGraphicDev, const WORD& wCntX, const WORD& wCntZ)
{
	CRcTexZ* pInstance = new CRcTexZ(pGraphicDev);

	if (FAILED(pInstance->CreateBuffer(wCntX, wCntZ)))
		Safe_Delete(pInstance);

	return pInstance;
}
