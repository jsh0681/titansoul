#include "TriCol.h"

USING(Engine)

Engine::CTriCol::CTriCol(LPDIRECT3DDEVICE9 pGraphicDev)
	:CVIBuffer(pGraphicDev)
{

}

Engine::CTriCol::~CTriCol(void)
{
	Release();
}

void Engine::CTriCol::Render(void)
{
	CVIBuffer::Render();
}

HRESULT Engine::CTriCol::CreateBuffer(void)
{
	m_dwVtxSize = sizeof(VTXCOL);
	m_dwVtxCnt = 3;
	m_dwVtxFVF = VTXFVF_COL;
	m_dwTriCnt = 1;
	
	FAILED_CHECK_RETURN(CVIBuffer::CreateBuffer(), E_FAIL);

	VTXCOL*		pVtxCol = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVtxCol, 0);

	// 1. 버텍스 저장된 메모리 공간을 잠근다
	// 2. 그 공간에 있는 첫 번째 버텍스의 주소값을 얻어온다.

	// 함수의 인자값 1. 어디서부터 잠글 것인가 2. 어디까지 잠글것인가(0이면 전체 영역), 4. 잠그는 형태를 묻는 옵션(정적버퍼인 경우 0)

	pVtxCol[0].vPos = D3DXVECTOR3(0.f, 1.f, 0.f);
	pVtxCol[0].dwColor = D3DCOLOR_ARGB(255, 255, 0, 0);

	pVtxCol[1].vPos = D3DXVECTOR3(1.f, -1.f, 0.f);
	pVtxCol[1].dwColor = D3DCOLOR_ARGB(255, 255, 0, 0);

	pVtxCol[2].vPos = D3DXVECTOR3(-1.f, -1.f, 0.f);
	pVtxCol[2].dwColor = D3DCOLOR_ARGB(255, 255, 0, 0);

	m_pVB->Unlock();

	return S_OK;
}

Engine::CTriCol* Engine::CTriCol::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CTriCol*	pInstance = new CTriCol(pGraphicDev);

	if (FAILED(pInstance->CreateBuffer()))
		Engine::Safe_Delete(pInstance);

	return pInstance;

}

void Engine::CTriCol::Release(void)
{
	
}