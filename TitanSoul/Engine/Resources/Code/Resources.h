#ifndef Resources_h__
#define Resources_h__

#include "Component.h"

BEGIN(Engine)

class ENGINE_DLL CResources : public CComponent
{
protected:
	explicit CResources(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CResources(void);

public:
	virtual void	Release(void);
	virtual void	Render(void)PURE;

public:
	virtual	CResources*		Clone(void)PURE;

protected:
	LPDIRECT3DDEVICE9		m_pGraphicDev = nullptr;
	WORD*					m_pRefCnt;
};


END
#endif // Resources_h__
