#ifndef Export_Utility_h__
#define Export_Utility_h__

#include "Management.h"
#include "Transform.h"
#include "Pipeline.h"
#include "InfoSubject.h"

BEGIN(Engine)

inline CManagement*		Get_Management(void);
inline CInfoSubject*	Get_InfoSubject(void);

#include "Export_Utility.inl"

END
#endif // Export_Utility_h__
