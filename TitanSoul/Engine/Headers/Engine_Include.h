#ifndef Engine_Include_h__
#define Engine_Include_h__

#include <d3d9.h>
#include <d3dx9.h>
#include <vector>
#include <list>
#include <map>
#include <unordered_map>
#include <string>
#include <algorithm>
#include <ctime>
#include <functional>
#include <process.h>

using namespace std;

#include "Engine_macro.h"
#include "Engine_enum.h"
#include "Engine_function.h"
#include "Engine_functor.h"
#include "Engine_struct.h"
#include "Engine_constant.h"

#pragma warning(disable : 4251)

#endif // Engine_Include_h__
