#ifndef value_h__
#define value_h__

const WORD WINCX = 800;
const WORD WINCY = 600;

extern HWND g_hWnd;
extern wstring	g_wstrDir[8];
extern float	g_R2;
extern bool	g_BattleMode;
extern bool g_2DMode;
extern bool g_SystemMode;
const D3DXVECTOR3	g_vRight = D3DXVECTOR3(1.f, 0.f, 0.f);
const D3DXVECTOR3	g_vUp = D3DXVECTOR3(0.f, 1.f, 0.f);
const D3DXVECTOR3	g_vLook = D3DXVECTOR3(0.f, 0.f, 1.f);

using VEC3 = D3DXVECTOR3;

const WORD VTXCNTX = 220;
const WORD VTXCNTZ = 220;
const WORD VTXITV = 1;

#define KEY_STR 32
#define MIN_STR 64
#define MID_STR 128
#define MAX_STR 256

const float TERRAIN_Y = 0.f;
const float PLAYER_Y = 0.1f;
const float ARROW_Y = 0.2f;
const float WALL_Y = 0.5f;
const float BOSS_Y = 0.1f;

#define TILE_SIZE 4.06f
#define TILE_SIZE_HALF 2.03f

#define EYECUBE_MOVEING_TIME 0.825f
#define EYECUBE_RASER_TIME 1.0f
#define EYECUBE_MOVEING_VALUE 5.f
#define EYECUBE_ROTATING_ANGLE 110.f

#endif // value_h__
