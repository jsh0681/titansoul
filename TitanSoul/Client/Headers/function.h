#ifndef Myfunction_h__
#define Myfunction_h__

// ���� �Լ�
template <typename T>
int GetRandom(const int& min, const int& max)
{
	random_device rn;
	mt19937_64 rnd(rn());

	uniform_int_distribution<int> range(min, max);

	return range(rnd);
}

template <typename T>
float GetRandom(const float& min, const float& max)
{
	random_device rn;
	mt19937_64 rnd(rn());

	uniform_real_distribution<float> range(min, max);

	return range(rnd);
}
#endif // !Myfunction_h__
