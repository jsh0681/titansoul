#ifndef Struct_h__
#define Struct_h__
#include "Engine_Include.h"

using IMGPATH = struct tagImagePath
{
	wstring wstrType = L"";
	wstring wstrObjKey = L"";
	wstring wstrStateKey = L"";
	wstring wstrDirKey = L"";
	int		iCount = 0;
	wstring	wstrPath = L"";
};

using DATAPATH = struct tagDataPath
{
	wstring wstrType = L"";
	wstring wstrObjKey = L"";
	wstring wstrStateKey = L"";
	wstring wstrFileName = L"";
	wstring	wstrPath = L"";
};

using TEXINFO = struct tagTexInfo
{
	int		iType = 0;
	wstring wstrTexKey = L"";

	float fRatioWidth = 0.f;
	float fRatioHeight = 0.f;

	int		iNum = 0;
	int		iNextNum = 0;
	float	fDelay = 0.f;


	D3DXVECTOR3 vScale = { 0.f,0.f,0.f };
	D3DXVECTOR3 vRotation = { 0.f,0.f,0.f };
	D3DXVECTOR3 vMove = { 0.f,0.f,0.f };
};

using EFFECTINFO = struct EffectInfo
{
	wstring wstrResourceKey = L"";
	wstring wstrFilePath = L"";
	WORD wCnt = 0;

	wstring wstrBufferKey = L"";
	Engine::BUFFERTYPE eBufferType;

	float fRatioWidth = 0.f;
	float fRatioHeight = 0.f;
};

using PARTICLEINFO = struct tagParticleInfo
{
	bool	bCheckFire;
	float	fFirePosX;
	float	fFirePosY;
	float	fFirePosZ;

	float	fFireRangeX;
	float	fFireRangeY;
	float	fFireRangeZ;

	float	fSizeMax;
	float	fSizeMin;

	float	fColorFade;
	float	fLifeTime;
	float	fSpeedMax;
	float	fSpeedMin;

	bool	bCheckShow;
	float	fBoundingBoxX;
	float	fBoundingBoxY;
	float	fBoundingBoxZ;
	int		iNumParticles;
};

using MAPINFO = struct tagMapInfo
{
	wstring wstrMapBufferKey;		// 맵 버퍼			// MID_STR(128)로 파일에 저장되어있음
	wstring wstrMapKey;				// 맵 키값			// MID_STR(128)로 파일에 저장되어있음
	wstring wstrMapTexKey;			// 맵 텍스쳐 키값	// MID_STR(128)로 파일에 저장되어있음

	D3DXVECTOR3 vStartingPoint;		//시작위치
	D3DXVECTOR3 vResurrectionPoint; //부활위치

	D3DXVECTOR3 vDestination;		//종료위치
	D3DXVECTOR3 vBossPoint;			//보스위치

	float fSizeX;					//맵 사이즈
	float fSizeZ;					//맵 사이즈
	int   iIndex;
};

using PLANEINFO = struct tagPlaneInfo
{
	wstring wstrPlaneBufferKey;	//플레인 키임.			// MID_STR(128)로 파일에 저장되어있음
	wstring wstrPlaneTexKey;	// 플레인 텍스쳐 키임	// MID_STR(128)로 파일에 저장되어있음

	D3DXVECTOR3 vVertex0;		//버
	D3DXVECTOR3 vVertex1;		//텍
	D3DXVECTOR3 vVertex2;		//스
	D3DXVECTOR3 vVertex3;		//예

	int iOption;				//플레인옵션임
	int iTrigger;				//트리거 : 해당 평면을 밟으면... 어느 방으로 이동시킬지 등...

	float fY;					//평면의 Y값임.
};


using OBB = struct tagObbColision // OBB구조체
{
	D3DXVECTOR3 vCenterPos; // 상자 중앙의 좌표
	D3DXVECTOR3 vAxisDir[3]; //상자에 평행한 세 축의 단위벡터
	float  fAxisLen[3]; // 상자의 평행한 세 축의 길이 fAxisLen[n]은 vAxisDir[n]에 각각 대응한다.
};


typedef struct NorEffect
{
	D3DXVECTOR3 vPos;
	D3DXVECTOR3 vDir;
	D3DXVECTOR3 vAcc;

	float fSpd;
	float fDisappearSpd;//소멸속도 (fScale 감소속도)
	float fLifeTime;
	float fScale;
} NORMALEFFECT;

typedef struct tagCubeInfo
{
	D3DXVECTOR3 vCenter; //큐브 센터좌표임
	D3DXVECTOR3 vPos; // 낮은와이 3번인덱스... 센터의 7시 방향.

	int			iTexInx; // 큐브 그림구분할 인덱스으이이잉.
}CUBEINFO;




#endif // !Struct_h__
