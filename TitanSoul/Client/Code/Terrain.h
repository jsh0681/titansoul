#ifndef Terrain_h__
#define Terrain_h__

/*!
* \class CTerrain
*
* \brief
*
* \수정자 윤유석
* \수정일 1월 7 2019
* \수정 내용 : 프로토타입 패턴 적용(Clone함수 생성)
복사 생성자 재정의, RefCount 변수 추가, Release함수 수정
MapInfo 구조체 변수 추가,
Initialize 함수 파라미터 추가(*MAPINFO)


* \수정일 1월 8 2019
PlaneLst 변수는 여기에서 지워주지 않음 -> TerrainMgr에서 지워줌
PlaneLst 변수는 처음에 넣어주지 않고 Clone에서 만들어질때 넣어줌 원본에서는 nullptr임
*/


#include "GameObject.h"
#include "CameraObserver.h"
namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
	class CResourcesMgr;
	class CInfoSubject;
}
class CArrow;
class CPlayer;
class CPlane;
class CIceCube;
class CTerrain : public Engine::CGameObject
{
private:
	explicit CTerrain(LPDIRECT3DDEVICE9 pGraphicDev);
	CTerrain(const CTerrain& rhs);
public:
	~CTerrain();

public:
	enum VERTEXDIRECTION { UPLINE, RIGHTLINE, DOWNLINE, LEFTLINE, ENDLINE};
	const Engine::VTXTEX* GetTerrainVertex(void) { return m_pVertex; }
	CTerrain*	Clone();

	const MAPINFO*	GetDataInfo() { return m_pMapInfo; }

public:
	void		Update(void);
	void		Render(void);

	virtual void Release(void);

	bool CollisionPlaneBullet(const D3DXVECTOR3& vPos);
	int CollisionPlane(CPlayer* pPlayer);
	int CollisionPlaneMap1(CPlayer* pPlayer);

	void CollisoinPlanetoArrow(CArrow* pArrow, CPlayer* pPlayer);
	void CollisoinPlanetoIceCube(CIceCube * pIceCube, CPlayer * pPlayer);
	void CollisoinPlanetoArrowBack(CArrow* pArrow, CPlayer* pPlayer);
	void CollisoinPlanetoArrowBackAgain(CArrow* pArrow, CPlayer* pPlayer);
	D3DXVECTOR3 CheckLine(D3DXVECTOR3 Line1_Start, D3DXVECTOR3 Line1_End, 
						  D3DXVECTOR3 Line2_Start, D3DXVECTOR3 Line2_End);

	D3DXVECTOR3 GetNormalVector(D3DXVECTOR3 vFirst, D3DXVECTOR3 vSecond, D3DXVECTOR3 vCrossPnt);

	void		SetPlaneRender(const bool& bRend) { m_bPlaneRender = bRend; }

public:
	void	SetPlaneList(list<CPlane*>* pPlaneList) { m_pPlaneList = pPlaneList; }
	const list<CPlane*>* GetPlaneList() { return m_pPlaneList; }

private:
	HRESULT		AddComponent(void);
	HRESULT		Initialize(MAPINFO* pMapInfo);
	void		SetDirection(void);
	void		SetTransform(void);


private:
	Engine::CVIBuffer*				m_pBufferCom = nullptr;
	Engine::CTexture*				m_pTextureCom = nullptr;
	CCameraObserver*				m_pCameraObserver = nullptr;

	Engine::VTXTEX*					m_pVertex = nullptr;
	Engine::VTXTEX*					m_pConvertVertex = nullptr;

	DWORD							m_dwVtxCnt;

	MAPINFO*						m_pMapInfo = nullptr;
	list<CPlane*>*					m_pPlaneList = nullptr;

	int*							m_pRefCnt;

	bool							m_bPlaneRender = false;

public:
	static CTerrain*		Create(LPDIRECT3DDEVICE9 pGraphicDev, MAPINFO* pMapInfo);
};



#endif // Terrain_h__
