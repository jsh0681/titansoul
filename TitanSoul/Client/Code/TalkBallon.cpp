#include "stdafx.h"
#include "TalkBallon.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"

CTalkBallon::CTalkBallon(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement = Engine::Get_Management();
	m_pInfoSubject = Engine::Get_InfoSubject();
}


CTalkBallon::~CTalkBallon()
{
	Release();
}

void CTalkBallon::Update()
{
	if (m_bOn)
	{
		CGameObject::Update();
		SetTransform();
	}
}

void CTalkBallon::Render()
{
	if (m_bOn)
	{
		m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_TalkBalloon", m_pConvertVertex);

		m_pGraphicDev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
		m_pGraphicDev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);

		m_pTexture->Render(m_iIndex);
		m_pBuffer->Render();

		m_pGraphicDev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		m_pGraphicDev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	}
}

HRESULT CTalkBallon::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_TalkBalloon");
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrImgName);
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CTalkBallon::Initialize(VEC3& vPos, VEC3& vSize, const int& iIndex)
{
	m_wstrImgName = L"UiDefaultBalloon";

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_iIndex = iIndex;

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_TalkBalloon", m_pVertex);
	InitVertex();

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos = vPos;
	m_pInfo->m_vScale = vSize;

	m_pInfo->m_vPos.y = 0.2f;
	m_pInfo->m_vPos.x += 1.5f;
	m_pInfo->m_vPos.z += 1.2f;

	return S_OK;
}

void CTalkBallon::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CTalkBallon::InitVertex()
{
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_TalkBalloon", m_pVertex);
}

void CTalkBallon::SetTransform()
{
	const D3DXMATRIX* matView = m_pCameraObserver->GetView();
	const D3DXMATRIX* matProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, matView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, matProj);
}

void CTalkBallon::SetPos(const VEC3& vPos)
{
	m_pInfo->m_vPos = vPos;
	m_pInfo->m_vPos.y = 0.2f;
	m_pInfo->m_vPos.x += 1.5f;
	m_pInfo->m_vPos.z += 1.2f;
}

void CTalkBallon::SetScale(const VEC3 vScale)
{
	m_pInfo->m_vScale = vScale;
}

CTalkBallon* CTalkBallon::Create(LPDIRECT3DDEVICE9 pGraphicDev, VEC3& vPos, VEC3& vSize, const int& iIndex)
{
	CTalkBallon* pInstance = new CTalkBallon(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos, vSize, iIndex)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
