#ifndef CollisionMgr_h__
#define CollisionMgr_h__

#include "Engine_Include.h"

namespace Engine
{
	class CCollision;
}

#include "TerrainColl.h"
#include "MouseCol.h"

class CCollisionMgr
{
public:
	DECLARE_SINGLETON(CCollisionMgr)

public:
	enum COLLISIONID { COL_TERRAIN, COL_MOUSE, COL_PLANE, COL_SHPERE ,COL_CUBE, COL_RECT };

private:
	explicit CCollisionMgr(void);
	~CCollisionMgr();


public:
	HRESULT	AddColObject(COLLISIONID eCollisionID);
	Engine::CCollision*	CloneColObject(COLLISIONID eCollisionID);

private:
	void	Release(void);

private:
	map<COLLISIONID, Engine::CCollision*>		m_mapCollision;

};


#endif // CollisionMgr_h__
