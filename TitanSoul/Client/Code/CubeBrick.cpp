
#include "stdafx.h"
#include "CubeBrick.h"
#include "CameraObserver.h"

#include "Export_Function.h"
#include "Stage3.h"

CCubeBrick::CCubeBrick(LPDIRECT3DDEVICE9 pGraphicDev)
	:CGameObject(pGraphicDev)
{
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pResourcesMgr = Engine::Get_ResourceMgr();
}


CCubeBrick::~CCubeBrick()
{
	Release();
}

void CCubeBrick::InitVertex()
{
	m_pVertex[0].vPos = D3DXVECTOR3(-0.5f, 0.5f, -0.5f);
	m_pVertex[1].vPos = D3DXVECTOR3(0.5f, 0.5f, -0.5f);
	m_pVertex[2].vPos = D3DXVECTOR3(0.5f, -0.5f, -0.5f);
	m_pVertex[3].vPos = D3DXVECTOR3(-0.5f, -0.5f, -0.5f);
	m_pVertex[4].vPos = D3DXVECTOR3(-0.5f, 0.5f, 0.5f);
	m_pVertex[5].vPos = D3DXVECTOR3(0.5f, 0.5f, 0.5f);
	m_pVertex[6].vPos = D3DXVECTOR3(0.5f, -0.5f, 0.5f);
	m_pVertex[7].vPos = D3DXVECTOR3(-0.5f, -0.5f, 0.5f);

}

HRESULT CCubeBrick::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	// buffer
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Brick");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);

	// texture
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"CubeBrickBrickCube");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTextureCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTextureCom);


	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CCubeBrick::Initialize(CUBEINFO * pCubeInfo, const float fSpd, const int iNum)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pCubeInfo = pCubeInfo;
	m_fSpeed = fSpd;
	m_iNum = iNum;
	//버퍼를 가져와서 만들고...

	m_pCubeInfo->vCenter;
	m_pCubeInfo->vPos; // 아래 3번 == 7번꺼 좌표임.


	m_pVertex = new Engine::VTXCUBE[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXCUBE[m_dwVtxCnt];
	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Brick", m_pVertex);
	InitVertex();


	float fRand1 = GetRandom<float>(0.f, 3.f);


	m_pVertex[0].vPos = m_pCubeInfo->vPos;
	m_pVertex[0].vPos.y -= 1.f;
	m_pVertex[0].vPos.z += 1.f;

	m_pVertex[1].vPos = m_pCubeInfo->vPos;
	m_pVertex[1].vPos.y -= 1.f;
	m_pVertex[1].vPos.x += 1.f;
	m_pVertex[1].vPos.z += 1.f;

	m_pVertex[2].vPos = m_pCubeInfo->vPos;
	m_pVertex[2].vPos.y -= 1.f;
	m_pVertex[2].vPos.x += 1.f;

	m_pVertex[3].vPos = m_pCubeInfo->vPos;
	m_pVertex[3].vPos.y -= 1.f;


	m_pVertex[4].vPos = m_pCubeInfo->vPos;
	m_pVertex[4].vPos.z += 1.f;
	m_pVertex[4].vPos.y += 5.f + fRand1;


	m_pVertex[5].vPos = m_pCubeInfo->vPos;
	m_pVertex[5].vPos.x += 1.f;
	m_pVertex[5].vPos.z += 1.f;
	m_pVertex[5].vPos.y += 5.f + fRand1;


	m_pVertex[6].vPos = m_pCubeInfo->vPos;
	m_pVertex[6].vPos.x += 1.f;
	m_pVertex[6].vPos.y += 5.f + fRand1;


	m_pVertex[7].vPos = m_pCubeInfo->vPos;
	m_pVertex[7].vPos.y += 5.f + fRand1;

	AddComponent();


	m_fOriginY = m_pInfo->m_vPos.y;

	m_pInfo->m_vPos.y = 20.f;

	return S_OK;
}

void CCubeBrick::Update(void)
{
	float fTime = Engine::Get_TimeMgr()->GetTime();

	if (m_bFallingStart)
	{
		m_fFallTime += fTime * 20.f;

		if (m_fFallTime > (float)m_iNum)
		{
			if (m_pInfo->m_vPos.y > m_fOriginY)
			{
				m_pInfo->m_vPos.y -= fTime * m_fSpeed;
			}
			else
			{
				//m_pStage3->BrickEffect(m_pInfo->m_vPos);
				m_pInfo->m_vPos.y = m_fOriginY;
				m_bFallingStart = FALSE;
			}
		}
	}



	CGameObject::Update();
	SetTransform();
}

void CCubeBrick::Render(void)
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Brick", m_pConvertVertex);

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000045);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	m_pTextureCom->Render(m_pCubeInfo->iTexInx);
	m_pBufferCom->Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CCubeBrick::Release(void)
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);

}

void CCubeBrick::SetTransform()
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);
}

CCubeBrick * CCubeBrick::Create(LPDIRECT3DDEVICE9 pGraphicDev, CUBEINFO * pCubeInfo, const float _fSpd, const int iNum)
{
	CCubeBrick*	pInstance = new CCubeBrick(pGraphicDev);

	if (FAILED(pInstance->Initialize(pCubeInfo, _fSpd, iNum)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
