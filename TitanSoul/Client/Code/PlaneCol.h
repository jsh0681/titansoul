#ifndef PlaneCol_h__
#define PlaneCol_h__

/*!
 * \class CPlaneCol
 *
 * \간략정보 평면충돌 객체
 *
 * \상세정보 업데이트 이후 얼마나 충돌한 거리를 알 수 있음
 *
 * \작성자 윤유석
 *
 * \date 1월 8 2019
 *
 */


#include "Collision.h"

class CPlaneCol : public Engine::CCollision
{
private:
	explicit CPlaneCol();
public:
	virtual ~CPlaneCol();

public:
	virtual void Update();
	void		Release(void);
	void		SetColInfo(D3DXVECTOR3* pPos, const Engine::VTXTEX* pPlaneVtx);

	const D3DXVECTOR3&	GetColVec();
	float		GetColDisX();
	float		GetColDisY();
	float		GetColDisZ();


private:
	D3DXVECTOR3*				m_pPos;
	const Engine::VTXTEX*		m_pPlaneVtx;
	D3DXVECTOR3					m_vDis;
	D3DXPLANE					m_Plane;
	
public:
	static	CPlaneCol*				Create(void);
	virtual Engine::CCollision*		Clone(void);
};

#endif // !PlaneCol_h__