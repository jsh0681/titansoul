#include "stdafx.h"
#include "Arrow.h"

#include "Export_Function.h"
#include "SphereColBox.h"


#include "TexAni.h"
#include "SphereColBox.h"
#include "Stage3.h"
#define FULLGAUAGE  0.8f
#define FRICTION 0.8f;
CArrow::CArrow(LPDIRECT3DDEVICE9 pGraphicDev)
	: CGameObject(pGraphicDev)
{
	m_vAccel = { 0.f,0.f, 0.f };
}


CArrow::~CArrow()
{
	Release();
}

void CArrow::Update(void)
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	SetDirection();

	if (m_bIsFire && m_dArrowState != A_IDLE && m_dArrowState != A_ZOOM)
	{
		m_fFireEffectTime += fTime;
		m_fFireTime += fTime;

		if (m_fFireEffectTime > 0.05f)
		{
			if (m_pScene != nullptr)
			{
				dynamic_cast<CStage3*>(m_pScene)->FireArrowEffect(m_pInfo->m_vPos + m_pInfo->m_vDir);
			}
			m_fFireEffectTime = 0.f;
		}

		if (m_fFireTime > 3.f)
		{
			m_fFireTime = 0.f;
			m_bIsFire = FALSE;
		}
	}

	if (m_bIsShouting)
	{
		float fDistance = D3DXVec3Length(&D3DXVECTOR3(m_pInfo->m_vDir - m_vAccel));
		fDistance = fabs(fDistance);
		m_pInfo->m_vDir += m_vAccel * fTime;

		if (fDistance < 1.1f)
		{
			m_bIsShouting = FALSE;
			m_fSpeed = 0.f;
			test = 0;
			m_dArrowState = A_STOP;
		}
	}

	if (m_dArrowState == A_STOP)
	{
		D3DXVECTOR3 vLength = m_pTargetInfo->m_vPos - m_pInfo->m_vPos;
		float fLength = D3DXVec3Length(&vLength);
		if (fLength < 1.f)
		{
			m_bReady = TRUE;
		}
	}


	if (!m_bIsRecovery)
	{
		m_vBackPos = m_pInfo->m_vPos;
		m_pInfo->m_vPos += m_pInfo->m_vDir * fTime * m_fSpeed;
		m_vPosCol = m_pInfo->m_vPos;
		m_vPosCol += m_pInfo->m_vDir;
	}
	else //회수상태
	{
		//충돌
		D3DXVECTOR3 vLength = m_pTargetInfo->m_vPos - m_pInfo->m_vPos;

		float fLength = D3DXVec3Length(&vLength);

		if (fLength < 1.f)
		{
			m_bReady = TRUE;
		}

		if (!m_bPressRecovery) //키를 누르고 있지 않은경우....
		{
			float fDistance = D3DXVec3Length(&D3DXVECTOR3(m_pInfo->m_vDir - m_vAccel));
			fDistance = fabs(fDistance);
			m_pInfo->m_vDir += m_vAccel * fTime;

			if (fDistance < 0.8f)
			{
				m_fRecoverySpd = 0.f;
			}

			m_pInfo->m_vPos += m_pInfo->m_vDir * fTime * m_fRecoverySpd;
			m_vPosCol = m_pInfo->m_vPos;

			if (m_bCollisionPlane)
			{
				m_pInfo->m_vPos -= m_pInfo->m_vDir * fTime * m_fRecoverySpd;
				m_vPosCol = m_pInfo->m_vPos;
			}
			//m_vPosCol += m_pInfo->m_vDir;
		}
	}

	Engine::CGameObject::Update();
	SetTransform();

	m_bPressRecovery = FALSE;
	m_bCollisionPlane = FALSE;
}

void CArrow::Render(void)
{
	if (!m_bValid) return;

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Arrow", m_pConvertVertex);

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pTextureCom->Render(0);
	m_pBufferCom->Render();
	//m_pSphereColBox->Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CArrow::SetValid(bool _bResult)
{
	if (m_bValid != _bResult)
	{
		if (!m_bValid)
		{
			m_fRealDistance = 0.f;
			m_fGauge = FULLGAUAGE;
			m_fSaveDistance = 0.f;
			m_bFullGauge = FALSE;
			SettingStartPoint();
			m_bIsNew = FALSE;
		}
		else
		{
			m_bIsNew = TRUE;
		}
		m_bValid = _bResult;
	}


}

void CArrow::ResetData()
{
	m_dArrowState = A_IDLE;
	m_bReady = FALSE;
	m_vAccel = { 0.f,0.f, 0.f };
	m_bIsRecovery = FALSE;
	m_fRecoverySpd = 0.f;
	m_bValid = FALSE;
	m_bIsNew = TRUE;
	m_bFullGauge = FALSE; //게이지가 풀일때....
	m_fGauge = FULLGAUAGE;
	m_fSaveDistance = 0.f; // 화살 줌할때 뒤로 밀림. 얼마나 밀렸는지 계산할 녀셕임. 줌하면서 화살 방향 바꿀때 써먹어야지.
	m_fRealDistance = 0.f; // 얼마나 밀렸는지 실제 숫자;
	m_fAngle = 0.f; //화살이 가리키는 각도!. 예측하지 못하여 노가다로 비슷한 값 잡아냄..
	m_bIsShouting = FALSE;// 화살이 날아가는 상황...
	m_fSpeed = 0.f; //화살이 날아가는 스피드를 의미한다.
	m_fDuration = 0.f; //화살이 날아가는 지속 시간을 의미한다.
}

void CArrow::SettingStartPoint()
{

	m_pInfo->m_vPos.x = m_pTargetInfo->m_vPos.x;
	m_pInfo->m_vPos.z = m_pTargetInfo->m_vPos.z;

	if (!m_bFullGauge)
	{
		if (m_bIsNew)
		{
			m_pInfo->m_vPos += m_pTargetInfo->m_vDir * (FULLGAUAGE);
		}
		else
		{
			m_pInfo->m_vPos += (m_pTargetInfo->m_vDir * (FULLGAUAGE)) - (m_pTargetInfo->m_vDir  * m_fRealDistance);

			//m_pInfo->m_vPos = m_pInfo->m_vPos + m_pTargetInfo->m_vDir * m_fRealDistance * (FULLGAUAGE - m_fSaveDistance);

		}
		//m_pInfo->m_vPos = m_pInfo->m_vPos + m_pTargetInfo->m_vDir * (FULLGAUAGE - m_fSaveDistance) * 2.f;
	}
	else
		m_pInfo->m_vPos = m_pInfo->m_vPos + m_pTargetInfo->m_vDir * m_fRealDistance * 2.f;





	////줌하는 도중 방향을 바꾸고 이게 실행된 상태라면... 계산해놓은 거리만큼 뒤로 빼준다.
	//m_pInfo->m_vPos -= m_pTargetInfo->m_vDir * m_fSaveDistance;


}

bool CArrow::Shout()
{
	//화살을 쏘는 함수..ㅇ으.으.

	//만약 게이지가 좃도 안차있는 상황이라면? 화살은 나가지 않는다. 실패시 TRUE 반환!!!
	if (m_fSaveDistance <= 0.168f)
		return TRUE;

	m_dArrowState = A_SHOT;
	m_fSaveDistance -= 0.1f;
	m_fSaveDistance *= 100.f;


	m_fSpeed = m_fSaveDistance;


	m_pInfo->m_vDir = m_pTargetInfo->m_vDir;

	m_bIsShouting = TRUE;


	m_vAccel = -m_pInfo->m_vDir * FRICTION;

	m_vStart = m_pInfo->m_vPos;
	CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_ARROW);
	CSoundMgr::GetInstance()->PlaySoundw(L"Arrow_Shoot.ogg", CSoundMgr::SOUND_ARROW);

	return FALSE;
}

void CArrow::DirectionChange(const float & _fAngle)
{
	if (-22.5f <= _fAngle && _fAngle <= 22.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::RR;
	}
	else if (22.5f <= _fAngle && _fAngle <= 67.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DR;
	}
	else if (67.5f <= _fAngle  && _fAngle <= 112.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DD;

	}
	else if (112.5f <= _fAngle && _fAngle <= 157.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DL;
	}
	else if (-157.5f <= _fAngle && _fAngle <= -112.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UL;
	}
	else if (-112.5f <= _fAngle  && _fAngle <= -67.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UU;
	}
	else if (-67.5 <= _fAngle  && _fAngle <= -22.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UR;
	}
	else
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::LL;
	}

	m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(_fAngle);

}

bool CArrow::Recovery(const float & _fTime, const bool& bResult)
{
	m_dArrowState = A_RECOVERY;
	m_bPressRecovery = TRUE;
	if (bResult)
	{
		m_bIsRecovery = TRUE;
		m_fRecoverySpd = 0.f;

		if (m_fAngle > 0.f) //양수라면...
		{
			m_fAngle = m_fAngle - 180.f;
		}
		else
		{
			m_fAngle = m_fAngle + 180.f;
		}

		DirectionChange(-m_fAngle);

		m_fAngle = -m_fAngle;
		//가속설정

		m_pInfo->m_vDir = m_pTargetInfo->m_vPos - m_pInfo->m_vPos;
		D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
		m_vAccel = -m_pInfo->m_vDir * FRICTION;
	}

	m_fRecoverySpd += _fTime * 30.3f;

	m_pInfo->m_vPos += m_pInfo->m_vDir * _fTime * m_fRecoverySpd;
	m_vPosCol = m_pInfo->m_vPos;

	if (m_bCollisionPlane)
	{
		m_pInfo->m_vPos -= m_pInfo->m_vDir * _fTime * m_fRecoverySpd;
		m_vPosCol = m_pInfo->m_vPos;
	}

	return FALSE;
}

bool CArrow::Get_DirectLength() // 엑셀(반대방향), 방향 차의 길이벡터를 구해서 길이가 작으면 TRUE 반환
{
	float fDistance = D3DXVec3Length(&D3DXVECTOR3(m_pInfo->m_vDir - m_vAccel));
	fDistance = fabs(fDistance);

	if (fDistance < 1.1f)
	{
		m_fRecoverySpd = 0.f;
		m_fSpeed = 0.f;
		test = 0;
	}
	return TRUE;

}

void CArrow::StopArrow()
{
	m_dArrowState = A_STOP;
	m_bIsShouting = FALSE;
	m_fSpeed = 0.f;
	test = 0;
}

void CArrow::StopRecovery(const float& fTime)
{
	m_pInfo->m_vPos -= m_pInfo->m_vDir * fTime * m_fRecoverySpd;

}

void CArrow::Reflect(const D3DXVECTOR3 & _vDir,  const D3DXVECTOR3& _vPoint)
{
	if (test > 5)
	{
		StopArrow();
		return;
	}

	//m_pInfo->m_vPos = _vPoint;
	//m_vStart = _vPoint;
	
	m_vStart = m_pInfo->m_vPos;
	m_pInfo->m_vPos -= m_pInfo->m_vDir * Engine::Get_TimeMgr()->GetTime() * 3.f;
	////법선검사
	//m_pInfo->m_vDir = _vDir;
	//m_fSpeed *= 0.5f;
	//m_vAccel = -m_pInfo->m_vDir;

	//return;
	//번선 _vDir;

	m_pInfo->m_vDir;// = _vDir;
	m_fSpeed *= 0.5f;

	m_pInfo->m_vDir = m_pInfo->m_vDir - ((_vDir * (D3DXVec3Dot(&_vDir, &m_pInfo->m_vDir))) * 2);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
	m_vAccel = -m_pInfo->m_vDir;


	//방향을 구해주자.

	float fCos = D3DXVec3Dot(&m_pInfo->m_vDir, &g_vRight);

	fCos = acosf(fCos);
	fCos = D3DXToDegree(fCos);

	if (m_pInfo->m_vPos.z < m_pInfo->m_vPos.z + m_pInfo->m_vDir.z)
		fCos *= -1.f;

	if (fCos < -180.f || fCos > 180.f )
	{
		StopArrow();
		return;
	}


	DirectionChange(fCos);
	test++;

}

const float & CArrow::GetRadius()
{
	return m_pSphereColBox->GetRadius();
}

HRESULT CArrow::AddComponent(void)
{
	Engine::CComponent*		pComponent = nullptr;

	// buffer
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Arrow");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	// texture
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"ArrowDefaultDD");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	// Transform
	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);


	return S_OK;
}

HRESULT CArrow::Initialize(void)
{
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pResourcesMgr = Engine::Get_ResourceMgr();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);


	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Arrow", m_pVertex);

	m_pVertex[0].vPos = { -0.5f, 0.f, 0.5f };
	m_pVertex[0].vTex = { 0, 0 };

	m_pVertex[1].vPos = { 0.5f, 0.f, 0.5f };
	m_pVertex[1].vTex = { 1, 0 };

	m_pVertex[2].vPos = { 0.5f, 0.f, -0.5f };
	m_pVertex[2].vTex = { 1, 1 };

	m_pVertex[3].vPos = { -0.5f, 0.f, -0.5f };
	m_pVertex[3].vTex = { 0, 1 };



	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos = m_pTargetInfo->m_vPos;
	m_pSphereColBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 0.2f, TRUE);
	ResetData();
	return S_OK;
}

void CArrow::SetDirection(void)
{
	//D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	//D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CArrow::Release(void)
{
	Engine::Safe_Delete(m_pSphereColBox);
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	if (nullptr != m_pVertex)
	{
		Engine::Safe_Delete_Array(m_pVertex);
	}

	if (nullptr != m_pConvertVertex)
	{
		Engine::Safe_Delete_Array(m_pConvertVertex);
	}

}

void CArrow::SetTransform(void)
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);


	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);
}

void CArrow::Set_Fire(const bool & bResult)
{
	m_fFireTime = 0.f;
	m_bIsFire = bResult;
	CSoundMgr::GetInstance()->PlaySoundw(L"Arrow_Ignite.ogg", CSoundMgr::SOUND_ARROW);

}

void CArrow::SetTarget(const Engine::CTransform * pInfo)
{
	m_pTargetInfo = pInfo; //플레이어 Info를 Arrow 클래스에 저장
}

void CArrow::SetTargetDirection(Engine::DIRECTION _direct)
{
	m_DIR[Engine::CUR] = _direct;

	if (m_DIR[Engine::CUR] != m_DIR[Engine::PRE])
	{
		D3DXMATRIX matRotX;
		D3DXMATRIX matTrans;
		SettingStartPoint();

		switch (m_DIR[Engine::CUR])
		{
		case Engine::DD:
			m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(90.f);

			m_pInfo->m_vPos.y = ARROW_Y;
			m_vPosCol = m_pInfo->m_vPos;
			m_vPosCol.z -= 0.5f;
			break;

		case Engine::UU:
			m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(-90.f);
			m_pInfo->m_vPos.y = 0.05f;
			m_vPosCol = m_pInfo->m_vPos;

			m_vPosCol.z += 0.5f;
			break;

		case Engine::LL:
			m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(180.f);;
			m_pInfo->m_vPos.y = ARROW_Y;
			m_vPosCol = m_pInfo->m_vPos;

			m_vPosCol.x -= 0.5f;
			break;

		case Engine::RR:
			m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(0.f);
			m_pInfo->m_vPos.y = ARROW_Y;
			m_vPosCol = m_pInfo->m_vPos;

			m_vPosCol.x += 0.5f;
			break;

		case Engine::DR:
			m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(45.f);
			m_pInfo->m_vPos.y = ARROW_Y;
			m_vPosCol = m_pInfo->m_vPos;

			m_vPosCol.x += 0.5f;
			m_vPosCol.z -= 0.5f;
			break;

		case Engine::DL:
			m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(135.f);
			m_pInfo->m_vPos.y = ARROW_Y;
			m_vPosCol = m_pInfo->m_vPos;

			m_vPosCol.x -= 0.5f;
			m_vPosCol.z -= 0.5f;
			break;

		case Engine::UL:
			m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(-135.f);
			m_pInfo->m_vPos.y = 0.05f;
			m_vPosCol = m_pInfo->m_vPos;
			m_vPosCol.x -= 0.5f;
			m_vPosCol.z += 0.5f;
			break;

		case Engine::UR:
			m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(-45.f);
			m_pInfo->m_vPos.y = 0.05f;
			m_vPosCol = m_pInfo->m_vPos;
			m_vPosCol.x += 0.5f;
			m_vPosCol.z += 0.5f;
			break;
		}
		m_DIR[Engine::PRE] = m_DIR[Engine::CUR];
	}



}

void CArrow::SetTargetDirection(Engine::DIRECTION _direct, bool bFlag)
{
	m_DIR[Engine::CUR] = _direct;
	m_DIR[Engine::PRE] = _direct;

	SettingStartPoint();
	switch (m_DIR[Engine::CUR])
	{
	case Engine::DD:
		m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(90.f);

		m_pInfo->m_vPos.y = ARROW_Y;
		m_vPosCol = m_pInfo->m_vPos;
		m_vPosCol.z -= 0.5f;
		break;

	case Engine::UU:
		m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(-90.f);
		m_pInfo->m_vPos.y = 0.05f;
		m_vPosCol = m_pInfo->m_vPos;

		m_vPosCol.z += 0.5f;
		break;

	case Engine::LL:
		m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(180.f);;
		m_pInfo->m_vPos.y = ARROW_Y;
		m_vPosCol = m_pInfo->m_vPos;

		m_vPosCol.x -= 0.5f;
		break;

	case Engine::RR:
		m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(0.f);
		m_pInfo->m_vPos.y = ARROW_Y;
		m_vPosCol = m_pInfo->m_vPos;

		m_vPosCol.x += 0.5f;
		break;

	case Engine::DR:
		m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(45.f);
		m_pInfo->m_vPos.y = ARROW_Y;
		m_vPosCol = m_pInfo->m_vPos;

		m_vPosCol.x += 0.5f;
		m_vPosCol.z -= 0.5f;
		break;

	case Engine::DL:
		m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(135.f);
		m_pInfo->m_vPos.y = ARROW_Y;
		m_vPosCol = m_pInfo->m_vPos;

		m_vPosCol.x -= 0.5f;
		m_vPosCol.z -= 0.5f;
		break;

	case Engine::UL:
		m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(-135.f);
		m_pInfo->m_vPos.y = 0.05f;
		m_vPosCol = m_pInfo->m_vPos;
		m_vPosCol.x -= 0.5f;
		m_vPosCol.z += 0.5f;
		break;

	case Engine::UR:
		m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(-45.f);
		m_pInfo->m_vPos.y = 0.05f;
		m_vPosCol = m_pInfo->m_vPos;
		m_vPosCol.x += 0.5f;
		m_vPosCol.z += 0.5f;
		break;
	}
}

void CArrow::Gauge(const int & _iLv, const float& _fTime)
{
	if (m_fGauge <= 0.f)
	{
		m_fGauge = 0.f;
		m_fSaveDistance = FULLGAUAGE;
		m_pInfo->m_vPos = m_pInfo->m_vPos;

		m_bFullGauge = TRUE;
		return;
	}

	m_fGauge -= _fTime;
	m_fSaveDistance = FULLGAUAGE - m_fGauge;

	m_pInfo->m_vPos = m_pInfo->m_vPos - m_pTargetInfo->m_vDir * _fTime * m_fGauge;
	m_fRealDistance += _fTime * m_fGauge;

	//각도방향으로
	//m_pInfo->m_vPos -= m_pTargetInfo->m_vDir * _fTime;
}

CArrow * CArrow::Create(LPDIRECT3DDEVICE9 pGraphicDev, Engine::CTransform* pTrnasform)
{
	CArrow*	pInstance = new CArrow(pGraphicDev);
	pInstance->SetTarget(pTrnasform);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
