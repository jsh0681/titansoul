#include "stdafx.h"
#include "Snow.h"
#include "Export_Function.h"

#include"CameraObserver.h"

Snow::Snow(PARTICLEINFO* pParticleInfo)
{

	m_pParticleInfo = pParticleInfo;
	Engine::BOUNDINGBOX BoundingBox;
	BoundingBox._min = D3DXVECTOR3(-m_pParticleInfo->fBoundingBoxX, -m_pParticleInfo->fBoundingBoxY, -m_pParticleInfo->fBoundingBoxZ);
	BoundingBox._max = D3DXVECTOR3(m_pParticleInfo->fBoundingBoxX, m_pParticleInfo->fBoundingBoxY, m_pParticleInfo->fBoundingBoxZ);

	m_BoundingBox = BoundingBox;

	m_fSize = 0.03f;			//시스템 내 모든 파티클의 크기 
	m_pVBSize = 2048;		//버텍스 버퍼가 보관할 수 있는 파티클의 수 이 값은 실제 파티클 시스템 내의 파티클 수와는 독립적이다
	m_pVBOffset = 0;		//시작 오프셋
	m_pVBBatchSize = 512;	//하나의 단계에 정의된 파티클의 수

	for (int i = 0; i <m_pParticleInfo->iNumParticles; i++)
		AddParticle();
}

Snow::Snow(Engine::tagBoundingBox* boundingBox, int numParticles)
{
	m_BoundingBox = *boundingBox;
	m_fSize = 0.3f;			//시스템 내 모든 파티클의 크기 
	m_pVBSize = 2048;		//버텍스 버퍼가 보관할 수 있는 파티클의 수 이 값은 실제 파티클 시스템 내의 파티클 수와는 독립적이다
	m_pVBOffset = 0;		//시작 오프셋
	m_pVBBatchSize = 512;	//하나의 단계에 정의된 파티클의 수

	for (int i = 0; i < numParticles; i++)
		AddParticle();
}

void Snow::ResetParticle(Engine::tagAttribute* attribute)
{
	attribute->_isAlive = true;

	//스노우 플레이크의 위치에 대한 임의의 x, z 좌표를 얻습니다.
	CMathMgr::GetRandomVector(
		&attribute->_position,
		&m_BoundingBox._min,
		&m_BoundingBox._max);

	// 높이 (y 좌표)에 대한 임의성이 없습니다. 스노우 플레이크는 항상 경계 상자의 맨 위에서 시작됩니다.
	//attribute->_position.z = m_BoundingBox._max.z;
	//위에서 내리게 하고 싶으면 위에꺼 주석 푸세요!

	// 눈 조각은 아래쪽으로 약간 왼쪽으로 떨어집니다.
	attribute->_velocity.x = CMathMgr::GetRandomFloat(0.0f, 1.0f) * -1.0f;
	attribute->_velocity.y = 0.0f;
	attribute->_velocity.z = CMathMgr::GetRandomFloat(0.0f, 1.0f) * -1.0f;

	attribute->_color = D3DCOLOR_XRGB(255, 255, 255);
}

void Snow::Update(float timeDelta)
{

	std::list<Engine::tagAttribute>::iterator i;
	for (i = _particles.begin(); i != _particles.end(); i++)
	{

		i->_position += i->_velocity * timeDelta;

		if (m_BoundingBox.isPointInside(i->_position) == false)
		{
			ResetParticle(&(*i));
		}
	}

}