#include "stdafx.h"
#include "SphereColBox.h"


CSphereColBox::CSphereColBox(LPDIRECT3DDEVICE9 pGraphicDev) :
	m_pGraphicDev(pGraphicDev)
{
}


CSphereColBox::~CSphereColBox()
{
	Release();
}

HRESULT CSphereColBox::Initialize(const D3DXMATRIX* pWorld, const float& fRadius,const bool& bBlending)
{
	if (pWorld == nullptr)
		return E_FAIL;

	D3DXMatrixIdentity(&m_matIdtt);
	m_pWorld = pWorld;
	m_fRadius = fRadius;
	m_bBlending = bBlending;

	D3DXCreateSphere(m_pGraphicDev, m_fRadius, 10, 10, &m_pMesh, nullptr);

	return S_OK;
}

void CSphereColBox::Release()
{
	m_pWorld = nullptr;

	if (m_pMesh != nullptr)
		m_pMesh->Release();
}

void CSphereColBox::Render()
{
	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	if (m_bBlending)
	{
		m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		m_pGraphicDev->SetTransform(D3DTS_WORLD, m_pWorld);
		m_pMesh->DrawSubset(0);
		m_pGraphicDev->SetTransform(D3DTS_WORLD, &m_matIdtt);
		m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
		m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	}
	else
	{
		m_pGraphicDev->SetTransform(D3DTS_WORLD, m_pWorld);
		m_pMesh->DrawSubset(0);
		m_pGraphicDev->SetTransform(D3DTS_WORLD, &m_matIdtt);
	}

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

}

void CSphereColBox::SetRadius(const float& fRadius)
{
	if (m_pMesh != nullptr)
		m_pMesh->Release();

	m_fRadius = fRadius;
	D3DXCreateSphere(m_pGraphicDev, m_fRadius, 10, 10, &m_pMesh, nullptr);
}

CSphereColBox* CSphereColBox::Create(LPDIRECT3DDEVICE9 pGraphicDev,const D3DXMATRIX* pWorld, 
										const float& fRadius, const bool& bBlending)
{
	CSphereColBox* pinstance = new CSphereColBox(pGraphicDev);
	if (FAILED(pinstance->Initialize(pWorld, fRadius, bBlending)))
		Engine::Safe_Delete(pinstance);

	return pinstance;
}