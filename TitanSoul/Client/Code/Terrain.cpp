#include "stdafx.h"
#include "Terrain.h"

#include "Export_Function.h"
#include "Include.h"
#include "Plane.h"
#include "Player.h"
#include "Arrow.h"
#include "IceCube.h"
#define NO_EVENT 0

CTerrain::CTerrain(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CGameObject(pGraphicDev)
	, m_pRefCnt(new int(0))
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
}

//************************************
// Method	:  CTerrain
// 작성자	:  윤유석
// Date		:  2019/01/07
// Message	:  복사 생성자 재정의
//			   Terrain 클론 사용시 Component들은 부모(CGameObject)에 의해서 삭제 되므로
//			   AddComponent 부분은 재호출, 나머지는 대입 연산(포인터는 얕은 복제)을 함.
//			   observer는 clone 되어 질때만 만들어짐, Delete시 삭제 시킴.
// Parameter: 
//************************************
CTerrain::CTerrain(const CTerrain& rhs) :
	Engine::CGameObject(rhs.m_pGraphicDev)
{
	m_pResourcesMgr = rhs.m_pResourcesMgr;
	m_pInfoSubject = rhs.m_pInfoSubject;

	m_pVertex = rhs.m_pVertex;
	m_pConvertVertex = rhs.m_pConvertVertex;

	m_dwVtxCnt = rhs.m_dwVtxCnt;

	m_pMapInfo = rhs.m_pMapInfo;
	m_pPlaneList = rhs.m_pPlaneList;

	m_pRefCnt = rhs.m_pRefCnt;

	AddComponent();			// Info,Buffer,Texture클론으로 새로 받음(컴포넌트 들은 지워져서 항상 새로 받아야함)

	m_pInfo->m_vPos = rhs.m_pInfo->m_vPos;

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);
}

CTerrain::~CTerrain()
{
	Release();
}

CTerrain* CTerrain::Clone()
{
	++(*m_pRefCnt);
	return new CTerrain(*this);;
}

void CTerrain::Update(void)
{
	SetDirection();

	Engine::CGameObject::Update();

	SetTransform();

	for (auto& pPlane : *m_pPlaneList)
	{
		pPlane->Update();
	}

}

void CTerrain::Render(void)
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_pMapInfo->wstrMapBufferKey, m_pConvertVertex);
	m_pTextureCom->Render(m_pMapInfo->iIndex);
	m_pBufferCom->Render();

	if(m_bPlaneRender)
		for (auto& pPlane : *m_pPlaneList)
			pPlane->Render();


}

HRESULT CTerrain::AddComponent(void)
{
	Engine::CComponent*		pComponent = nullptr;

	// buffer
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_pMapInfo->wstrMapBufferKey);
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	// texture
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_pMapInfo->wstrMapTexKey);
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	// Transform
	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

HRESULT CTerrain::Initialize(MAPINFO* pMapInfo)
{

	m_dwVtxCnt = 4;

	m_pMapInfo = pMapInfo;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_pMapInfo->wstrMapBufferKey, m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos.x += (m_pMapInfo->fSizeX / 2.f);
	m_pInfo->m_vPos.z += (m_pMapInfo->fSizeZ / 2.f);

	return S_OK;
}

void CTerrain::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CTerrain::Release(void)
{
	if ((*m_pRefCnt) == 0)
	{
		m_pPlaneList = nullptr;

		if (m_pCameraObserver != nullptr)
		{
			m_pInfoSubject->UnSubscribe(m_pCameraObserver);
			Engine::Safe_Delete(m_pCameraObserver);
		}

		Engine::Safe_Delete_Array(m_pVertex);
		Engine::Safe_Delete_Array(m_pConvertVertex);

		Engine::Safe_Delete(m_pMapInfo);
		Engine::Safe_Delete(m_pRefCnt);
	}
	else
	{
		--(*m_pRefCnt);

		m_pPlaneList = nullptr;
		m_pInfoSubject->UnSubscribe(m_pCameraObserver);
		Engine::Safe_Delete(m_pCameraObserver);
	}
}

bool CTerrain::CollisionPlaneBullet(const D3DXVECTOR3& vPos)
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	float fU;
	float fV;
	float fDist;

	D3DXMATRIX	matView = *m_pCameraObserver->GetView();
	D3DXMATRIX	matProj = *m_pCameraObserver->GetProj();
	D3DXVECTOR3 m_vRayPos = D3DXVECTOR3(0.f, 0.f, 0.f);

	// 월드 좌표로 변환
	D3DXMatrixInverse(&matView, 0, &matView);
	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matView);

	D3DXVECTOR3 m_vRayDir = vPos - m_vRayPos;
	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);

	//플레이어와...
	for (auto& Plane : *m_pPlaneList)
	{

		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex1,
			&Plane->Get_PlaneInfo()->vVertex2,
			&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
		{

			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}

			if (Plane->Get_PlaneInfo()->iOption == BLOCK || bIceCheck)
			{
				return TRUE;

			}
			else
			{
				return FALSE;
			}
		}

		// 왼쪽 아래
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex2,
			&Plane->Get_PlaneInfo()->vVertex3,
			&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
		{
			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}
			if (Plane->Get_PlaneInfo()->iOption == BLOCK || bIceCheck)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}

	return FALSE;
}

int CTerrain::CollisionPlane(CPlayer * pPlayer)
{
	if (!pPlayer) return NO_EVENT;

	float fTime = Engine::Get_TimeMgr()->GetTime();
	float fU;
	float fV;
	float fDist;

	D3DXMATRIX		matView = *m_pCameraObserver->GetView();
	D3DXMATRIX	matProj = *m_pCameraObserver->GetProj();
	D3DXVECTOR3 m_vRayPos = D3DXVECTOR3(0.f, 0.f, 0.f);

	// 월드 좌표로 변환
	D3DXMatrixInverse(&matView, 0, &matView);
	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matView);

	D3DXVECTOR3 m_vRayDir = pPlayer->Get_Trnasform()->m_vPos - m_vRayPos;
	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);

	//플레이어와...
	for (auto& Plane : *m_pPlaneList)
	{
		//enum OPTION { BLOCK, WATER, ICE, VINE, FRONT, LEFT, RIGHT, SNOW, NEXT, PLOP_END };

			if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
				&Plane->Get_PlaneInfo()->vVertex1,
				&Plane->Get_PlaneInfo()->vVertex2,
				&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
			{

				bool bIceCheck = FALSE;

				if (g_BattleMode == TRUE)
				{
					if (Plane->Get_PlaneInfo()->iOption == ICE)
					{
						bIceCheck = TRUE;
					}
				}

				if (Plane->Get_PlaneInfo()->iOption == BLOCK || bIceCheck)
				{			

					//구르는 상태라면"?
					if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
					{
						//if(!pPlayer->Get_Collision())
							pPlayer->ReverseRoll(fTime);
						//else
						{
							//그만굴러
						//	pPlayer->StopReverseRoll();
						}
					}
					else
					{
						pPlayer->ReverseMove(fTime);
						pPlayer->SetPush(1);
					}
					return NO_EVENT;
				}

				else if (Plane->Get_PlaneInfo()->iOption == FRONT)
				{
					//구르는 상태이면서... 앞으로 구르면?
					if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
					{

						if (pPlayer->Get_PlayerDirect() <= Engine::DIRECTION::DL)
						{
							//이러면 구르기 허용할 뿐만아니라 속도까지 왕창 늘어남.
							pPlayer->Set_DoubleSpeed(1.5f);
						}
						else
						{
							//튕기기 == 인덱스 0초과하면 구르는 중이므로 튕기자
							if (pPlayer->Get_AnimationIndex() > 0)
							{
								pPlayer->ReverseRoll(fTime);
							}
							else
							{
								//구르지마!
								pPlayer->RollVan();
							}
						}
					}
					else
						pPlayer->Set_CollisionSpeed(0.5f);
					return NO_EVENT;
				}
				else if (Plane->Get_PlaneInfo()->iOption == LEFT)
				{
					if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
					{
						if (pPlayer->Get_PlayerDirect() == Engine::DIRECTION::DL)
						{
							pPlayer->Set_DoubleSpeed(1.5f);
						}
						else if (pPlayer->Get_PlayerDirect() == Engine::LL)
						{
							//내리락
							pPlayer->GoUpStairs(Engine::LL, -1.f);
						}
						else
						{
							if (pPlayer->Get_AnimationIndex() > 0)
							{
								pPlayer->ReverseRoll(fTime);
							}
							else
							{
								//구르지마!
								pPlayer->RollVan();
							}
						}
					}
					else
					{
						//pPlayer->Set_CollisionSpeed(0.5f);
						if (pPlayer->Get_PlayerDirect() == Engine::RR)
						{
							//오르락
							pPlayer->GoUpStairs(Engine::LL, 1.f);

						}
						else if (pPlayer->Get_PlayerDirect() == Engine::LL)
						{
							//내리락
							pPlayer->GoUpStairs(Engine::LL, -1.f);
						}
					}
					return NO_EVENT;
				}
				else if (Plane->Get_PlaneInfo()->iOption == RIGHT)
				{
					if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
					{
						if (pPlayer->Get_PlayerDirect() == Engine::DIRECTION::DR)
						{
							pPlayer->Set_DoubleSpeed(1.5f);
						}
						else if (pPlayer->Get_PlayerDirect() == Engine::RR)
						{
							//내리락
							pPlayer->GoUpStairs(Engine::LL, -1.f);
						}
						else
						{
							if (pPlayer->Get_AnimationIndex() > 0)
							{
								pPlayer->ReverseRoll(fTime);
							}
							else
							{
								//구르지마!
								pPlayer->RollVan();
							}
						}
					}
					else
					{
						//pPlayer->Set_CollisionSpeed(0.5f);
						if (pPlayer->Get_PlayerDirect() == Engine::RR)
						{
							//내리락
							pPlayer->GoUpStairs(Engine::LL, -1.f);

						}
						else if (pPlayer->Get_PlayerDirect() == Engine::LL)
						{
							//오르락
							pPlayer->GoUpStairs(Engine::LL, 1.f);
						}
					}
					return NO_EVENT;
				}
				else if (Plane->Get_PlaneInfo()->iOption == WATER)
				{
					int i = 0;
					pPlayer->SetSwim(TRUE);
					return NO_EVENT;

				}

				else if (Plane->Get_PlaneInfo()->iOption == VINE)
				{
					return NO_EVENT;

				}
				else if (Plane->Get_PlaneInfo()->iOption == NEXT)
				{
					return Plane->Get_PlaneInfo()->iTrigger;
				}
				else
				{
					pPlayer->Set_CollisionSpeed(1.f);
					return NO_EVENT;
				}
			}

			// 왼쪽 아래
			if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
				&Plane->Get_PlaneInfo()->vVertex2,
				&Plane->Get_PlaneInfo()->vVertex3,
				&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
			{
				bool bIceCheck = FALSE;

				if (g_BattleMode == TRUE)
				{
					if (Plane->Get_PlaneInfo()->iOption == ICE)
					{
						bIceCheck = TRUE;
					}
				}

				if (Plane->Get_PlaneInfo()->iOption == BLOCK || bIceCheck)
				{
					//구르는 상태라면"?
					if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
					{
						//if (!pPlayer->Get_Collision())
							pPlayer->ReverseRoll(fTime);
					//	else
						{
							//그만굴러
						//	pPlayer->StopReverseRoll();
						}
					}
					else
					{
						pPlayer->ReverseMove(fTime);
						pPlayer->SetPush(1);
					}
					return NO_EVENT;
				}
				else if (Plane->Get_PlaneInfo()->iOption == FRONT)
				{
					//구르는 상태이면서... 앞으로 구르면?
					if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
					{
						
						//enum DIRECTION { DD, DR, DL, UU, UR, UL, RR, LL, END_DIR };
						
						if (pPlayer->Get_PlayerDirect() <= Engine::DIRECTION::DL)
						{
							pPlayer->Set_DoubleSpeed(1.5f);
							//이러면 구르기 허용할 뿐만아니라 속도까지 왕창 늘어남.
						}
						else
						{
							//튕기기 == 인덱스 0초과하면 구르는 중이므로 튕기자
							if (pPlayer->Get_AnimationIndex() > 0)
							{
								pPlayer->ReverseRoll(fTime);
							}
							else
							{
								//구르지마!
								pPlayer->RollVan();
							}
						}
					}
					else
						pPlayer->Set_CollisionSpeed(0.5f);
					return NO_EVENT;
				}
				else if (Plane->Get_PlaneInfo()->iOption == LEFT)
				{
					if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
					{
						if (pPlayer->Get_PlayerDirect() == Engine::DIRECTION::DL)
						{
							pPlayer->Set_DoubleSpeed(1.5f);
						}
						else if (pPlayer->Get_PlayerDirect() == Engine::LL)
						{
							//내리락
							pPlayer->GoUpStairs(Engine::LL, -1.f);
						}
						else
						{
							if (pPlayer->Get_AnimationIndex() > 0)
							{
								pPlayer->ReverseRoll(fTime);
							}
							else
							{
								//구르지마!
								pPlayer->RollVan();
							}
						}
					}
					else
					{
						//pPlayer->Set_CollisionSpeed(0.5f);
						if (pPlayer->Get_PlayerDirect() == Engine::RR)
						{
							//오르락
							pPlayer->GoUpStairs(Engine::LL, 1.f);

						}
						else if (pPlayer->Get_PlayerDirect() == Engine::LL)
						{
							//내리락
							pPlayer->GoUpStairs(Engine::LL, -1.f);
						}
					}
					return NO_EVENT;
				}
				else if (Plane->Get_PlaneInfo()->iOption == RIGHT)
				{
					if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
					{
						if (pPlayer->Get_PlayerDirect() == Engine::DIRECTION::DR)
						{
							pPlayer->Set_DoubleSpeed(1.5f);
						}
						else if (pPlayer->Get_PlayerDirect() == Engine::RR)
						{
							//내리락
							pPlayer->GoUpStairs(Engine::LL, -1.f);
						}
						else
						{
							if (pPlayer->Get_AnimationIndex() > 0)
							{
								pPlayer->ReverseRoll(fTime);
							}
							else
							{
								//구르지마!
								pPlayer->RollVan();
							}
						}
					}
					else
					{
						//pPlayer->Set_CollisionSpeed(0.5f);
						if (pPlayer->Get_PlayerDirect() == Engine::RR)
						{
							//내리락
							pPlayer->GoUpStairs(Engine::LL, -1.f);

						}

						else if (pPlayer->Get_PlayerDirect() == Engine::LL)
						{
							//오르락
							pPlayer->GoUpStairs(Engine::LL, 1.f);
						}
					}
					return NO_EVENT;
				}
				else if (Plane->Get_PlaneInfo()->iOption == WATER)
				{
					pPlayer->SetSwim(TRUE);

					return NO_EVENT;

				}
				else if (Plane->Get_PlaneInfo()->iOption == VINE)
				{
					return NO_EVENT;

				}
				else if (Plane->Get_PlaneInfo()->iOption == NEXT)
				{
					return Plane->Get_PlaneInfo()->iTrigger;
				}
				else
				{
					pPlayer->Set_CollisionSpeed(1.f);
					return NO_EVENT;
				}
			}

			pPlayer->Set_CollisionSpeed(1.f);
			pPlayer->SetSwim(FALSE);
			//pPlayer->StopReverseRoll();
		}
		return 0;
}

int CTerrain::CollisionPlaneMap1(CPlayer * pPlayer)
{
	if (!pPlayer) return NO_EVENT;

	float fTime = Engine::Get_TimeMgr()->GetTime();
	float fU;
	float fV;
	float fDist;

	D3DXMATRIX		matView = *m_pCameraObserver->GetView();
	D3DXMATRIX	matProj = *m_pCameraObserver->GetProj();
	D3DXVECTOR3 m_vRayPos = D3DXVECTOR3(0.f, 0.f, 0.f);

	// 월드 좌표로 변환
	D3DXMatrixInverse(&matView, 0, &matView);
	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matView);

	D3DXVECTOR3 m_vRayDir = pPlayer->Get_Trnasform()->m_vPos - m_vRayPos;
	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);

	//플레이어와...
	for (auto& Plane : *m_pPlaneList)
	{
		//enum OPTION { BLOCK, WATER, ICE, VINE, FRONT, LEFT, RIGHT, SNOW, NEXT, PLOP_END };

		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex1,
			&Plane->Get_PlaneInfo()->vVertex2,
			&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
		{

			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}

			if (Plane->Get_PlaneInfo()->iOption == BLOCK || bIceCheck)
			{

				//구르는 상태라면"?
				if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
				{
					pPlayer->ReverseRoll(fTime);
				}
				else
				{
					pPlayer->ReverseMove(fTime);
					pPlayer->SetPush(1);
				}
				return NO_EVENT;
			}

			else if (Plane->Get_PlaneInfo()->iOption == FRONT)
			{
				//구르는 상태이면서... 앞으로 구르면?
				if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
				{

					if (pPlayer->Get_PlayerDirect() <= Engine::DIRECTION::DL)
					{
						//이러면 구르기 허용할 뿐만아니라 속도까지 왕창 늘어남.
						pPlayer->Set_DoubleSpeed(1.5f);
					}
					else
					{
						//튕기기 == 인덱스 0초과하면 구르는 중이므로 튕기자
						if (pPlayer->Get_AnimationIndex() > 0)
						{
							pPlayer->ReverseRoll(fTime);
						}
						else
						{
							//구르지마!
							pPlayer->RollVan();
						}
					}
				}
				else
					pPlayer->Set_CollisionSpeed(0.5f);
				return NO_EVENT;
			}
			else if (Plane->Get_PlaneInfo()->iOption == LEFT-1)
			{
				if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
				{
					if (pPlayer->Get_PlayerDirect() == Engine::DIRECTION::DL)
					{
						pPlayer->Set_DoubleSpeed(1.5f);
					}
					else if (pPlayer->Get_PlayerDirect() == Engine::LL)
					{
						//내리락
						pPlayer->GoUpStairs(Engine::LL, -1.f);
					}
					else
					{
						if (pPlayer->Get_AnimationIndex() > 0)
						{
							pPlayer->ReverseRoll(fTime);
						}
						else
						{
							//구르지마!
							pPlayer->RollVan();
						}
					}
				}
				else
				{
					//pPlayer->Set_CollisionSpeed(0.5f);
					if (pPlayer->Get_PlayerDirect() == Engine::RR)
					{
						//오르락
						pPlayer->GoUpStairs(Engine::LL, 1.f);

					}
					else if (pPlayer->Get_PlayerDirect() == Engine::LL)
					{
						//내리락
						pPlayer->GoUpStairs(Engine::LL, -1.f);
					}
				}
				return NO_EVENT;
			}
			else if (Plane->Get_PlaneInfo()->iOption == RIGHT+1)
			{
				if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
				{
					if (pPlayer->Get_PlayerDirect() == Engine::DIRECTION::DR)
					{
						pPlayer->Set_DoubleSpeed(1.5f);
					}
					else if (pPlayer->Get_PlayerDirect() == Engine::RR)
					{
						//내리락
						pPlayer->GoUpStairs(Engine::LL, -1.f);
					}
					else
					{
						if (pPlayer->Get_AnimationIndex() > 0)
						{
							pPlayer->ReverseRoll(fTime);
						}
						else
						{
							//구르지마!
							pPlayer->RollVan();
						}
					}
				}
				else
				{
					//pPlayer->Set_CollisionSpeed(0.5f);
					if (pPlayer->Get_PlayerDirect() == Engine::RR)
					{
						//내리락
						pPlayer->GoUpStairs(Engine::LL, -1.f);

					}
					else if (pPlayer->Get_PlayerDirect() == Engine::LL)
					{
						//오르락
						pPlayer->GoUpStairs(Engine::LL, 1.f);
					}
				}
				return NO_EVENT;
			}
			else if (Plane->Get_PlaneInfo()->iOption == WATER)
			{
				int i = 0;

				return NO_EVENT;

			}

			else if (Plane->Get_PlaneInfo()->iOption == VINE)
			{
				return NO_EVENT;

			}
			else if (Plane->Get_PlaneInfo()->iOption == NEXT)
			{
				return Plane->Get_PlaneInfo()->iTrigger;
			}
			else
			{
				pPlayer->Set_CollisionSpeed(1.f);
				return NO_EVENT;
			}
		}

		// 왼쪽 아래
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex2,
			&Plane->Get_PlaneInfo()->vVertex3,
			&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
		{
			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}

			if (Plane->Get_PlaneInfo()->iOption == BLOCK || bIceCheck)
			{
				//구르는 상태라면"?
				if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
				{
					pPlayer->ReverseRoll(fTime);
				}
				else
				{
					pPlayer->ReverseMove(fTime);
					pPlayer->SetPush(1);
				}
				return NO_EVENT;
			}
			else if (Plane->Get_PlaneInfo()->iOption == FRONT)
			{
				//구르는 상태이면서... 앞으로 구르면?
				if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
				{

					//enum DIRECTION { DD, DR, DL, UU, UR, UL, RR, LL, END_DIR };

					if (pPlayer->Get_PlayerDirect() <= Engine::DIRECTION::DL)
					{
						pPlayer->Set_DoubleSpeed(1.5f);
						//이러면 구르기 허용할 뿐만아니라 속도까지 왕창 늘어남.
					}
					else
					{
						//튕기기 == 인덱스 0초과하면 구르는 중이므로 튕기자
						if (pPlayer->Get_AnimationIndex() > 0)
						{
							pPlayer->ReverseRoll(fTime);
						}
						else
						{
							//구르지마!
							pPlayer->RollVan();
						}
					}
				}
				else
					pPlayer->Set_CollisionSpeed(0.5f);
				return NO_EVENT;
			}
			else if (Plane->Get_PlaneInfo()->iOption == LEFT-1)
			{
				if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
				{
					if (pPlayer->Get_PlayerDirect() == Engine::DIRECTION::DL)
					{
						pPlayer->Set_DoubleSpeed(1.5f);
					}
					else if (pPlayer->Get_PlayerDirect() == Engine::LL)
					{
						//내리락
						pPlayer->GoUpStairs(Engine::LL, -1.f);
					}
					else
					{
						if (pPlayer->Get_AnimationIndex() > 0)
						{
							pPlayer->ReverseRoll(fTime);
						}
						else
						{
							//구르지마!
							pPlayer->RollVan();
						}
					}
				}
				else
				{
					//pPlayer->Set_CollisionSpeed(0.5f);
					if (pPlayer->Get_PlayerDirect() == Engine::RR)
					{
						//오르락
						pPlayer->GoUpStairs(Engine::LL, 1.f);

					}
					else if (pPlayer->Get_PlayerDirect() == Engine::LL)
					{
						//내리락
						pPlayer->GoUpStairs(Engine::LL, -1.f);
					}
				}
				return NO_EVENT;
			}
			else if (Plane->Get_PlaneInfo()->iOption == RIGHT+1)
			{
				if (pPlayer->Get_PlayerState() == P_STATE::P_ROLL)
				{
					if (pPlayer->Get_PlayerDirect() == Engine::DIRECTION::DR)
					{
						pPlayer->Set_DoubleSpeed(1.5f);
					}
					else if (pPlayer->Get_PlayerDirect() == Engine::RR)
					{
						//내리락
						pPlayer->GoUpStairs(Engine::LL, -1.f);
					}
					else
					{
						if (pPlayer->Get_AnimationIndex() > 0)
						{
							pPlayer->ReverseRoll(fTime);
						}
						else
						{
							//구르지마!
							pPlayer->RollVan();
						}
					}
				}
				else
				{
					//pPlayer->Set_CollisionSpeed(0.5f);
					if (pPlayer->Get_PlayerDirect() == Engine::RR)
					{
						//내리락
						pPlayer->GoUpStairs(Engine::LL, -1.f);

					}

					else if (pPlayer->Get_PlayerDirect() == Engine::LL)
					{
						//오르락
						pPlayer->GoUpStairs(Engine::LL, 1.f);
					}
				}
				return NO_EVENT;
			}
			else if (Plane->Get_PlaneInfo()->iOption == WATER)
			{
				int i = 0;

				return NO_EVENT;

			}
			else if (Plane->Get_PlaneInfo()->iOption == VINE)
			{
				return NO_EVENT;

			}
			else if (Plane->Get_PlaneInfo()->iOption == NEXT)
			{
				return Plane->Get_PlaneInfo()->iTrigger;
			}
			else
			{
				pPlayer->Set_CollisionSpeed(1.f);
				return NO_EVENT;
			}
		}

		pPlayer->Set_CollisionSpeed(1.f);
	}
	return 0;
}


void CTerrain::CollisoinPlanetoArrow(CArrow * pArrow, CPlayer * pPlayer)
{
	if (!pArrow) return;
	if (!pArrow->GetValid()) return; // 화살이 유효상태가 아니면 리턴해야지 않니?

	float fTime = Engine::Get_TimeMgr()->GetTime();
	float fU;
	float fV;
	float fDist;

	//반사벡터
	VEC3 vReflec;

	D3DXVECTOR3 vBackPos;

	D3DXVECTOR3 vUpLength;
	D3DXVECTOR3 vDownLength;
	D3DXVECTOR3 vRightLength;
	D3DXVECTOR3 vLeftLength;

	D3DXVECTOR3 vTemp; //내적용 tempvector;
	float fLeftLegth = 0.f;
	float fDownLegth = 0.f;
	float fRighLegth = 0.f;
	float fUpLegth = 0.f;

	float fLeftDot = 0.f;
	float fDownDot = 0.f;
	float fRightDot = 0.f;
	float fUpDot = 0.f;

	D3DXVECTOR3 vBackand0;
	D3DXVECTOR3 vBackand1;
	D3DXVECTOR3 vBackand2;
	D3DXVECTOR3 vBackand3;

	float fBackand0 = 0.f;
	float fBackand1 = 0.f;
	float fBackand2 = 0.f;
	float fBackand3 = 0.f;

	float fResult1 = 0.f;
	float fResult2 = 0.f;

	D3DXMATRIX		matView = *m_pCameraObserver->GetView();
	D3DXMATRIX	matProj = *m_pCameraObserver->GetProj();
	D3DXVECTOR3 m_vRayPos = D3DXVECTOR3(0.f, 0.f, 0.f);

	// 월드 좌표로 변환
	D3DXMatrixInverse(&matView, 0, &matView);
	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matView);

	m_vRayPos = pPlayer->Get_Trnasform()->m_vPos;
	m_vRayPos.y = 1.f;

	D3DXVECTOR3 m_vRayDir = pArrow->Get_ColPos()- m_vRayPos;
	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);


	//실험... 플레이어에서 쏴보자.

	//vright 벡터와 각 축을 내적해서... 양수 (예각)이면 x 축을 반전, 음수(둔각)이면 z축을 반전, 0이면 따로 예외처리

	//플레이어와...
	for (auto& Plane : *m_pPlaneList)
	{
		//오른쪽위
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex1,
			&Plane->Get_PlaneInfo()->vVertex2,
			&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
		{
			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}


			if (Plane->Get_PlaneInfo()->iOption == BLOCK || bIceCheck)
			{
				//타임백
				vBackPos = pArrow->Get_ColPos() - pArrow->Get_Trnasform()->m_vDir;

				//오른쪽 위 삼각형이니... 길이를 구하자.
				//우선 0 - 1 버텍스를 이어주는 윗변

				vUpLength = Plane->Get_PlaneInfo()->vVertex1 - Plane->Get_PlaneInfo()->vVertex0;
				vTemp = vUpLength;
				fUpDot = D3DXVec3Dot(&vTemp, &g_vRight);
				D3DXVec3Normalize(&vTemp, &vTemp);
				fUpLegth = D3DXVec3Length(&vUpLength);

				//Back과 0의 거리
				vBackand0 = vBackPos - Plane->Get_PlaneInfo()->vVertex0;
				fBackand0 = D3DXVec3Length(&vBackand0);
				fBackand0 = fabs(fBackand0);

				//Back과 1의 거리
				vBackand1 = vBackPos - Plane->Get_PlaneInfo()->vVertex1;
				fBackand1 = D3DXVec3Length(&vBackand1);
				fBackand1 = fabs(fBackand1);

				fResult1 = (fBackand1 + fBackand0) - fUpLegth;


				//다음 1 - 2을 이어주는 오른쪽면
				vRightLength = Plane->Get_PlaneInfo()->vVertex1 - Plane->Get_PlaneInfo()->vVertex2;
				fRighLegth = D3DXVec3Length(&vRightLength);
				vTemp = vRightLength;
				D3DXVec3Normalize(&vTemp, &vTemp);
				fRightDot = D3DXVec3Dot(&vTemp, &g_vRight);

				//Back과 1의 거리
				vBackand1 = vBackPos - Plane->Get_PlaneInfo()->vVertex1;
				fBackand1 = D3DXVec3Length(&vBackand0);
				fBackand1 = fabs(fBackand1);

				//Back과 3의 거리
				 vBackand2 = vBackPos - Plane->Get_PlaneInfo()->vVertex2;
				 fBackand2 = D3DXVec3Length(&vBackand2);
				fBackand2 = fabs(fBackand2);

				 fResult2 = (fBackand2 + fBackand1) - fRighLegth;

				 if (fResult1 > fResult2)
				 {
					 //작은것이 선택되는거임.
					 //지금은
					 //여기는... 12 오른쪽면이 처 맞은거

					 if (Plane->Get_PlaneInfo()->vVertex1.x == Plane->Get_PlaneInfo()->vVertex2.x) //0이면...
					 {
						 vRightLength.x += 1.f;
						 vRightLength.z = 0.f;
					 }
					 else if (Plane->Get_PlaneInfo()->vVertex1.x < Plane->Get_PlaneInfo()->vVertex2.x) //둔각이면...
					 {
						 vRightLength.z *= -1.f;
					 }
					 else
					 {
						 vRightLength.x *= -1.f;
					 }
					D3DXVec3Normalize(&vRightLength, &vRightLength);
					vReflec = vRightLength;
				}
				else
				{
					//여기는 01 윗변이 맞은거
					if (Plane->Get_PlaneInfo()->vVertex0.z == Plane->Get_PlaneInfo()->vVertex1.z) //0이면...
					{
						vUpLength.x = 0.f;
						vUpLength.z += 1.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex0.z > Plane->Get_PlaneInfo()->vVertex1.z) //둔각이면...
					{
						vUpLength.z *= -1.f;
					}
					else
					{
						vUpLength.x *= -1.f;
					}
					D3DXVec3Normalize(&vUpLength, &vUpLength);
					vReflec = vUpLength;
				}
				//pArrow->Reflect(vReflec, );
				return;
			}
		}

		// 왼쪽 아래
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex2,
			&Plane->Get_PlaneInfo()->vVertex3,
			&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
		{

			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}


			if (Plane->Get_PlaneInfo()->iOption == BLOCK || bIceCheck)
			{
				//타임백
				vBackPos = pArrow->Get_ColPos() - pArrow->Get_Trnasform()->m_vDir;

				//왼쪽아래삼각형
				//우선 3 - 2 버텍스를 이어주는 밑변
				vDownLength = Plane->Get_PlaneInfo()->vVertex2 - Plane->Get_PlaneInfo()->vVertex3;
				fDownLegth = D3DXVec3Length(&vDownLength);
				vTemp = vDownLength;
				D3DXVec3Normalize(&vTemp, &vTemp);
				fDownDot = D3DXVec3Dot(&vTemp, &g_vRight);

				//Back과 3의 거리
				vBackand3 = vBackPos - Plane->Get_PlaneInfo()->vVertex3;
				fBackand3 = D3DXVec3Length(&vBackand3);
				fBackand3 = fabs(fBackand3);

				//Back과 2의 거리
				vBackand2 = vBackPos - Plane->Get_PlaneInfo()->vVertex2;
				fBackand2 = D3DXVec3Length(&vBackand2);
				fBackand2 = fabs(fBackand2);

				fResult1 = (fBackand3 + fBackand2) - fDownLegth;


				//다음 0 - 3을 이어주는 왼쪽
				vLeftLength = Plane->Get_PlaneInfo()->vVertex3 - Plane->Get_PlaneInfo()->vVertex0;
				fLeftLegth = D3DXVec3Length(&vLeftLength);
				vTemp = vLeftLength;
				D3DXVec3Normalize(&vTemp, &vTemp);
				fLeftDot = D3DXVec3Dot(&vTemp, &g_vRight);


				//Back과 3의 거리
				vBackand3 = vBackPos - Plane->Get_PlaneInfo()->vVertex3;
				fBackand3 = D3DXVec3Length(&vBackand3);
				fBackand3 = fabs(fBackand1);

				//Back과 0의 거리
				vBackand0 = vBackPos - Plane->Get_PlaneInfo()->vVertex0;
				fBackand0 = D3DXVec3Length(&vBackand0);
				fBackand0 = fabs(fBackand0);

				 fResult2 = (fBackand0 + fBackand1) - fLeftLegth;

				if (fResult1 > fResult2)
				{
					//지금은
					//result2 즉... 0-3 왼쪽면이 처맞은거임.
					if (Plane->Get_PlaneInfo()->vVertex3.x == Plane->Get_PlaneInfo()->vVertex0.x) //0이면...
					{
						vLeftLength.x -= 1.f;
						vLeftLength.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex3.x < Plane->Get_PlaneInfo()->vVertex0.x) //둔각이면...
					{
						vLeftLength.z *= -1.f;

					}
					else
					{
						vLeftLength.x *= -1.f;
					}

					D3DXVec3Normalize(&vLeftLength, &vLeftLength);
					vReflec = vLeftLength;
				}
				else
				{
					//여기는 23 밑변이 처맞은거임.
					if (Plane->Get_PlaneInfo()->vVertex2.z == Plane->Get_PlaneInfo()->vVertex3.z) //0이면...
					{
						vDownLength.z -= 1.f;
						vDownLength.x = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex2.z > Plane->Get_PlaneInfo()->vVertex3.z) //둔각이면...
					{
						vDownLength.z *= -1.f;
					}
					else
					{
						vDownLength.x *= -1.f;
					}
					D3DXVec3Normalize(&vDownLength, &vDownLength);
					vReflec = vDownLength;
				}
				//pArrow->Reflect(vReflec);
				return;
			}
		}
	}
}

void CTerrain::CollisoinPlanetoArrowBack(CArrow * pArrow, CPlayer * pPlayer)
{
	if (!pArrow) return;
	if (!pArrow->GetValid()) return; // 화살이 유효상태가 아니면 리턴해야지 않니?

	float fTime = Engine::Get_TimeMgr()->GetTime();
	float fU;
	float fV;
	float fDist;

	//반사벡터
	VEC3 vReflec;

	D3DXVECTOR3 vBackPos;

	D3DXVECTOR3 vUpLength;
	D3DXVECTOR3 vDownLength;
	D3DXVECTOR3 vRightLength;
	D3DXVECTOR3 vLeftLength;

	D3DXVECTOR3 vTemp; //내적용 tempvector;
	float fLeftLegth = 0.f;
	float fDownLegth = 0.f;
	float fRighLegth = 0.f;
	float fUpLegth = 0.f;

	float fLeftDot = 0.f;
	float fDownDot = 0.f;
	float fRightDot = 0.f;
	float fUpDot = 0.f;

	D3DXVECTOR3 vBackand0;
	D3DXVECTOR3 vBackand1;
	D3DXVECTOR3 vBackand2;
	D3DXVECTOR3 vBackand3;

	float fBackand0 = 0.f;
	float fBackand1 = 0.f;
	float fBackand2 = 0.f;
	float fBackand3 = 0.f;

	float fResult1 = 0.f;
	float fResult2 = 0.f;

	D3DXMATRIX		matView = *m_pCameraObserver->GetView();
	D3DXMATRIX	matProj = *m_pCameraObserver->GetProj();
	D3DXVECTOR3 m_vRayPos = D3DXVECTOR3(0.f, 0.f, 0.f);

	// 월드 좌표로 변환
	D3DXMatrixInverse(&matView, 0, &matView);
	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matView);

	m_vRayPos = pPlayer->Get_Trnasform()->m_vPos;
	m_vRayPos.y = 1.f;

	D3DXVECTOR3 m_vRayDir = pArrow->Get_ColPos() - pArrow->Get_Trnasform()->m_vDir - m_vRayPos;
	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);


	//실험... 플레이어에서 쏴보자.

	//vright 벡터와 각 축을 내적해서... 양수 (예각)이면 x 축을 반전, 음수(둔각)이면 z축을 반전, 0이면 따로 예외처리

	//플레이어와...
	for (auto& Plane : *m_pPlaneList)
	{
		//오른쪽위
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex1,
			&Plane->Get_PlaneInfo()->vVertex2,
			&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
		{
			if (Plane->Get_PlaneInfo()->iOption == BLOCK)
			{
				//타임백
				vBackPos = pArrow->Get_ColPos() - pArrow->Get_Trnasform()->m_vDir;

				//오른쪽 위 삼각형이니... 길이를 구하자.
				//우선 0 - 1 버텍스를 이어주는 윗변

				vUpLength = Plane->Get_PlaneInfo()->vVertex1 - Plane->Get_PlaneInfo()->vVertex0;
				vTemp = vUpLength;
				fUpDot = D3DXVec3Dot(&vTemp, &g_vRight);
				D3DXVec3Normalize(&vTemp, &vTemp);
				fUpLegth = D3DXVec3Length(&vUpLength);

				//Back과 0의 거리
				vBackand0 = vBackPos - Plane->Get_PlaneInfo()->vVertex0;
				fBackand0 = D3DXVec3Length(&vBackand0);
				fBackand0 = fabs(fBackand0);

				//Back과 1의 거리
				vBackand1 = vBackPos - Plane->Get_PlaneInfo()->vVertex1;
				fBackand1 = D3DXVec3Length(&vBackand1);
				fBackand1 = fabs(fBackand1);

				fResult1 = (fBackand1 + fBackand0) - fUpLegth;


				//다음 1 - 2을 이어주는 오른쪽면
				vRightLength = Plane->Get_PlaneInfo()->vVertex1 - Plane->Get_PlaneInfo()->vVertex2;
				fRighLegth = D3DXVec3Length(&vRightLength);
				vTemp = vRightLength;
				D3DXVec3Normalize(&vTemp, &vTemp);
				fRightDot = D3DXVec3Dot(&vTemp, &g_vRight);

				//Back과 1의 거리
				vBackand1 = vBackPos - Plane->Get_PlaneInfo()->vVertex1;
				fBackand1 = D3DXVec3Length(&vBackand0);
				fBackand1 = fabs(fBackand1);

				//Back과 3의 거리
				vBackand2 = vBackPos - Plane->Get_PlaneInfo()->vVertex2;
				fBackand2 = D3DXVec3Length(&vBackand2);
				fBackand2 = fabs(fBackand2);

				fResult2 = (fBackand2 + fBackand1) - fRighLegth;

				if (fResult1 > fResult2)
				{
					//작은것이 선택되는거임.
					//지금은
					//여기는... 12 오른쪽면이 처 맞은거

					if (Plane->Get_PlaneInfo()->vVertex1.x == Plane->Get_PlaneInfo()->vVertex2.x) //0이면...
					{
						vRightLength.x += 1.f;
						vRightLength.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex1.x < Plane->Get_PlaneInfo()->vVertex2.x) //둔각이면...
					{
						vRightLength.z *= -1.f;
					}
					else
					{
						vRightLength.x *= -1.f;
					}
					D3DXVec3Normalize(&vRightLength, &vRightLength);
					vReflec = vRightLength;
				}
				else
				{
					//여기는 01 윗변이 맞은거
					if (Plane->Get_PlaneInfo()->vVertex0.z == Plane->Get_PlaneInfo()->vVertex1.z) //0이면...
					{
						vUpLength.x = 0.f;
						vUpLength.z += 1.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex0.z > Plane->Get_PlaneInfo()->vVertex1.z) //둔각이면...
					{
						vUpLength.z *= -1.f;
					}
					else
					{
						vUpLength.x *= -1.f;
					}
					D3DXVec3Normalize(&vUpLength, &vUpLength);
					vReflec = vUpLength;
				}
				//pArrow->Reflect(vReflec);
				return;
			}
		}

		// 왼쪽 아래
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex2,
			&Plane->Get_PlaneInfo()->vVertex3,
			&m_vRayPos, &(m_vRayDir), &fU, &fV, &fDist))
		{
			if (Plane->Get_PlaneInfo()->iOption == BLOCK)
			{
				//타임백
				vBackPos = pArrow->Get_ColPos() - pArrow->Get_Trnasform()->m_vDir;

				//왼쪽아래삼각형
				//우선 3 - 2 버텍스를 이어주는 밑변
				vDownLength = Plane->Get_PlaneInfo()->vVertex2 - Plane->Get_PlaneInfo()->vVertex3;
				fDownLegth = D3DXVec3Length(&vDownLength);
				vTemp = vDownLength;
				D3DXVec3Normalize(&vTemp, &vTemp);
				fDownDot = D3DXVec3Dot(&vTemp, &g_vRight);

				//Back과 3의 거리
				vBackand3 = vBackPos - Plane->Get_PlaneInfo()->vVertex3;
				fBackand3 = D3DXVec3Length(&vBackand3);
				fBackand3 = fabs(fBackand3);

				//Back과 2의 거리
				vBackand2 = vBackPos - Plane->Get_PlaneInfo()->vVertex2;
				fBackand2 = D3DXVec3Length(&vBackand2);
				fBackand2 = fabs(fBackand2);

				fResult1 = (fBackand3 + fBackand2) - fDownLegth;


				//다음 0 - 3을 이어주는 왼쪽
				vLeftLength = Plane->Get_PlaneInfo()->vVertex3 - Plane->Get_PlaneInfo()->vVertex0;
				fLeftLegth = D3DXVec3Length(&vLeftLength);
				vTemp = vLeftLength;
				D3DXVec3Normalize(&vTemp, &vTemp);
				fLeftDot = D3DXVec3Dot(&vTemp, &g_vRight);


				//Back과 3의 거리
				vBackand3 = vBackPos - Plane->Get_PlaneInfo()->vVertex3;
				fBackand3 = D3DXVec3Length(&vBackand3);
				fBackand3 = fabs(fBackand1);

				//Back과 0의 거리
				vBackand0 = vBackPos - Plane->Get_PlaneInfo()->vVertex0;
				fBackand0 = D3DXVec3Length(&vBackand0);
				fBackand0 = fabs(fBackand0);

				fResult2 = (fBackand0 + fBackand1) - fLeftLegth;

				if (fResult1 > fResult2)
				{
					//지금은
					//result2 즉... 0-3 왼쪽면이 처맞은거임.
					//여기는 01 윗변이 맞은거
					if (Plane->Get_PlaneInfo()->vVertex3.x == Plane->Get_PlaneInfo()->vVertex0.x) //0이면...
					{
						vLeftLength.x -= 1.f;
						vLeftLength.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex3.x < Plane->Get_PlaneInfo()->vVertex0.x) //둔각이면...
					{
						vLeftLength.z *= -1.f;

					}
					else
					{
						vLeftLength.x *= -1.f;
					}

					D3DXVec3Normalize(&vLeftLength, &vLeftLength);
					vReflec = vLeftLength;
				}
				else
				{
					//여기는 23 밑변이 처맞은거임.
					if (Plane->Get_PlaneInfo()->vVertex2.z == Plane->Get_PlaneInfo()->vVertex3.z) //0이면...
					{
						vDownLength.z -= 1.f;
						vDownLength.x = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex2.z > Plane->Get_PlaneInfo()->vVertex3.z) //둔각이면...
					{
						vDownLength.z *= -1.f;
					}
					else
					{
						vDownLength.x *= -1.f;
					}
					D3DXVec3Normalize(&vDownLength, &vDownLength);
					vReflec = vDownLength;
				}
				//pArrow->Reflect(vReflec);
				return;
			}
		}
	}
}

void CTerrain::CollisoinPlanetoArrowBackAgain(CArrow * pArrow, CPlayer * pPlayer)
{
	if (!pArrow) return;
	if (!pArrow->GetValid()) return; // 화살이 유효상태가 아니면 리턴해야지 않니?

	float fTime = Engine::Get_TimeMgr()->GetTime();
	float fU;
	float fV;
	float fDist;

	//반사벡터
	VEC3 vReflec;
	VEC3 vDest;
	VEC3 vStart;


	VEC3 vVTXLine;

	//교점
	VEC3 vPnt[ENDLINE];

	D3DXMATRIX		matView = *m_pCameraObserver->GetView();
	D3DXMATRIX	matProj = *m_pCameraObserver->GetProj();
	D3DXVECTOR3 m_vRayPos = D3DXVECTOR3(0.f, 0.f, 0.f);


	// 월드 좌표로 변환
	D3DXMatrixInverse(&matView, 0, &matView);
	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matView);
	D3DXVECTOR3 m_vRayDir = pArrow->Get_ColPos() - m_vRayPos;
	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);


	for (auto& Plane : *m_pPlaneList)
	{
		//Plane->Get_PlaneInfo()->vVertex0 = index + vtxcntx + 1
		//Plane->Get_PlaneInfo()->vVertex1 = index + 1
		//Plane->Get_PlaneInfo()->vVertex2 = index + vtxcntx

		// 오른쪽 위
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex1,
			&Plane->Get_PlaneInfo()->vVertex2,
			&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
		{

			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}

			if (Plane->Get_PlaneInfo()->iOption == 0 || bIceCheck)
			{

				if (pArrow->Get_Recovery())
				{
					pArrow->Set_CollsionPlane();
					pArrow->StopRecovery(fTime);
					return;
				}

					pPlayer->Get_Trnasform()->m_vPos;
					vDest = pArrow->Get_Trnasform()->m_vPos;
					vDest = pArrow->Get_ColPos();

					//출발지부터 목적지까지의 벡터를 구하고..
					vStart = pArrow->Get_StartVector();
					//vStartToDest = vDest - pArrow->Get_StartVector();
					//이 직선을 방정식으로 만들면..
					//y = ax + b
					float 기울기 = (vDest.z - vStart.z) / (vDest.x - vStart.x);

					//4개의 직선을 구하고.

					//vStart = pArrow->Get_Trnasform()->m_vPos - pArrow->Get_Trnasform()->m_vDir * 3.f;

					vPnt[UPLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex0, Plane->Get_PlaneInfo()->vVertex1);
					vPnt[RIGHTLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex1, Plane->Get_PlaneInfo()->vVertex2);
					vPnt[DOWNLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex2, Plane->Get_PlaneInfo()->vVertex3);
					vPnt[LEFTLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex0, Plane->Get_PlaneInfo()->vVertex3);


					D3DXVECTOR3 vLength;
					float fLength = 0.f;
					float fMinLength = 1000.f;

					int iCheck = -1;
					for (int i = 0; i < ENDLINE; ++i)
					{
						vLength = vPnt[i] - vStart;
						fLength = D3DXVec3Length(&vLength);
						fLength = fabs(fLength);

						if (fLength < fMinLength)
						{
							fMinLength = fLength;
							iCheck = i;
						}
					}

					if (iCheck == -1)
					{
						pArrow->StopArrow();
						return;
					}

					switch (iCheck)
					{
					case UPLINE:
						//여기는 01 윗변이 맞은거
						vVTXLine = Plane->Get_PlaneInfo()->vVertex0 - Plane->Get_PlaneInfo()->vVertex1;
						if (Plane->Get_PlaneInfo()->vVertex0.z == Plane->Get_PlaneInfo()->vVertex1.z) //0이면...
						{
							vVTXLine.x = 0.f;
							vVTXLine.z += 1.f;
						}
						else if (Plane->Get_PlaneInfo()->vVertex0.z > Plane->Get_PlaneInfo()->vVertex1.z) //둔각이면...
						{
							vVTXLine.z *= -1.f;
						}
						else
						{
							vVTXLine.x *= -1.f;
						}
						D3DXVec3Normalize(&vVTXLine, &vVTXLine);
						vReflec = vVTXLine;
						break;

					case DOWNLINE:
						vVTXLine = Plane->Get_PlaneInfo()->vVertex2 - Plane->Get_PlaneInfo()->vVertex3;
						if (Plane->Get_PlaneInfo()->vVertex2.z == Plane->Get_PlaneInfo()->vVertex3.z) //0이면...
						{
							vVTXLine.z -= 1.f;
							vVTXLine.x = 0.f;
						}
						else if (Plane->Get_PlaneInfo()->vVertex2.z > Plane->Get_PlaneInfo()->vVertex3.z) //둔각이면...
						{
							vVTXLine.z *= -1.f;
						}
						else
						{
							vVTXLine.x *= -1.f;
						}
						D3DXVec3Normalize(&vVTXLine, &vVTXLine);
						vReflec = vVTXLine;
						break;

					case LEFTLINE:
						vVTXLine = Plane->Get_PlaneInfo()->vVertex3 - Plane->Get_PlaneInfo()->vVertex0;
						if (Plane->Get_PlaneInfo()->vVertex3.x == Plane->Get_PlaneInfo()->vVertex0.x) //0이면...
						{
							vVTXLine.x -= 1.f;
							vVTXLine.z = 0.f;
						}
						else if (Plane->Get_PlaneInfo()->vVertex3.x < Plane->Get_PlaneInfo()->vVertex0.x) //둔각이면...
						{
							vVTXLine.z *= -1.f;

						}
						else
						{
							vVTXLine.x *= -1.f;
						}

						D3DXVec3Normalize(&vVTXLine, &vVTXLine);
						vReflec = vVTXLine;
						break;

					case RIGHTLINE:
						vVTXLine = Plane->Get_PlaneInfo()->vVertex1 - Plane->Get_PlaneInfo()->vVertex2;
						if (Plane->Get_PlaneInfo()->vVertex1.x == Plane->Get_PlaneInfo()->vVertex2.x) //0이면...
						{
							vVTXLine.x += 1.f;
							vVTXLine.z = 0.f;
						}
						else if (Plane->Get_PlaneInfo()->vVertex1.x < Plane->Get_PlaneInfo()->vVertex2.x) //둔각이면...
						{
							vVTXLine.z *= -1.f;
						}
						else
						{
							vVTXLine.x *= -1.f;
						}
						D3DXVec3Normalize(&vVTXLine, &vVTXLine);
						vReflec = vVTXLine;
						break;
					}
					pArrow->Reflect(vReflec, vPnt[iCheck]);
					return;
				}
				else
					return;
			}


			// 왼쪽 아래
			if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex3,
				&Plane->Get_PlaneInfo()->vVertex0,
				&Plane->Get_PlaneInfo()->vVertex2,
				&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
			{
				bool bIceCheck = FALSE;

				if (g_BattleMode == TRUE)
				{
					if (Plane->Get_PlaneInfo()->iOption == ICE)
					{
						bIceCheck = TRUE;
					}
				}
				if (Plane->Get_PlaneInfo()->iOption == 0 || bIceCheck)
				{
				//우선 회수중인지를 검사해봐야지...
				if(pArrow->Get_Recovery())
				{
					pArrow->Set_CollsionPlane();
					pArrow->StopRecovery(fTime);
					return;
				}
				

				pPlayer->Get_Trnasform()->m_vPos;
				vDest = pArrow->Get_ColPos();
				//출발지부터 목적지까지의 벡터를 구하고..
				vStart = pArrow->Get_StartVector();
				//vStartToDest = vDest - pArrow->Get_StartVector();
				//이 직선을 방정식으로 만들면..
				//y = ax + b
				float 기울기 = (vDest.z - vStart.z) / (vDest.x - vStart.x);

				//4개의 직선을 구하고.


				vPnt[UPLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex0, Plane->Get_PlaneInfo()->vVertex1);
				vPnt[RIGHTLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex1, Plane->Get_PlaneInfo()->vVertex2);
				vPnt[DOWNLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex2, Plane->Get_PlaneInfo()->vVertex3);
				vPnt[LEFTLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex0, Plane->Get_PlaneInfo()->vVertex3);


				D3DXVECTOR3 vLength;
				float fLength = 0.f;
				float fMinLength = 1000.f;

				int iCheck = -1;
				for (int i = 0; i < ENDLINE; ++i)
				{
					vLength = vPnt[i] - vStart;
					fLength = D3DXVec3Length(&vLength);
					fLength = fabs(fLength);

					if (fLength < fMinLength)
					{
						fMinLength = fLength;
						iCheck = i;
					}
				}

				if (iCheck == -1)
				{
					pArrow->StopArrow();
					return;

				}

				switch (iCheck)
				{
				case UPLINE:
					//여기는 01 윗변이 맞은거
					vVTXLine = Plane->Get_PlaneInfo()->vVertex0 - Plane->Get_PlaneInfo()->vVertex1;
					if (Plane->Get_PlaneInfo()->vVertex0.z == Plane->Get_PlaneInfo()->vVertex1.z) //0이면...
					{
						vVTXLine.x = 0.f;
						vVTXLine.z += 1.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex0.z > Plane->Get_PlaneInfo()->vVertex1.z) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case DOWNLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex2 - Plane->Get_PlaneInfo()->vVertex3;
					if (Plane->Get_PlaneInfo()->vVertex2.z == Plane->Get_PlaneInfo()->vVertex3.z) //0이면...
					{
						vVTXLine.z -= 1.f;
						vVTXLine.x = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex2.z > Plane->Get_PlaneInfo()->vVertex3.z) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case LEFTLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex3 - Plane->Get_PlaneInfo()->vVertex0;
					if (Plane->Get_PlaneInfo()->vVertex3.x == Plane->Get_PlaneInfo()->vVertex0.x) //0이면...
					{
						vVTXLine.x -= 1.f;
						vVTXLine.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex3.x < Plane->Get_PlaneInfo()->vVertex0.x) //둔각이면...
					{
						vVTXLine.z *= -1.f;

					}
					else
					{
						vVTXLine.x *= -1.f;
					}

					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case RIGHTLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex1 - Plane->Get_PlaneInfo()->vVertex2;
					if (Plane->Get_PlaneInfo()->vVertex1.x == Plane->Get_PlaneInfo()->vVertex2.x) //0이면...
					{
						vVTXLine.x += 1.f;
						vVTXLine.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex1.x < Plane->Get_PlaneInfo()->vVertex2.x) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;
				}
				pArrow->Reflect(vReflec, vPnt[iCheck]);
				return;
			}
			else
				return;

		}


	}

}

D3DXVECTOR3 CTerrain::CheckLine(D3DXVECTOR3 Line1_Start, D3DXVECTOR3 Line1_End, D3DXVECTOR3 Line2_Start, D3DXVECTOR3 Line2_End)
{
	// Given two lines, the first line is p1-p2
	//the second line is p3-p4

	float fTemp = 1.f;

	
	float x1 = Line1_Start.x, x2 = Line1_End.x, x3 = Line2_Start.x, x4 = Line2_End.x;
	float y1 = Line1_Start.z, y2 = Line1_End.z, y3 = Line2_Start.z, y4 = Line2_End.z;

	float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
	// If d is zero, there is no intersection
	if (d == 0) return D3DXVECTOR3(9999.f, 9999.f, 9999.f);

	// Get the x and y
	float pre = (x1*y2 - y1*x2), post = (x3*y4 - y3*x4);
	float x = (pre * (x3 - x4) - (x1 - x2) * post) / d;
	float y = (pre * (y3 - y4) - (y1 - y2) * post) / d;

	// Check if the x and y coordinates are within both lines
	if (x+fTemp < min(x1, x2) || x-fTemp > max(x1, x2) ||
		x+fTemp < min(x3, x4) || x-fTemp > max(x3, x4)) return D3DXVECTOR3(9999.f, 9999.f, 9999.f);
	if (y+fTemp < min(y1, y2) || y-fTemp > max(y1, y2) ||
		y+fTemp < min(y3, y4) || y-fTemp > max(y3, y4)) return D3DXVECTOR3(9999.f, 9999.f, 9999.f);

	//sprintf(string, "CrossX: %d ", x);
	//drawText(string, 10, 60);

	//sprintf(string, "CrossY: %d ", y);
	//drawText(string, 10, 50);

	// Return the point of intersection
	D3DXVECTOR3 ret = { x, 1.f, y };
	//ret->x = x;
	//ret->y = y;
	return ret;



	//float x1, y1, x2, y2, x3, y3, x4, y4;

	//x1 = Line1_Start.x;
	//y1 = Line1_Start.z;

	//x2 = Line1_End.x;
	//y2 = Line1_End.z;

	//x3 = Line2_Start.x;
	//y3 = Line2_Start.z;

	//x4 = Line2_End.x;
	//y4 = Line2_End.z;


	//float px, py;

	//px = ((x1 * y2 - y1 *x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4) )
	//	/ ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4));

	//py = ( (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2)* (x3*y4 - y3* x4)) /  ( (x1 - x2) * (y3-y4) - (y1-y2) * (x3-x4));

	//return D3DXVECTOR3(px, 1.f, py);
}

D3DXVECTOR3 CTerrain::GetNormalVector(D3DXVECTOR3 vFirst, D3DXVECTOR3 vSecond, D3DXVECTOR3 vCrossPnt)
{
	//법선벡터 구해야지 짜샤


	return D3DXVECTOR3();
}


void CTerrain::SetTransform(void)
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
		//Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pmatView);
		//
		//if (m_pConvertVertex[i].vPos.z < 1.f)
		//	m_pConvertVertex[i].vPos.z = 1.f;
		//
		//Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pmatProj);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);
}

CTerrain* CTerrain::Create(LPDIRECT3DDEVICE9 pGraphicDev, MAPINFO* pMapInfo)
{
	CTerrain*	pInstance = new CTerrain(pGraphicDev);

	if (FAILED(pInstance->Initialize(pMapInfo)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}





void CTerrain::CollisoinPlanetoIceCube(CIceCube * pIceCube, CPlayer * pPlayer)
{
	if (!pIceCube) 
		return;


	float fTime = Engine::Get_TimeMgr()->GetTime();
	float fU;
	float fV;
	float fDist;

	//반사벡터
	VEC3 vReflec;
	VEC3 vDest;
	VEC3 vStart;


	VEC3 vVTXLine;

	//교점
	VEC3 vPnt[ENDLINE];

	D3DXMATRIX		matView = *m_pCameraObserver->GetView();
	D3DXMATRIX		matProj = *m_pCameraObserver->GetProj();
	D3DXVECTOR3		m_vRayPos = D3DXVECTOR3(0.f, 0.f, 0.f);


	// 월드 좌표로 변환
	D3DXMatrixInverse(&matView, 0, &matView);
	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matView);
	D3DXVECTOR3 m_vRayDir = pIceCube->GetInfo()->m_vPos - m_vRayPos;
	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);


	for (auto& Plane : *m_pPlaneList)
	{

		// 오른쪽 위
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex1,
			&Plane->Get_PlaneInfo()->vVertex2,
			&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
		{

			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}

			if (Plane->Get_PlaneInfo()->iOption == 0 || bIceCheck)
			{

		

				pPlayer->Get_Trnasform()->m_vPos;
				vDest = pIceCube->GetInfo()->m_vPos;

				//출발지부터 목적지까지의 벡터를 구하고..
				vStart = pIceCube->Get_StartVector();//처음 시작 

				//이 직선을 방정식으로 만들면..
				//y = ax + b
				float 기울기 = (vDest.z - vStart.z) / (vDest.x - vStart.x);

				//4개의 직선을 구하고.

				//vStart = pArrow->Get_Trnasform()->m_vPos - pArrow->Get_Trnasform()->m_vDir * 3.f;

				vPnt[UPLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex0, Plane->Get_PlaneInfo()->vVertex1);
				vPnt[RIGHTLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex1, Plane->Get_PlaneInfo()->vVertex2);
				vPnt[DOWNLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex2, Plane->Get_PlaneInfo()->vVertex3);
				vPnt[LEFTLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex0, Plane->Get_PlaneInfo()->vVertex3);


				D3DXVECTOR3 vLength;
				float fLength = 0.f;
				float fMinLength = 1000.f;

				int iCheck = -1;
				for (int i = 0; i < ENDLINE; ++i)
				{
					vLength = vPnt[i] - vStart;
					fLength = D3DXVec3Length(&vLength);
					fLength = fabs(fLength);

					if (fLength < fMinLength)
					{
						fMinLength = fLength;
						iCheck = i;
					}
				}

				if (iCheck == -1)
				{
					return;
				}

				switch (iCheck)
				{
				case UPLINE:
					//여기는 01 윗변이 맞은거
					vVTXLine = Plane->Get_PlaneInfo()->vVertex0 - Plane->Get_PlaneInfo()->vVertex1;
					if (Plane->Get_PlaneInfo()->vVertex0.z == Plane->Get_PlaneInfo()->vVertex1.z) //0이면...
					{
						vVTXLine.x = 0.f;
						vVTXLine.z += 1.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex0.z > Plane->Get_PlaneInfo()->vVertex1.z) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case DOWNLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex2 - Plane->Get_PlaneInfo()->vVertex3;
					if (Plane->Get_PlaneInfo()->vVertex2.z == Plane->Get_PlaneInfo()->vVertex3.z) //0이면...
					{
						vVTXLine.z -= 1.f;
						vVTXLine.x = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex2.z > Plane->Get_PlaneInfo()->vVertex3.z) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case LEFTLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex3 - Plane->Get_PlaneInfo()->vVertex0;
					if (Plane->Get_PlaneInfo()->vVertex3.x == Plane->Get_PlaneInfo()->vVertex0.x) //0이면...
					{
						vVTXLine.x -= 1.f;
						vVTXLine.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex3.x < Plane->Get_PlaneInfo()->vVertex0.x) //둔각이면...
					{
						vVTXLine.z *= -1.f;

					}
					else
					{
						vVTXLine.x *= -1.f;
					}

					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case RIGHTLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex1 - Plane->Get_PlaneInfo()->vVertex2;
					if (Plane->Get_PlaneInfo()->vVertex1.x == Plane->Get_PlaneInfo()->vVertex2.x) //0이면...
					{
						vVTXLine.x += 1.f;
						vVTXLine.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex1.x < Plane->Get_PlaneInfo()->vVertex2.x) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;
				}
				pIceCube->Reflect(vReflec, vPnt[iCheck]);
				return;
			}
			else
				return;
		}


		// 왼쪽 아래
		if (D3DXIntersectTri(&Plane->Get_PlaneInfo()->vVertex3,
			&Plane->Get_PlaneInfo()->vVertex0,
			&Plane->Get_PlaneInfo()->vVertex2,
			&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
		{
			bool bIceCheck = FALSE;

			if (g_BattleMode == TRUE)
			{
				if (Plane->Get_PlaneInfo()->iOption == ICE)
				{
					bIceCheck = TRUE;
				}
			}
			if (Plane->Get_PlaneInfo()->iOption == 0 || bIceCheck)
			{
				
				pPlayer->Get_Trnasform()->m_vPos;
				vDest = pIceCube->GetInfo()->m_vPos;

				//출발지부터 목적지까지의 벡터를 구하고..
				vStart = pIceCube->Get_StartVector();//처음 시작 
				//vStartToDest = vDest - pArrow->Get_StartVector();
				//이 직선을 방정식으로 만들면..
				//y = ax + b
				float 기울기 = (vDest.z - vStart.z) / (vDest.x - vStart.x);

				//4개의 직선을 구하고.


				vPnt[UPLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex0, Plane->Get_PlaneInfo()->vVertex1);
				vPnt[RIGHTLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex1, Plane->Get_PlaneInfo()->vVertex2);
				vPnt[DOWNLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex2, Plane->Get_PlaneInfo()->vVertex3);
				vPnt[LEFTLINE] = CheckLine(vStart, vDest, Plane->Get_PlaneInfo()->vVertex0, Plane->Get_PlaneInfo()->vVertex3);


				D3DXVECTOR3 vLength;
				float fLength = 0.f;
				float fMinLength = 1000.f;

				int iCheck = -1;
				for (int i = 0; i < ENDLINE; ++i)
				{
					vLength = vPnt[i] - vStart;
					fLength = D3DXVec3Length(&vLength);
					fLength = fabs(fLength);

					if (fLength < fMinLength)
					{
						fMinLength = fLength;
						iCheck = i;
					}
				}

				if (iCheck == -1)
				{
					return;
				}
				switch (iCheck)
				{
				case UPLINE:
					//여기는 01 윗변이 맞은거
					vVTXLine = Plane->Get_PlaneInfo()->vVertex0 - Plane->Get_PlaneInfo()->vVertex1;
					if (Plane->Get_PlaneInfo()->vVertex0.z == Plane->Get_PlaneInfo()->vVertex1.z) //0이면...
					{
						vVTXLine.x = 0.f;
						vVTXLine.z += 1.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex0.z > Plane->Get_PlaneInfo()->vVertex1.z) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case DOWNLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex2 - Plane->Get_PlaneInfo()->vVertex3;
					if (Plane->Get_PlaneInfo()->vVertex2.z == Plane->Get_PlaneInfo()->vVertex3.z) //0이면...
					{
						vVTXLine.z -= 1.f;
						vVTXLine.x = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex2.z > Plane->Get_PlaneInfo()->vVertex3.z) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case LEFTLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex3 - Plane->Get_PlaneInfo()->vVertex0;
					if (Plane->Get_PlaneInfo()->vVertex3.x == Plane->Get_PlaneInfo()->vVertex0.x) //0이면...
					{
						vVTXLine.x -= 1.f;
						vVTXLine.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex3.x < Plane->Get_PlaneInfo()->vVertex0.x) //둔각이면...
					{
						vVTXLine.z *= -1.f;

					}
					else
					{
						vVTXLine.x *= -1.f;
					}

					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;

				case RIGHTLINE:
					vVTXLine = Plane->Get_PlaneInfo()->vVertex1 - Plane->Get_PlaneInfo()->vVertex2;
					if (Plane->Get_PlaneInfo()->vVertex1.x == Plane->Get_PlaneInfo()->vVertex2.x) //0이면...
					{
						vVTXLine.x += 1.f;
						vVTXLine.z = 0.f;
					}
					else if (Plane->Get_PlaneInfo()->vVertex1.x < Plane->Get_PlaneInfo()->vVertex2.x) //둔각이면...
					{
						vVTXLine.z *= -1.f;
					}
					else
					{
						vVTXLine.x *= -1.f;
					}
					D3DXVec3Normalize(&vVTXLine, &vVTXLine);
					vReflec = vVTXLine;
					break;
				}
				pIceCube->Reflect(vReflec, vPnt[iCheck]);
				return;
			}
			else
				return;

		}
	}
}