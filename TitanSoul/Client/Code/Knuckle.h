#ifndef CKnuckle_h__
#define CKnuckle_h__

/*!
* \class CKnuckle_Part
*
* \간략정보 철퇴 몬스터의 본체
*
* \상세정보
*
* \작성자 윤유석
*
* \date 1월 12 2019
*
*/

#include "GameObject.h"

namespace Engine
{
	class CTimeMgr;
}

class CPlayerObserver;
class CKnuckle_Part;
class CKnuckle_Body;
class CKnuckle_Mace;
class CKnuckle_Chain;
class CSphereColBox;
class CKnuckle : public Engine::CGameObject
{
	enum KNUCKLE_OBJ { SHADOW, CHAIN, MACE, BODY, OBJ_END };	// 렌더링 순서
	enum PART_DIR { P_LEFT, P_RIGHT, P_END };
	enum SHADOW_SORT { SH_BODY, SH_LMACE, SH_RMACE, SH_END };

	enum KNUCKLE_STATE { SLEEP, WAKE_UP, CHECKBACK, ATTACK, BODY_ROT, ATTACK_ROT, ATTACK_JUMP, S_END };


	enum ROTATION_DIR { NO, RD_LEFT, RD_RIGHT };


	enum BODY_DIR { DD, DL, DR, LL, RR, UL, UR, UU, D_END };

	const unsigned int	m_iMaxChainCount = 9;					// 오른팔, 왼팔 사슬 각각해서 18개임
	const float			m_iMaxLength = 13.f;

private:
	explicit CKnuckle(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CKnuckle();

private:
	HRESULT		AddComponent();
	HRESULT		CreateObj();

	virtual HRESULT	Initialize(const VEC3& vPos);
	virtual void	Release();

public:
	virtual void	Update();
	virtual void	Render();

public:
	int						GetState()		{ return m_eState; }
	Engine::CTransform*		GetInfo()		{ return m_pInfo; }
	const float&			GetRadius()		{ return m_fRadius; }
	const VEC3&				GetPos();
	const float&			GetWeakRadius() { return m_fWeakRadius; }

	void					SetState(int iState);
	const VEC3&				GetDiePos() { return m_vDIePos; }
	void					SetDie(const bool& bDie) { m_bDie = bDie; }
	bool					GetJumpEnd() { return m_bJumpEnd; }
	void					SetInvincibility(const bool& bInvi) { m_bInvincibility = bInvi; }

	bool					GetInvincibility() { return m_bInvincibility;	}

	VEC3					GetMacePos(const int& iIndex);
	float					GetMaceRadius(const int& iIndex);

	bool					GetAttackEnd()	{ return m_bAttackEnd; }
	
private:
	void					HehavioFunc();
	void					PlayingFunc();
	void					HehavioCheckEnd();

	void					CheckInvici();

	float					GetDgreeAngle(const VEC3& vTarget, const VEC3& vPos, const bool& bYResst);
	void					SetMinAngle_RotationDir();
	void					SetDirScene();
	void					SetMacePos();

private:
	const float				m_fRadius = 1.6f;
	const float				m_fWeakRadius = 0.7f;

	const float&			m_fBodyShadow = 2.f;
	const float&			m_fMaceShadow = 1.7f;
	const int&				m_iMaxAccCount = 3;

	const float&			m_fMinAngle = 22.5f;
	const float&			m_fRotationCorr = 3.5f;

	const float&			m_fWalkUpTime = 0.4f;
	const float&			m_fCheckBackTime = 0.1f;
	const float&			m_fAttackTime = 0.5f;
	const float&			m_fBody_Rat = 0.8f;
	const float&			m_fRotationTime = 3.f;
	const float&			m_fJumpTime = 1.5f;

private:
	Engine::CTimeMgr*		m_pTimeMgr;

	CKnuckle_Body*			m_pBody;
	CKnuckle_Mace*			m_pMace[P_END];
	CKnuckle_Chain*			m_pChain[2][9];

	KNUCKLE_STATE			m_eState = SLEEP;
	KNUCKLE_STATE			m_ePreState = SLEEP;

	CPlayerObserver*		m_pPlayerObserver;
	CSphereColBox*			m_pShpereBox;
	CSphereColBox*			m_pWeakShpereBox;
	CSphereColBox*			m_pMaceShpereBox[2];

	PART_DIR				m_eAttackDir = P_RIGHT;
	ROTATION_DIR			m_eRotationDir = NO;
	BODY_DIR				m_eBodyDir = DD;

	vector<list<CKnuckle_Part*>>		m_vecKnucklePart;

	bool					m_bPlaying = false;
	float					m_fBehavioTime = 0.f;
	float					m_fEndTime = 0.f;
	int						m_iAccCount = 0;

	VEC3					m_vTargetPos;
	float					m_fLookAngle = 179.f;			// 내가 보고 있는 밑에 각도
	float					m_fTargetAngle = 0.f;			// 타겟이 있는 각도
	float					m_fMinAngleSave = 0.f;
	bool					m_bRotationCorre = false;

	D3DXVECTOR3				m_vMacePos[P_END];
	D3DXVECTOR3				m_vChainPos[P_END];
	D3DXVECTOR3				m_vDIePos;

	D3DXMATRIX				m_matDie;
	bool					m_bDie = false;
	bool					m_bJumpEnd = false;
	bool					m_bAttackEnd = false;

	float					m_fInvincibility = 0.f;			// 무적시간
	bool					m_bInvincibility = false;

public:
	static	CKnuckle* Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos);
};

#endif // !CKnuckle_h__