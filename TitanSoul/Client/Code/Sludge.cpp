#include "stdafx.h"	
#include "Sludge.h"

// Engine
#include "Export_Function.h"
#include "Transform.h"

// Client
#include "Include.h"

// Obj
#include "SludgePart.h"
#include "Sludge_Body.h"
#include "Sludge_Heart.h"
#include "Sludge_Shadow.h"

#include "Mucus.h"
#include "Trace.h"

#include "PlayerObserver.h"
#include "SphereColBox.h"
#include "StaticCamera2D.h"

bool CSludge::m_sbSludgeDie = false;
list<CMucus*> CSludge::m_MucusLst = list<CMucus*>();
list<CTrace*> CSludge::m_TraceLst = list<CTrace*>();

CSludge::CSludge(PDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject	= Engine::Get_InfoSubject();
	m_pManagement	= Engine::Get_Management();
	m_pTimeMgr		= Engine::Get_TimeMgr();
}

CSludge::~CSludge()
{
	Release();
}

HRESULT CSludge::Initialize(const bool& bTrue, const SL_SIZE& eSize, 
							const D3DXVECTOR3& vPos, Engine::CLayer* pUpdateLayer, 
							Engine::CCamera* pCamera, Engine::CLayer* pRenderLayer, 
							list<CSludge*>* pSludgeLst)
{
	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_bTrue = bTrue;
	m_eSize = eSize;

	m_pSludgeList = pSludgeLst;
	m_pSludgeList->emplace_back(this);						// 자기 자신을 추가해줌

	m_pUpdateLayer = pUpdateLayer;
	m_pRenderLayer = pRenderLayer;
	m_pUpdateLayer->AddObject(L"Boss", this);
	m_pRenderLayer->AddObject(L"Boss", this);

	m_pCamera = dynamic_cast<CStaticCamera2D*>(pCamera);

	m_fSpeed = 1.5f;

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos = vPos;

	FAILED_CHECK_RETURN(CreateObj(), E_FAIL);

	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, float(m_eSize), true);

	if (m_eSize != SL_BIG)
	{
		m_eState = SLS_SPLIT;
		m_bHit = true;
	}
	
	SetRandomDir();


	return S_OK;
}

void CSludge::Release()
{
	Engine::Safe_Delete(m_pSphereBox);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	for (int i = 0; i < SLO_END; ++i)
		Engine::Safe_Delete(m_pPart[i]);

	m_pUpdateLayer = nullptr;
	m_pRenderLayer = nullptr;
}

HRESULT CSludge::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CSludge::CreateObj()
{
	m_pPart[SHADOW] = CSludge_Shadow::Create(m_pGraphicDev, m_pInfo->m_vPos, m_eSize);

	if (m_bTrue)
	{
		if(m_eSize == SL_VERYSMALL)
			m_pPart[HEART] = CSludge_Heart::Create(m_pGraphicDev, m_pInfo->m_vPos, m_eSize, true);
		else
			m_pPart[HEART] = CSludge_Heart::Create(m_pGraphicDev, m_pInfo->m_vPos, m_eSize, false);
	}
	else
		m_pPart[HEART] = nullptr;
	
	m_pPart[BODY] = CSludge_Body::Create(m_pGraphicDev, m_pInfo->m_vPos, m_eSize);

	return S_OK;
}

void CSludge::Update()
{
	if (m_eSize == SL_END)
		return;
	
	m_pTargetTrasform = m_pPlayerObserver->GetPlayerTransform();

	BehavioFunc();
	CGameObject::Update();
	BehavioEndFunc();
	HitTimeCounting();
}

void CSludge::Render()
{
	for (int i = 0; i < SLO_END; ++i)
	{
		if (m_pPart[i] != nullptr)
			m_pPart[i]->Render();
	}
}

void CSludge::SplitSludge()
{
	if (m_eSize == SL_END)
		return;

	m_eState = SLS_SPLIT;
	m_fBehavioTime = 0.f;
	m_bHit = true;

	SetRandomDir();
	if (m_bTrue)							// 진짜임
	{
		if (m_eSize == SL_VERYSMALL)		// 가장 작음 리턴
		{
			Engine::Safe_Delete(m_pPart[BODY]);		// 몸통을 지워버림
			m_eSize = SL_HEART;
			m_pSphereBox->SetRadius(float(SL_VERYSMALL));

			dynamic_cast<CSludge_Heart*>(m_pPart[HEART])->SetSolo();
			return;
		}
		else if (m_eSize == SL_HEART)
		{
			m_eSize = SL_END;
			dynamic_cast<CSludge_Heart*>(m_pPart[HEART])->SetDie();
			m_sbSludgeDie = true;
			m_pInfo->m_vPos.y = 0.1f;
			return;
		}
		SplitResizeAndCreate();
	}
	else
	{
		if (m_eSize == SL_VERYSMALL)		// 진짜가 아닌데 가장 작음 리턴
			return;

		SplitResizeAndCreate();
	}
}

const float& CSludge::GetBodyY()
{
	if (m_pPart[BODY] != nullptr)
		return m_pPart[BODY]->GetY();
	return m_pInfo->m_vPos.y;
}

void CSludge::BehavioFunc()
{
	const float& fDeltaTime = m_pTimeMgr->GetTime();
	m_fBehavioTime += fDeltaTime;

	m_BehavioEnd = false;

	for (int i = 0; i < SLO_END; ++i)
	{
		if (m_pPart[i] != nullptr)
		{
			m_pPart[i]->SetTargetPos(m_pTargetTrasform->m_vPos);
			switch (m_eState)
			{
			case SL_IDLE:
				m_BehavioEnd = m_pPart[i]->IdleFunc(fDeltaTime, m_fBehavioTime, m_fUpDownTime);
				break;
			case SLS_JUMP:
				SludgeMove(fDeltaTime);
				m_pPart[i]->SetSynchPosXZ(m_pInfo->m_vPos.x, m_pInfo->m_vPos.z);// 좌표동기화
				m_BehavioEnd = m_pPart[i]->JumpFunc(fDeltaTime, m_fBehavioTime, m_fMaxJumpTime);
				break;
			case SLS_FALL:
				SludgeMove(fDeltaTime);
				m_pPart[i]->SetSynchPosXZ(m_pInfo->m_vPos.x, m_pInfo->m_vPos.z);
				m_BehavioEnd = m_pPart[i]->FallFunc(fDeltaTime, m_fBehavioTime, m_fMaxFallTime);
				break;
			case SLS_SPLIT:
				SludgeMove(fDeltaTime);
				m_pPart[i]->SetSynchPosXZ(m_pInfo->m_vPos.x, m_pInfo->m_vPos.z);
				if(m_bSplitUp)								// true 점프
					m_BehavioEnd = m_pPart[i]->SplitFunc(fDeltaTime, m_fBehavioTime, m_fSplitJumpTime, m_bSplitUp);
				else										// false 떨어짐
					m_BehavioEnd = m_pPart[i]->SplitFunc(fDeltaTime, m_fBehavioTime, m_fSplitFallTime, m_bSplitUp);
				break;
			}
			m_pPart[i]->Update();
		}
	}
}

void CSludge::BehavioEndFunc()
{
	if (m_BehavioEnd)
	{
		if (m_eSize == SL_BIG)
		{
			m_eState = SL_IDLE;
			m_fBehavioTime = 0.f;
			return;
		}

		m_fBehavioTime = 0.f;

		switch (m_eState)
		{
		case SL_IDLE:

			if (GetRandom<int>(1, 10) > 7)
				m_eState = SL_IDLE;
			else
			{
				m_eState = SLS_JUMP;
				Traking();
				m_pInfo->m_vPos.y = 0.3f;
			}
			break;
		case SLS_JUMP:
			m_eState = SLS_FALL;
			Traking();
			m_pInfo->m_vPos.y = 0.2f;
			break;
		case SLS_FALL:
			m_eState = SL_IDLE;
			m_pInfo->m_vPos.y = BOSS_Y;

			if (m_eSize > SL_HALF)
			{
				CreateMucus();
				ShakingCamera();
			}
			switch (m_eSize)
			{
			case CSludge::SL_VERYSMALL:
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_SL_HALF.ogg", CSoundMgr::SOUND_BOSS);
				break;
			case CSludge::SL_QUARTER:
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_SL_NORMAL2.ogg", CSoundMgr::SOUND_BOSS);
				break;
			case CSludge::SL_HALF:
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_SL_NORMAL.ogg", CSoundMgr::SOUND_BOSS);
				break;
			case CSludge::SL_NORMAL:
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_SL_BIG2.ogg", CSoundMgr::SOUND_BOSS);
				break;
			case CSludge::SL_BIG:
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_SL_BIG.ogg", CSoundMgr::SOUND_BOSS);
				break;
			case CSludge::SL_HEART:
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_SL_HEART.ogg", CSoundMgr::SOUND_BOSS);
				break;
			case CSludge::SL_END:
				break;
			}
			break;
		case SLS_SPLIT:
			if (m_bSplitUp)
			{
				m_eState = SLS_SPLIT;
				m_bSplitUp = false;
			}
			else
			{
				m_eState = SL_IDLE;
				m_bSplitUp = true;
			}
			break;
		}
		if (m_eState == SL_IDLE)
		{
			m_pInfo->m_vPos.y = 0.f;
			for (int i = 0; i < SLO_END; ++i)
				if (m_pPart[i] != nullptr)
				{
					m_pPart[i]->ResetSize();
					m_pPart[i]->InitSize();
					m_pPart[i]->SetSynchPos(m_pInfo->m_vPos);
				}
		}
	}
}

void CSludge::Traking()
{
	m_pInfo->m_vDir = m_pTargetTrasform->m_vPos - m_pInfo->m_vPos;

	float fLength = D3DXVec3Length(&m_pInfo->m_vDir);

	if (fLength > 12.f)
		SetRandomDir();
	else if(m_eSize == SL_HEART)
		SetRandomDir();
	else 
	{
		m_pInfo->m_vDir.y = 0.f;	// y값은 빼준다.
		D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
	}
}

void CSludge::SplitResizeAndCreate()
{
	switch (m_eSize)
	{
	case CSludge::SL_QUARTER:
		m_eSize = SL_VERYSMALL;
		break;
	case CSludge::SL_HALF:
		m_eSize = SL_QUARTER;
		break;
	case CSludge::SL_NORMAL:
		m_eSize = SL_HALF;
		break;
	case CSludge::SL_BIG:
		m_eSize = SL_NORMAL;
		break;
	}
	for (size_t i = 0; i < SLO_END; ++i)
	{
		if (m_pPart[i] != nullptr)
			m_pPart[i]->DownGrade(m_eSize);		// Retsize, initsize해줌
	}
	if(m_eSize != SL_HEART)
		m_pSphereBox->SetRadius(float(m_eSize));
	else if (m_eSize == SL_HEART)
		m_pSphereBox->SetRadius(float(SL_VERYSMALL));

	CSludge::Create(m_pGraphicDev, m_pInfo->m_vPos, m_pUpdateLayer,m_pRenderLayer, m_pSludgeList, m_pCamera, false, m_eSize);
}

void CSludge::SetRandomDir()
{
	//  랜덤 방향벡터 생성
	m_pInfo->m_vDir.x = GetRandom<float>(-10.f, 10.f);
	m_pInfo->m_vDir.z = GetRandom<float>(-10.f, 10.f);
	m_pInfo->m_vDir.y = 0.f;
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CSludge::HitTimeCounting()
{
	if (m_bHit)
	{
		const float& fDeltaTime = m_pTimeMgr->GetTime();
		m_iHitTime += fDeltaTime;

		if (m_iHitTime > m_fMaxHitTime)
		{
			m_iHitTime = 0.f;
			m_bHit = false;
		}
	}
}

void CSludge::ShakingCamera()
{
	m_pCamera->ShakingStart(0.7f, 1.2f, 1.f);
}

void CSludge::SludgeMove(const float& fDelta)
{
	if (!m_bPlaneCol)
		m_pInfo->m_vPos += m_pInfo->m_vDir * m_fSpeed * fDelta;		// 방향 계산 끝나고 좌표 더 해줌
}

void CSludge::CreateMucus()
{
	size_t iCount = GetRandom<int>(0, 2);

	for (size_t i = 0; i < iCount; ++i)
	{
		if (m_eSize != SL_HEART)
			m_MucusLst.push_back(CMucus::Create(m_pGraphicDev, m_pInfo->m_vPos));
		else
			CreateTrace();
	}
}

void CSludge::CreateTrace()
{
	m_TraceLst.push_back(CTrace::Create(m_pGraphicDev, m_pInfo->m_vPos));
}


const float& CSludge::GetRadius()
{
	return m_pSphereBox->GetRadius();
}

CSludge* CSludge::Create(LPDIRECT3DDEVICE9 pGraphicDev, const D3DXVECTOR3& vPos, Engine::CLayer* pUpdateLayer, 
							Engine::CLayer* pRenderLayer,	list<CSludge*>* pSludgeLst, Engine::CCamera* pCamera,
							const bool& bTrue /*true*/, const SL_SIZE& eSize/*BIG*/)
{
	CSludge* pInstance = new CSludge(pGraphicDev);

	if (FAILED(pInstance->Initialize(bTrue, eSize, vPos, pUpdateLayer, pCamera, pRenderLayer, pSludgeLst)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
