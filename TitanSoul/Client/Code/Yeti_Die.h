#ifndef Yeti_Die_h__
#define Yeti_Die_h__

#include "YetiState.h"
class CYeti;
class CYeti_Die
	: public CYetiState

{
public:
	CYeti_Die(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
	virtual ~CYeti_Die();

public:
	virtual HRESULT Initialize();
	virtual void Update(const float& fTime);
	virtual void Release();
	
public:
	static CYeti_Die*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);

};

#endif // Yeti_Idle_h__
