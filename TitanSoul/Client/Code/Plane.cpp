#include "stdafx.h"
#include "Plane.h"
#include "Export_Function.h"
#include "Include.h"
#include "CameraObserver.h"
#include "SphereColBox.h"

CPlane::CPlane(LPDIRECT3DDEVICE9 pGraphicDev)
	:CGameObject(pGraphicDev)
	, m_pResourcesMgr(Engine::Get_ResourceMgr())
	, m_pInfoSubject(Engine::Get_InfoSubject())
{
}

CPlane::~CPlane()
{
	Release();
}

HRESULT CPlane::Initialize(const wstring& wstrMapKey, PLANEINFO* pPlaneInfo)
{
	m_pCameraObserver = CCameraObserver::Create();	//카메라옵저버 생성
	m_pInfoSubject->Subscribe(m_pCameraObserver);   //카메라옵저버 구독신청

	m_pPlaneInfo = pPlaneInfo;

	m_pVtx = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVtx = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_pPlaneInfo->wstrPlaneBufferKey, m_pVtx);


	PlaneCorrection(wstrMapKey);

	m_pVtx[0].vPos = m_pPlaneInfo->vVertex0;
	m_pVtx[1].vPos = m_pPlaneInfo->vVertex1;
	m_pVtx[2].vPos = m_pPlaneInfo->vVertex2;
	m_pVtx[3].vPos = m_pPlaneInfo->vVertex3;

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		//if (m_pPlaneInfo->fY < 0.05f)
		//	m_pVtx[i].vPos.y = 0.5f;
		//else
		//	m_pVtx[i].vPos.y = m_pPlaneInfo->fY;
	}

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);


	return S_OK;
}

void CPlane::Update()
{
	SetDirection();

	CGameObject::Update();

	SetTransform();
}

void CPlane::Render()
{
	if (m_bIsSelect)
		m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_pPlaneInfo->wstrPlaneBufferKey, m_pConvertVtx);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	m_pTextureCom->Render(m_pPlaneInfo->iOption);
	m_pBufferCom->Render();

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
}

void CPlane::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);   //카메라옵저버 구독신청
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete(m_pPlaneInfo);

	Engine::Safe_Delete_Array(m_pVtx);
	Engine::Safe_Delete_Array(m_pConvertVtx);
}

HRESULT CPlane::AddComponent(void)
{
	Engine::CComponent*		pComponent = nullptr;

	// Buffer
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_pPlaneInfo->wstrPlaneBufferKey);
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	// Texture
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_pPlaneInfo->wstrPlaneTexKey);
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTextureCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	// Transform
	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

void CPlane::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CPlane::SetTransform(void)
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVtx[i] = m_pVtx[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVtx[i].vPos, &m_pConvertVtx[i].vPos, &m_pInfo->m_matWorld);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);
}

//************************************
// Method	:  PlaneCorrection
// 작성자	:  윤유석
// Date		:  2019/01/13
// Message	:  플래인 버텍스 생성시 좌표 보정을 해주는 함수
// Parameter: 
//************************************
void CPlane::PlaneCorrection(const wstring& wstrBufferKey)
{
	m_pPlaneInfo->vVertex0.y = 0.f;
	m_pPlaneInfo->vVertex1.y = 0.f;
	m_pPlaneInfo->vVertex2.y = 0.f;
	m_pPlaneInfo->vVertex3.y = 0.f;

	if (wstrBufferKey == L"MapTerrainMap0")
	{
		m_pPlaneInfo->vVertex0.x += 0.5f;
		m_pPlaneInfo->vVertex1.x += 0.5f;
		m_pPlaneInfo->vVertex2.x += 0.5f;
		m_pPlaneInfo->vVertex3.x += 0.5f;

		m_pPlaneInfo->vVertex0.z += 0.5f;
		m_pPlaneInfo->vVertex1.z += 0.5f;
		m_pPlaneInfo->vVertex2.z += 0.5f;
		m_pPlaneInfo->vVertex3.z += 0.5f;
	}

	else if (wstrBufferKey == L"MapTerrainMap1")
	{
	}
	else if (wstrBufferKey == L"MapTerrainStage0")
	{
		m_pPlaneInfo->vVertex0.x += 3.7f;
		m_pPlaneInfo->vVertex1.x += 3.7f;
		m_pPlaneInfo->vVertex2.x += 3.7f;
		m_pPlaneInfo->vVertex3.x += 3.7f;

		m_pPlaneInfo->vVertex0.z += 1.f;
		m_pPlaneInfo->vVertex1.z += 1.f;
		m_pPlaneInfo->vVertex2.z += 1.f;
		m_pPlaneInfo->vVertex3.z += 1.f;
	}
	else if (wstrBufferKey == L"MapTerrainStage1")
	{
		m_pPlaneInfo->vVertex0.x += 0.6f;
		m_pPlaneInfo->vVertex1.x += 0.6f;
		m_pPlaneInfo->vVertex2.x += 0.6f;
		m_pPlaneInfo->vVertex3.x += 0.6f;

		m_pPlaneInfo->vVertex0.z += 1.5f;
		m_pPlaneInfo->vVertex1.z += 1.5f;
		m_pPlaneInfo->vVertex2.z += 1.5f;
		m_pPlaneInfo->vVertex3.z += 1.5f;

		m_pPlaneInfo->vVertex0.z -= 0.5f;
		m_pPlaneInfo->vVertex1.z -= 0.5f;
		m_pPlaneInfo->vVertex2.z -= 0.5f;
		m_pPlaneInfo->vVertex3.z -= 0.5f;
	}
	else if (wstrBufferKey == L"MapTerrainStage2")
	{
		m_pPlaneInfo->vVertex0.x += 1.3f;
		m_pPlaneInfo->vVertex1.x += 1.3f;
		m_pPlaneInfo->vVertex2.x += 1.3f;
		m_pPlaneInfo->vVertex3.x += 1.3f;
	}
	else if (wstrBufferKey == L"MapTerrainStage3")
	{
		m_pPlaneInfo->vVertex0.x -= 0.5f;
		m_pPlaneInfo->vVertex1.x -= 0.5f;
		m_pPlaneInfo->vVertex2.x -= 0.5f;
		m_pPlaneInfo->vVertex3.x -= 0.5f;

		m_pPlaneInfo->vVertex0.z += 0.7f;
		m_pPlaneInfo->vVertex1.z += 0.7f;
		m_pPlaneInfo->vVertex2.z += 0.7f;
		m_pPlaneInfo->vVertex3.z += 0.7f;

		m_pPlaneInfo->vVertex0.y = 1.0f;
		m_pPlaneInfo->vVertex1.y = 1.0f;
		m_pPlaneInfo->vVertex2.y = 1.0f;
		m_pPlaneInfo->vVertex3.y = 1.0f;

	}
	else if (wstrBufferKey == L"MapTerrainStage4")
	{
		m_pPlaneInfo->vVertex0.x += 4.5f;
		m_pPlaneInfo->vVertex1.x += 4.5f;
		m_pPlaneInfo->vVertex2.x += 4.5f;
		m_pPlaneInfo->vVertex3.x += 4.5f;

		m_pPlaneInfo->vVertex0.z += 0.5f;
		m_pPlaneInfo->vVertex1.z += 0.5f;
		m_pPlaneInfo->vVertex2.z += 0.5f;
		m_pPlaneInfo->vVertex3.z += 0.5f;
	}
	else if (wstrBufferKey == L"MapTerrainStage5")
	{

	}
}

CPlane* CPlane::Create(LPDIRECT3DDEVICE9 pGraphicDev, const wstring& wstrMapKey, PLANEINFO* pPlaneInfo)
{
	CPlane*	pInstance = new CPlane(pGraphicDev);
	if (FAILED(pInstance->Initialize(wstrMapKey,pPlaneInfo)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

