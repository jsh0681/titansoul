#include "stdafx.h"
#include "YetiShadow.h"
#include "Yeti.h"
#include "CameraObserver.h"
#include "Export_Function.h"
#include "Transform.h"

CYetiShadow::CYetiShadow(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();
}


CYetiShadow::~CYetiShadow()
{
	Release();
}

HRESULT CYetiShadow::Initialize(CYeti * pYeti)
{
	m_pYeti = pYeti;

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pBufferCom = nullptr;
	m_pTextureCom = nullptr;

	m_pVertex = nullptr;
	m_pConvertVertex = nullptr;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_YetiShadow", m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);


	return S_OK;
}

void CYetiShadow::Update()
{
	m_pInfo->m_vPos = m_pYeti->GetInfo()->m_vPos;
	m_pInfo->m_vPos.y = 0.1f;

	float fscale = m_pYeti->GetInfo()->m_vPos.y * 10 / 30;

	m_pInfo->m_vPos.x -= fscale;

	m_pInfo->m_vScale = { fscale , fscale , fscale };

	//
	//float fDeltaTime = Engine::Get_TimeMgr()->GetTime();
	//Engine::CGameObject::Update();

	//float fScale = 0;
	//D3DXMATRIX matScale, matTrans;
	//fScale = m_pYeti->GetInfo()->m_vPos.y / 25;
	//float fZ = (m_pYeti->GetInfo()->m_vPos.y)*0.1f;

	//m_pInfo->m_vScale = { 1 - fScale, 1 - fScale ,1 - fScale };

	//D3DXMatrixTranslation(&matTrans, m_pYeti->GetInfo()->m_vPos.x - 0.1f, 0.01f, m_pYeti->GetInfo()->m_vPos.z - 1.2f - fZ);
	//m_pInfo->m_vPos.x = m_pYeti->GetInfo()->m_vPos.x - 0.1f;
	//m_pInfo->m_vPos.y = m_pYeti->GetInfo()->m_vPos.x - 0.01f;
	//m_pInfo->m_vPos.z = m_pYeti->GetInfo()->m_vPos.z - 3.2f - fZ;

	m_pInfo->m_vPos.z -= 3.f;
	CGameObject::Update();
	SetTransform();
}

void CYetiShadow::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_YetiShadow", m_pConvertVertex);
	if (m_pYeti->GetState() != CYeti::YETI_ROLL) return;

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(50, 255, 255, 255));

	m_pTextureCom->Render(0);
	m_pBufferCom->Render();

	// 반투명 해제
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}

HRESULT CYetiShadow::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	// buffer
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_YetiShadow");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	// texture
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"ShadowYetiBody");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	// Transform
	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

void CYetiShadow::SetTransform()
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);
}

void CYetiShadow::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	if (nullptr != m_pVertex)
	{
		Engine::Safe_Delete_Array(m_pVertex);
	}

	if (nullptr != m_pConvertVertex)
	{
		Engine::Safe_Delete_Array(m_pConvertVertex);
	}

}

CYetiShadow * CYetiShadow::Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti * pYeti)
{
	CYetiShadow* pInstance = new CYetiShadow(pGraphicDev);

	if (FAILED(pInstance->Initialize(pYeti)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
