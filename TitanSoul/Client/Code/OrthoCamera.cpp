#include "stdafx.h"
#include "OrthoCamera.h"


#include "Export_Function.h"
#include "Include.h"
#include "Transform.h"

COrthoCamera::COrthoCamera(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CCamera(pGraphicDev),
	m_pInfoSubject(Engine::Get_InfoSubject())
{
	D3DXMatrixIdentity(&m_matOrtho);
}


COrthoCamera::~COrthoCamera()
{
	Release();
}

void COrthoCamera::SetCameraTransform(const D3DXVECTOR3& vPos)
{
}


HRESULT COrthoCamera::Initialize()
{
	m_pInfoSubject->AddData(CAMERAOPTION::ORTHO, &m_matOrtho);

	return S_OK;
}

void COrthoCamera::Update()
{
	m_pInfoSubject->Notify(CAMERAOPTION::ORTHO);
}

void COrthoCamera::Release()
{
	m_pInfoSubject->RemoveData(CAMERAOPTION::ORTHO, &m_matOrtho);
}

void COrthoCamera::SetOrthoMatrix(const float& fFovY, const float& fHeight, const float& fWidth)
{
	float fFar = 1.f;
	float fNear = 0.f;

	float fH = 2.f / fHeight;
	float fW = 2.f / fWidth;
	float fA = 1.f;
	float fB = 0.f;

	m_matOrtho =
	{
		fW, 0.f, 0.f, 0.f,
		0.f, fH, 0.f, 0.f,
		0.f, 0.f, fA, 0.f,
		0.f, 0.f, fB, 1.f
	};
}

COrthoCamera* COrthoCamera::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	COrthoCamera* pInstance = new COrthoCamera(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	pInstance->SetProjectionMatrix(D3DXToRadian(45.f), float(WINCX) / WINCY, 1.f, 1000.f);
	pInstance->SetOrthoMatrix(D3DXToRadian(45.f), WINCY, WINCX);

	return pInstance;
}
