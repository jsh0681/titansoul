#include "stdafx.h"
#include "MainApp.h"

#include "Export_Function.h"
#include "SceneSelector.h"

CMainApp::CMainApp(void)
	: m_pGraphicDev(Engine::Get_GraphicDev())
	, m_pManagement(Engine::Get_Management())
{

}

CMainApp::~CMainApp(void)
{
	Release();
}

void CMainApp::Update(void)
{
	Engine::Get_KeyMgr()->KeyCheck();
	Engine::Get_TimeMgr()->SetTime();
	CSoundMgr::GetInstance()->Update();
	m_pManagement->Update();
}

void CMainApp::Render(void)
{
	m_pManagement->Render(Engine::Get_TimeMgr()->GetTime());
}

void CMainApp::Release(void)
{
	CTextureInfoMgr::GetInstance()->DestroyInstance();
	CEffectInfoMgr::GetInstance()->DestroyInstance();
	CTerrainMgr::GetInstance()->DestroyInstance();
	CCollisionMgr::GetInstance()->DestroyInstance();
	CParticleMgr::GetInstance()->DestroyInstance();
	CSoundMgr::GetInstance()->DestroyInstance();



	Engine::Get_KeyMgr()->DestroyInstance();

	// utility
	Engine::Safe_Single_Destory(m_pManagement);
	
	// ī�޶�
	Engine::Get_InfoSubject()->DestroyInstance();

	// system
	Engine::Safe_Single_Destory(m_pGraphicDev);
}

HRESULT CMainApp::Initialize(void)
{
	FAILED_CHECK_RETURN(m_pGraphicDev->InitGraphicDev(Engine::CGraphicDev::MODE_FULL, g_hWnd, WINCX, WINCY), E_FAIL);

	m_pDevice = m_pGraphicDev->GetDevice();
	NULL_CHECK_RETURN(m_pDevice, E_FAIL);

	Engine::Get_TimeMgr()->InitTime();

	CSoundMgr::GetInstance()->Initialize();

	ShowCursor(FALSE);

	FAILED_CHECK_RETURN(m_pManagement->Initialize(m_pDevice), E_FAIL);
	FAILED_CHECK_RETURN(m_pManagement->SceneChange(CSceneSelector(SC_LOGO,nullptr,nullptr)),E_FAIL);

	return S_OK;
}

CMainApp* CMainApp::Create(void)
{
	CMainApp*	pInstance = new CMainApp;

	if (FAILED(pInstance->Initialize()))
	{
		delete pInstance;
		pInstance = nullptr;
	}

	return pInstance;
}

