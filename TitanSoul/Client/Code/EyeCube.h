#ifndef EyeCube_h__
#define EyeCube_h__

#include "Export_Function.h"
#include"GameObject.h"
#include"CameraObserver.h"
#include"PlayerObserver.h"
#include"CollisionMgr.h"
#include"TerrainColl.h"
#include"Camera.h"

class CArrow;
class CSphereColBox;

namespace Engine 
{
	class CScene;
}
class CEyeCubeRaser;
class CCubeAni;

class CEyeCube : public Engine::CGameObject
{
public:
	enum LASER_DIR { NO, EAST, WEST, SOUTH, NORTH, UP, DOWN };
private:
	explicit CEyeCube(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CEyeCube(void);

public:
	const float & GetRadius();

	Engine::CTransform* GetInfo() { return m_pInfo; }
	D3DXMATRIX* GetWorldMatrix() { return m_pCollisionWorld; }
	bool GetPlayerDead() { return m_bPlayerDead; }

	 void SetStartPos(D3DXVECTOR3 vStartPos) { m_pInfo->m_vPos = vStartPos; }
	virtual void Render();
	virtual void Update();
	virtual void Release();
	int GetIndex() { return m_iIndex; }

	D3DXVECTOR3			GetRight() { return vRight; }
	D3DXVECTOR3			GetUp() { return vUp; }
	D3DXVECTOR3			GetLook() { return vLook; }


private:

	D3DXVECTOR3		vRight;
	D3DXVECTOR3		vUp;
	D3DXVECTOR3		vLook;
	D3DXVECTOR3		vPos;

public:
	void SetCamera(Engine::CCamera* pCamera) { m_pCamera = pCamera; }
private:
	Engine::CCamera* m_pCamera = nullptr;
public:
	void SetPlayerDead() { m_bPlayerDead = false; }
	void ReadjustmentAxis();
	void PlayerInTile();
	void StateChange();

	void SetArrow(CArrow* pArrow) { m_pArrow = pArrow; }
	void SetWake() { m_bWake = true; }
	void SetDead() { m_bDead = true; }
	bool& GetWake() { return m_bWake; }
	bool& GetDead() { return m_bDead; }
	bool& GetAttack() { return m_bAttack; }
	bool& GetCurseOn() { return m_bCurseOn; };
private:
	bool m_bCurseOn = false;
	bool m_bPlayerDead = false;
	virtual HRESULT Initialize();
	virtual HRESULT AddComponent(void);
	virtual void SetTransform();

	virtual		void SetDirection(void);
	void		Move(float fDeltaTime);
	void RaserPattern();
	void HitWeakness();
	void SetRotateAngle();
	void SpreadPlayer(D3DXVECTOR3 vPlayerPos, D3DXVECTOR3 vEyeCubePos, float fRange);
private:
	float m_fCurseTime = 0.f;
	int m_iRaserDir = 0;

	bool m_bAttack = false;
	bool m_bWake = false;
	bool m_bDead= false;
	
	int iRollingCount = 0;
	D3DXVECTOR3 m_vPlayerPos;
	D3DXVECTOR3 m_vCalPlayerPos;

	CCameraObserver*				m_pCameraObserver = nullptr;

	CPlayerObserver*				m_pPlayerObserver = nullptr;

	float								m_fAngleX = 0.f;
	float								m_fAngleZ = 0.f;
	
	int m_iCalculate = 0;
	float m_fRaserTime = 0.f;
	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;
	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *					m_pTerrainCol = nullptr;
	D3DXVECTOR3						m_vDestPos;
private:
	bool m_bMoveRightX = false;
	bool m_bMoveUpZ = false;
	bool m_bMoveLeftX = false;

	float fRotateTime = 0;
	bool m_bCalculate = false;

	CSphereColBox*			m_pSphereBox = nullptr;
	D3DXMATRIX* m_pMatWorld;
	D3DXMATRIX* m_pCollisionWorld;

	D3DXMATRIX* m_pMatView;
	D3DXMATRIX* m_pMatProj;
	CEyeCubeRaser* m_pRaser = nullptr;\
private:

	bool m_bMoveDownZ = false;
	Engine::VTXCUBE*					m_pVertex = nullptr;
	Engine::VTXCUBE*					m_pConvertVertex = nullptr;

	DWORD							m_dwVtxCnt;
	int m_iIndex = 0;
	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...
	CCubeAni* m_pCubeAni = nullptr;
	bool m_bRaserOn = false;
private:
	CArrow* m_pArrow;

	D3DXVECTOR3						m_vRayPos;
	D3DXVECTOR3						m_vRayDir;

public:
	static CEyeCube* Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos);
private:

protected:
};
#endif // EyeCube_h__
