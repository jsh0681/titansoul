#include "stdafx.h"
#include "AlphaAniEffect.h"

#include "CameraObserver.h"

CAlphaAniEffect::CAlphaAniEffect(LPDIRECT3DDEVICE9 pGraphicDev)
	:CNormalEffect(pGraphicDev)
{
}


CAlphaAniEffect::~CAlphaAniEffect()
{
	Release();
}


HRESULT CAlphaAniEffect::Initialize(NorEffect tNorEffect, wstring wstrBuffer, wstring wstrTex, int iIndexCnt)
{

	if (GetRandom<int>(0, 1) == 1)
	{
		m_bAngleDir = TRUE;
	}
	else
		m_bAngleDir = FALSE;


	m_iIndex = GetRandom<int>(0, iIndexCnt - 1);

	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pResourcesMgr = Engine::Get_ResourceMgr();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_tEffect = tNorEffect;
	m_wstrBufferKey = wstrBuffer;
	m_wstrTextureKey = wstrTex;
	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pVertex);
	InitVertex();

	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferKey);
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, wstrTex);
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	m_mapComponent.emplace(L"Texture", m_pTextureCom);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	m_pInfo->m_vDir = m_tEffect.vDir;
	m_pInfo->m_vPos = m_tEffect.vPos;

	m_vAccel = -m_pInfo->m_vDir;
	
	m_fScale = m_tEffect.fScale;
	m_pInfo->m_vScale = { m_tEffect.fScale, m_tEffect.fScale ,m_tEffect.fScale };
	//버텍스 초기화 해주고

	m_fLifeTime = m_tEffect.fLifeTime;
	m_fSpeed = m_tEffect.fSpd;
	return S_OK;
}

void CAlphaAniEffect::InitVertex()
{
	m_pVertex[0].vPos = { -1.f, 1.f, 0.f };
	m_pVertex[0].vTex = { 0, 0 };

	m_pVertex[1].vPos = { 1.f, 1.f, 0.f };
	m_pVertex[1].vTex = { 1, 0 };

	m_pVertex[2].vPos = { 1.f, -1.f, 0.f };
	m_pVertex[2].vTex = { 1, 1 };

	m_pVertex[3].vPos = { -1.f,- 1.f, 0.f };
	m_pVertex[3].vTex = { 0, 1 };
}

void CAlphaAniEffect::Release(void)
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	if (nullptr != m_pVertex)
	{
		Engine::Safe_Delete_Array(m_pVertex);
	}

	if (nullptr != m_pConvertVertex)
	{
		Engine::Safe_Delete_Array(m_pConvertVertex);
	}
}

void CAlphaAniEffect::Update()
{
	float fTime =  Engine::Get_TimeMgr()->GetTime();
	m_fLifeTime -= fTime;
	if (m_fLifeTime <= 0.f)
	{
		m_bIsDead = TRUE;
		return;
	}
	
	if (m_bAcc)
	{
		m_pInfo->m_vPos += m_pInfo->m_vDir * fTime* m_fSpeed;
		m_fSpeed += fTime * m_fSpeed2;

		m_fScale -= fTime * m_tEffect.fDisappearSpd;
		if (m_fScale < 0.f)
		{
			m_fScale = 0.00f;
		}
		m_pInfo->m_vScale = { m_fScale, m_fScale ,m_fScale };
	}
	else
	{
		m_pInfo->m_vDir += m_vAccel * fTime;

		D3DXVECTOR3 vTemp = m_pInfo->m_vDir - m_vAccel;
		float fLength = D3DXVec3Length(&vTemp);
		if (fLength < 1.1f)
		{
			m_pInfo->m_vDir = m_tEffect.vAcc;
			m_fSpeed2 = m_fSpeed;
			m_fSpeed = 0.f;
			m_bAcc = TRUE;
		}
		else
			m_pInfo->m_vPos += m_pInfo->m_vDir * fTime * m_fSpeed;
			

	}

	if (m_bAngleDir)
	{
		m_fDegree += 0.5f;
	}
	else
		m_fDegree -= 0.5f;

	m_pInfo->m_fAngle[1] = D3DXToRadian(m_fDegree);

	Engine::CGameObject::Update();
	SetTransform();
}

void CAlphaAniEffect::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pConvertVertex);

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_MAX);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pTextureCom->Render(m_iIndex);
	m_pBufferCom->Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CAlphaAniEffect::SetTransform()
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);


}

CAlphaAniEffect * CAlphaAniEffect::Create(LPDIRECT3DDEVICE9 pGraphicDev, NorEffect tNorEffect, wstring wstrBuffer, wstring wstrTex, int iIndexCnt)
{
	CAlphaAniEffect*	pInstance = new CAlphaAniEffect(pGraphicDev);

	if (FAILED(pInstance->Initialize(tNorEffect, wstrBuffer, wstrTex, iIndexCnt)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

