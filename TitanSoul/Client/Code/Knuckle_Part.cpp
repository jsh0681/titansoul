#include "stdafx.h"
#include "Knuckle_Part.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"
#include "CameraObserver.h"

CKnuckle_Part::CKnuckle_Part(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();
}


CKnuckle_Part::~CKnuckle_Part()
{
	Release();
}

void CKnuckle_Part::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CKnuckle_Part::ResetBuffer()
{
	// 정점 초기화
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);
}

void CKnuckle_Part::Update()
{
	Engine::CGameObject::Update();
}

void CKnuckle_Part::SetSynchPos(const VEC3& vPos)
{
	m_pInfo->m_vPos = vPos;
}

void CKnuckle_Part::SetSynchPosXZ(const float& fX, const float& fZ)
{
	m_pInfo->m_vPos.x = fX;
	m_pInfo->m_vPos.z = fZ;
}

void CKnuckle_Part::SetSynchDir(const VEC3& vDir)
{
	memcpy(&m_pInfo->m_vDir, &vDir, sizeof(float) * 3);
}

void CKnuckle_Part::SetSynchDirXZ(const float& fX, const float& fZ)
{
	m_pInfo->m_vDir.x = fX;
	m_pInfo->m_vDir.z = fZ;
}

void CKnuckle_Part::SetTargetVec(const VEC3& vPos)
{
	memcpy(&m_vTargetPos, &vPos, sizeof(float) * 3);
}

const VEC3& CKnuckle_Part::GetPos()
{
	return m_pInfo->m_vPos;
}

void CKnuckle_Part::SetState(const int& iState)
{
	m_eState = (PART_STATE)iState;
}
