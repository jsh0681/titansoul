#ifndef Yeti_Roll_h__
#define Yeti_Roll_h__

#include "YetiState.h"
class CYeti;
class CYeti_Roll
	: public CYetiState

{
public:
	CYeti_Roll(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
	virtual ~CYeti_Roll();

public:
	virtual HRESULT Initialize();
	virtual void Update(const float& fTime);
	virtual void Release();
public:
	static CYeti_Roll*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);


private:
	D3DXVECTOR3	m_vStart; //구르기 시작점 : 여기부터 충돌지점까지 고드름을 만들어야지
	D3DXVECTOR3	m_vReverseDir; //역방향벡터
	bool	m_bPreventOverlap = FALSE; //역시 중복방지이다. 각도로 씬ㅊ
	float	m_fDegree = 0.f; //각도는 이걸 이용하자. 여기서 처음에 각도 초기화 ㄱ;
	float	m_fTimeSave = 0.f; //시간을 저장하여 충돌 오버랩방지  == 일정 시간이 지나야만 평면과 충돌처리

	int		m_iCnt = 0;//구르는 횟수
};

#endif // Yeti_Idle_h__
