#ifndef PlayerObserver_h__
#define PlayerObserver_h__

#include "Observer.h"

namespace Engine
{
	class CInfoSubject;
	class CTransform;
}

class CPlayerObserver : public Engine::CObserver
{
private:
	CPlayerObserver(void);
public:
	virtual ~CPlayerObserver(void);

public:
	const Engine::CTransform*		GetPlayerTransform(void) { return m_matTransform; }

public:
	virtual void Update(int iFlag);

private:
	Engine::CInfoSubject*	m_pInfoSubject;
	Engine::CTransform*		m_matTransform;

public:
	static CPlayerObserver*		Create(void);

};



#endif // PlayerObserver_h__
