#ifndef IceCube_h__
#define IceCube_h__

#include "Export_Function.h"
#include"GameObject.h"
#include"CameraObserver.h"
#include"PlayerObserver.h"
#include"CollisionMgr.h"
#include"TerrainColl.h"
#include"Camera.h"

class CIceCubeShadow;
class CArrow;
class CSphereColBox;

namespace Engine
{
	class CScene;
}



class CCubeAni;


class CIceCube : public Engine::CGameObject
{

private:
	explicit CIceCube(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CIceCube(void);

public:
	Engine::CTransform* GetInfo() { return m_pInfo; }

	void				SetStartPos(D3DXVECTOR3 vStartPos) { m_pInfo->m_vPos = vStartPos; }
	virtual void		Render();
	virtual void		Update();
	virtual void		Release();
	int					GetIndex() { return m_iIndex; }
	void				StopIceCube();

public:
	void				SetCamera(Engine::CCamera* pCamera) { m_pCamera = pCamera; }
private:
	Engine::CCamera* m_pCamera = nullptr;

public:
	Engine::CTransform* Get_Transform() { return m_pInfo; };
	void				StateChange();
	const D3DXVECTOR3&	GetTempDir() { return m_vTempDir; };
	void				SetArrow(CArrow* pArrow) { m_pArrow = pArrow; }
	void				SetWake() { m_bWake = true; }
	void				SetDead() { m_bDead = true; }
	bool&				GetWake() { return m_bWake; }
	bool&				GetDead() { return m_bDead; }
	bool&				GetAttack() { return m_bAttack; }
	D3DXVECTOR3			Get_OnePos() { return m_vOnePos; };
	D3DXVECTOR3			Get_StartVector() { return m_vBackPos; }
	D3DXVECTOR3			GetRight() { return vRight; }
	D3DXVECTOR3			GetUp() { return vUp; }
	D3DXVECTOR3			GetLook() { return vLook; }
	CSphereColBox*		GetColBox() { return m_pSphereBox; };
	void				ReverseMove();
	bool GetIsBottom() { return m_bIsBottom; }
private:

	D3DXVECTOR3		vRight;
	D3DXVECTOR3		vUp;
	D3DXVECTOR3		vLook;
	D3DXVECTOR3		vPos;
	D3DXVECTOR3		m_vBackPos;

	int m_iCollisionCount = 0;
	bool m_bIsBottom = false;
	int m_iAttackPattern = 0;

	virtual HRESULT		Initialize();
	virtual HRESULT		AddComponent(void);
	virtual void		SetTransform();

	float				CalculateSpeed(float fDeltaTime);

	void				Dash(float fDeltaTime);

	void				Jump(float fDeltaTime);

	virtual	void		SetDirection(void);
	void				Move(float fDeltaTime);

	bool PositionLock();

	
	void				SetRotateAngle();
	void				SpreadPlayer(D3DXVECTOR3 vPlayerPos, D3DXVECTOR3 vIceCubePos, float fRange);
private:
	bool m_bIsJump = false;
	D3DXVECTOR3 m_vOnePos;
	D3DXVECTOR3 m_vTempPos;

	D3DXVECTOR3 m_vStart;
	D3DXVECTOR3 m_vAccel;

	bool m_bCalculate = true;
	float m_fDashTime = 0.f;
	float m_fJumpTime = 0.f;


	float m_fJumpAcc = 0.f;
	float m_fFallAcc = 0.f;

	bool m_bAttack = false;
	bool m_bWake = false;
	bool m_bDead = false;

	float m_fSpeed = 25.f;
	float m_fFriction = 0.99f;

	D3DXVECTOR3 m_vColPos;
	D3DXVECTOR3 m_vPlayerPos;
	D3DXVECTOR3 m_vTempDir;
	CCameraObserver*				m_pCameraObserver = nullptr;

	CPlayerObserver*				m_pPlayerObserver = nullptr;

	float								m_fAngleY = 0.f;

	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *					m_pTerrainCol = nullptr;
	CSphereColBox*					m_pSphereBox = nullptr;

private:
	D3DXMATRIX* m_pMatWorld;
	D3DXMATRIX* m_pMatView;
	D3DXMATRIX* m_pMatProj;
private:

	Engine::VTXCUBE*					m_pVertex = nullptr;
	Engine::VTXCUBE*					m_pConvertVertex = nullptr;

	DWORD							m_dwVtxCnt;
	int m_iIndex = 0;
	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...
	CCubeAni* m_pCubeAni = nullptr;
private:
	CArrow* m_pArrow;
	D3DXVECTOR3 m_vRayPos;
	D3DXVECTOR3	m_vRayDir;

	CIceCubeShadow* m_pIceCubeShadow = nullptr;
public:
	static CIceCube*	Create(LPDIRECT3DDEVICE9 pGraphicDev);
	void				Reflect(const D3DXVECTOR3 & _vDir, const D3DXVECTOR3 & _vPoint);
private:

protected:
};
#endif // IceCube_h__
