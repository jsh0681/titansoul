#ifndef Player_Idle_h__
#define Player_Idle_h__

#include "Player_State.h"

class CPlayer_Idle
	: public Engine::CPlayer_State
{
public:
	CPlayer_Idle();
	virtual ~CPlayer_Idle();
};


#endif // Player_h__