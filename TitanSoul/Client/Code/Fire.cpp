#include "stdafx.h"
#include"Fire.h"
#include "Export_Function.h"
#include "Include.h"
#include "TexAni.h"
#include"SphereColBox.h"
#include "Arrow.h"
#include"StaticCamera2D.h"

CFire::CFire(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{

}

CFire::~CFire()
{
	Release();
}

HRESULT CFire::Initialize(D3DXVECTOR3 vStartPos)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);


	m_wstrStateKey = L"IceCubeToolFire";

	FAILED_CHECK_RETURN(AddComponent(vStartPos), E_FAIL);
	return S_OK;
}


HRESULT CFire::AddComponent(D3DXVECTOR3 vStartPos)
{
	Engine::CComponent*		pComponent = nullptr;

	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_Fire");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);
	//중심 좌표가 되어 각 파트들에 넘겨줄 좌표를 생성할것

	m_pInfo->m_vPos = vStartPos;

	return S_OK;

}

void CFire::Update(void)
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	Engine::CGameObject::Update();

	SetDirection();

	if (!g_2DMode)
	{
		if (m_bTurn)
		{
			if (m_fDegree > -90.f)
				m_fDegree -= fTime * 60.f;

			m_pInfo->m_fAngle[Engine::ANGLE_X] = D3DXToRadian(m_fDegree);
		}
	}
	SetTransform();

}

void CFire::Render(void)
{
	m_pTexAni->Render();
}

void CFire::SetTransform(void)
{
	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);
}

void CFire::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CFire::Release(void)
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);
}



CFire* CFire::Create(LPDIRECT3DDEVICE9 pGraphicDev,D3DXVECTOR3 vStartPos)
{
	CFire* pInstance = new CFire(pGraphicDev);
	if (FAILED(pInstance->Initialize(vStartPos)))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}

