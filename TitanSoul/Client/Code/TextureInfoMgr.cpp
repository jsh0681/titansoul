#include "stdafx.h"
#include "TextureInfoMgr.h"

#include "Engine_function.h"

IMPLEMENT_SINGLETON(CTextureInfoMgr)

CTextureInfoMgr::CTextureInfoMgr()
{
}

CTextureInfoMgr::~CTextureInfoMgr()
{
	for_each(m_mapTexInfo.begin(), m_mapTexInfo.end(), [](auto& myPair)
	{
		for (auto& pTexinfo : myPair.second)
			Engine::Safe_Delete(pTexinfo);
		myPair.second.clear();
		myPair.second.shrink_to_fit();
	});
	m_mapTexInfo.clear();
}

void CTextureInfoMgr::AddTexInfoData(const wstring& wstrKey, TEXINFO* pTexinfo)
{
	if (pTexinfo == nullptr)
		return;

	auto& it_find = m_mapTexInfo.find(wstrKey);

	if (it_find == m_mapTexInfo.end())
		m_mapTexInfo[wstrKey] = vector<TEXINFO*>();

	m_mapTexInfo[wstrKey].emplace_back(pTexinfo);
}

//************************************
// Method	:  GetTexInfoDataList
// 작성자	:  윤유석
// Date		:  2019/01/03
// Message	:  절대로 반환 받은 List의 값을 지우면 안됨.
// Parameter:  찾는 이미지 데이터의 키 값.
//************************************
const vector<TEXINFO*>* CTextureInfoMgr::GetTexInfoDataVec(const wstring& wstrKey)
{
	auto& it_find = m_mapTexInfo.find(wstrKey);

	if (it_find == m_mapTexInfo.end())
		return nullptr;

	return &it_find->second;
}

void CTextureInfoMgr::SetVecReserve(const wstring& wstrkey, const int& iSize)
{
	auto& it_find = m_mapTexInfo.find(wstrkey);

	if (it_find == m_mapTexInfo.end())
		m_mapTexInfo[wstrkey] = vector<TEXINFO*>();

	m_mapTexInfo[wstrkey].reserve(iSize);
}
