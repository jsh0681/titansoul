#include "stdafx.h"
#include "TalkWindow.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"


CTalkWindow::CTalkWindow(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement = Engine::Get_Management();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pTimeMgr = Engine::Get_TimeMgr();
	m_pKeyMgr = Engine::Get_KeyMgr();
}


CTalkWindow::~CTalkWindow()
{
	Release();
}

void CTalkWindow::Update()
{
	if (m_bOn)
	{
		if (m_fTalkSize < m_iTalkMaxSize)
			m_fTalkSize += m_pTimeMgr->GetTime() * 10.f;
		else
			m_bTalkEnd = true;

		TalkSkip();

		CGameObject::Update();
		SetTransform();
	}
}

void CTalkWindow::Render()
{
	if (m_bOn)
	{
		m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_TalkWindow", m_pConvertVertex);

		m_pGraphicDev->GetTransform(D3DTS_VIEW, &m_matView);
		m_pGraphicDev->GetTransform(D3DTS_PROJECTION, &m_matProj);

		m_pMatOrtho = m_pCameraObserver->GetOrtho();
		m_pGraphicDev->SetTransform(D3DTS_VIEW, &m_matIdentity);
		m_pGraphicDev->SetTransform(D3DTS_PROJECTION, m_pMatOrtho);


		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
		m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(180, 255, 255, 255));

		m_pTexture->Render(0);
		m_pBuffer->Render();

		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

		m_pGraphicDev->SetTransform(D3DTS_VIEW, &m_matView);
		m_pGraphicDev->SetTransform(D3DTS_PROJECTION, &m_matProj);

		m_pFont->DrawText(nullptr, m_wstrTalk.c_str(), (int)m_fTalkSize, &m_tRc, DT_NOCLIP, D3DCOLOR_ARGB(255, 0, 0, 0));
	}
}

HRESULT CTalkWindow::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_TalkWindow");
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrImgName);
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CTalkWindow::Initialize(const wstring& wstrTalk)
{
	m_wstrTalk = wstrTalk;
	m_wstrImgName = L"UiDefaultWindow";

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	D3DXMatrixIdentity(&m_matIdentity);

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_iTalkMaxSize = lstrlen(m_wstrTalk.c_str());

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_TalkWindow", m_pVertex);
	InitVertex();

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = { 0.f, 230.f, 0.11f };
	m_pInfo->m_vScale = { 400.f, 70.f, 0.f };

	m_tRc = { 60, 20, 700, 180 };
	m_pFont = m_pManagement->GetRender()->GetFont();

	return S_OK;
}

void CTalkWindow::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CTalkWindow::SetTalk(const wstring& wstrTalk)
{
	m_wstrTalk = wstrTalk;
	m_fTalkSize = 0.f;
	m_bTalkEnd = false;
	m_iTalkMaxSize = lstrlen(m_wstrTalk.c_str());
}

void CTalkWindow::InitVertex()
{
	m_pVertex[0].vPos = { -1.f,  1.f,  0.f };
	m_pVertex[1].vPos = { 1.f,   1.f,  0.f };
	m_pVertex[2].vPos = { 1.f,  -1.f,  0.f };
	m_pVertex[3].vPos = { -1.f, -1.f,  0.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_TalkWindow", m_pVertex);
}

void CTalkWindow::SetTransform()
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
}

void CTalkWindow::TalkSkip()
{
	if (m_pKeyMgr->KeyDown(KEY_SPACE))
		m_fTalkSize = (float)m_iTalkMaxSize;
}


CTalkWindow* CTalkWindow::Create(LPDIRECT3DDEVICE9 pGraphicDev, const wstring& wstrTalk)
{
	CTalkWindow* pInstance = new CTalkWindow(pGraphicDev);
	if (FAILED(pInstance->Initialize(wstrTalk)))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}