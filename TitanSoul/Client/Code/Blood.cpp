#include "stdafx.h"
#include "Blood.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"


CBlood::CBlood(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement = Engine::Get_Management();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pTimeMgr = Engine::Get_TimeMgr();
}


CBlood::~CBlood()
{
	Release();
}

void CBlood::Update()
{
	float fTime = m_pTimeMgr->GetTime();
	if (m_bOn)
	{
		m_fTime += fTime;
		if (m_fTime > 0.8f)
		{
			m_fTime = 0.f;
			m_bOn = FALSE;
		}
		CGameObject::Update();
		SetTransform();
	}
}

void CBlood::Render()
{
	if (m_bOn)
	{
		m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Blood", m_pConvertVertex);

		m_pGraphicDev->GetTransform(D3DTS_VIEW, &m_matView);
		m_pGraphicDev->GetTransform(D3DTS_PROJECTION, &m_matProj);

		m_pMatOrtho = m_pCameraObserver->GetOrtho();
		m_pGraphicDev->SetTransform(D3DTS_VIEW, &m_matIdentity);
		m_pGraphicDev->SetTransform(D3DTS_PROJECTION, m_pMatOrtho);


		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
		m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(180, 255, 255, 255));

		m_pTexture->Render(0);
		m_pBuffer->Render();

		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

		m_pGraphicDev->SetTransform(D3DTS_VIEW, &m_matView);
		m_pGraphicDev->SetTransform(D3DTS_PROJECTION, &m_matProj);
	}
}

HRESULT CBlood::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Blood");
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrImgName);
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CBlood::Initialize()
{
	m_wstrImgName = L"UiDefaultBlood";

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	D3DXMatrixIdentity(&m_matIdentity);

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];


	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Blood", m_pVertex);
	InitVertex();

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = { 0.f, 0.f, 0.0f };
	m_pInfo->m_vScale = { 800.f, 600.f, 0.f };


	return S_OK;
}

void CBlood::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CBlood::BloodStart()
{
	m_bOn = TRUE;
	m_fTime = 0.f;


}


void CBlood::InitVertex()
{
	m_pVertex[0].vPos = { -1.f,  1.f,  0.f };
	m_pVertex[1].vPos = { 1.f,   1.f,  0.f };
	m_pVertex[2].vPos = { 1.f,  -1.f,  0.f };
	m_pVertex[3].vPos = { -1.f, -1.f,  0.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Blood", m_pVertex);
}

void CBlood::SetTransform()
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
}



CBlood* CBlood::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CBlood* pInstance = new CBlood(pGraphicDev);
	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}