#include "stdafx.h"
#include "GoliathRShadow.h"
#include"Export_Function.h"
#include "Include.h"
#include "TexAni.h"

CGoliathRShadow::CGoliathRShadow(LPDIRECT3DDEVICE9 pGraphicDev) 
	:Engine::CGameObject(pGraphicDev)
{

}

CGoliathRShadow::~CGoliathRShadow()
{
	Release();
}

HRESULT CGoliathRShadow::Initialize(void)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pCollisionMgr = CCollisionMgr::GetInstance();
	m_pManagement = Engine::Get_Management();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_wstrStateKey = L"GoliathRShadowAttack";

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	return S_OK;
}


HRESULT CGoliathRShadow::AddComponent(void)
{
	Engine::CComponent* pComponent = nullptr;
	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_GoliathRShadow");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);


	m_pInfo->m_vPos.y = 0.01f;


	return S_OK;
}


void CGoliathRShadow::Update(void)
{
	float fDeltaTime = Engine::Get_TimeMgr()->GetTime();
	Engine::CGameObject::Update();

	float fScale = 0;
	D3DXMATRIX matScale, matTrans;
	fScale = m_vTarget.y /25;
	float fZ = (m_vTarget.y)*0.1f;

	D3DXMatrixScaling(&matScale, 1 - fScale, 1 - fScale, 1 - fScale);
	D3DXMatrixTranslation(&matTrans, m_vTarget.x + 0.1f, 0.01f, m_vTarget.z - 1.3f - fZ);
	m_pInfo->m_matWorld = matScale*matTrans;
	
	SetDirection();
	m_pTerrainVertex = m_pManagement->GetTerrainVertex(Engine::LAYER_GAMEOBJECT, L"Terrain");


	StateKeyChange();
	SetTransform();
}

void CGoliathRShadow::Render(void)
{
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(130, 255, 255, 255));

	m_pTexAni->Render();
	// 반투명 해제
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}

void CGoliathRShadow::Release(void)
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

}
void CGoliathRShadow::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

}

void CGoliathRShadow::SetTransform(void)
{
	const D3DXMATRIX* pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);

}

void CGoliathRShadow::StateKeyChange()
{
	if (m_wstrHandStateKey == L"GoliathRHandAttack")
	{
		m_wstrStateKey = L"GoliathRShadowAttack";
	}
	if (m_wstrHandStateKey == L"GoliathRHandDefense")
	{
		m_wstrStateKey = L"GoliathRShadowDefense";
	}
	m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
	
}

CGoliathRShadow* CGoliathRShadow::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CGoliathRShadow* pInstance = new CGoliathRShadow(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}

