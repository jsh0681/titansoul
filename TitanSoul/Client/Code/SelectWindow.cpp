#include "stdafx.h"
#include "SelectWindow.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"
#include "SelectObj.h"


CSelectWindow::CSelectWindow(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement = Engine::Get_Management();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pTimeMgr = Engine::Get_TimeMgr();
	m_pKeyMgr = Engine::Get_KeyMgr();
}

CSelectWindow::~CSelectWindow()
{
	Release();
}

HRESULT CSelectWindow::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferName);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrImgName);
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CSelectWindow::Initialize(const wstring& wstrTalk1, const wstring& wstrTalk2, const wstring& wstrTalk3)
{
	m_wstrTalk1 = wstrTalk1;
	m_wstrTalk2 = wstrTalk2;
	m_wstrTalk3 = wstrTalk3;

	m_wstrTalkSum = m_wstrTalk1 + L"\n" + m_wstrTalk2 + L"\n" + m_wstrTalk3;

	m_wstrBufferName = L"Buffer_TalkWindow";		// window�� ���̾�
	m_wstrImgName	 = L"UiDefaultWindow";			// window�� ���̾�

	m_pSelectObj = CSelectObj::Create(m_pGraphicDev, { -360.f, 270.f, 0.09f });
	NULL_CHECK_RETURN(m_pSelectObj, E_FAIL);

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	D3DXMatrixIdentity(&m_matIdentity);

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);
	InitVertex();

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = { 0.f, 230.f, 0.1f };
	m_pInfo->m_vScale = { 400.f, 70.f, 0.f };

	m_tRc = { 60, 20, 700,180 };
	m_pFont = m_pManagement->GetRender()->GetFont();

	return S_OK;
}

void CSelectWindow::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete(m_pSelectObj);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CSelectWindow::Update()
{
	if (m_bOn)
	{
		CGameObject::Update();
		SetTransform();

		m_pSelectObj->Update();
		PushKey();
	}
}

void CSelectWindow::Render()
{
	if (m_bOn)
	{
		m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pConvertVertex);

		m_pGraphicDev->GetTransform(D3DTS_VIEW, &m_matView);
		m_pGraphicDev->GetTransform(D3DTS_PROJECTION, &m_matProj);

		m_pMatOrtho = m_pCameraObserver->GetOrtho();
		m_pGraphicDev->SetTransform(D3DTS_VIEW, &m_matIdentity);
		m_pGraphicDev->SetTransform(D3DTS_PROJECTION, m_pMatOrtho);

		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
		m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(180, 255, 255, 255));

		m_pTexture->Render(0);
		m_pBuffer->Render();

		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

		m_pSelectObj->Render();

		m_pGraphicDev->SetTransform(D3DTS_VIEW, &m_matView);
		m_pGraphicDev->SetTransform(D3DTS_PROJECTION, &m_matProj);

		m_pFont->DrawText(nullptr, m_wstrTalkSum.c_str(), lstrlen(m_wstrTalkSum.c_str()), &m_tRc, DT_NOCLIP, D3DCOLOR_ARGB(255, 0, 0, 0));
	}
}

void CSelectWindow::InitVertex()
{
	m_pVertex[0].vPos = { -1.f,  1.f,  0.f };
	m_pVertex[1].vPos = { 1.f,   1.f,  0.f };
	m_pVertex[2].vPos = { 1.f,  -1.f,  0.f };
	m_pVertex[3].vPos = { -1.f, -1.f,  0.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);
}

void CSelectWindow::SetTransform()
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
}

void CSelectWindow::PushKey()
{
	if (m_pKeyMgr->KeyDown(KEY_DOWN))
	{
		++m_iSelect;
		m_pSelectObj->SetPosY(m_pSelectObj->GetPosY() - 20.f);

		if (m_iSelect > 2)
		{
			m_iSelect = 0;
			m_pSelectObj->SetPosY(270.f);
		}
	}
	else if (m_pKeyMgr->KeyDown(KEY_UP))
	{
		--m_iSelect;
		m_pSelectObj->SetPosY(m_pSelectObj->GetPosY() + 20.f);

		if (m_iSelect < 0)
		{
			m_iSelect = 2;
			m_pSelectObj->SetPosY(230.f);
		}
	}
}

CSelectWindow* CSelectWindow::Create(LPDIRECT3DDEVICE9 pGraphicDev, const wstring& wstrTalk1, const wstring& wstrTalk2, const wstring& wstrTalk3)
{
	CSelectWindow* pInstance = new CSelectWindow(pGraphicDev);
	if (FAILED(pInstance->Initialize(wstrTalk1, wstrTalk2, wstrTalk3)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
