#include "stdafx.h"

#include "Yeti_Move.h"
CYeti_Move::CYeti_Move(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti)
	: CYetiState(pGraphicDev, pYeti)
{
}

CYeti_Move::~CYeti_Move()
{
}

HRESULT CYeti_Move::Initialize()
{
	m_pYeti->SetOverlap(0);
	m_fTime = 0.f;
	m_iRand = 0;
	return S_OK;
}

void CYeti_Move::Update(const float & fTime)
{
	m_fTime += fTime;

	if (m_fTime >= 2.f)//특정 시간이 지나면 랜덤함수를 굴려 공격 패턴을 설정
	{
		m_pYeti->SetState(CYeti::YETI_THROW);
	}
}

void CYeti_Move::Release()
{
}


CYeti_Move * CYeti_Move::Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti * pYeti)
{
	CYeti_Move* pInstance = new CYeti_Move(pGraphicDev, pYeti);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}