#ifndef Icicle_h__
#define Icicle_h__

#include "GameObject.h"

namespace Engine
{
	class CTransform;
	class CTimeMgr;
	class CResourcesMgr;
	class CInfoSubject;
	class CVIBuffer;
	class CTexture;
}
class CCameraObserver;
class CSphereColBox;
class CIcicle
	:public Engine::CGameObject

{
private:
	explicit CIcicle(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CIcicle();
public:
	virtual HRESULT Initialize(const D3DXVECTOR3& vPos);
	virtual void Update();
	virtual void Render();
	virtual void Release();
	virtual HRESULT AddComponent();
	virtual void SetTansform();
	virtual void InitVertex();

public:
	Engine::CTransform* GetInfo() { return m_pInfo; };
	void BreakIce();
	const float&   GetRadius();
	const bool&		GetBreak() { return m_bBreakMode; };

public:
	static CIcicle* Create(LPDIRECT3DDEVICE9 pDevice, const D3DXVECTOR3& vPos);

private:
	Engine::CResourcesMgr*		m_pResourcesMgr = nullptr;
	Engine::CTimeMgr*			m_pTimeMgr = nullptr;
	Engine::CInfoSubject*		m_pInfoSubject = nullptr;

	Engine::CTransform*			m_pInfo;
	CCameraObserver*			m_pCameraObserver;

	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;

	CSphereColBox*				m_pSphereBox = nullptr;

	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;

	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...

	DWORD m_dwVtxCnt = 4;
	float		m_fSpeed = 0.f;

	float m_fJumpPow = 0.f;
	float m_fJumpAcc = 0.f;
	int	  m_iResourceIndex = 0;
	bool  m_bBreakMode = FALSE;
};

#endif // SnowBullet_h__