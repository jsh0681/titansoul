#ifndef Yeti_Throw_h__
#define Yeti_Throw_h__

#include "YetiState.h"

class CYeti;
class CYeti_Throw
	: public CYetiState

{
public:
	CYeti_Throw(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
	virtual ~CYeti_Throw();

public:
	virtual HRESULT Initialize();
	virtual void Update(const float& fTime);
	virtual void Release();
public:
	static CYeti_Throw*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);


private:
	bool	m_bPreventOverlap = FALSE; //눈덩이 굴리기 중복방지 (한프레임에서 같은 인덱스가 나올 때 눈덩이 계속 굴리기 방지)
	int		m_iCnt =0; //눈덩이를 얼마나 굴렸는지 판단할 변수. 
	float	m_fDegree = 0.f; //각도는 이걸 이용하자. 여기서 처음에 각도 초기화 ㄱ;
};

#endif // Yeti_Idle_h__
