#ifndef NormalEffect_h__
#define NormalEffect_h__

#include "GameObject.h"
#include "Export_Function.h"
#include "Engine_Include.h"

class CCameraObserver;
class CNormalEffect
	:public Engine::CGameObject
{
public:
	enum { TIMEEFFECT, INDEXEFFECT};
	CNormalEffect(LPDIRECT3DDEVICE9 pGraphicDev);
	virtual ~CNormalEffect();

	virtual HRESULT Initialize();
	virtual void Render(void) = 0;
	virtual void Release(void) = 0;
	
public:
	const bool&		CheckIsDead() { return m_bIsDead; };

protected:
	NorEffect			m_tEffect;
	wstring m_wstrBufferKey;
	wstring m_wstrTextureKey;

	float m_fSpeed = 0.f;
	float m_fLifeTime = 0.f;
	
	bool  m_bIsDead = FALSE;

	Engine::CVIBuffer*				m_pBufferCom = nullptr;
	Engine::CTexture*				m_pTextureCom = nullptr;
	CCameraObserver*				m_pCameraObserver = nullptr;

	Engine::VTXTEX*					m_pVertex = nullptr;
	Engine::VTXTEX*					m_pConvertVertex = nullptr;

	DWORD							m_dwVtxCnt = 4;
	int								m_iIndex = 0;
};

#endif // NormalEffect_h__
