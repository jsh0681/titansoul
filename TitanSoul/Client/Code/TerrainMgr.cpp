#include "stdafx.h"
#include "TerrainMgr.h"

#include "Export_System.h"
#include "Export_Resources.h"

#include "Terrain.h"
#include "Plane.h"

IMPLEMENT_SINGLETON(CTerrainMgr)

CTerrainMgr::CTerrainMgr()
{
	m_pGraphicDev = Engine::Get_GraphicDev()->GetDevice();
}

CTerrainMgr::~CTerrainMgr()
{
	Release();
}

void CTerrainMgr::Release()
{
	// Terrain ����
	for_each(m_mapTerrain.begin(), m_mapTerrain.end(), [](auto& MyPair)
	{
		Engine::Safe_Delete(MyPair.second);
	});
	m_mapTerrain.clear();

	// Plane ����
	for (auto& it : m_mapPlane)
	{
		for_each(it.second->begin(), it.second->end(), Engine::Safe_Delete<CPlane*>);
		it.second->clear();
		Engine::Safe_Delete(it.second);
	}
	m_mapPlane.clear();

	for (auto& CubeInfo : m_pCubeInfoList)
	{
		Engine::Safe_Delete(CubeInfo);
	}
}

HRESULT CTerrainMgr::CreateTerrain(MAPINFO* pMapInfo)
{
	pMapInfo->wstrMapBufferKey = L"Buffer_" + pMapInfo->wstrMapKey;
	FAILED_CHECK_RETURN(CreateTerrainBuffer(pMapInfo->wstrMapBufferKey, int(pMapInfo->fSizeZ * 10 / 2), int(pMapInfo->fSizeX * 10 / 2)), E_FAIL);

	CTerrain* pTerrain = CTerrain::Create(m_pGraphicDev, pMapInfo);
	NULL_CHECK_RETURN(pTerrain, E_FAIL);

	m_mapTerrain.emplace(pMapInfo->wstrMapKey, pTerrain);
	return S_OK;
}

HRESULT CTerrainMgr::CreatePlane(const wstring& wstrTerrainName, list<PLANEINFO*>& pPlaneInfoLst)
{
	auto& it_find = m_mapPlane.find(wstrTerrainName);

	if (it_find != m_mapPlane.end())
		return E_FAIL;

	m_mapPlane[wstrTerrainName] = new list<CPlane*>();

	for (auto& it : pPlaneInfoLst)
	{
		SetPlaneInfoText(it);
		CPlane* pPlane = CPlane::Create(m_pGraphicDev, wstrTerrainName, it);

		m_mapPlane[wstrTerrainName]->push_back(pPlane);
	}
	return S_OK;
}

HRESULT CTerrainMgr::CreateBrick(list<CUBEINFO*>& pCubeInfoLst)
{
	m_pCubeInfoList = pCubeInfoLst;
	return S_OK;
}

CTerrain* CTerrainMgr::GetTerrain(const wstring& wstrTerrainName)
{
	auto& it_find = m_mapTerrain.find(wstrTerrainName);

	if (it_find == m_mapTerrain.end())
		return nullptr;

	return it_find->second;
}

const list<CPlane*>* CTerrainMgr::GetPlane(const wstring& wstrTerrainName)
{
	auto& it_find = m_mapPlane.find(wstrTerrainName);

	if (it_find == m_mapPlane.end())
		return nullptr;

	return it_find->second;
}

const MAPINFO* CTerrainMgr::GetMapInfo(const wstring& wstrTerrainName)
{
	auto& it_find = m_mapTerrain.find(wstrTerrainName);

	if (it_find == m_mapTerrain.end())
		return nullptr;

	return it_find->second->GetDataInfo();
}

CTerrain* CTerrainMgr::Clone(const wstring& wstrTerrainName)
{
	auto& it_find = m_mapTerrain.find(wstrTerrainName);
	if (it_find == m_mapTerrain.end())
		return nullptr;

	auto& Plane_find = m_mapPlane.find(wstrTerrainName);
	list<CPlane*>* pPlaneList = nullptr;

	if (Plane_find != m_mapPlane.end())
		pPlaneList = Plane_find->second;

	CTerrain* pTerrain = it_find->second->Clone();
	pTerrain->SetPlaneList(pPlaneList);

	return pTerrain;
}

void CTerrainMgr::SetPlaneInfoText(PLANEINFO* pPlaneInfo)
{
	pPlaneInfo->wstrPlaneBufferKey = L"Buffer_Plane";
	pPlaneInfo->wstrPlaneTexKey = L"PlaneColorImg";
}

HRESULT CTerrainMgr::CreateTerrainBuffer(const wstring& wstrBufferKey, const int& iHeight, const int& iWidth)
{
	FAILED_CHECK_RETURN(Engine::Get_ResourceMgr()->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		wstrBufferKey, iWidth, iHeight),
		E_FAIL);

	return S_OK;
}
