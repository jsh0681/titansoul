#ifndef LogoButton_h__
#define LogoButton_h__

 /*!
 * \class CLogoButton
 *
 * \간략정보 Logo에서 나오는 버튼을 보여줄 클래스
 *
 * \상세정보
 *
 * \작성자 윤유석
 *
 * \date 1월 5 2019
 *
 */

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
	class CResourcesMgr;
	class CTimeMgr;
	class CInfoSubject;
}

class CCameraObserver;
class CLogoButton : public Engine::CGameObject
{
private:
	explicit CLogoButton(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CLogoButton();

private:
	HRESULT	AddComponent();
	virtual HRESULT	Initialize(const wstring& wstrAnikey, const int& iTexCnt, const D3DXVECTOR3& vPos);
	virtual void	Release();

public:
	virtual void Update()	override;
	virtual void Render()	override;

private:
	void		SetTransform();

public:
	void		SetIndex(const int& iIndex)			{ m_iTexCnt = iIndex; }
	void		AddAngleX(const float& fAngle);
	const float& GetAngleX();

private:
	Engine::CResourcesMgr*		m_pResourcesMgr;
	Engine::CTimeMgr*			m_pTimeMgr;
	Engine::CInfoSubject*		m_pInfoSubject = nullptr;

	Engine::CVIBuffer*			m_pBufferCom	= nullptr;
	Engine::CTexture*			m_pTextureCom	= nullptr;
	Engine::CTransform*			m_pInfo			= nullptr;

	CCameraObserver*			m_pCameraObserver = nullptr;

	wstring						m_wstrTexKey = L"";
	int							m_iTexCnt = 0;

	DWORD						m_dwVtxCnt = 0;
	float						m_fHeight = 0.f;
	float						m_fWidth = 0.f;

	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;


public:
	static CLogoButton* Create(LPDIRECT3DDEVICE9 pGraphicDev, const wstring& wstrAnikey, 
								const int& iTexCnt, const D3DXVECTOR3& vPos);
};

#endif // !LogoButton_h__