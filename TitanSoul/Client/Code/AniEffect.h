#ifndef AniEffect_h__
#define AniEffect_h__

#include "NormalEffect.h"

namespace Engine
{
	class CTimeMgr;
}

class CTexAni;
class CAniEffect : public CNormalEffect
{
public:
	enum OPTION {UP, UPDOWN };
private:
	explicit CAniEffect(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CAniEffect();

private:
	virtual HRESULT Initialize(const NorEffect& tNorEffect, const wstring& wstrBuffer, const wstring& wstrAni);
	virtual HRESULT AddComponent();
	virtual void Release(void) override;

	virtual void SetTransform();

public:
	virtual void Update();
	virtual void Render();

public:
	void	ResetEffect();
	void	SetOption(const OPTION& eOption)	{ m_eOption = eOption; }

private:
	virtual void InitVertex();

	void	Up(const float& fDelata);
	void	UpDown(const float& fDelata);

private:
	Engine::CTimeMgr* m_pTimeMgr;
	CTexAni*		m_pTexAni = nullptr;

	float		m_fDegree = 0.f;
	float		m_fScale = 0.f;
	float		m_fSpeed = 0.f;
	float		m_fSpeed2 = 0.f;
	D3DXVECTOR3 m_vAccel;
	bool		m_bAcc = FALSE;
	bool		m_bAngleDir = FALSE;

	OPTION		m_eOption = UP;


public:
	static CAniEffect* Create(LPDIRECT3DDEVICE9 pGraphicDev, const NorEffect& tNorEffect, const wstring& wstrBuffer, const wstring& wstrAni);
};

#endif // AniEffect_h__
