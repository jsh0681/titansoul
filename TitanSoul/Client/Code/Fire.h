#ifndef Fire_h__
#define Fire_h__

#include "Export_Function.h"
#include "GameObject.h"
#include "CameraObserver.h"


namespace Engine
{
	class CScene;
}
class CTexAni;

class CFire : public Engine::CGameObject
{
private:
	explicit CFire(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CFire();

public:

	Engine::CTransform* GetInfo() { return m_pInfo; }

	virtual void	Update(void);
	virtual void	Render(void);
	virtual		void SetTransform(void);
	virtual		void SetDirection(void);

	void		SetTurn(bool bResult) { m_bTurn = bResult; };
private:
	void		Release(void);

	virtual		HRESULT AddComponent(D3DXVECTOR3 vStartPos);
	virtual		HRESULT Initialize(D3DXVECTOR3 vStartPos);

	D3DXVECTOR3 m_vTarget;
	CCameraObserver*				m_pCameraObserver = nullptr;


	D3DXVECTOR3						m_vDestPos;
public:

	static CFire*		Create(LPDIRECT3DDEVICE9 pGraphicDev,D3DXVECTOR3 vStartPos);
private:
	bool m_bTurn = false;
	float m_fDegree = 0.f;
	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...
	CTexAni* m_pTexAni = nullptr;

	int m_iIndex = 0;
};
#endif // Goliath_h__


