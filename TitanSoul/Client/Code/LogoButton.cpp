#include "stdafx.h"
#include "LogoButton.h"

#include "Export_Function.h"
#include "Include.h"

#include "CameraObserver.h"

CLogoButton::CLogoButton(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev),
	m_pResourcesMgr(Engine::Get_ResourceMgr()),
	m_pTimeMgr(Engine::Get_TimeMgr()),
	m_pInfoSubject(Engine::Get_InfoSubject())
{
}


CLogoButton::~CLogoButton()
{
	Release();
}

HRESULT CLogoButton::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_LogoButton");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer",m_pBufferCom);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrTexKey);
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTextureCom, E_FAIL);
	m_mapComponent.emplace(L"Texture",m_pTextureCom);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform",pComponent);

	return S_OK;
}

HRESULT CLogoButton::Initialize(const wstring& wstrTexkey, const int& iTexCnt, const D3DXVECTOR3& vPos)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_LogoButton", m_pVertex);

	m_wstrTexKey = wstrTexkey;
	m_iTexCnt = iTexCnt;
	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	
	m_fHeight = 1.4f;
	if (wstrTexkey == L"Texture_Start")
		m_fWidth = 5.8f;
	else if (wstrTexkey == L"Texture_Option")
		m_fWidth = 7.8f;
	else
		m_fWidth = 4.2f;

	m_pInfo->m_vScale = {3.f, 3.f, 1.f };
	m_pInfo->m_vPos = vPos;

	return S_OK;
}

void CLogoButton::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CLogoButton::Update()
{
	CGameObject::Update();
	SetTransform();
}

void CLogoButton::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_LogoButton", m_pConvertVertex);

	// 알파블랜딩
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//m_pGraphicDev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
	//m_pGraphicDev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
	// 알파 테스트
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000088);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	m_pTextureCom->Render(m_iTexCnt);
	m_pBufferCom->Render();

	//m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CLogoButton::SetTransform()
{
	D3DXMATRIX matView;
	const D3DXMATRIX* matOrtho = m_pCameraObserver->GetOrtho();

	D3DXMatrixIdentity(&matView);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		m_pConvertVertex[i].vPos.y *= m_fHeight;
		m_pConvertVertex[i].vPos.x *= m_fWidth;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &matView);
		if (m_pConvertVertex[i].vPos.z < 1.f)
			m_pConvertVertex[i].vPos.z = 1.f;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, matOrtho);
	}
}

void CLogoButton::AddAngleX(const float& fAngle)
{
	m_pInfo->m_fAngle[Engine::ANGLE_X] -= fAngle;
}

const float& CLogoButton::GetAngleX()
{
	return m_pInfo->m_fAngle[Engine::ANGLE_X];
}

CLogoButton* CLogoButton::Create(LPDIRECT3DDEVICE9 pGraphicDev, const wstring& wstrTexkey, 
								const int& iTexCnt, const D3DXVECTOR3& vPos)
{
	CLogoButton*	pInstance = new CLogoButton(pGraphicDev);

	if (FAILED(pInstance->Initialize(wstrTexkey, iTexCnt, vPos)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
