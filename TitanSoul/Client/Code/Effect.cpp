#include "stdafx.h"
#include "Effect.h"

#include "Export_Function.h"

CEffect::CEffect(LPDIRECT3DDEVICE9 pGraphicDev):
	m_pGraphicDev(pGraphicDev),
	m_pResourceMgr(Engine::Get_ResourceMgr()),
	m_pTimeMgr(Engine::Get_TimeMgr()) 
{

}

CEffect::~CEffect(void)
{
	Release();
}


void CEffect::Release()
{
	Engine::Safe_Delete(m_pBuffer);
	Engine::Safe_Delete(m_pTexture);

	m_pResourceMgr = nullptr;
	m_pGraphicDev = nullptr;
	m_pTimeMgr = nullptr;
}



HRESULT CEffect::CreateBuffer(EFFECTINFO* pEffectInfo)
{
	NULL_CHECK_RETURN(pEffectInfo, E_FAIL);
	m_pEffectInfo = pEffectInfo;

	Engine::CComponent* pComponent = nullptr;
	pComponent = m_pResourceMgr->Clone(Engine::RESOURCE_STATIC, m_pEffectInfo->wstrBufferKey);

	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	return S_OK;

}

HRESULT CEffect::CreateTexture(EFFECTINFO* pEffectInfo)
{
	NULL_CHECK_RETURN(pEffectInfo, E_FAIL);
	m_pEffectInfo = pEffectInfo;

	Engine::CComponent* pComponent = nullptr;
	pComponent = m_pResourceMgr->Clone(Engine::RESOURCE_STATIC, m_pEffectInfo->wstrResourceKey);

	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);

	return S_OK;
}


void CEffect::Update(void)
{
	//float fDeltaTime = m_pTimeMgr->GetTime();
	//if (m_iIndex > m_pEffectInfo->wCnt)
	//{
	//	return;
	//}
	//else if (fDeltaTime > 0.2f)
	//{
	//	m_iIndex++;
	//	return;
	//}
}

void CEffect::Render(void)
{
	m_pTexture->Render(m_iIndex);
	m_pBuffer->Render();
}

