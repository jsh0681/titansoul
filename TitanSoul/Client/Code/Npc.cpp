#include "stdafx.h"
#include "Npc.h"

#include "Export_Function.h"

#include "Include.h"


CNpc::CNpc(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement	= Engine::Get_Management();
	m_pInfoSubject	= Engine::Get_InfoSubject();
	m_pKeyMgr		= Engine::Get_KeyMgr();
	m_pTimeMgr		= Engine::Get_TimeMgr();
}


CNpc::~CNpc()
{
}
