#include "stdafx.h"
#include "SludgePart.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"
#include "CameraObserver.h"

CSludgePart::CSludgePart(PDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();
}


CSludgePart::~CSludgePart()
{
	Release();
}

void CSludgePart::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CSludgePart::Update()
{
	Engine::CGameObject::Update();
}

void CSludgePart::SetSynchPos(const D3DXVECTOR3& vPos)
{
	m_pInfo->m_vPos = vPos;
}

void CSludgePart::SetSynchPosXZ(const float& fX, const float& fZ)
{
	m_pInfo->m_vPos.x = fX;
	m_pInfo->m_vPos.z = fZ;
}

void CSludgePart::SetSynchDir(const D3DXVECTOR3& vDir)
{
	m_pInfo->m_vDir = vDir;
}

void CSludgePart::DownGrade(const int& iSize)
{
	m_iIndex = iSize;

	ResetSize();
	InitSize();
}

const float& CSludgePart::GetY()
{
	return m_pInfo->m_vPos.y;
}

