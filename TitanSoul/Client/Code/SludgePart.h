#ifndef SludgePart_h__
#define SludgePart_h__
/*!
 * \class CSludgePart
 *
 * \간략정보 슬라임 부품들의 부모
 *
 * \상세정보 
 *
 * \작성자 윤유석
 *
 * \date 1월 10 2019
 *
 */

#include "GameObject.h"

namespace Engine
{
	// GameObject에 ResourcesMgr, Menagement, Transform, infoSubject 있음
	class CVIBuffer;
	class CTexture;
}

class CCameraObserver;
class CSludgePart : public Engine::CGameObject
{
protected:
	explicit CSludgePart(PDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CSludgePart();	

public:
	virtual void	Release();
	virtual HRESULT	AddComponent()				PURE;

public:
	virtual void Update();
	virtual void Render() {};

	virtual void SetTransform()					PURE;
	virtual void ResetSize()					PURE;

	virtual void SetTargetPos(const D3DXVECTOR3& vPos)	{ m_vTargetPos = vPos; }
	virtual void SetSynchPos(const D3DXVECTOR3& vPos);
	virtual void SetSynchPosXZ(const float& fX, const float& fZ);
	virtual void SetSynchDir(const D3DXVECTOR3& vDir);

	virtual void DownGrade(const int& iSize);
	virtual void InitSize()						PURE;

	virtual const float& GetY();

public:
	virtual bool	IdleFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) PURE;
	virtual bool	JumpFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) PURE;
	virtual bool	FallFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) PURE;
	virtual bool	SplitFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime,const bool& bJump) PURE;

protected:
	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;

	Engine::VTXTEX*			m_pVertex			= nullptr;
	Engine::VTXTEX*			m_pConvertVertex	= nullptr;

	CCameraObserver*		m_pCameraObserver	= nullptr;

	DWORD					m_dwVtxCnt;

	D3DXVECTOR3				m_vTargetPos;

	int						m_iIndex = 0;
	int						m_iState = 0;
};

#endif // !SludgePart_h__