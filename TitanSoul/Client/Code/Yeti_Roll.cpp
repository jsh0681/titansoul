#include "stdafx.h"

#include "Yeti_Roll.h"
#include "Transform.h"
CYeti_Roll::CYeti_Roll(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti)
	:CYetiState(pGraphicDev,pYeti)

{
}

CYeti_Roll::~CYeti_Roll()
{
}

HRESULT CYeti_Roll::Initialize()
{
	CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Yeti_Roll.ogg", CSoundMgr::SOUND_BOSS);
	//플레이어와의 각도를 구해서 예티의 방향을 설정해주고
	m_fDegree = m_pYeti->GetPlayerDegree();
	m_pYeti->ChangeDirection(m_fDegree);
	m_pYeti->SettingDirection(m_fDegree);
	m_vReverseDir = m_pYeti->GetInfo()->m_vDir * -1.f;
	D3DXVec3Normalize(&m_vReverseDir, &m_vReverseDir);

	m_pYeti->SetStartVector();

	m_pYeti->SetRadius(2.0f);
	
	return S_OK;
}

void CYeti_Roll::Update(const float & fTime)
{
	m_fTimeSave += fTime;

	if (!m_pYeti->GetJump())
	{
		if (m_fTimeSave > 0.5f)
		{
			m_pYeti->SetOverlap(1);
		}
		m_pYeti->Move(fTime, 20.f);
	}
	else
	{
		if (m_bPreventOverlap == FALSE)
		{
			m_pYeti->ChangeDirection(-m_fDegree); //방향 반대로 전환
			m_pYeti->SetDir(m_vReverseDir);
			m_vReverseDir = m_pYeti->GetInfo()->m_vDir * -1.f;
			D3DXVec3Normalize(&m_vReverseDir, &m_vReverseDir);
			m_bPreventOverlap = TRUE;
		}
		bool bResult = m_pYeti->MovewithAccel(m_vReverseDir, fTime, 5.f);

		if (!bResult)
		{
			////이게 완전히 착지시... Move대신 Land가 들어가야해.
			m_pYeti->SetJump(0);
			m_pYeti->SetState(CYeti::YETI_LAND);
			--m_pYeti->SetRollCnt();
			return;
		}
	}
}

void CYeti_Roll::Release()
{
}

CYeti_Roll * CYeti_Roll::Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti * pYeti)
{
	CYeti_Roll* pInstance = new CYeti_Roll(pGraphicDev, pYeti);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}