#ifndef YetiState_h__
#define YetiState_h__

#include "Include.h"
#include "Engine_Include.h"
#include "Yeti.h"
class CYeti;
class CYetiState
{
public:
	CYetiState(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
	virtual ~CYetiState();

public:
	virtual HRESULT Initialize() PURE;
	virtual void Update(const float& fTime) PURE;
	virtual void Release() PURE;

protected:
	LPDIRECT3DDEVICE9 m_pGraphicDev = nullptr;
	CYeti*			  m_pYeti = nullptr;

	
};

#endif // YetiState_h__
