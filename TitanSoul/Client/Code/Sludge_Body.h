#ifndef CSludge_Body_h__
#define CSludge_Body_h__


#include "SludgePart.h"


class CSludge_Body : public CSludgePart
{
private:
	explicit CSludge_Body(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CSludge_Body();
private:
	virtual HRESULT Initialize(const D3DXVECTOR3& vPos, const int& iIndex);
	virtual HRESULT AddComponent()				override;

	virtual void	Update();
	virtual void	Render();

	virtual void	Release();

public:
	virtual void SetTransform()					override;
	virtual void ResetSize()					override;
	virtual void InitSize()						override;
	
private:
	virtual bool	IdleFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	JumpFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	FallFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	SplitFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime, const bool& bJump) override;

public:
	static CSludge_Body*	Create(LPDIRECT3DDEVICE9 pGraphicDev, const D3DXVECTOR3& vPos, const int& iIndex);
};

#endif // !#define CSludge_Body_h__