#ifndef SceneSelector_h__
#define SceneSelector_h__

#include "Include.h"

#include "Logo.h"
#include "Map0.h"
#include "Map1.h"
#include "Stage0.h"
#include "Stage1.h"
#include "Stage2.h"
#include "Stage3.h"
#include "Stage4.h"
#include "Stage5.h"


namespace Engine
{
	class CCamera;
}

class CPlayer;
class CSceneSelector
{
public:
	explicit CSceneSelector(SCENEID eScene, CPlayer* pPlayer, Engine::CCamera* pCamera)
		: m_eScene(eScene),m_pPlayer(pPlayer), m_pCamera(pCamera)
	{	}

public:
	HRESULT		operator()(Engine::CScene** ppScene, 
							LPDIRECT3DDEVICE9 pGraphicDev)
	{
		switch (m_eScene)
		{
		case SC_LOGO:
			*ppScene = CLogo::Create(pGraphicDev);
			break;
		case SC_MAP0_ST:
			*ppScene = CMap0::Create(pGraphicDev);
			break;
		case SC_MAP0:
			*ppScene = CMap0::Create(pGraphicDev, m_pPlayer,m_pCamera);
			break;
		case SC_MAP1:
			*ppScene = CMap1::Create(pGraphicDev, m_pPlayer,m_pCamera);
			break;
		case SC_STAGE0:
			*ppScene = CStage0::Create(pGraphicDev, m_pPlayer, m_pCamera);
			break;
		case SC_STAGE1:
			*ppScene = CStage1::Create(pGraphicDev, m_pPlayer, m_pCamera);
			break;
		case SC_STAGE2:
			*ppScene = CStage2::Create(pGraphicDev, m_pPlayer, m_pCamera);
			break;
		case SC_STAGE3:
			*ppScene = CStage3::Create(pGraphicDev, m_pPlayer, m_pCamera);
			break;
		case SC_STAGE4:
			*ppScene = CStage4::Create(pGraphicDev, m_pPlayer, m_pCamera);
			break;
		case SC_STAGE5:
			*ppScene = CStage5::Create(pGraphicDev, m_pPlayer, m_pCamera);
			break;
		}
		NULL_CHECK_RETURN(*ppScene, E_FAIL);

		return S_OK;
	}


private:
	SCENEID		m_eScene;
	CPlayer*	m_pPlayer;

	Engine::CCamera* m_pCamera;
};

#endif // SceneSelector_h__
