#include "stdafx.h"
#include "Knuckle_Chain.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"

CKnuckle_Chain::CKnuckle_Chain(LPDIRECT3DDEVICE9 pGraphicDev) :
	CKnuckle_Part(pGraphicDev)
{
}


CKnuckle_Chain::~CKnuckle_Chain()
{
	Release();
}

HRESULT CKnuckle_Chain::Initialize(const VEC3& vPos, const int& iDir, const int& iIndex)
{
	m_dwVtxCnt = 4;

	m_wstrBufferName = L"Buffer_Knuckle_Chain";

	if(iDir == 0)
		m_eDir = C_LEFT;
	else 
		m_eDir = C_RIGHT;
	m_iIndex = iIndex;

	// 부모에서 지워줌(카메라, 버텍스, 컴포넌트)
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = vPos;
	m_pInfo->m_vScale = { 0.5f, 1.f, 0.5f };

	m_pInfo->m_vPos.x += (float)iIndex * 0.3f * m_eDir;
	m_pInfo->m_vPos.z -= (float)iIndex * 0.1f;
	m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(45.f);

	ResetBuffer();

	return S_OK;
}

HRESULT CKnuckle_Chain::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferName);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"KnuckleChainNo");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

void CKnuckle_Chain::Update()
{
	UpdatePos();
	switch (m_eState)
	{
	case CKnuckle_Part::ATTACK_ROT:
		AttackRotation();
		break;
	case CKnuckle_Part::ATTACK_JUMP:
		AttackJump();
		break;
	}
	CKnuckle_Part::Update();
	SetTransform();
}

void CKnuckle_Chain::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pConvertVertex);

	m_pTexture->Render(0);
	m_pBuffer->Render();
}

void CKnuckle_Chain::SetBodyPos(const VEC3* vPos)
{
	if (vPos != nullptr)
		m_pBodyPos = vPos;
}

void CKnuckle_Chain::SetMacePos(const VEC3* vPos)
{
	if (vPos != nullptr)
		m_pMacePos = vPos;
}

void CKnuckle_Chain::UpdatePos()
{
	if (m_pBodyPos == nullptr)
		return;
	if (m_pMacePos == nullptr)
		return;

	m_pInfo->m_vDir = (*m_pMacePos) - (*m_pBodyPos);
	m_pInfo->m_vDir *= ((m_iIndex + 1) * 0.1f);

	m_pInfo->m_vPos = (*m_pBodyPos) + m_pInfo->m_vDir;
	m_pInfo->m_vPos.y = BOSS_Y;
}

void CKnuckle_Chain::AttackRotation()
{
}

void CKnuckle_Chain::AttackJump()
{
	m_pInfo->m_vPos.y = ((*m_pBodyPos) + m_pInfo->m_vDir).y;
}

void CKnuckle_Chain::SetTransform()
{
	const D3DXMATRIX* matView = m_pCameraObserver->GetView();
	const D3DXMATRIX* matProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, matView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, matProj);
}

void CKnuckle_Chain::Release()
{
	m_pBodyPos = nullptr;
	m_pMacePos = nullptr;
	ResetBuffer();
}

CKnuckle_Chain* CKnuckle_Chain::Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const int& iDir, const int& iIndex)
{
	CKnuckle_Chain* pInstance = new CKnuckle_Chain(pGraphicDev);

	if (FAILED(pInstance->Initialize(vPos, iDir, iIndex)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}