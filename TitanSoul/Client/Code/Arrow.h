#ifndef Arrow_h__
#define Arrow_h__
#define FULLGAUAGE 0.8f
#include "GameObject.h"
#include "CameraObserver.h"
#include "Export_Function.h"
namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
	class CResourcesMgr;
	class CInfoSubject;
	class CScene;
}

class CSphereColBox;
class CArrow
	: public Engine::CGameObject
{

private:
	explicit CArrow(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CArrow();

	enum ARROWSTATE { A_IDLE, A_ZOOM, A_SHOT, A_RECOVERY, A_STOP };
public:
	void		Update(void);
	void		Render(void);

	void		SetValid(bool _bResult); //유효여부 설정
	const bool&	GetValid() { return m_bValid; }; //유효 검사
	void		SetY(float y) { m_pInfo->m_vPos.y = y; }
	void		ResetData();
	void		SettingStartPoint(); //화살 시작점 조정
	bool		Shout();// 화살발사!!!
	void		DirectionChange(const float& _fAngle);
	bool		Recovery(const float & _fTime, const bool& bResult);
	bool		Get_DirectLength(); //액셀과 방향벡터의 거리를 반환... 날아가는 화살이 회수될 타이밍을 이 길이를 이용해서 결정할거임.
	void		StopArrow(); //임시함수임. 화살 벽에 박히면 정지.
	void		StopRecovery(const float& fTime); //회수하면서 벽에 부딧칠때...;
	void		Reflect(const D3DXVECTOR3& _vDir, const D3DXVECTOR3& _vPoint);

	const float&   GetRadius();

public:
	Engine::CTransform* Get_Trnasform() { return m_pInfo; };
	bool				Get_Recovery() { return m_bIsRecovery; };
	D3DXVECTOR3&		Get_ColPos() { return m_vPosCol; };
	D3DXVECTOR3&		Get_BackPos() { return m_vBackPos; };
	D3DXVECTOR3&		Get_StartVector() { return m_vStart; };
	bool				Get_Shouting() { return m_bIsShouting; };
	bool				Get_Ready() { return m_bReady; };
	bool				Get_PressingRecovery() { return m_bPressRecovery; };
	bool				Set_PressingRecovery() { return m_bPressRecovery = FALSE; };
	bool				Set_CollsionPlane() { return m_bCollisionPlane = TRUE; };
	void				SetScene(Engine::CScene* pScene) { m_pScene = pScene; };
	void		Set_Angle(const float& _fAngle) { m_fAngle = _fAngle; };
private:
	HRESULT		AddComponent(void);
	HRESULT		Initialize(void);
	void		SetDirection(void);
	void		Release(void);
	void		SetTransform(void);
public:
	//void Reverse 반사벡터 실행시켜야...
	const bool& Get_IsFire() { return m_bIsFire; };
	void Set_Fire(const bool& bResult);

	const ARROWSTATE& Get_ArrowState() { return m_dArrowState; };
	void SetState(ARROWSTATE dID) { m_dArrowState = dID; };
	void SetTarget(const Engine::CTransform* pInfo); //플레이어꺼 Info 넣으면 된다네요
	void SetTargetDirection(Engine::DIRECTION _direct);
	void SetTargetDirection(Engine::DIRECTION _direct, bool bFlag); //이건 무조건 방향전환 수행할 녀석...
	Engine::DIRECTION Get_Direction() { return m_DIR[Engine::CUR]; };
	void Gauge(const int& _iLv, const float& _fTime); //레벨(게이지 정도)에 따라 화살을 반대방향으로 이동시킨다.
public:
	static CArrow* Create(LPDIRECT3DDEVICE9 pGraphicDev, Engine::CTransform* pTrnasform);

private:
	ARROWSTATE						m_dArrowState; //화살상태
	bool							m_bIsFire = FALSE; // 불화살인지ㅇ닌지...
	float							m_fFireTime = 0.f; //불화살 지속시간...
	float							m_fFireEffectTime = 0.f;

	Engine::CScene*					m_pScene = nullptr;
	Engine::CTransform*				m_pInfo = nullptr;
	const Engine::CTransform* m_pTargetInfo = nullptr; //타겟의 pInfo;
	CSphereColBox*					m_pSphereColBox = nullptr;


	Engine::CVIBuffer*				m_pBufferCom = nullptr;
	Engine::CTexture*				m_pTextureCom = nullptr;
	Engine::CResourcesMgr*			m_pResourcesMgr = nullptr;
	Engine::CInfoSubject*			m_pInfoSubject = nullptr;
	CCameraObserver*				m_pCameraObserver = nullptr;

	Engine::VTXTEX*					m_pVertex = nullptr;
	Engine::VTXTEX*					m_pConvertVertex = nullptr;

	DWORD							m_dwVtxCnt;

	D3DXVECTOR3						m_vAccel;
	D3DXVECTOR3						m_vPosCol; //충돌지점 pos (화살촉)
	D3DXVECTOR3						m_vBackPos; //이전지점.
	D3DXVECTOR3						m_vStart; //화살이 출발한 지점;

	bool							m_bValid = FALSE;
	bool							m_bIsNew = TRUE;
	bool							m_bFullGauge = FALSE; //게이지가 풀일때....
	float							m_fGauge = FULLGAUAGE;
	float							m_fSaveDistance = 0.f; // 화살 줌할때 뒤로 밀림. 얼마나 밀렸는지 계산할 녀셕임. 줌하면서 화살 방향 바꿀때 써먹어야지.
	float							m_fRealDistance = 0.f; // 얼마나 밀렸는지 실제 숫자;

	float							m_fAngle = 0.f; //화살이 가리키는 각도!. 예측하지 못하여 노가다로 비슷한 값 잡아냄..

													//슈팅관련
	bool							m_bIsShouting = FALSE;// 화살이 날아가는 상황...
	float							m_fSpeed = 0.f; //화살이 날아가는 스피드를 의미한다.
	float							m_fDuration = 0.f; //화살이 날아가는 지속 시간을 의미한다.

													   //회수관련
	bool							m_bIsRecovery = FALSE;
	float							m_fRecoverySpd = 0.f; //회수 속도
	bool							m_bPressRecovery = FALSE; //회수키를 누르고 있는상태임.

	bool							m_bCollisionPlane = FALSE; //벽과 충돌중인지 아닌지를 판단.

	bool							m_bReady = FALSE; //화살이 잡혀있는상태
	int		test = 0;;
};

#endif // Arrow_h__
