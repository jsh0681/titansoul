#include "stdafx.h"
#include "TexAni.h"

#include "Export_Function.h"

CTexAni::CTexAni(LPDIRECT3DDEVICE9 pGraphicDev) :
	CAnimation(pGraphicDev)
{
}


CTexAni::~CTexAni()
{
	Release();
}

void CTexAni::Release()
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
		m_pConvertVertex[i] = m_pVertex[i];

	m_pResourceMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pConvertVertex);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

HRESULT CTexAni::Initialize(const vector<TEXINFO*>* pVecTexInfo, const wstring& wstrBufferKey)
{
	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourceMgr->EngineToClient(Engine::RESOURCE_STATIC, wstrBufferKey, m_pVertex);

	if (CreateBuffer(wstrBufferKey) == E_FAIL)
		return E_FAIL;
	if (CreateTexture(pVecTexInfo) == E_FAIL)
		return E_FAIL;

	return S_OK;
}

void CTexAni::Render()
{
	m_pResourceMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pConvertVertex);
	CAnimation::Render();
}

void CTexAni::SetTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pMatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pMatProj);
}

void CTexAni::SetRatioTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		m_pConvertVertex[i].vPos.z *= (*(*m_pVecTexInfo).begin())->fRatioHeight;
		m_pConvertVertex[i].vPos.x *= (*(*m_pVecTexInfo).begin())->fRatioWidth;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pMatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pMatProj);
}


CTexAni* CTexAni::Create(LPDIRECT3DDEVICE9 pGraphicDev, const vector<TEXINFO*>* pVecTexInfo, const wstring& wstrBufferKey)
{
	CTexAni* pInstance = new CTexAni(pGraphicDev);
	
	if (FAILED(pInstance->Initialize(pVecTexInfo, wstrBufferKey)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
