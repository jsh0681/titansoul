#ifndef GoliathLHand_h__
#define GoliathLHand_h__

#include "GameObject.h"
#include "CameraObserver.h"
#include "CollisionMgr.h"
#include "TerrainColl.h"
#include "GoliathLShadow.h"
#include"Camera.h"

class CTexAni;
class CSphereColBox;

class CGoliathLHand : public Engine::CGameObject
{
private:
	explicit CGoliathLHand(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CGoliathLHand();

public:
	const float & GetRadius();
	Engine::CTransform* GetInfo() { return m_pInfo; }
	virtual void	Update(void);
	virtual void	Render(void);
	void SetTarget(D3DXVECTOR3 vTarget) { m_vTarget = vTarget; }
	void SetBodyIndex(int iIndex) { m_iIndex = iIndex; }
	void SetBodyStateKey(wstring strBodyStateKey) { m_wstrBodyStateKey = strBodyStateKey; }
	void SetChangeHand(bool bChangeHand) { m_bChangeHand = bChangeHand; }
	void SetRadius(const float& fSize);
	void Attack(float fDeltaTime); 
	const bool& GetChangeLock() { return m_bChangeLock; }
private:
	bool m_bBodyDead = false;
	CGoliathLShadow* m_pShadow = nullptr;

	CSphereColBox*			m_pSphereBox = nullptr;
private:
	float fStopTime = 0.f;

	void Release(void);
	virtual void SetTransform(void);
	virtual HRESULT AddComponent(void);
	virtual HRESULT Initialize(void);
	virtual void SetDirection(void);
	void		StateKeyChange();
	


	

private:
	float fFallSpeed = 0.f;
	float fAttackTime = 0.f;
	bool m_bChangeLock = false;
	bool m_bIsActivate = false;
	bool m_bChangeHand = false;
	wstring m_wstrBodyStateKey;
	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...
	CCameraObserver*		m_pCameraObserver = nullptr;
	float m_fSpeed;
	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;
	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *					m_pTerrainCol = nullptr;
	D3DXVECTOR3						m_vDestPos;
	D3DXVECTOR3						m_vTarget;
	CTexAni* m_pTexAni = nullptr;

	int m_iIndex = 0;
	int m_iNextMove = 0;

public:
	void SetCamera(Engine::CCamera* pCamera) { m_pCamera = pCamera; }
private:
	Engine::CCamera* m_pCamera = nullptr;

public:
	static CGoliathLHand*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
};
#endif // GoliathShoulder_h__
