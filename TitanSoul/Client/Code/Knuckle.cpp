#include "stdafx.h"
#include "Knuckle.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "Knuckle_Part.h"
#include "Knuckle_Body.h"
#include "Knuckle_Mace.h"
#include "Knuckle_Chain.h"
#include "Knuckle_Shadow.h"
#include "PlayerObserver.h"
#include "SphereColBox.h"

CKnuckle::CKnuckle(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();
	m_pTimeMgr = Engine::Get_TimeMgr();
}


CKnuckle::~CKnuckle()
{
	Release();
}

HRESULT CKnuckle::Initialize(const VEC3& vPos)
{
	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_vecKnucklePart.resize(OBJ_END);

	// 자기 컴포넌트 생성
	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos = vPos;

	// 자기에 속한 객체들 생성
	FAILED_CHECK_RETURN(CreateObj(), E_FAIL);

	D3DXMatrixTranslation(&m_matDie, m_vDIePos.x, m_vDIePos.y, m_vDIePos.z);
	m_pShpereBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, m_fRadius, true);
	m_pWeakShpereBox = CSphereColBox::Create(m_pGraphicDev, &m_matDie, m_fWeakRadius, true);

	for (int i = 0; i < 2; ++i)
		m_pMaceShpereBox[i] = CSphereColBox::Create(m_pGraphicDev, m_pMace[i]->GetWorldMat(), m_pMace[i]->GetRadius(),true);

	return S_OK;
}

HRESULT CKnuckle::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CKnuckle::CreateObj()
{
	CKnuckle_Part* pPart = nullptr;

	pPart = m_pBody = CKnuckle_Body::Create(m_pGraphicDev, m_pInfo->m_vPos);				// 몸통 생성
	NULL_CHECK_RETURN(pPart, E_FAIL);
	m_vecKnucklePart[BODY].emplace_back(pPart);

	CKnuckle_Chain* pChain = nullptr;
	for (size_t i = 0; i < P_END; ++i)
	{
		pPart = m_pMace[i] = CKnuckle_Mace::Create(m_pGraphicDev, m_pInfo->m_vPos, i);	// 철퇴 생성
		NULL_CHECK_RETURN(pPart, E_FAIL);
		m_vecKnucklePart[MACE].emplace_back(pPart);

		for (size_t k = 0; k < m_iMaxChainCount; ++k)									// 사슬 생성
		{
			pPart = m_pChain[i][k] = pChain = CKnuckle_Chain::Create(m_pGraphicDev, m_pInfo->m_vPos, i, k);
			NULL_CHECK_RETURN(pPart, E_FAIL);

			pChain->SetMacePos(&m_pMace[i]->GetPos());									// 철퇴 좌표주기
			pChain->SetBodyPos(&m_pBody->GetPos());										// 몸통 좌표주기

			m_vecKnucklePart[CHAIN].emplace_back(pPart);
		}
	}

	CKnuckle_Shadow* pShadow = nullptr;
	pPart = pShadow = CKnuckle_Shadow::Create(m_pGraphicDev, m_pBody->GetPos(), m_fBodyShadow);	// 몸통 그림자
	NULL_CHECK_RETURN(pPart, E_FAIL);
	m_vecKnucklePart[SHADOW].emplace_back(pPart);
	pShadow->SetTargetPos(&m_pBody->GetPos());

	pPart = pShadow = CKnuckle_Shadow::Create(m_pGraphicDev, m_pMace[P_LEFT]->GetPos(), m_fMaceShadow);	// 왼쪽 철퇴 그림자
	NULL_CHECK_RETURN(pPart, E_FAIL);
	m_vecKnucklePart[SHADOW].emplace_back(pPart);
	pShadow->SetTargetPos(&m_pMace[P_LEFT]->GetPos());

	pPart = pShadow = CKnuckle_Shadow::Create(m_pGraphicDev, m_pMace[P_RIGHT]->GetPos(), m_fMaceShadow);	// 오른쪽 그림자
	NULL_CHECK_RETURN(pPart, E_FAIL);
	m_vecKnucklePart[SHADOW].emplace_back(pPart);
	pShadow->SetTargetPos(&m_pMace[P_RIGHT]->GetPos());

	return S_OK;
}

void CKnuckle::Release()
{
	m_pTimeMgr = nullptr;
	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	Engine::Safe_Delete(m_pShpereBox);
	Engine::Safe_Delete(m_pWeakShpereBox);
	for (size_t i = 0; i < 2; ++i)
		Engine::Safe_Delete(m_pMaceShpereBox[i]);

	for (auto& it : m_vecKnucklePart)
	{
		for_each(it.begin(), it.end(), Engine::Safe_Delete<CKnuckle_Part*>);
		it.clear();
	}
	m_vecKnucklePart.clear();
}

void CKnuckle::Update()
{
	m_vTargetPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;

	float fDelataTime = m_pTimeMgr->GetTime();
	m_fBehavioTime += fDelataTime;

	CGameObject::Update();
	CheckInvici();

	if (m_bDie)
		return;

	HehavioFunc();
	PlayingFunc();
	for (auto& it : m_vecKnucklePart)
	{
		for (auto& kt : it)
		{
			kt->SetDelataTime(fDelataTime);
			kt->Update();
		}
	}
	HehavioCheckEnd();
}

void CKnuckle::Render()
{
	for (auto& it : m_vecKnucklePart)
		for (auto& kt : it)
			kt->Render();


	//m_pShpereBox->Render();
	//m_pWeakShpereBox->Render();
	//for (int i = 0; i < 2; ++i)
	//	m_pMaceShpereBox[i]->Render();
}

const VEC3& CKnuckle::GetPos()
{
	return m_pInfo->m_vPos;
}

void CKnuckle::SetState(int iState)
{
	m_eState = (KNUCKLE_STATE)iState;
	m_bPlaying = false;
	m_fBehavioTime = 0.f;
}

VEC3 CKnuckle::GetMacePos(const int& iIndex)
{
	if (m_pMace[iIndex] != nullptr)
		return m_pMace[iIndex]->GetPos();

	return VEC3{ 0.f,0.f,0.f };
}

float CKnuckle::GetMaceRadius(const int& iIndex)
{
	if (m_pMace[iIndex] != nullptr)
		return m_pMace[iIndex]->GetRadius();

	return 0.f;
}

//************************************
// Method	:  HehavioFunc
// 작성자	:  윤유석
// Date		:  2019/01/14
// Message	:  페이즈가 시작할때 세팅을 해줌
// Parameter: 
//************************************
void CKnuckle::HehavioFunc()
{
	if (!m_bPlaying)
	{
		m_bPlaying = true;

		VEC3 vDIr = { 0.f,0.f, 0.f };
		float fAngle = 0.f;
		int iCount = 0;
		switch (m_eState)
		{
		case CKnuckle::WAKE_UP:
			m_fEndTime = m_fWalkUpTime;

			vDIr = m_pMace[P_RIGHT]->GetPos() - m_pBody->GetPos();
			m_pBody->SetSynchDirXZ(vDIr.x, vDIr.z);
			break;
		case CKnuckle::CHECKBACK:
			m_fEndTime = m_fCheckBackTime;

			m_fTargetAngle = GetDgreeAngle(m_vTargetPos, m_pInfo->m_vPos, true);
			SetMinAngle_RotationDir();

			if ((m_fMinAngle * 3.f)/*67.5도*/ < m_fMinAngleSave)
			{
				if (m_eState != m_ePreState)
				{
					if((m_fMinAngle * 6.f)/*22.5 * 6 도*/ < m_fMinAngleSave)
					++m_iAccCount;
				}

				SetDirScene();
				SetMacePos();
			}
			else
				m_eRotationDir = NO;

			for (size_t i = 0; i < P_END; ++i)
			{
				m_pMace[i]->SetTargetVec(m_vMacePos[i]);
				for (size_t k = 0; k < m_iMaxChainCount; ++k)
					m_pChain[i][k]->SetBodyPos(&m_vChainPos[i]);
			}

			m_bJumpEnd = false;
			break;
		case CKnuckle::ATTACK:
			m_fEndTime = m_fAttackTime;

			m_pMace[m_eAttackDir]->SetAttack(true);
			m_pMace[m_eAttackDir]->SetTargetVec(m_vTargetPos);	// 플레이어 좌표

			m_fTargetAngle = GetDgreeAngle(m_pMace[m_eAttackDir]->GetPos(), m_pBody->GetPos(), true);
			m_fTargetAngle *= -1;

			SetMinAngle_RotationDir();
			SetDirScene();

			SetMacePos();

			break;
		case CKnuckle::BODY_ROT:
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Knuckle_Move.ogg", CSoundMgr::SOUND_BOSS);
			m_bAttackEnd = false;
			m_fEndTime = m_fBody_Rat;

			m_pBody->SetTargetVec(m_pMace[m_eAttackDir]->GetPos());

			m_pMace[m_eAttackDir]->SetAttack(true);
			m_pMace[m_eAttackDir]->SetTargetVec(m_vTargetPos);	// 플레이어 좌표

			m_fTargetAngle = GetDgreeAngle(m_pMace[m_eAttackDir]->GetPos(), m_pBody->GetPos(), true);
			m_fTargetAngle *= -1;

			SetMinAngle_RotationDir();
			SetDirScene();
			//SetDirScene();
			//SetMacePos();

			if (m_eAttackDir == P_LEFT)
				m_pMace[P_RIGHT]->SetTargetVec(m_vMacePos[P_RIGHT]);
			else
				m_pMace[P_LEFT]->SetTargetVec(m_vMacePos[P_LEFT]);
			break;
		case CKnuckle::ATTACK_ROT:
			m_fEndTime = m_fRotationTime;

			m_pBody->SetTargetVec(m_vTargetPos);

			m_fTargetAngle = GetDgreeAngle(m_vTargetPos, m_pInfo->m_vPos, true);
			SetMinAngle_RotationDir();
			m_eRotationDir = RD_RIGHT;
			SetMacePos();

			for (size_t i = 0; i < P_END; ++i)
			{
				m_pMace[i]->SetTargetVec(m_vMacePos[i]);
				for (size_t k = 0; k < m_iMaxChainCount; ++k)
					m_pChain[i][k]->SetBodyPos(&m_vChainPos[i]);
			}

			break;
		case CKnuckle::ATTACK_JUMP:
			m_fEndTime = m_fJumpTime;

			m_fTargetAngle = GetDgreeAngle(m_vTargetPos, m_pInfo->m_vPos, true);
			SetMinAngle_RotationDir();
			SetDirScene();
			SetMacePos();

			break;
		}
		for (auto& it : m_vecKnucklePart)
			for (auto& kt : it)
			{
				kt->SetEndTime(m_fEndTime);
				kt->SetState(m_eState);
			}
	}
}

void CKnuckle::PlayingFunc()
{
	memcpy(&m_pInfo->m_vPos, &m_pBody->GetPos(), sizeof(float) * 3);
	m_pInfo->m_vPos.y = BOSS_Y;

	if (!m_bPlaying)
		return;

	int iCount = 0;
	switch (m_eState)
	{
	case CKnuckle::WAKE_UP:
		break;
	case CKnuckle::CHECKBACK:
		break;
	case CKnuckle::ATTACK:
		break;
	case CKnuckle::BODY_ROT:
		m_pBody->SetTargetVec(m_pMace[m_eAttackDir]->GetPos());

		if (m_eAttackDir == P_LEFT)
			m_pMace[P_RIGHT]->SetTargetVec(m_vMacePos[P_RIGHT]);
		else
			m_pMace[P_LEFT]->SetTargetVec(m_vMacePos[P_LEFT]);

		break;
	case CKnuckle::ATTACK_ROT:
		m_fLookAngle -= 6.2f;

		if (m_fLookAngle < -180.f)
			m_fLookAngle += 360.f;

		m_pBody->SetTargetVec(m_vTargetPos);
		for (size_t i = 0; i < P_END; ++i)
		{
			m_pMace[i]->SetTargetVec(m_vMacePos[i]);
			for (size_t k = 0; k < m_iMaxChainCount; ++k)
				m_pChain[i][k]->SetBodyPos(&m_vChainPos[i]);
		}
		break;
	case CKnuckle::ATTACK_JUMP:
		//m_fTargetAngle = GetDgreeAngle(m_vTargetPos, m_pInfo->m_vPos, true);
		//SetMinAngle_RotationDir();
		//SetDirScene();
		//SetMacePos();
		break;
	}

	SetMacePos();
	for (size_t i = 0; i < P_END; ++i)
		m_pMace[i]->SetBodyPos(m_pBody->GetPos());
}

//************************************
// Method	:  HehavioCheckEnd
// 작성자	:  윤유석
// Date		:  2019/01/14
// Message	:  후 처리를 해주는 함수(정리), 다음 행동을 결정해줌
// Parameter: 
//************************************
void CKnuckle::HehavioCheckEnd()
{
	if (m_fBehavioTime > m_fEndTime)
	{
		m_bPlaying = false;

		m_ePreState = m_eState;
		m_fBehavioTime = 0.f;
		switch (m_eState)
		{
		case CKnuckle::WAKE_UP:
			m_eState = CHECKBACK;
			break;
		case CKnuckle::CHECKBACK:
			if (m_eRotationDir != NO)
			{
				m_eState = CHECKBACK;
				m_eRotationDir = NO;
			}
			else
			{
				if (m_iAccCount >= m_iMaxAccCount)
					m_eState = ATTACK_ROT;
				else
					m_eState = ATTACK;
			}
			for (size_t i = 0; i < 2; ++i)
				m_pMace[i]->ResetAccel();

			break;
		case CKnuckle::ATTACK:
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Knuckle_Hit.ogg", CSoundMgr::SOUND_BOSS);
			m_eState = BODY_ROT;
			m_bAttackEnd = true;
			break;
		case CKnuckle::BODY_ROT:
			m_eState = CHECKBACK;

			for (size_t i = 0; i < 2; ++i)
				m_pMace[i]->SetAttack(false);

			m_bRotationCorre = false;
			break;
		case CKnuckle::ATTACK_ROT:
			m_eState = ATTACK_JUMP;
			break;
		case CKnuckle::ATTACK_JUMP:

			float fLeftLength = D3DXVec3Length(&(m_pMace[P_LEFT]->GetPos() - m_vTargetPos));
			float fRightLength = D3DXVec3Length(&(m_pMace[P_RIGHT]->GetPos() - m_vTargetPos));

			// 둘중 플레이어에게 가까운 쪽을 사용함.
			if (fLeftLength < fRightLength)
				m_eAttackDir = P_LEFT;
			else
				m_eAttackDir = P_RIGHT;

			m_iAccCount = 0;
			m_eState = WAKE_UP;
			m_bJumpEnd = true;
			break;
		}
		for (auto& it : m_vecKnucklePart)
			for (auto& kt : it)
				kt->ResetBehavioTime();			// 시간 초기화
	}
}

void CKnuckle::CheckInvici()
{
	if (m_bInvincibility)
	{
		m_fInvincibility += m_pTimeMgr->GetTime();
		if (m_fInvincibility > 1.f)
			m_bInvincibility = false;
	}
}

float CKnuckle::GetDgreeAngle(const VEC3& vTarget, const VEC3& vPos, const bool& bYResst)
{
	VEC3 vDir = vTarget - vPos;
	float fAngle = 0.f;

	if (bYResst)
		vDir.y = 0.f;

	D3DXVec3Normalize(&vDir, &vDir);
	fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vDir, &g_vLook)));

	if (m_vTargetPos.x > m_pInfo->m_vPos.x)
		fAngle *= -1.f;

	return fAngle;
}

void CKnuckle::SetMinAngle_RotationDir()
{
	float fCorrectionTargetAngle = m_fTargetAngle + 180.f;
	float fCorrectionLookAngle = m_fLookAngle + 180.f;

	// 각도가 얼마나 차이가 나는지
	m_fMinAngleSave = fabsf(fCorrectionTargetAngle - fCorrectionLookAngle);

	if (m_fMinAngle < m_fMinAngleSave)		// 각도가 많이 45도 이상 차이난다.
	{
		float fMin = fCorrectionLookAngle - fCorrectionTargetAngle;

		if (0 < fMin && fMin <= 180.f)			 m_eRotationDir = RD_RIGHT;
		else if (180 < fMin)					 m_eRotationDir = RD_LEFT;
		else if (fMin < 0.f && -180.f < fMin)	 m_eRotationDir = RD_LEFT;
		else if (fMin < -180.f)					 m_eRotationDir = RD_RIGHT;
	}
	else
	{
		m_eRotationDir = NO;
		return;
	}
}

void CKnuckle::SetDirScene()
{
	if (m_eRotationDir == NO)
		return;

	else if (m_eRotationDir == RD_LEFT)
	{
		m_fLookAngle += 22.5f;

		if (180.f <= m_fLookAngle)
			m_fLookAngle -= 360.f;
	}

	else
	{
		m_fLookAngle -= 22.5f;
		if (m_fLookAngle < -180.f)
			m_fLookAngle += 360.f;
	}
}

void CKnuckle::SetMacePos()
{
	float fDis = 2.f;
	float fChainDis = 1.f;

	if (m_eState == ATTACK_ROT || m_eState == ATTACK_JUMP)
		fDis += m_fRotationCorr;

	if (-22.5f <= m_fLookAngle && m_fLookAngle <= 22.5f)
	{
		m_pBody->SetDirection(UU);
		m_vMacePos[P_LEFT] = { m_pBody->GetPos().x - fDis, m_pBody->GetPos().y, m_pBody->GetPos().z };
		m_vMacePos[P_RIGHT] = { m_pBody->GetPos().x + fDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z };

		m_vChainPos[P_LEFT] = { m_pBody->GetPos().x - fChainDis, m_pBody->GetPos().y, m_pBody->GetPos().z };
		m_vChainPos[P_RIGHT] = { m_pBody->GetPos().x + fChainDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z };

		m_vDIePos = { m_pBody->GetPos().x, m_pBody->GetPos().y, m_pBody->GetPos().z - 1.3f };
	}
	else if (22.5f <= m_fLookAngle	&& m_fLookAngle <= 67.5f)
	{
		m_pBody->SetDirection(UL);
		m_vMacePos[P_LEFT] = { m_pBody->GetPos().x - fDis, m_pInfo->m_vPos.y,m_pBody->GetPos().z - fDis };
		m_vMacePos[P_RIGHT] = { m_pBody->GetPos().x + fDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z + fDis };

		m_vChainPos[P_LEFT] = { m_pBody->GetPos().x - fChainDis, m_pInfo->m_vPos.y,m_pBody->GetPos().z - fChainDis };
		m_vChainPos[P_RIGHT] = { m_pBody->GetPos().x + fChainDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z + fChainDis };

		m_vDIePos = { m_pBody->GetPos().x + 0.9f, m_pBody->GetPos().y, m_pBody->GetPos().z - 0.8f };
	}
	else if (67.5f <= m_fLookAngle  && m_fLookAngle <= 112.5f)
	{
		m_pBody->SetDirection(LL);
		m_vMacePos[P_LEFT] = { m_pBody->GetPos().x, m_pInfo->m_vPos.y, m_pBody->GetPos().z - fDis };
		m_vMacePos[P_RIGHT] = { m_pBody->GetPos().x, m_pInfo->m_vPos.y, m_pBody->GetPos().z + fDis };

		m_vChainPos[P_LEFT] = { m_pBody->GetPos().x, m_pInfo->m_vPos.y, m_pBody->GetPos().z - fChainDis };
		m_vChainPos[P_RIGHT] = { m_pBody->GetPos().x, m_pInfo->m_vPos.y, m_pBody->GetPos().z + fChainDis };

		m_vDIePos = { m_pBody->GetPos().x + 1.1f, m_pBody->GetPos().y, m_pBody->GetPos().z };
	}
	else if (112.5f <= m_fLookAngle && m_fLookAngle <= 157.5f)
	{
		m_pBody->SetDirection(DL);
		m_vMacePos[P_LEFT] = { m_pBody->GetPos().x + fDis, m_pInfo->m_vPos.y,m_pBody->GetPos().z - fDis };
		m_vMacePos[P_RIGHT] = { m_pBody->GetPos().x - fDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z + fDis };

		m_vChainPos[P_LEFT] = { m_pBody->GetPos().x + fChainDis, m_pInfo->m_vPos.y,m_pBody->GetPos().z - fChainDis };
		m_vChainPos[P_RIGHT] = { m_pBody->GetPos().x - fChainDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z + fChainDis };

		m_vDIePos = { m_pBody->GetPos().x + 0.9f, m_pBody->GetPos().y, m_pBody->GetPos().z + 1.4f };
	}
	else if (-157.5f <= m_fLookAngle && m_fLookAngle <= -112.5f)
	{
		m_pBody->SetDirection(DR);
		m_vMacePos[P_LEFT] = { m_pBody->GetPos().x + fDis, m_pInfo->m_vPos.y,m_pBody->GetPos().z + fDis };
		m_vMacePos[P_RIGHT] = { m_pBody->GetPos().x - fDis, m_pBody->GetPos().y, m_pBody->GetPos().z - fDis };

		m_vChainPos[P_LEFT] = { m_pBody->GetPos().x + fChainDis, m_pInfo->m_vPos.y,m_pBody->GetPos().z + fChainDis };
		m_vChainPos[P_RIGHT] = { m_pBody->GetPos().x - fChainDis, m_pBody->GetPos().y, m_pBody->GetPos().z - fChainDis };

		m_vDIePos = { m_pBody->GetPos().x - 0.9f, m_pBody->GetPos().y, m_pBody->GetPos().z + 1.4f };
	}
	else if (-112.5f <= m_fLookAngle  && m_fLookAngle <= -67.5f)
	{
		m_pBody->SetDirection(RR);
		m_vMacePos[P_LEFT] = { m_pBody->GetPos().x, m_pInfo->m_vPos.y, m_pBody->GetPos().z + fDis };
		m_vMacePos[P_RIGHT] = { m_pBody->GetPos().x, m_pInfo->m_vPos.y, m_pBody->GetPos().z - fDis };

		m_vChainPos[P_LEFT] = { m_pBody->GetPos().x, m_pInfo->m_vPos.y, m_pBody->GetPos().z + fChainDis };
		m_vChainPos[P_RIGHT] = { m_pBody->GetPos().x, m_pInfo->m_vPos.y, m_pBody->GetPos().z - fChainDis };

		m_vDIePos = { m_pBody->GetPos().x - 1.1f, m_pBody->GetPos().y, m_pBody->GetPos().z };
	}
	else if (-67.5 <= m_fLookAngle	&& m_fLookAngle <= -22.5f)
	{
		m_pBody->SetDirection(UR);
		m_vMacePos[P_LEFT] = { m_pBody->GetPos().x - fDis,m_pBody->GetPos().y, m_pBody->GetPos().z + fDis };
		m_vMacePos[P_RIGHT] = { m_pBody->GetPos().x + fDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z - fDis };

		m_vChainPos[P_LEFT] = { m_pBody->GetPos().x - fChainDis,m_pBody->GetPos().y, m_pBody->GetPos().z + fChainDis };
		m_vChainPos[P_RIGHT] = { m_pBody->GetPos().x + fChainDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z - fChainDis };

		m_vDIePos = { m_pBody->GetPos().x - 0.9f, m_pBody->GetPos().y, m_pBody->GetPos().z - 0.8f };
	}
	else
	{
		m_pBody->SetDirection(DD);
		m_vMacePos[P_LEFT] = { m_pBody->GetPos().x + fDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z };
		m_vMacePos[P_RIGHT] = { m_pBody->GetPos().x - fDis, m_pInfo->m_vPos.y,m_pBody->GetPos().z };

		m_vChainPos[P_LEFT] = { m_pBody->GetPos().x + fChainDis, m_pInfo->m_vPos.y, m_pBody->GetPos().z };
		m_vChainPos[P_RIGHT] = { m_pBody->GetPos().x - fChainDis, m_pInfo->m_vPos.y,m_pBody->GetPos().z };

		m_vDIePos = { m_pBody->GetPos().x, m_pBody->GetPos().y, m_pBody->GetPos().z + 1.3f };
	}
	D3DXMatrixTranslation(&m_matDie, m_vDIePos.x, m_vDIePos.y, m_vDIePos.z);
}

CKnuckle* CKnuckle::Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos)
{
	CKnuckle* pInstance = new CKnuckle(pGraphicDev);

	if (FAILED(pInstance->Initialize(vPos)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
