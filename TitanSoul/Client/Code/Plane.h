#ifndef Plane_h__
#define Plane_h__


/*!
* \class CTerrain
*
* \brief
*
* \수정자 윤유석
* \수정일 1월 8 2019
* \수정 내용 :	필요없는 데이터들 삭제,
PLANEDATA 구조체 추가
m_pPlaneInfo -> Release에서 삭제
*/


#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
	class CResourcesMgr;
	class CInfoSubject;
}

class CCameraObserver;
class CPlane
	: public Engine::CGameObject
{
private:
	explicit CPlane(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CPlane();

	HRESULT Initialize(const wstring& wstrMapKey, PLANEINFO* pPlaneInfo);
	void Update();
	void Render();
	void Release();

	HRESULT		AddComponent(void);

	void		SetDirection(void);
	void		SetTransform(void);

	PLANEINFO* Get_PlaneInfo() { return m_pPlaneInfo; };

public:
	static CPlane* Create(LPDIRECT3DDEVICE9 pGraphicDev, const wstring& wstrMapKey, PLANEINFO* pPlaneInfo);

	void		PlaneCorrection(const wstring& wstrBufferKey);

private:
	bool m_bIsSelect = false;

private:
	Engine::CTransform*			m_pInfo = nullptr;
	Engine::CResourcesMgr*		m_pResourcesMgr;
	Engine::CVIBuffer*			m_pBufferCom = nullptr;
	Engine::CTexture*			m_pTextureCom = nullptr;

	Engine::VTXTEX*				m_pVtx;
	Engine::VTXTEX*				m_pConvertVtx;
	DWORD						m_dwVtxCnt = 4;

	CCameraObserver*			m_pCameraObserver;
	Engine::CInfoSubject*		m_pInfoSubject;

	PLANEINFO*					m_pPlaneInfo;
};

#endif // Plane_h__
