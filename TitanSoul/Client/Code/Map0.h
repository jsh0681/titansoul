#ifndef Map0_h__
#define Map0_h__

/*!
 * \class CMap0 (바뀌기 전 Stage)
 *
 * \brief 플레이어는 여기서 만들어주어야 함. 그리고 씬 바뀔떄 씬에 플레이어 포인터를 넘겨줘야 함.
 *
 * \수정자 윤유석
 * \수정일 1월 6 2019
 * \수정 내용 : 파일 이름 및 클래스 이름 수정
 */


#include "Scene.h"

#include "Player.h"
#include "Terrain.h"
#include "Monster.h"
#include "CubeObj.h"

#include "StaticCamera.h"
#include "StaticCamera2D.h"
#include"ParticleSystem.h"

namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CCamera;
};
class CGoliath;
class CShpereCol;
class CPlayer;
class CStaticCamera2D;
class CTerrain;
class CShpereCol;
class CArrow;
class CGoliathLHand;
class CGoliathRHand;
class CDisplay;



class CMap0 : public Engine::CScene
{
private:
	explicit CMap0(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CMap0(void);

public:
	virtual HRESULT Initialize(void) override;
	virtual HRESULT Initialize(CPlayer* pPlayer, Engine::CCamera* pCamera);
	virtual void Update(void) override;
	virtual void Render(void) override;
	virtual void Release(void) override;

	//임시
	virtual void RemoveEffect(void);
	virtual void FireArrowEffect(const D3DXVECTOR3& vPos);
public:
	virtual void Triger(int _iID);


private:
	HRESULT     AddResources(void);
	HRESULT		Add_Environment_Layer(void);
	HRESULT		Add_GameObject_Layer(void);
	HRESULT		Add_UI_Layer(void);

	CPlayer*	m_pPlayer;
	CTerrain*	m_pTerrain;

	bool m_bChangeScene = false;

private:
	CGoliathLHand* m_pLHand = nullptr;
	CGoliathRHand* m_pRHand = nullptr;
	Engine::ParticleSystem * m_pParticle = nullptr;

	CGoliath* m_pGoliath = nullptr;
	CShpereCol*						m_pShpereCol = nullptr;
	CArrow*						m_pArrow = nullptr;
private:
	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;
	Engine::CCamera*				m_pCamera = nullptr;
	CDisplay*						m_pDisplay = nullptr;

	int j = 0;
	int i = 0;
public:
	static bool			m_sbMap0_Resource;
	static CMap0*	Create(LPDIRECT3DDEVICE9 pGraphicDev);
	static CMap0*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
	void CeremonyYeah();
	void ColPlayerMonster(const float& fTime);
	void BloodMode();
	void ColMonsterArrow(const float& fTime);
};


#endif // Map0_h__
