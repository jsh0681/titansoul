#include "stdafx.h"
#include "Mucus.h"

#include "Export_Function.h"
#include "Engine_function.h"

#include "Include.h"

#include "SphereColBox.h"
#include "CameraObserver.h"

CMucus::CMucus(LPDIRECT3DDEVICE9 pGraphicDev):
	Engine::CGameObject(pGraphicDev),
	m_pTimeMgr(Engine::Get_TimeMgr())
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
}


CMucus::~CMucus()
{
	Release();
}

void CMucus::Release()
{
	Resize();
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Mucus", m_pVertex);

	Engine::Safe_Delete(m_pShereColBox);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

HRESULT CMucus::Initialize(const VEC3& vPos)
{
	m_dwVtxCnt = 4;

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Mucus", m_pVertex);

	m_iIndex = GetRandom<int>(0, 5);
	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pShereColBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 0.3f,true);
	m_pInfo->m_vPos = vPos;
	SetRandomPos();
	Resize();

	return S_OK;
}

HRESULT CMucus::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Mucus");
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"AcidnerveMucusNo");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

void CMucus::Update()
{
	m_fDeltaTime = m_pTimeMgr->GetTime();;
	m_fLifeTime += m_fDeltaTime;

	if (m_fLifeTime > m_fMaxLifeTime)
		m_bDie = true;
	
	if (m_fLifeTime < m_fUpMaxTime)
		JumpFunc();
	else if ((m_fLifeTime - m_fUpMaxTime) < m_fDownMaxTime)
		FallFunc();
	else
		m_pInfo->m_vPos.y = 0.2f;

	CGameObject::Update();
	SetTransform();
}

void CMucus::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Mucus", m_pConvertVertex);

	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(100, 255, 255, 255));

	m_pTexture->Render(m_iIndex);
	m_pBuffer->Render();

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}

void CMucus::SetTransform()
{
	const D3DXMATRIX* matView = m_pCameraObserver->GetView();
	const D3DXMATRIX* matProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, matView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, matProj);
}

void CMucus::Resize()
{
	// 정점 초기화
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };
}

void CMucus::JumpFunc()
{
	//float m_fPower = 1.5f;

	//m_pInfo->m_vPos.y += 10.f * m_fPower * m_fDeltaTime;
	//m_pInfo->m_vPos += m_pInfo->m_vDir * m_fDeltaTime * 7.f;
}

void CMucus::FallFunc()
{
	//float m_fPower = 1.3f;

	//m_pInfo->m_vPos.y -= 10.5f * m_fPower * m_fDeltaTime;
	//m_pInfo->m_vPos += m_pInfo->m_vDir * m_fDeltaTime * 7.f;

	//if (m_pInfo->m_vPos.y < 0.1f)
	//	m_pInfo->m_vPos.y = 0.1f;
}

void CMucus::SetRandomPos()
{
	m_pInfo->m_vDir.x = GetRandom<float>(-5.f, 5.f);
	m_pInfo->m_vDir.z = GetRandom<float>(-5.f, 5.f);
	m_pInfo->m_vDir.y = 0.f;
	m_pInfo->m_vPos += m_pInfo->m_vDir;
}

CMucus* CMucus::Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos)
{
	CMucus*	pInstance = new CMucus(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos)))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}
