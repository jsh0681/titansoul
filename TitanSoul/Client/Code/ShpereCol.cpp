#include "stdafx.h"
#include "ShpereCol.h"


CShpereCol::CShpereCol()
{
}

 
CShpereCol::~CShpereCol()
{
	Release();
}


void CShpereCol::Release()
{
}

bool CShpereCol::CheckCollision(const VEC3& vDstPos, const float& fDstRadiuss, const VEC3& vSrcPos, const float& fSrcRadiusss)
{
	float fDis = D3DXVec3Length(&(vDstPos - vSrcPos));

	if (fDis < (fDstRadiuss + fSrcRadiusss))
		return true;

	return false;
}
bool CShpereCol::CheckCollision(const D3DXMATRIX& matWorld, const float& fDstRadiuss, const VEC3& vSrcPos, const float& fSrcRadiusss)
{
	VEC3 vTemp;
	vTemp.x = matWorld.m[3][0];
	vTemp.y = matWorld.m[3][1];
	vTemp.z = matWorld.m[3][2];

	float fDis = D3DXVec3Length(&(vTemp - vSrcPos));

	if (fDis < (fDstRadiuss + fSrcRadiusss))
		return true;

	return false;
}


CShpereCol* CShpereCol::Create()
{
	return new CShpereCol;
}

Engine::CCollision* CShpereCol::Clone()
{
	(*m_pwRefCnt)++;
	return new CShpereCol(*this);
}
