#include "stdafx.h"
#include "Cat.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

//Obj
#include "TexAni.h"
#include "CameraObserver.h"
#include "PlayerObserver.h"

#include "Player.h"

#include "TalkBallon.h"
#include "TalkWindow.h"
#include "SelectWindow.h"

bool CCat::m_bCatInit = false;

int	CCat::m_iEvent = 0;
int	CCat::m_iEvent_TalkCnt = 0;


CCat::CCat(LPDIRECT3DDEVICE9 pGraphicDev) :
	CNpc(pGraphicDev)
{
}

CCat::~CCat()
{
	Release();
}

HRESULT CCat::Initialize(const VEC3& vPos, CPlayer* pPlayer)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_wstrCurStateKey = L"CatIdleDD";
	SetStateKey();
	SetTrakingPos();

	m_pPlayer = pPlayer;

	m_eState[Engine::PRE] = C_STATE::IDLE;
	m_eState[Engine::CUR] = C_STATE::IDLE;
	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = vPos;
	//m_pInfo->m_vPos.z += 100.f;
	FAILED_CHECK_RETURN(InitTalkWindow(), E_FAIL);
	FAILED_CHECK_RETURN(InitTalkBalloon(), E_FAIL);


	if (!m_bCatInit) //두번 이닛되지 않게하자.
	{

	}
	else
	{
		m_pTalkWindow->SetTalkEnd(TRUE);
	}

	m_bCatInit = TRUE;
	return S_OK;
}

HRESULT CCat::AddComponent()
{
	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrCurStateKey), L"Buffer_Cat");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	m_pInfo->m_vScale = { 2.f, 1.f, 2.f };
	return S_OK;
}

void CCat::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	m_fBalloneRandTime += fTime;

	if (m_fBalloneRandTime > 5.f)
	{
		m_fBalloneRandTime = 0.f;
		int iIndex = GetRandom<int>(1, 3);
		m_pTalkBalloon->SetIndex(iIndex);
	}

	m_vPlayerPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;

	CheckEvent();
	SceneChange();
	ReturnIdle();
	CGameObject::Update();
	SetTransform();


	m_pTalkBalloon->SetPos(m_pInfo->m_vPos);
	m_pTalkBalloon->Update();

	m_pTalkWindow->Update();
}

void CCat::Render()
{
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pTexAni->Render();
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

	m_pTalkBalloon->Render();
	m_pTalkWindow->Render();
}

void CCat::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	Engine::Safe_Delete(m_pTalkBalloon);		// 말풍선
	Engine::Safe_Delete(m_pTalkWindow);			// 대화창
}

void CCat::SetStateKey()
{
	m_wstrStateKey[IDLE] = L"CatIdleDD";
	m_wstrStateKey[ATTACK] = L"CatAttackDD";
}

void CCat::SetTrakingPos()
{
	m_vTrakingPos[0] = { 110.f, 0.1f, 63.5f };
	m_vTrakingPos[1] = { 125.f, 0.1f, 63.5f };
	m_vTrakingPos[2] = { 125.f, 0.1f, 70.f };
	m_vTrakingPos[3] = { 110.f, 0.1f, 70.f };
	m_vTrakingPos[4] = { 110.f, 0.1f, 76.f };
	m_vTrakingPos[5] = { 107.f, 0.1f, 76.f };
}


void CCat::ShowTalkBalloon()
{
	if (m_bBalloon)
	{
		m_pTalkBalloon->SetOnOff(true);
		m_fBalloonCool += m_pTimeMgr->GetTime();

		if (m_fBalloonCool > m_fBalloonCoolTime)
		{
			m_pTalkBalloon->SetOnOff(false);
			m_fBalloonCool = 0.f;
			m_bBalloon = false;
		}
	}
	else
	{
		m_fBalloonRestTime += m_pTimeMgr->GetTime();

		if (m_fBalloonRestTime > m_fBalloonRestCoolTime)
		{
			m_pTalkBalloon->SetOnOff(true);
			m_fBalloonRestTime = 0.f;
			m_bBalloon = true;
		}
	}
}

HRESULT CCat::InitTalkBalloon()
{
	VEC3 vSize = { 1.5f, 1.f, 1.2f };
	m_pTalkBalloon = CTalkBallon::Create(m_pGraphicDev, m_pInfo->m_vPos, vSize, 1);
	NULL_CHECK_RETURN(m_pTalkBalloon, E_FAIL);
	m_pTalkBalloon->SetOnOff(TRUE);

	return S_OK;
}

HRESULT CCat::InitTalkWindow()
{
	m_pTalkWindow = CTalkWindow::Create(m_pGraphicDev, L"안녕. 가까이 와봐.");
	NULL_CHECK_RETURN(m_pTalkWindow, E_FAIL);
	m_pTalkWindow->SetOnOff(false);

	return S_OK;
}


void CCat::SceneChange()
{
	if (m_eState[Engine::CUR] != m_eState[Engine::PRE])
	{
		switch (m_eState[Engine::CUR])
		{
		case C_STATE::IDLE:
			m_wstrCurStateKey = m_wstrStateKey[IDLE];
			break;

		case C_STATE::ATTACK:
			m_wstrCurStateKey = m_wstrStateKey[ATTACK];
			break;
		}
		m_eState[Engine::PRE] = m_eState[Engine::CUR];
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrCurStateKey));
	}
}

void CCat::CheckEvent()
{
	switch (m_iEvent)
	{
	case CCat::E_NO:
		Event_No();
		break;
	case CCat::E_HI:
		Event_Hi();
		break;
	case CCat::E_FISH:
		Event_Fish();
		break;
	case CCat::E_SLUDGE:
		Event_SLUDGE();
		break;
	case CCat::E_EYE:
		Event_EYE();
		break;

	case CCat::E_GOLIATH:
		Event_GOLIATH();
		break;

	case CCat::E_QAYIN:
		Event_QAYIN();
		break;

	case CCat::E_ENDTALK:
		Event_ENDTALK();
		break;
	}
}

void CCat::Event_No()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 5.f)									// 가까이 왔을때 말이 걸린다
	{
		m_pTalkBalloon->SetOnOff(false);
		m_pTalkWindow->SetOnOff(true);
		m_iEvent = E_HI;
		m_iEvent_TalkCnt = 0;
	}
	else
		TalkAfter(fDis);
}

void CCat::Event_Hi()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkWindow->SetTalk(L"반가워. 내이름은 고냥이.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetTalk(L"배가 고프네. 아래 물가에서 생선좀 가져와.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					m_iEvent_TalkCnt = 0;
					m_iEvent = E_EVENT::E_FISH;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(true);
				}
			}
		}
	}
}

void CCat::Event_Fish()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					if ((m_pPlayer->GetDBoss() & FISHBIT) == 0)
					{
						m_pTalkWindow->SetOnOff(TRUE);
						m_pTalkWindow->SetTalkEnd(FALSE);
						m_pTalkWindow->SetTalk(L"가져오라니깐!.");
						m_iEvent_TalkCnt = 1; //끈다
											  //안가져옴
					}
					else
					{
						m_pTalkWindow->SetOnOff(TRUE);
						m_pTalkWindow->SetTalkEnd(FALSE);
						m_pTalkWindow->SetTalk(L"좋았어!. 다음 부탁도 있으니 준비되면 이야기해.");
						m_iEvent_TalkCnt = 2; //다음 이벹느 진행
											  //가져옴
					}
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(true);
					m_iEvent_TalkCnt = 0;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(true);
					m_iEvent_TalkCnt = 0;
					m_iEvent = E_SLUDGE;
				}
			}
		}
	}
}

void CCat::Event_SLUDGE()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkBalloon->SetOnOff(false);
					m_pTalkWindow->SetOnOff(true);
					m_pTalkWindow->SetTalk(L"이 동네에 슬라임이 사는데 잡아줘.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetTalk(L"녀석은 두번째 동굴에 살아.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					++m_iEvent_TalkCnt;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
				}
				else if (m_iEvent_TalkCnt == 3)
				{
					DWORD Temp = m_pPlayer->GetDBoss() & BOSSSTAGE0;
					if (Temp == 0x00000000)
					{
						m_pTalkBalloon->SetOnOff(false);
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalk(L"빨리 잡아줘...");
						++m_iEvent_TalkCnt;
						//제대로 안함
					}
					else
					{
						m_pTalkBalloon->SetOnOff(false);
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalk(L"잘했어. 다음부탁도 있으니 준비하라구.");
						m_eState[Engine::CUR] = C_STATE::ATTACK;
						m_iEvent_TalkCnt =5;
					}
				}
				else if (m_iEvent_TalkCnt == 4)
				{
					--m_iEvent_TalkCnt;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
				}
				else if (m_iEvent_TalkCnt == 5)
				{
					m_iEvent_TalkCnt = 0;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
					m_iEvent = E_EVENT::E_EYE;
				}
			}
		}
	}
}

void CCat::Event_QAYIN()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkBalloon->SetOnOff(false);
					m_pTalkWindow->SetOnOff(true);
					m_pTalkWindow->SetTalk(L"");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetTalk(L"맨 오른쪽에 있는 방에가면 못생긴 괴물이 있어.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					m_pTalkWindow->SetTalk(L"녀석을 처치해줘. 난 못생긴건 싫어해.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 3)
				{
					++m_iEvent_TalkCnt;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
				}
				else if (m_iEvent_TalkCnt == 4)
				{
					DWORD Temp = m_pPlayer->GetDBoss() & BOSSSTAGE2;
					if (Temp == 0x00000000)
					{
						m_pTalkBalloon->SetOnOff(false);
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalk(L"안가고 뭐하고 있어!");
						++m_iEvent_TalkCnt;
						//제대로 안함
					}
					else
					{
						m_pTalkBalloon->SetOnOff(false);
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalk(L"좋아. 마지막 부탁이 있으니 기다리고 있을게.");
						m_iEvent_TalkCnt = 6;
						m_eState[Engine::CUR] = C_STATE::ATTACK;

					}
				}
				else if (m_iEvent_TalkCnt == 5)
				{
					--m_iEvent_TalkCnt;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
				}
				else if (m_iEvent_TalkCnt == 6)
				{
					m_iEvent_TalkCnt = 0;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
					m_iEvent = E_EVENT::E_GOLIATH;
				}
			}
		}
	}
}
void CCat::Event_GOLIATH()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkBalloon->SetOnOff(false);
					m_pTalkWindow->SetOnOff(true);
					m_pTalkWindow->SetTalk(L"어서와 친구.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetTalk(L"위쪽으로 가면 커다란 돌덩이가 있을거야.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					m_pTalkWindow->SetTalk(L"매일 땅을 치면서 내 낮잠을 방해하는 녀석이야.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 3)
				{
					m_pTalkWindow->SetTalk(L"시끄러운 녀석이니 빨리가서 혼내줘");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 4)
				{
					++m_iEvent_TalkCnt;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
				}
				else if (m_iEvent_TalkCnt == 5)
				{
					DWORD Temp = m_pPlayer->GetDBoss() & BOSSMAP0;
					if (Temp == 0x00000000)
					{
						m_pTalkBalloon->SetOnOff(false);
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalk(L"길을 잊어버렸어? 방 위에 있는 공간이야.");
						++m_iEvent_TalkCnt;
						//제대로 안함
					}
					else
					{
						m_pTalkBalloon->SetOnOff(false);
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalk(L"정말 고마워. 이제 조용한 곳에서 살 수 있겠어.");
						m_iEvent_TalkCnt = 7;
						m_eState[Engine::CUR] = C_STATE::ATTACK;

					}
				}
				else if (m_iEvent_TalkCnt == 6)
				{
					--m_iEvent_TalkCnt;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
				}
				else if (m_iEvent_TalkCnt == 7) //대화종료
				{
					m_pTalkWindow->SetTalk(L"넌 나가는 길을 찾고있지?");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 8) //대화종료
				{
					m_pTalkWindow->SetTalk(L"좀더 위로 올라가면 계단이 보일거야. ");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 9) //대화종료
				{
					m_pTalkWindow->SetTalk(L"거긴 굉장히 추운 곳이니 조심해. ");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 10) //대화종료
				{
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
					m_iEvent_TalkCnt = 0;
					m_iEvent = E_ENDTALK;
				}
			}
		}
	}
}

void CCat::Event_EYE()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkBalloon->SetOnOff(false);
					m_pTalkWindow->SetOnOff(true);
					m_pTalkWindow->SetTalk(L"가장 왼쪽에 있는 방이 보이지?");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetTalk(L"거기에 상자녀석이 살고있어.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					m_pTalkWindow->SetTalk(L"녀석을 상자로 이용하고 싶으니 처지해줘.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 3)
				{
					++m_iEvent_TalkCnt;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
				}
				else if (m_iEvent_TalkCnt == 4)
				{
					DWORD Temp = m_pPlayer->GetDBoss() & BOSSSTAGE1;
					if (Temp == 0x00000000)
					{
						m_pTalkBalloon->SetOnOff(false);
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalk(L"안가고 뭐해...");
						++m_iEvent_TalkCnt;
						//제대로 안함
					}
					else
					{
						m_pTalkBalloon->SetOnOff(false);
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalk(L"잘했어. 다음부탁도 있으니 준비하라구.");
						m_eState[Engine::CUR] = C_STATE::ATTACK;
						m_iEvent_TalkCnt = 6;
					}
				}
				else if (m_iEvent_TalkCnt == 5)
				{
					--m_iEvent_TalkCnt;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
				}
				else if (m_iEvent_TalkCnt == 6)
				{
					m_iEvent_TalkCnt = 0;
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
					m_iEvent = E_EVENT::E_QAYIN;
				}
			}
		}
	}
}

void CCat::Event_ENDTALK()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkBalloon->SetOnOff(false);
					m_pTalkWindow->SetOnOff(true);
					m_pTalkWindow->SetTalk(L"나가는 길은 북쪽에 있는 큰 계단이야.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetOnOff(FALSE);
					m_pTalkWindow->SetTalkEnd(TRUE);
					--m_iEvent_TalkCnt;
				}
			}
		}
	}
}

void CCat::TalkAfter(const float& fDis)
{
	if (m_pTalkWindow->GetOnOff())
	{
		if (fDis > 8.f)
		{
			m_pTalkWindow->SetOnOff(false);
			m_pTalkWindow->SetTalkEnd(true);
			C_STATE::IDLE;
		}
	}
	ShowTalkBalloon();
}


void CCat::ReturnIdle()
{
	if (m_eState[Engine::CUR] == C_STATE::ATTACK)
	{
		m_fCnt += Engine::Get_TimeMgr()->GetTime();

		if (m_pTexAni->CheckAniEnd())
		{
			if (m_fCnt > 3.f)
			{
				m_eState[Engine::CUR] = C_STATE::IDLE;
				m_fCnt =  0.f;
			}
		}
	}
}

void CCat::SetTransform()
{
	const D3DXMATRIX* pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);
	const D3DXMATRIX* pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);
}

CCat* CCat::Creare(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, CPlayer* pPlayer)
{
	CCat* pInstance = new CCat(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos, pPlayer)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}