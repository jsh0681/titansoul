#ifndef Corver_h__
#define Corver_h__

#include "GameObject.h"

namespace Engine
{
	class CTransform;
	class CVIBuffer;
	class CTexture;
}

class CTerrain;
class CCameraObserver;
class CCorver : public Engine::CGameObject
{
private:
	explicit CCorver(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CCorver();

private:
	virtual HRESULT Initialize(CTerrain* pTerrain, const int& iIndex);
	virtual void	Release();

	HRESULT			AddComponent();

public:
	virtual void	Update();
	virtual void	Render();

private:
	void			SetTransform();
	void			InitVertex();

private:
	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	DWORD					m_dwVtxCnt;
	int						m_iIndex = 0;

	CTerrain*				m_pTerrain;

	wstring					m_wstrBufferKey;
	wstring					m_wstrTexKey;


	CCameraObserver*	m_pCameraObserver;

public:
	static CCorver*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CTerrain* pTerrain, const int& iIndex);
};

#endif // !Corver_h__