#include "stdafx.h"
#include "Icicle.h"

#include "CameraObserver.h"
#include "Transform.h"
#include "Export_Function.h"
#include "SphereColBox.h"

#define GV 9.8f
#define JUMPPW 18.0f
#define JUMPAC 3.5f

CIcicle::CIcicle(LPDIRECT3DDEVICE9 pGraphicDev)
	:CGameObject(pGraphicDev)
	, m_pResourcesMgr(Engine::Get_ResourceMgr())
	, m_pTimeMgr(Engine::Get_TimeMgr())
	, m_pInfoSubject(Engine::Get_InfoSubject())
{
}


CIcicle::~CIcicle()
{
	Release();
}

HRESULT CIcicle::Initialize(const D3DXVECTOR3 & vPos)
{
	//자기자신 pos, 플레이어를향햔 dir
	m_pCameraObserver = CCameraObserver::Create();
	Engine::Get_InfoSubject()->Subscribe(m_pCameraObserver);

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Icicle", m_pVertex);
	InitVertex();

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos = vPos;
	m_pInfo->m_vDir = { 0.f, -1.f, 0.f };

	m_fSpeed = 10.f;

	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 1.f, true);

	m_fJumpAcc = JUMPAC;
	m_fJumpPow = JUMPPW;



	int i =GetRandom<int>(0, 6);
	if (i % 2 != 0)
	{
		++i;
	}
	m_iResourceIndex = i;



	return S_OK;
}

void CIcicle::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	if (!m_bBreakMode)
	{
		m_pInfo->m_vPos += m_pInfo->m_vDir * m_fSpeed * fTime;
	}
	CGameObject::Update();
	SetTansform();
}

void CIcicle::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Icicle", m_pConvertVertex);

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pTextureCom->Render(m_iResourceIndex);
	m_pBufferCom->Render();
	//m_pSphereBox->Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CIcicle::Release()
{
	Engine::Get_InfoSubject()->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete(m_pSphereBox);


	if (nullptr != m_pVertex)
	{
		Engine::Safe_Delete_Array(m_pVertex);
	}

	if (nullptr != m_pConvertVertex)
	{
		Engine::Safe_Delete_Array(m_pConvertVertex);
	}
}

HRESULT CIcicle::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	// buffer
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Icicle");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);

	// texture

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"BulletYetiIcicle");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTextureCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTextureCom);


	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

void CIcicle::SetTansform()
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);
}

void CIcicle::InitVertex()
{
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[0].vTex = { 0, 0 };

	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[1].vTex = { 1, 0 };

	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[2].vTex = { 1, 1 };

	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };
	m_pVertex[3].vTex = { 0, 1 };
}

void CIcicle::BreakIce()
{
	if (!m_bBreakMode)
	{
		m_bBreakMode = TRUE;
		m_pInfo->m_vPos.y = 0.1f;
		m_iResourceIndex += 1;
	}
}

const float & CIcicle::GetRadius()
{
	return m_pSphereBox->GetRadius();
}

CIcicle * CIcicle::Create(LPDIRECT3DDEVICE9 pDevice, const D3DXVECTOR3 & vPos)
{
	CIcicle*	pInstance = new CIcicle(pDevice);

	if (FAILED(pInstance->Initialize(vPos)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
