#ifndef SelectObj_h__
#define SelectObj_h__

/*!
 * \class CSelectObj
 *
 * \간략정보 선택창에서 움직일 모양
 *
 * \상세정보 
 *
 * \작성자 윤유석
 *
 * \date 1월 18 2019
 *
 */

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
}

class CCameraObserver;
class CSelectObj :	public Engine::CGameObject 
{
private:
	explicit CSelectObj(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CSelectObj();

public:
	virtual void Update();
	virtual void Render();

private:
	HRESULT	AddComponent();
	virtual HRESULT Initialize(const VEC3& vPos);
	virtual void	Release();
	virtual void	SetTransform();

	void	InitVertex();

public:
	void	SetPos(const VEC3& vPos);
	void	SetPosY(const float& fY);

	const float& GetPosY();
	const VEC3& GetPos();

private:
	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	CCameraObserver*		m_pCameraObserver;

	DWORD					m_dwVtxCnt;
	wstring					m_wstrBuffer;
	wstring					m_wstrTexture;

public:
	static CSelectObj*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos);
};

#endif // !SelectObj_h__