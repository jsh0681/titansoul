#ifndef TalkWindow_h__
#define TalkWindow_h__

#include "GameObject.h"


/*!
 * \class CTalkWindow
 *
 * \간략정보 대화창
 *
 * \상세정보 
 *
 * \작성자 윤유석
 *
 * \date 1월 18 2019
 *
 */

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTimeMgr;
	class CKeyMgr;
}

class CCameraObserver;
class CTalkWindow : public Engine::CGameObject
{
private:
	explicit CTalkWindow(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CTalkWindow();

public:
	virtual void Update();
	virtual void Render();

private:
	HRESULT	AddComponent();
	virtual HRESULT Initialize(const wstring& wstrTalk);
	virtual void	Release();

public:
	void			SetTalk(const wstring& wstrTalk);
	const bool&		GetTalkEnd()						{ return m_bTalkEnd; }
	void			SetOnOff(const bool& bOn)			{ m_bOn = bOn; }
	void			SetTalkEnd(const bool& bEnd)		{ m_bTalkEnd = bEnd; }
	bool			GetOnOff()							{ return m_bOn; }
	void			SetTalkSize(const float& fSize)		{ m_fTalkSize = fSize; }

private:
	void			InitVertex();
	void			SetTransform();
	void			TalkSkip();

private:
	D3DXMATRIX			m_matView;
	D3DXMATRIX			m_matProj;
	const D3DXMATRIX*	m_pMatOrtho = nullptr;
	D3DXMATRIX			m_matIdentity;


	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;
	Engine::CTimeMgr*		m_pTimeMgr;
	Engine::CKeyMgr*		m_pKeyMgr;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	LPD3DXFONT				m_pFont = nullptr;

	CCameraObserver*		m_pCameraObserver;

	DWORD					m_dwVtxCnt;

	wstring					m_wstrImgName;

	int						m_iTalkMaxSize = 0;
	float					m_fTalkSize = 0;

	bool					m_bTalkEnd	= false;;
	bool					m_bOn		= false;

	wstring					m_wstrTalk = L"";
	RECT					m_tRc;

public:
	static		CTalkWindow*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const wstring& wstrTalk);
};

#endif // !TalkWindow_h__