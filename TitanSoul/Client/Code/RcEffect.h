#ifndef RcEffect_h__
#define RcEffect_h__


#include "Effect.h"

class CRcEffect : public CEffect
{
private:
	explicit CRcEffect(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CRcEffect();
	virtual void Release();
public:
	virtual void Render();
	virtual void SetTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)		override;

private:
	HRESULT	Initialize(EFFECTINFO* pEffectInfo);


public:
	static CRcEffect* Create(LPDIRECT3DDEVICE9 pGraphicDev, EFFECTINFO* pEffectInfo);
};
#endif // RcEffect_h__
