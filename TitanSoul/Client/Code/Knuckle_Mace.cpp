#include "stdafx.h"
#include "Knuckle_Mace.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"

CKnuckle_Mace::CKnuckle_Mace(LPDIRECT3DDEVICE9 pGraphicDev) :
	CKnuckle_Part(pGraphicDev)
{
}


CKnuckle_Mace::~CKnuckle_Mace()
{
	Release();
}

HRESULT CKnuckle_Mace::Initialize(const VEC3& vPos, const int& iDir)
{
	m_dwVtxCnt = 4;
	m_wstrBufferName = L"Buffer_Knuckle_Mace";

	// 부모에서 지워줌(카메라, 버텍스, 컴포넌트)
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = vPos;

	if (iDir == M_LEFT)
	{
		m_pInfo->m_vPos.x += 4.f;
		m_eMaceDir = M_LEFT;

	}
	else if (iDir == M_RIGHT)
	{
		m_pInfo->m_vPos.x -= 4.f;
		m_eMaceDir = M_RIGHT;
	}

	m_pInfo->m_vPos.z -= 0.7f;
	m_pInfo->m_vScale = { 1.7f, 1.f, 1.7f };

	ResetBuffer();

	return S_OK;
}

HRESULT CKnuckle_Mace::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferName);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"KnuckleMaceNo");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

void CKnuckle_Mace::SetTransform()
{
	const D3DXMATRIX* matView = m_pCameraObserver->GetView();
	const D3DXMATRIX* matProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, matView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, matProj);
}

void CKnuckle_Mace::Update()
{
	switch (m_eState)
	{
	case CKnuckle_Part::CHECKBACK:
		Body_Rot();
		break;
	case CKnuckle_Part::ATTACK:
		AttackFunc();
		break;
	case CKnuckle_Part::BODY_ROT:
		Body_Rot();
		break;
	case CKnuckle_Part::ATTACK_ROT:
		AttackRotation();
		break;
	case CKnuckle_Part::ATTACK_JUMP:
		AttackJump();
		break;
	}
	CKnuckle_Part::Update();

	SetTransform();
}

void CKnuckle_Mace::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pConvertVertex);

	m_pTexture->Render(0);
	m_pBuffer->Render();
}

D3DXMATRIX* CKnuckle_Mace::GetWorldMat()
{
	return &m_pInfo->m_matWorld;
}

void CKnuckle_Mace::Body_Rot()
{
	if (m_bAttack)
		return;

	m_fLength = D3DXVec3Length(&(m_vTargetPos - m_pInfo->m_vPos));
	m_pInfo->m_vDir = m_vTargetPos - m_pInfo->m_vPos;
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

	m_pInfo->m_vPos += m_pInfo->m_vDir * m_fDeltaTime * m_fLength * m_fAccel;
	m_fAccel += 0.06f;

	if (m_fAccel > 15.f)
		m_fAccel = 15.f;
}

void CKnuckle_Mace::AttackFunc()
{
	if (!m_bAttack)
		return;

	m_fBehavioTime += m_fDeltaTime;

	m_fEndTime;
	m_pInfo->m_vDir = m_vTargetPos - m_pInfo->m_vPos;
	m_fLength = D3DXVec3Length(&m_pInfo->m_vDir);

	if (m_fLength > 15.f)
		m_fLength = 15.f;

	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

	m_pInfo->m_vPos += m_pInfo->m_vDir * m_fDeltaTime * m_fLength * 4.f;

	if (m_fBehavioTime < (m_fEndTime * 0.8f))
		m_pInfo->m_vPos.y += 30.f * m_fDeltaTime;
	else
	{
		m_pInfo->m_vPos.y -= 50.f * m_fDeltaTime;
		if (m_pInfo->m_vPos.y < BOSS_Y)
			m_pInfo->m_vPos.y = BOSS_Y;
	}
}

void CKnuckle_Mace::AttackRotation()
{
	m_fLength = D3DXVec3Length(&(m_vTargetPos - m_pInfo->m_vPos));
	m_pInfo->m_vDir = m_vTargetPos - m_pInfo->m_vPos;
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

	m_pInfo->m_vPos += m_pInfo->m_vDir * m_fDeltaTime * m_fLength * m_fAccel;
	m_fAccel += 0.03f;

	m_pInfo->m_vPos.y = BOSS_Y;

	if (m_fAccel > 10.f)
		m_fAccel = 10.f;
}

void CKnuckle_Mace::AttackJump()
{
	m_fBehavioTime += m_fDeltaTime;

	if (m_fBehavioTime < (m_fEndTime * 0.6f))
	{
		m_pInfo->m_vPos.y += 3.2f * m_fDeltaTime * 1.2f;
		m_pInfo->m_vPos.z += 2.f * m_fDeltaTime * 1.3f;
	}
	else if (m_fBehavioTime < (m_fEndTime * 0.7f))
	{
		m_pInfo->m_vPos.y -= 3.2f * m_fDeltaTime * 2.3f;
		m_pInfo->m_vPos.z -= 2.f * m_fDeltaTime * 1.6f;

		if (m_pInfo->m_vPos.y < 0.1f)
			m_pInfo->m_vPos.y = 0.1f;
	}
	else
	{
		m_pInfo->m_vPos.y -= 3.2f * m_fDeltaTime * 6.7f;
		m_pInfo->m_vPos.z -= 2.f * m_fDeltaTime * 1.6f;

		if (m_pInfo->m_vPos.y < 0.1f)
			m_pInfo->m_vPos.y = 0.1f;
	}
}

void CKnuckle_Mace::Release()
{
	ResetBuffer();
}

CKnuckle_Mace* CKnuckle_Mace::Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const int& iDir)
{
	CKnuckle_Mace* pInstance = new CKnuckle_Mace(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos, iDir)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}