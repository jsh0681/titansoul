#ifndef Stage5_h__
#define Stage5_h__

/*!
* \class CStage5
*
* \간략정보 장어 맵
*
* \상세정보 초기화 완료
*
* \작성자 윤유석
*
* \date 1월 6 2019
*
*/


namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CCamera;
}

#include "Scene.h"
class CPlayer;
class CStage5 : public Engine::CScene
{
private:
	explicit CStage5(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
public:
	virtual ~CStage5();
private:
	virtual HRESULT Initialize();
	virtual void	Release();

	HRESULT		Add_Environment_Layer();
	HRESULT		Add_GameObject_Layer();
	HRESULT		Add_UI_Layer();

public:
	virtual void Update();
	virtual void Render();

private:
	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;
	Engine::CCamera*				m_pCamera;

	CPlayer*	m_pPlayer;

public:
	static CStage5*		Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
};

#endif // !Stage5_h__