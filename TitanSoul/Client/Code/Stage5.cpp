#include "stdafx.h"
#include "Stage5.h"



// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

//Obj
#include "Player.h"
#include "StaticCamera2D.h"


CStage5::CStage5(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera) :
	Engine::CScene(pGraphicDev),
	m_pPlayer(pPlayer),
	m_pCamera(m_pCamera)
{
}

CStage5::~CStage5()
{
	Release();
}

HRESULT CStage5::Initialize()
{

	FAILED_CHECK_RETURN(Add_Environment_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_GameObject_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_UI_Layer(), E_FAIL);

	return S_OK;
}

void CStage5::Release()
{
}

HRESULT CStage5::Add_Environment_Layer()
{
	return S_OK;
}

HRESULT CStage5::Add_GameObject_Layer()
{
	return S_OK;
}

HRESULT CStage5::Add_UI_Layer()
{
	return S_OK;
}

void CStage5::Update()
{
}

void CStage5::Render()
{
}

CStage5* CStage5::Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	CStage5* pInstance = new CStage5(pGraphicDev, pPlayer, pCamera);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
