#include "stdafx.h"
#include "CubeAni.h"

#include "Export_Function.h"

CCubeAni::CCubeAni(LPDIRECT3DDEVICE9 pGraphicDev) :
	CAnimation(pGraphicDev)
{
}

CCubeAni::~CCubeAni()
{
	Release();
}

void CCubeAni::Release()
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
		m_pConvertVertex[i] = m_pVertex[i];

	m_pResourceMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pConvertVertex);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

HRESULT CCubeAni::Initialize(const vector<TEXINFO*>* pVecTexInfo, const wstring& wstrBufferKey)
{
	m_dwVtxCnt = 8;

	m_pVertex = new Engine::VTXCUBE[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXCUBE[m_dwVtxCnt];

	m_pResourceMgr->EngineToClient(Engine::RESOURCE_STATIC, wstrBufferKey, m_pVertex);

	if (CreateBuffer(wstrBufferKey) == E_FAIL)
		return E_FAIL;
	if (CreateTexture(pVecTexInfo) == E_FAIL)
		return E_FAIL;

	return S_OK;
}

void CCubeAni::Render()
{
	m_pResourceMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pConvertVertex);
	CAnimation::Render();
}


void CCubeAni::SetTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatWorld);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, pMatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pMatProj);
}

void CCubeAni::SetRatioTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		m_pConvertVertex[i].vPos.z *= (*(*m_pVecTexInfo).begin())->fRatioHeight;
		m_pConvertVertex[i].vPos.x *= (*(*m_pVecTexInfo).begin())->fRatioWidth;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatWorld);
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatView);
		if (m_pConvertVertex[i].vPos.z < 1.f)
			m_pConvertVertex[i].vPos.z = 1.f;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatProj);
	}
}

CCubeAni* CCubeAni::Create(LPDIRECT3DDEVICE9 pGraphicDev, const vector<TEXINFO*>* pVecTexInfo, const wstring& wstrBufferKey)
{
	CCubeAni* pInstance = new CCubeAni(pGraphicDev);
	if (FAILED(pInstance->Initialize(pVecTexInfo, wstrBufferKey)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
