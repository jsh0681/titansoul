#ifndef Knuckle_Part_h__
#define Knuckle_Part_h__
/*!
* \class CKnuckle_Part
*
* \간략정보 철퇴 부품들의 부모
*
* \상세정보
*
* \작성자 윤유석
*
* \date 1월 12 2019
*
*/

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
}

class CCameraObserver;
class CKnuckle_Part : public Engine::CGameObject
{
protected:
	enum PART_STATE { SLEEP, WAKE_UP, CHECKBACK, ATTACK, BODY_ROT, ATTACK_ROT, ATTACK_JUMP, S_END };
	explicit CKnuckle_Part(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CKnuckle_Part();

protected:
	virtual HRESULT AddComponent()			PURE;
	virtual void SetTransform()				PURE;
	virtual void ResetBuffer();

public:
	virtual void Update();
	virtual void Render() {};
	virtual void Release();

	virtual void SetSynchPos(const VEC3& vPos);
	virtual void SetSynchPosXZ(const float& fX, const float& fZ);
	virtual void SetSynchDir(const VEC3& vDir);
	virtual void SetSynchDirXZ(const float& fX, const float& fZ);

	virtual void SetTargetVec(const VEC3& vPos);
	virtual void SetDelataTime(const float& fDelata) { m_fDeltaTime = fDelata; }
	virtual void SetEndTime(const float& fEndTime) { m_fEndTime = fEndTime; }
	virtual void ResetBehavioTime()					{ m_fBehavioTime = 0.f; }

	virtual const VEC3& GetPos();

public:
	virtual void	SetState(const int& iState);

protected:
	virtual	void			Walk_UpFunc() {};
	virtual void			AttackFunc() {};
	virtual void			Body_Rot() {};
	virtual void			AttackRotation() {};
	virtual void			AttackJump() {};

protected:
	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	DWORD					m_dwVtxCnt;
	VEC3					m_vTargetPos;
	PART_STATE				m_eState;

	CCameraObserver*		m_pCameraObserver = nullptr;

	wstring					m_wstrBufferName;
	float					m_fDeltaTime = 0.f;
	float					m_fEndTime = 0.f;
	float					m_fBehavioTime = 0.f;
};

#endif // !Knuckle_Part_h__