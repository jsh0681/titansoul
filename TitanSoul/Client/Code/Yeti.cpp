#include "stdafx.h"
#include "Yeti.h"


//예티씬
#include "Yeti_Idle.h"
#include "Yeti_Move.h"
#include "Yeti_Roll.h"
#include "Yeti_Throw.h"
#include "Yeti_Land.h"
#include "Yeti_Die.h"


#include "TexAni.h"
#include "Export_Function.h"
#include "Include.h"
#include "PlayerObserver.h"
#include "SphereColBox.h"
#include "CollisionMgr.h"
#include "SnowBullet.h"
#include "Stage4.h"
#include "StaticCamera2D.h"

#include "NonAniEffect.h"
#define GV 9.8f
#define JUMPPW 17.0f
#define JUMPAC 3.0f


CYeti::CYeti(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pCollisionMgr = CCollisionMgr::GetInstance();
	m_pManagement = Engine::Get_Management();
	m_pTimeMgr = Engine::Get_TimeMgr();
}

CYeti::~CYeti()
{
	Release();
}

HRESULT CYeti::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;
	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_Yeti");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CYeti::CreateObj()
{
	return E_NOTIMPL;
}

HRESULT CYeti::Initialize(const D3DXVECTOR3& vPos, CStaticCamera2D* pCamera)
{

	m_pCamera = pCamera;
	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_wstrStateKey = L"YetiIdleDD";
	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = vPos;

	//씬설정
	m_State[Engine::PRE] = YETI_IDLE;
	m_State[Engine::CUR] = YETI_IDLE;
	m_DIR[Engine::PRE] = Engine::DD;
	m_DIR[Engine::CUR] = Engine::DD;
	StateKey();


	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 2.0f, true);
	m_pSphereHeap = CSphereColBox::Create(m_pGraphicDev, &m_HeapWorld, 0.5f, true);
	m_pTargetInfo = m_pPlayerObserver->GetPlayerTransform();

	m_fJumpPow = JUMPPW;
	m_fJumpAcc = JUMPAC;

	m_iThrowCnt = 4;
	m_iRollCnt = 3;
	m_pYetiScene = CYeti_Idle::Create(m_pGraphicDev, this);

	return S_OK;
}

void CYeti::Release()
{
	Engine::Safe_Delete(m_pYetiScene);

	Engine::Safe_Delete(m_pSphereBox);
	Engine::Safe_Delete(m_pSphereHeap);


	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);
}

void CYeti::StateKey()
{
	//enum eYETISTATE { YETI_IDLE, YETI_MOVE, YETI_THROW, YETI_ROLL, YETI_DIE, YETI_END };
	m_wstrYETI_StateKey[YETI_IDLE] = L"YetiIdle";
	m_wstrYETI_StateKey[YETI_MOVE] = L"YetiMove";;
	m_wstrYETI_StateKey[YETI_THROW] = L"YetiThrow";;
	m_wstrYETI_StateKey[YETI_ROLL] = L"YetiRoll";;
	m_wstrYETI_StateKey[YETI_LAND] = L"YetiLand";;
	m_wstrYETI_StateKey[YETI_DIE] = L"YetiDie";;
}

void CYeti::SceneChange()
{
	if (m_State[Engine::PRE] != m_State[Engine::CUR])
	{
		
		m_wstrStateKey = m_wstrYETI_StateKey[m_State[Engine::CUR]];
		if (m_DIR[Engine::CUR] % 2 == 0 && m_DIR[Engine::CUR] != 0)
		{
			m_wstrStateKey = m_wstrStateKey + g_wstrDir[m_DIR[Engine::CUR]-1];
		}
		else
		{
			m_wstrStateKey = m_wstrStateKey + g_wstrDir[m_DIR[Engine::CUR]];

		}
		m_State[Engine::PRE] = m_State[Engine::CUR];
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
		ScenePointerChange(m_State[Engine::CUR]);
	}

	//예티는 L방향이 없으므로... 예외처리가 필요하다고.
	//예외는 0이 아니고 짝수일시 -1로 해주고... 스케일을 -1로 곱해주면 된다 ㅋㅋ

	if (m_DIR[Engine::PRE] != m_DIR[Engine::CUR])
	{
		m_wstrStateKey.pop_back();
		m_wstrStateKey.pop_back();

		if (m_DIR[Engine::CUR] % 2 == 0 && m_DIR[Engine::CUR] != 0)
		{
			m_wstrStateKey = m_wstrStateKey + g_wstrDir[m_DIR[Engine::CUR] - 1];
			m_pInfo->m_vScale = { -1.f, 1.f, 1.f };
		}
		else
		{
			m_wstrStateKey = m_wstrStateKey + g_wstrDir[m_DIR[Engine::CUR]];
			m_pInfo->m_vScale = { 1.f, 1.f, 1.f };
		}

		m_DIR[Engine::PRE] = m_DIR[Engine::CUR];
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
	}
}

void CYeti::ScenePointerChange(eYETISTATE eID)
{
	Engine::Safe_Delete(m_pYetiScene);

	switch (eID)
	{
	case YETI_IDLE:
		m_pYetiScene = CYeti_Idle::Create(m_pGraphicDev, this);
		break;

	case YETI_ROLL:
		m_pYetiScene = CYeti_Roll::Create(m_pGraphicDev, this);
		break;

	case YETI_MOVE:
		m_pYetiScene = CYeti_Move::Create(m_pGraphicDev, this);
		break;

	case YETI_THROW:
		m_pYetiScene = CYeti_Throw::Create(m_pGraphicDev, this);
		break;

	case YETI_LAND:
		m_pYetiScene = CYeti_Land::Create(m_pGraphicDev, this);
		break;

	case YETI_DIE:
		m_pYetiScene = CYeti_Die::Create(m_pGraphicDev, this);
		break;
	}
}

void CYeti::SetTransform()
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pmatView, pmatProj);
}

void CYeti::SetHeapPos()
{
//	FLOAT _41, FLOAT _42, FLOAT _43, FLOAT _44 );
	m_HeapWorld = m_pInfo->m_matWorld;
	switch (m_DIR[Engine::CUR])
	{
	case Engine::DD:
		m_HeapWorld._41;
		m_HeapWorld._42;
		m_HeapWorld._43 += 1.2f;
		break;

	case Engine::UU:
		m_HeapWorld._41;
		m_HeapWorld._42;
		m_HeapWorld._43 -= 1.2f;
		break;

	case Engine::RR:
		m_HeapWorld._41 -= 0.8f;
		m_HeapWorld._42;
		m_HeapWorld._43 -= 0.8f;
		break;

	case Engine::LL:
		m_HeapWorld._41 += 0.8f;
		m_HeapWorld._42;
		m_HeapWorld._43 -= 0.8f;
		break;
		
	case Engine::UR:
		m_HeapWorld._41 -= 0.4f;
		m_HeapWorld._42;
		m_HeapWorld._43 -= 0.9f;

		break;

	case Engine::UL:
		m_HeapWorld._41 += 0.4f;
		m_HeapWorld._42;
		m_HeapWorld._43 -= 0.9f;
		break;

	case Engine::DR:
		m_HeapWorld._41 -= 0.8f;
		m_HeapWorld._42;
		m_HeapWorld._43 -= 0.3f;

		break;

	case Engine::DL:
		m_HeapWorld._41 += 0.8f;
		m_HeapWorld._42;
		m_HeapWorld._43 -= 0.3f;

		break;
	}
}

void CYeti::ChangeDirection(const float & _fAngle)
{
	if (-22.5f <= _fAngle && _fAngle <= 22.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::RR;
	}
	else if (22.5f <= _fAngle && _fAngle <= 67.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DR;
	}
	else if (67.5f <= _fAngle  && _fAngle <= 112.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DD;

	}
	else if (112.5f <= _fAngle && _fAngle <= 157.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DL;
	}
	else if (-157.5f <= _fAngle && _fAngle <= -112.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UL;

	}
	else if (-112.5f <= _fAngle  && _fAngle <= -67.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UU;

	}
	else if (-67.5 <= _fAngle  && _fAngle <= -22.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UR;
	}
	else
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::LL;
	}
}

void CYeti::SettingDirection(const float & fAngle)
{
	m_pInfo->m_vDir.x = cosf(D3DXToRadian(fAngle));
	m_pInfo->m_vDir.z = -sinf(D3DXToRadian(fAngle));

	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CYeti::Move(const float & fTime, const float & fSpd)
{
	m_pInfo->m_vPos += m_pInfo->m_vDir * fTime * fSpd;

	m_fEffectTime += fTime;
	if (m_fEffectTime > 0.03f)
	{
		SnowBallEffect(m_pInfo->m_vPos, 0.5f, 1.3f);
		SnowBallEffect(m_pInfo->m_vPos, 0.2f, 0.4f);

		m_fEffectTime = 0.f;
	}
}

bool CYeti::MovewithAccel(D3DXVECTOR3 vAccel, const float & fTime, const float & fSpd)
{
	// 포물선 공식( 수직 낙하 )
	// y = 힘(v) * sin@ * 가속도(t) - 9.8(g) * t * t * 0.5f

	float f포물선 = m_fJumpPow * m_fJumpAcc -
		GV * m_fJumpAcc * m_fJumpAcc * 0.5f;

	m_pInfo->m_vPos.y += f포물선 * fTime * 2.f;
	m_fJumpAcc += fTime * 0.5f; // 가속도는 꾸준히 증가한다.

	m_pInfo->m_vDir += vAccel * fTime;

	if (m_pInfo->m_vPos.y < 0.05f)
	{
		m_pInfo->m_vPos.y = 0.1f;
		m_fJumpPow = JUMPPW;
		m_fJumpAcc = JUMPAC;
		return FALSE;
	}

	if (D3DXVec3Length(&m_pInfo->m_vDir) < 0.2f)
	{
		

		m_pInfo->m_vDir = { 0.f, 0.f, 0.f };
	}
	m_pInfo->m_vPos += m_pInfo->m_vDir * fTime * fSpd;
	return TRUE;
}

void CYeti::SnowBallEffect(const D3DXVECTOR3 & vPos, float fScale, float fRange)
{
	Engine::CGameObject* pGameObject = nullptr;
	//여기서 이펙트 뿅 고드름 깨질때 터지는 이펙트로
	NorEffect Temp;
	float fRand;

	Temp.fLifeTime = 1.5f;
	Temp.vPos = vPos;


	Temp.vDir = { 0.f, 0.f, 0.f };
	Temp.fScale = fScale;
	Temp.vAcc = { 0.f, 0.f, 1.f };
	Temp.fDisappearSpd = 0.2f;
	Temp.fSpd = 3.f;

	fRand = GetRandom<float>(-fRange, fRange);
	Temp.vPos.x += fRand;
	Temp.vPos.z += fRand;
	pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectLittleSnowNO", 4);
	m_pStage->ObjListAdd(Engine::LAYER_GAMEOBJECT, L"Effect", pGameObject);

	fRand = GetRandom<float>(-fRange, fRange);
	Temp.vPos.x += fRand;
	Temp.vPos.z += fRand;
	pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectNiceSnowNO", 2);
	m_pStage->ObjListAdd(Engine::LAYER_GAMEOBJECT, L"Effect", pGameObject);

	fRand = GetRandom<float>(-fRange, fRange);
	Temp.vPos.x += fRand;
	Temp.vPos.z += fRand;
	Temp.vPos.x += fRand;
	Temp.vPos.z += fRand;
	pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectCircleSnowNO", 3);
	m_pStage->ObjListAdd(Engine::LAYER_GAMEOBJECT, L"Effect", pGameObject);
}

void CYeti::LandBallEffect(const D3DXVECTOR3 & vPos, int iCnt)
{
	Engine::CGameObject* pGameObject = nullptr;
	//여기서 이펙트 뿅 고드름 깨질때 터지는 이펙트로
	NorEffect Temp;
	float fRand;

	Temp.fLifeTime = 1.5f;
	Temp.vPos = vPos;
	Temp.vPos.z -= 2.f;

	Temp.vDir = { 0.f, 0.f, 0.f };
	Temp.fScale = 0.45f;
	Temp.vAcc = { 0.f, 0.f, -1.f };
	Temp.fDisappearSpd = 0.2f;
	Temp.fSpd = 3.f;

	float fAngle = 0.f;
	for (int i = 0; i < iCnt; ++i)
	{
		fRand = GetRandom<float>(1.5f, 10.f);
		Temp.fSpd = fRand;

		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectCircleSnowNO", 3);
		m_pStage->ObjListAdd(Engine::LAYER_GAMEOBJECT, L"Effect", pGameObject);

		float fRand = GetRandom<float>(7.f, 10.f);

		fAngle -= fRand;
	}

	fAngle = 0.f;

	for (int i = 0; i < iCnt; ++i)
	{
		fRand = GetRandom<float>(1.5f, 10.f);
		Temp.fDisappearSpd = 0.1f;
		Temp.fSpd = fRand;

		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectNiceSnowNO", 2);
		m_pStage->ObjListAdd(Engine::LAYER_GAMEOBJECT, L"Effect", pGameObject);

		float fRand = GetRandom<float>(7.f, 10.f);
		fAngle -= fRand;
	}
	fAngle = 0.f;

	for (int i = 0; i < iCnt; ++i)
	{
		fRand = GetRandom<float>(1.5f, 10.f);
		Temp.fSpd = fRand;
		Temp.fDisappearSpd = 0.1f;

		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectLittleSnowNO", 4);
		m_pStage->ObjListAdd(Engine::LAYER_GAMEOBJECT, L"Effect", pGameObject);

		float fRand = GetRandom<float>(7.f, 10.f);

		fAngle -= fRand;
	}
}

void CYeti::Update()
{
	if (m_bDead) return;

	float fTime = m_pTimeMgr->GetTime();

	m_pYetiScene->Update(fTime);
	SceneChange();

	CGameObject::Update();
	
	SetHeapPos();
	SetTransform();
}

void CYeti::Render()
{
	if (m_bDead) return;

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	//m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	//m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	//m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000088);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	m_pTexAni->Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CYeti::MakeSnowBullet(const float& fDegree)
{
	D3DXVECTOR3 vDir = { 0.f, 0.1f, 0.f };

	vDir.x = cosf(D3DXToRadian(fDegree));
	vDir.z = -sinf(D3DXToRadian(fDegree));

	D3DXVec3Normalize(&vDir, &vDir);



	
	//여기서 방향 체인지!!!!!!!!!!!!!!!!!!
	ChangeDirection(fDegree);


	dynamic_cast<CStage4*>(m_pStage)->MakeSnowBullet(m_pInfo->m_vPos, vDir);

	
}

void CYeti::MakeIcicle(const D3DXVECTOR3 & vDest)
{
	
	//고드름을만들자 뾰오ㅓㅇ

	D3DXVECTOR3 vLength = vDest - m_vStart;
	D3DXVECTOR3 vTemp;
	float fLeng = D3DXVec3Length(&vLength);
	D3DXVec3Normalize(&vLength, &vLength);

	float fRandLegth = 0.f;
	float fRandX = 0.f;
	float fRandZ = 0.f;
	float fRandY = 0.f;
	float fY = 30.f;
	while (1)
	{
		if (fLeng < 0.f) return;

		fRandLegth = GetRandom<float>(0.f, 2.5f);
		fRandY = GetRandom<float>(0.f, 1.0f);
		fRandX = GetRandom<float>(-2.5f, 2.5f);
		fRandZ = GetRandom<float>(-2.5f, 2.5f);

		fY += fRandY;
		vTemp = m_vStart + vLength * fLeng;
		vTemp.x += fRandX;
		vTemp.z += fRandX;

		vTemp.y = fY;
		fLeng -= 2.f + fRandLegth;

		//여기서 고드름 크리에이트
		dynamic_cast<CStage4*>(m_pStage)->MakeIcicleBullet(vTemp);
	}




}

void CYeti::ShakeCamera()
{
	m_pCamera->ShakingStart(0.7f, 1.5f, 1.f);
}


const float & CYeti::Get_HeapRadius()
{
	return m_pSphereHeap->GetRadius();
}

void CYeti::Set_HeapRadius(const float & fRadius)
{
	m_pSphereHeap->SetRadius(fRadius);

}

const float & CYeti::GetRadius()
{
	return m_pSphereBox->GetRadius();
}

void CYeti::SetRadius(const float & fRadius)
{
	m_pSphereBox->SetRadius(fRadius);
}

bool CYeti::CheckSceneEnd()
{
	if (m_pTexAni->CheckAniEnd())
	{
		return TRUE;
	}
	else
		return false;
}

int CYeti::GetSceneIndex()
{
	int i = m_pTexAni->GetIndex();
	return i;
}

float CYeti::GetPlayerDegree()
{
	//각도 구해서 예티 돌리고

	//플레이어
	D3DXVECTOR3 vDir = m_pTargetInfo->m_vPos - m_pInfo->m_vPos;
	D3DXVec3Normalize(&vDir, &vDir);

	float fCos = D3DXVec3Dot(&vDir, &g_vRight);

	fCos = acosf(fCos);
	fCos = D3DXToDegree(fCos);

	if (m_pInfo->m_vPos.z < m_pTargetInfo->m_vPos.z)
		fCos *= -1.f;


	//일정 각도 범위 지정하여 r- way 탄으로 날리면 더더욱 깔끔할 듯 싶다.

	//머리가 잘 굴러간다면야 첫번째 눈덩이 굴릴때 웨이브를 구현해도 좋겠다네.

	//vDir.x = +cosf(D3DXToRadian(fCos));
	//vDir.z = -sinf(D3DXToRadian(fCos));

	//D3DXVec3Normalize(&vDir, &vDir);

	return fCos;
}

void CYeti::SetDir(const D3DXVECTOR3 & _vDir)
{
	m_pInfo->m_vDir = _vDir;
}

void CYeti::SetStartVector()
{
	m_vStart = m_pInfo->m_vPos;
}

CYeti * CYeti::Create(LPDIRECT3DDEVICE9 pGraphicDev, CStaticCamera2D* pCamera, const D3DXVECTOR3 & vPos)
{
	CYeti* pInstance = new CYeti(pGraphicDev);

	if (FAILED(pInstance->Initialize(vPos, pCamera)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

