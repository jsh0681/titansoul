#ifndef Animal_h__
#define Animal_h__

/*!
* \class CLogoArrow
*
* \간략정보 로딩이 된 정도를 알려주는 클래스
*
* \상세정보
*
* \작성자 윤유석
*
* \date 1월 5 2019
*
*/

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
	class CResourcesMgr;
	class CTimeMgr;
	class CInfoSubject;
}

class CCameraObserver;
class CAnimal : public Engine::CGameObject
{
private:
	explicit CAnimal(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CAnimal();	

private:
	HRESULT	AddComponent();
	virtual HRESULT	Initialize();
	virtual void	Release();

public:
	virtual void Update();
	virtual void Render();

private:
	void		SetTransform();

public:
	const D3DXVECTOR3&		GetPos();
	void		SetPos(const D3DXVECTOR3& vPos);

private:
	Engine::CResourcesMgr*		m_pResourcesMgr = nullptr;
	Engine::CTimeMgr*			m_pTimeMgr = nullptr;
	Engine::CInfoSubject*		m_pInfoSubject = nullptr;

	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;
	Engine::CTransform*			m_pInfo;

	CCameraObserver*			m_pCameraObserver = nullptr;

	float						m_fFrameCnt = 0.f;
	float						m_fFrameMax = 10.f;

	DWORD						m_dwVtxCnt = 0;

	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;

public:
	static CAnimal*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
};

#endif // !Animal_h__