#ifndef EyeCubeRaser_h__
#define EyeCubeRaser_h__

#include "Export_Function.h"
#include"GameObject.h"
#include"CameraObserver.h"
#include"PlayerObserver.h"
#include"CollisionMgr.h"
#include"TerrainColl.h"

class CTexAni;

class CEyeCubeRaser : public Engine::CGameObject
{
private:
	explicit CEyeCubeRaser(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CEyeCubeRaser(void);

	void DrawRaser();

public:
	void SetStartPos(D3DXVECTOR3 vStartPos) { m_pInfo->m_vPos = vStartPos; }
	virtual void Render();
	virtual void Update();
	virtual void Release();
	void SetAngleX(float fAngleX) { m_fAngleX = fAngleX; }
	void SetAngleZ(float fAngleZ) { m_fAngleZ = fAngleZ; }
	void SetEyeCubePos(D3DXVECTOR3 vEyeCubePos) { m_vEyeCubePos = vEyeCubePos; }
	


public:
	
	void SettingDir(int iDir) {
		m_iDir = iDir;
	}
	void SetDir(D3DXVECTOR3 vDir) { m_vEyeDir = vDir; }
private:
	virtual HRESULT Initialize();
	virtual HRESULT AddComponent(void);
	virtual void SetTransform();
	virtual		void SetDirection(void);
	
private:
	int m_iDir=0;
	float m_fAngleZ = 0.f;
	float m_fAngleX = 0.f;
	D3DXVECTOR3 m_vRight;
	D3DXVECTOR3 m_vLook;

	D3DXVECTOR3 m_vEyeDir;

	CCameraObserver*				m_pCameraObserver = nullptr;
	CPlayerObserver*				m_pPlayerObserver = nullptr;

	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;
	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *					m_pTerrainCol = nullptr;
	D3DXVECTOR3						m_vEyeCubePos;

	ID3DXMesh*						pillar = 0;


private:
	bool m_bMoveRightX = false;
	bool m_bMoveUpZ = false;
	bool m_bMoveLeftX = false;
	bool m_bMoveDownZ = false;

	float fRotateTime = 0;
	bool m_bCalculate = false;

	D3DXMATRIX* m_pMatWorld;
	D3DXMATRIX* m_pMatView;
	D3DXMATRIX* m_pMatProj;
private:
	int m_iIndex = 0;
	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...
	CTexAni* m_pTexAni = nullptr;
public:
	static CEyeCubeRaser* Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos);
private:

protected:
};
#endif // EyeCubeRaser_h__
