#include"stdafx.h"
#include"EyeCubeRaser.h"
#include "Export_Function.h"
#include "Include.h"
#include"TexAni.h"
#include<cmath>


CEyeCubeRaser::CEyeCubeRaser(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{

}

CEyeCubeRaser::~CEyeCubeRaser(void)
{
	Release();
}
void CEyeCubeRaser::DrawRaser()
{
	
}
void CEyeCubeRaser::Release()
{
	Engine::Safe_Release<ID3DXMesh*>(pillar);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

}

HRESULT CEyeCubeRaser::Initialize()
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();
	m_pCollisionMgr = CCollisionMgr::GetInstance();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_wstrStateKey = L"EyeCubeAttackCurse";

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);


	return S_OK;
}

HRESULT CEyeCubeRaser::AddComponent(void)
{
	Engine::CComponent* pComponent = nullptr;

	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_EyeCubeCurse");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	m_pInfo->m_vPos = D3DXVECTOR3(30.f, 2.f, 40.f);

	m_pMatWorld = &(m_pInfo->m_matWorld);
	m_pMatView = const_cast<D3DXMATRIX*>(m_pCameraObserver->GetView());
	m_pMatProj = const_cast<D3DXMATRIX*>(m_pCameraObserver->GetProj());

	return S_OK;
}

void CEyeCubeRaser::Render()
{
	m_pTexAni->Render();
}

void CEyeCubeRaser::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	m_iIndex = m_pTexAni->GetIndex();


	Engine::CGameObject::Update();

	SetDirection();
	SetTransform();

}


void CEyeCubeRaser::SetTransform()
{

	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);
}

void CEyeCubeRaser::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

}

CEyeCubeRaser* CEyeCubeRaser::Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos)
{
	CEyeCubeRaser* pInstance = new CEyeCubeRaser(pGraphicDev);
	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	//pInstance->SetStartPos(vStartPos);

	return pInstance;
}
