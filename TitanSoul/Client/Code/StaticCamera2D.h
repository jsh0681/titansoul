#ifndef StaticCamera2D_h__
#define StaticCamera2D_h__

#include "Camera.h"

namespace Engine
{
	class CTimeMgr;
	class CTransform;
	class CInfoSubject;
}

class CStaticCamera2D : public Engine::CCamera
{
private:
	explicit CStaticCamera2D(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CStaticCamera2D();

public:
	HRESULT Initialize(void);
	virtual void Update(void);
	virtual void Render(void);
	virtual void Release(void);

public:
	void SetCameraTarget(const Engine::CTransform* pInfo);
	void TargetRenewal();
	void FocusPlayer();
	void ShakingCamera(const float& fTime);
	void ShakingStart(const float& fTime, const float& fPower, const float& fSpd);
	void ZoomIn(const D3DXVECTOR3 vPos, const float& fSpd, const float& fYSpd);
	void ZoomOut();
	void SetCameraSpeed(const float& fSpd) { m_fPlayerSpd = fSpd; };


public:
	static CStaticCamera2D* Create(LPDIRECT3DDEVICE9 pGraphicDev, const Engine::CTransform* pTransform);
private: //멤버변수

	D3DXVECTOR3 m_vDir;//본인


	const Engine::CTransform* m_pInfo; //타겟의 pInfo;
	Engine::CTimeMgr*	m_pTimeMgr; //타임Mgr 멤버변수로 가지고 있음.

	Engine::CInfoSubject*	m_pInfoSubject;

	D3DXVECTOR3				m_vZoomPos;
	float						m_fPlayerSpd = 1.f;
	float						m_fZoomYSpd = 0.f;
	float						m_fZoomSpd = 0.f;
	float						m_fDistance = 22.5f;
	float						m_fSpeed = 5.5f;
	float						m_fAngle = 89.5f;
	float						m_fStaticDistance = 8.f;

	float						m_fCameraX = 0.f;
	float						m_fCameraY = 100.f;

	float						m_fTime = 0.f;
	float						m_fShakingSpd = 0.f;
	float						m_fShakingPower = 0.f;
	float						m_fZoomY = 18.f;
	bool						m_bShakingMode = FALSE;
	
	bool						m_bZoomIn = FALSE;
	bool						m_bZoomOut = FALSE;

	bool						m_b3DMode = FALSE;

};

#endif // StaticCamera2D_h__
