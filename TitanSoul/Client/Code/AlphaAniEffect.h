#ifndef AlphaAniEffect_h__
#define AlphaAniEffect_h__

#include "NormalEffect.h"

namespace
{
}
class CAlphaAniEffect :
	public CNormalEffect
{
public:
	CAlphaAniEffect(LPDIRECT3DDEVICE9 pGraphicDev);
	virtual ~CAlphaAniEffect();

public:
	virtual HRESULT Initialize(NorEffect tNorEffect, wstring wstrBuffer, wstring wstrTex, int iIndexCnt = 0);
	virtual void InitVertex();
	virtual void Release(void) override;
	virtual void Update();
	virtual void Render();
	virtual void SetTransform();

public:
	static CAlphaAniEffect* Create(LPDIRECT3DDEVICE9 pGraphicDev, NorEffect tNorEffect, wstring wstrBuffer, wstring wstrTex, int iIndexCnt = 0);

private:
	float		m_fDegree = 0.f;
	float		m_fScale = 0.f;
	float		m_fSpeed = 0.f;
	float		m_fSpeed2 = 0.f;
	D3DXVECTOR3 m_vAccel;
	bool		m_bAcc = FALSE;
	bool		m_bAngleDir = FALSE;
	
};

#endif // AlphaAniEffect_h__
