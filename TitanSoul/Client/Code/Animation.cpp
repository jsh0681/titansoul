#include "stdafx.h"
#include "Animation.h"

#include "Export_Function.h"

CAnimation::CAnimation(LPDIRECT3DDEVICE9 pGraphicDev) :
	m_pGraphicDev(pGraphicDev),
	m_pResourceMgr(Engine::Get_ResourceMgr()),
	m_pTimeMgr(Engine::Get_TimeMgr())
{
}

CAnimation::~CAnimation()
{
	Release();
}

void CAnimation::Release()
{
	Engine::Safe_Delete(m_pBuffer);
	Engine::Safe_Delete(m_pTexture);

	m_pResourceMgr	= nullptr;
	m_pGraphicDev	= nullptr;
	m_pTimeMgr		= nullptr;
}

HRESULT CAnimation::CreateBuffer(const wstring& wstrBufferKey)
{
	m_wstrBufferKey = wstrBufferKey;

	Engine::CComponent*	pComponent = nullptr;
	pComponent = m_pResourceMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferKey);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);

	return S_OK;
}

HRESULT CAnimation::CreateTexture(const vector<TEXINFO*>* pVecTexInfo)
{
	NULL_CHECK_RETURN(pVecTexInfo, E_FAIL);
	m_pVecTexInfo = pVecTexInfo;

	Engine::CComponent*	pComponent = nullptr;
	pComponent = m_pResourceMgr->Clone(Engine::RESOURCE_STATIC, (*m_pVecTexInfo)[0]->wstrTexKey);
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);

	return S_OK;
}

void CAnimation::Update()
{
	fTimer += m_pTimeMgr->GetTime();

	if (m_pVecTexInfo != nullptr)
	{
		if ((*m_pVecTexInfo)[m_iIndex]->fDelay < fTimer)
		{
			fTimer = 0.f;
			m_iIndex = (*m_pVecTexInfo)[m_iIndex]->iNextNum;

			if (m_iIndex == 0)
				m_bEnd = true;
			else
				m_bEnd = false;
		}
	}
}

void CAnimation::Render()
{
	m_pTexture->Render(m_iIndex);
	m_pBuffer->Render();
}

void CAnimation::SetVecTexInfo(const vector<TEXINFO*>* pVecInfo)
{
	if (pVecInfo == nullptr)
		return;

	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourceMgr->Clone(Engine::RESOURCE_STATIC, (*pVecInfo)[0]->wstrTexKey);
	NULL_CHECK(pComponent);
	Engine::Safe_Delete(m_pTexture);

	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	m_pVecTexInfo = pVecInfo;
	m_iIndex = 0;
	fTimer = 0.f;
	m_bEnd = FALSE;
}

const D3DXVECTOR3& CAnimation::GetScale()
{
	if (m_pVecTexInfo != nullptr)
		return (*m_pVecTexInfo)[m_iIndex]->vScale;
	return move(D3DXVECTOR3{ 0.f,0.f,0.f });
}

const D3DXVECTOR3& CAnimation::GetRotation()
{
	if (m_pVecTexInfo != nullptr)
		return (*m_pVecTexInfo)[m_iIndex]->vRotation;
	return move(D3DXVECTOR3{ 0.f,0.f,0.f });
}

const D3DXVECTOR3& CAnimation::GetMove()
{
	if (m_pVecTexInfo != nullptr)
		return (*m_pVecTexInfo)[m_iIndex]->vMove;
	return move(D3DXVECTOR3{ 0.f,0.f,0.f });
}

const int& CAnimation::GetIndex()
{
	return m_iIndex;
}
