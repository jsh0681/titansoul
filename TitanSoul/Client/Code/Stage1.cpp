#include "stdafx.h"
#include "Stage1.h"


// Engine
#include"SceneSelector.h"
#include "Export_Function.h"

#include "NonAniEffect.h"
// Client
#include "Include.h"

#include"ShpereCol.h"
#include"CubeCol.h"

//Obj
#include "Player.h"
#include "Plane.h"
#include "Arrow.h"
#include"EyeCube.h"
#include "Display.h"
#include "StaticCamera2D.h"
#include "OrthoCamera.h"
#include "CeremonyEffect.h"
#include "CeremoneyEffect_List.h"


CStage1::CStage1(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera) :
	Engine::CScene(pGraphicDev),
	m_pManagement(Engine::Get_Management()),
	m_pResourcesMgr(Engine::Get_ResourceMgr()),
	m_pPlayer(pPlayer),
	m_pCamera(pCamera)
{
	m_pDisplay = CDisplay::Create(m_pGraphicDev);
	m_pDisplay->StartFade(CDisplay::FADEIN);
}

CStage1::~CStage1()
{
	Release();
}

HRESULT CStage1::Initialize()
{
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_CUBETEX,
		L"Buffer_EyeCube", 20, 20),
		E_FAIL);



	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_EyeCubeCurse", 10, 10),
		E_FAIL);


	SetStartPoint();

	m_pShpereCol = dynamic_cast<CShpereCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_SHPERE));

	m_pCubeCol = dynamic_cast<CCubeCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_CUBE));

	FAILED_CHECK_RETURN(Add_Environment_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_GameObject_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_UI_Layer(), E_FAIL);
	m_tPlayer = new tagObbColision;
	m_tEyeCube = new tagObbColision;
	m_tArrow = new tagObbColision;

	CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);

	return S_OK;
}

void CStage1::Release()
{
	Engine::Safe_Delete(m_pDisplay);

	if (m_bChangeScene)
	{
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"Player");
		m_mapLayer[Engine::LAYER_UI]->ClearList(L"Camera");
	}

	Engine::Safe_Delete(m_tPlayer);
	Engine::Safe_Delete(m_tArrow);
	Engine::Safe_Delete(m_tEyeCube);

	Engine::Safe_Delete(m_pCubeCol);
	Engine::Safe_Delete(m_pShpereCol);
}

HRESULT CStage1::Add_Environment_Layer()
{
	return S_OK;
}

HRESULT CStage1::Add_GameObject_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	Engine::CGameObject*		pGameObject = nullptr;

	pLayer->AddObject(L"Player", m_pPlayer);
	pBlendLayer->AddObject(L"Player", m_pPlayer);
	m_pPlayer->SetScene(this);
	m_pPlayer->CameraCreate();
	m_pPlayer->CreateArrow();

	CArrow* pArrow = m_pArrow = m_pPlayer->Get_ArrowPointer();
	Engine::CGameObject* pObject = dynamic_cast<Engine::CGameObject*>(pArrow);
	pLayer->AddObject(L"Arrow", pObject);
	pBlendLayer->AddObject(L"Arrow", pObject);

	pGameObject = m_pTerrain = CTerrainMgr::GetInstance()->Clone(L"MapTerrainStage1");
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Terrain", pGameObject);
	pNoBlendLayer->AddObject(L"Terrain", pGameObject);

	const D3DXVECTOR3& vBossPos = CTerrainMgr::GetInstance()->GetMapInfo(L"MapTerrainStage1")->vBossPoint;

	pGameObject = m_pEyeCube = CEyeCube::Create(m_pGraphicDev, vBossPos);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"EyeCube", pGameObject);
	pBlendLayer->AddObject(L"EyeCube", pGameObject);
	m_pEyeCube->SetArrow(m_pArrow);


	pLayer->AddObject(L"Effect", pGameObject);
	pBlendLayer->AddObject(L"Effect", pGameObject);

	pLayer->ClearList(L"Effect");
	pBlendLayer->ClearList(L"Effect");

	m_mapLayer.emplace(Engine::LAYER_GAMEOBJECT, pLayer);

	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	return S_OK;
}

void CStage1::SetStartPoint()
{
	const D3DXVECTOR3& vStartPos = CTerrainMgr::GetInstance()->GetMapInfo(L"MapTerrainStage1")->vStartingPoint;

	m_pPlayer->SetPosXZ(D3DXVECTOR3(30.5f, 0.1f, 24.08f));
	m_pPlayer->SetDirUp();
	m_pPlayer->SceneChange();
	dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();
}

HRESULT CStage1::Add_UI_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	pLayer->AddObject(L"Camera", m_pCamera);
	pNoBlendLayer->AddObject(L"Camera", m_pCamera);

	Engine::CGameObject * pGameObject = COrthoCamera::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"OrthoCamera", pGameObject);
	pNoBlendLayer->AddObject(L"OrthoCamera", pGameObject);

	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);
	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	m_pEyeCube->SetCamera(m_pCamera);
	return S_OK;
}

void CStage1::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	Engine::CScene::Update();

	if(g_BattleMode)
	{
		m_pArrow->SetY(1.5f);
		m_pPlayer->SetY(1.5f);
	}
	int i = m_pTerrain->CollisionPlane(m_pPlayer);
	//m_pTerrain->CollisoinPlanetoArrow(m_pPlayer->Get_ArrowPointer(), m_pPlayer);
	m_pTerrain->CollisoinPlanetoArrowBackAgain(m_pPlayer->Get_ArrowPointer(), m_pPlayer);
	if (m_pEyeCube->GetCurseOn())
		m_pPlayer->SetCurse(true);
	else
		m_pPlayer->SetCurse(false);
	
	if(!m_pEyeCube->GetDead())
		ColPlayerMonster(fTime);
	
	ColMonsterArrow(fTime);
	RemoveEffect();
	
	if (GetAsyncKeyState('H') & 0x8000)
	{
		m_pTerrain->SetPlaneRender(true);
	}

	if (GetAsyncKeyState('J') & 0x8000)
	{
		m_pTerrain->SetPlaneRender(false);
	}

	m_pDisplay->Update();

	CheckPlaneColTriger(i); 

}

void CStage1::Render()
{
	for (auto& it : m_mapRenderLayer[Engine::LAYER_NONALPHA])
		it.second->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000045);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	for (auto& it : m_mapRenderLayer[Engine::LAYER_ALPHA])
		it.second->Render();

	m_pDisplay->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

CStage1* CStage1::Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	CStage1* pInstance = new CStage1(pGraphicDev, pPlayer, pCamera);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

void CStage1::ColPlayerMonster(const float& fTime)
{
	CPlayer* pPlayer = m_pPlayer;

		m_tPlayer->fAxisLen[0] = 0.4f;
		m_tPlayer->fAxisLen[1] = 0.4f;
		m_tPlayer->fAxisLen[2] = 0.2f;
		m_tPlayer->vAxisDir[0] = D3DXVECTOR3(1.f, 0.f, 0.f);
		m_tPlayer->vAxisDir[1] = D3DXVECTOR3(0.f, 1.f, 0.f);
		m_tPlayer->vAxisDir[2] = D3DXVECTOR3(0.f, 0.f, 1.f);
		m_tPlayer->vCenterPos = m_pPlayer->Get_Trnasform()->m_vPos;

		m_tEyeCube->fAxisLen[0] = 2.03f;
		m_tEyeCube->fAxisLen[1] = 2.03f;
		m_tEyeCube->fAxisLen[2] = 2.03f;
		m_tEyeCube->vAxisDir[0] = m_pEyeCube->GetRight();
		m_tEyeCube->vAxisDir[1] = m_pEyeCube->GetUp();
		m_tEyeCube->vAxisDir[2] = m_pEyeCube->GetLook();
		m_tEyeCube->vCenterPos = m_pEyeCube->GetInfo()->m_vPos;

		if (m_pCubeCol->CheckOBBCollision(m_tPlayer, m_tEyeCube))
		{
			pPlayer->ReverseMove(fTime);
			pPlayer->SetPush(1);
		}
	
		if (m_pEyeCube->GetPlayerDead())
			BloodMode();
}

void CStage1::ColMonsterArrow(const float& fTime)
{
	if (m_pEyeCube->GetDead() && i == 0)
	{
		i++;
		Engine::Get_TimeMgr()->SlowModeStart(5.f, 0.1f);
		
		CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
		CSoundMgr::GetInstance()->PlayBGM(L"BossDead.mp3");

		DWORD temp = BOSSSTAGE1;
		m_pPlayer->SetDBoss(temp);
		CeremonyYeah();
	}

	if (m_pArrow->Get_ArrowState() == CArrow::A_SHOT || m_pArrow->Get_ArrowState() == CArrow::A_RECOVERY)
	{
		m_tArrow->fAxisLen[0] = 0.2f;
		m_tArrow->fAxisLen[1] = 0.2f;
		m_tArrow->fAxisLen[2] = 0.2f;
		m_tArrow->vAxisDir[0] = D3DXVECTOR3(1.f, 0.f, 0.f);
		m_tArrow->vAxisDir[1] = D3DXVECTOR3(0.f, 1.f, 0.f);
		m_tArrow->vAxisDir[2] = D3DXVECTOR3(0.f, 0.f, 1.f);
		m_tArrow->vCenterPos = m_pArrow->Get_Trnasform()->m_vPos;

		m_tEyeCube->fAxisLen[0] = 2.03f;
		m_tEyeCube->fAxisLen[1] = 2.03f;
		m_tEyeCube->fAxisLen[2] = 2.03f;
		m_tEyeCube->vAxisDir[0] = m_pEyeCube->GetRight();
		m_tEyeCube->vAxisDir[1] = m_pEyeCube->GetUp();
		m_tEyeCube->vAxisDir[2] = m_pEyeCube->GetLook();
		m_tEyeCube->vCenterPos = m_pEyeCube->GetInfo()->m_vPos;

		if (m_pCubeCol->CheckOBBCollision(m_tArrow, m_tEyeCube))
		{
			if (!m_pEyeCube->GetWake()&& !m_pEyeCube->GetAttack())//자고있다면
			{
				dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(0.3f, 2.f, 0.8f);
				CSoundMgr::GetInstance()->PlayBGM(L"Boss_Eyecube.ogg");
				g_BattleMode = true;
				m_pEyeCube->SetWake();//깨워준다
			}
			m_pArrow->StopArrow();
		}
	}
}

void CStage1::CheckPlaneColTriger(const int & iTriger)
{
	switch (iTriger)
	{
	case PL_EXIT:

		if (g_BattleMode)
			return;

		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}

		if (m_pDisplay->CheckFadeFinish())
		{
			m_pPlayer->SetDirDown();
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			m_pManagement->SceneChange(CSceneSelector(SC_MAP0, m_pPlayer, m_pCamera));
		}
		break;
	}
}
void CStage1::BloodMode()
{
	m_pDisplay->StartBlood();
}

void CStage1::CeremonyYeah()
{
	//여기서 일단 세레모니 띄워보자.
	NorEffect Temp;
	Temp.fDisappearSpd = 0.f;
	Temp.fLifeTime = 15.f;
	Temp.fScale = 2.f;
	Temp.fSpd = 1.0f;
	Temp.vAcc = m_pPlayer->Get_Trnasform()->m_vPos; //플레이엉위치넣고
	Temp.vPos = m_pPlayer->Get_ArrowPointer()->Get_Trnasform()->m_vPos;
	Temp.vPos.y = 0.2f;

	float fAngle = 0.f;
	float fRand = 0.f;

	list<Engine::CGameObject*>* pUpdateList = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	list<Engine::CGameObject*>* pRenderList = m_mapRenderLayer[Engine::LAYER_ALPHA][Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	for (int i = 0; i < 14; ++i)
	{
		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;
		fRand = GetRandom<float>(1.f, 2.f);
		Temp.fSpd = fRand;

		Engine::CGameObject* pGameObject = CCeremoneyEffect_List::Create(m_pGraphicDev, Temp, L"Buffer_Effect", L"EffectCeremony2NO", m_pPlayer->Get_Trnasform()->m_matWorld, pUpdateList, pRenderList, m_pPlayer);
		pUpdateList->push_back(pGameObject);
		pRenderList->push_back(pGameObject);

		fRand = GetRandom<float>(0.f, 30.f);
		fAngle += 45.f + fRand;
	}
}

void CStage1::RemoveEffect()
{
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->end();

	CNormalEffect* pEffect = nullptr;

	list<Engine::CGameObject*>* pRenderList = m_mapRenderLayer[Engine::LAYER_ALPHA][Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pEffect = dynamic_cast<CNormalEffect*>(*iter);
		if (pEffect == nullptr) return;

		if (pEffect->CheckIsDead())
		{
			//죽여
			auto& it_find = find_if(pRenderList->begin(), pRenderList->end(), [pEffect](Engine::CGameObject* pObj)
			{
				if (pObj == pEffect)
					return true;
				return false;
			});

			if (it_find != pRenderList->end())
				pRenderList->erase(it_find);

			Engine::Safe_Delete((*iter));
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->erase(iter);
		}
		else
			++iter;
	}
}