#ifndef SnowBullet_h__
#define SnowBullet_h__


#include "GameObject.h"

namespace Engine
{
	class CTransform;
	class CTimeMgr;
	class CResourcesMgr;
	class CInfoSubject;
	class CVIBuffer;
	class CTexture;
}
class CYeti;
class CCameraObserver;
class CSphereColBox;
class CSnowBullet
	: public Engine::CGameObject
{
private:
	explicit CSnowBullet(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CSnowBullet();

public:
	virtual HRESULT Initialize(const D3DXVECTOR3& vPos, const D3DXVECTOR3& vDir, CYeti* pYeti);
	virtual void Update();
	virtual void Render();
	virtual void Release();
	virtual HRESULT AddComponent();
	virtual void SetTansform();
	virtual void InitVertex();
public:
	virtual Engine::CTransform* Get_Transform() { return m_pInfo; };

public:
	static CSnowBullet* Create(LPDIRECT3DDEVICE9 pDevice, const D3DXVECTOR3& vPos, const D3DXVECTOR3& vDir, CYeti* pYeti);

private:
	CYeti*						m_pYeti = nullptr;
	Engine::CResourcesMgr*		m_pResourcesMgr = nullptr;
	Engine::CTimeMgr*			m_pTimeMgr = nullptr;
	Engine::CInfoSubject*		m_pInfoSubject = nullptr;


	Engine::CTransform*			m_pInfo;
	CCameraObserver*			m_pCameraObserver;

	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;

	CSphereColBox*				m_pSphereBox = nullptr;


	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;

	
	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...

	DWORD m_dwVtxCnt = 4;
	float		m_fSpeed = 0.f;

	float m_fJumpPow = 0.f;
	float m_fJumpAcc = 0.f;

	float		m_fEffectTime = 0.f; //����Ʈ�� ������ ����...
};

#endif // SnowBullet_h__
