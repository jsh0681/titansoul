#ifndef Map1_h__
#define Map1_h__

/*!
 * \class CMap1
 *
 * \간략정보 얼음 맵, 눈 파티클 생성.
 *
 * \상세정보 초기화 완료
 *
 * \작성자 윤유석
 *
 * \date 1월 6 2019
 *
 */


#include "Scene.h"

namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CCamera;
	class ParticleSystem;
}

class CDisplay;
class CTerrain;
class CArrow;
class CShpereCol;
class CPlayer;
class CMap1 : public Engine::CScene
{
private:
	//		 씬, 플레이어 초기화
	explicit CMap1(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
public:
	virtual ~CMap1();

private:
	virtual HRESULT Initialize();
	virtual void	Release();

	HRESULT		Add_Environment_Layer();
	HRESULT		Add_GameObject_Layer();
	HRESULT		Add_UI_Layer();

public:
	void AddResources();
	void SetStartingPoint();
	int ColPlanePlayer();
	void ColPlaneArrow();

	void CheckPlaneColTriger(const int& iResult);
public:
	virtual void Update();
	virtual void Render();

private:
	Engine::CCamera* m_pCamera;
	CPlayer*	m_pPlayer;

	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;

	//파티클
	Engine::ParticleSystem* m_pParticle;

	//이펙트

	CTerrain*	m_pTerrain = nullptr;
	CArrow*		m_pArrow = nullptr;

	bool		m_pDieCheck = false;
	bool		m_bChangeScene = false;

	//충돌체크
	CShpereCol*						m_pShpereCol = nullptr;
	CDisplay*						m_pDisplay = nullptr;




public:
	static CMap1*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
	static bool			m_sbMap1_Resource;
};

#endif // !Map1_h__