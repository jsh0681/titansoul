#include "stdafx.h"
#include "CubeObj.h"

#include "Export_Function.h"
#include "Include.h"

CCubeObj::CCubeObj(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CGameObject(pGraphicDev)
	, m_pResourcesMgr(Engine::Get_ResourceMgr())
	, m_pInfoSubject(Engine::Get_InfoSubject())
	, m_pCollisionMgr(CCollisionMgr::GetInstance())
	, m_pManagement(Engine::Get_Management())
{

}

CCubeObj::~CCubeObj()
{
	Release();
}

void CCubeObj::SetPos(D3DXVECTOR3& vPos)
{
	m_pInfo->m_vPos = vPos;
}

HRESULT CCubeObj::Initialize(void)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 8;

	m_pVertex = new Engine::VTXCUBE[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXCUBE[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_CubeTex", m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);


	return S_OK;
}
void CCubeObj::Update(void)
{
	SetDirection();

	m_pTerrainVertex = m_pManagement->GetTerrainVertex(Engine::LAYER_GAMEOBJECT, L"Terrain");
	
	m_pTerrainCol->SetColInfo(&m_pInfo->m_vPos, m_pTerrainVertex);

	Engine::CGameObject::Update();

	SetTransform();
}

void CCubeObj::Render(void)
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_CubeTex", m_pConvertVertex);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000088);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	m_pTextureCom->Render(0);
	m_pBufferCom->Render();

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);

}


HRESULT CCubeObj::AddComponent(void)
{
	Engine::CComponent*		pComponent = nullptr;

	// buffer
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_CubeTex");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	// texture
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Texture_CubeObj");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	// Transform
	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	// Collision Terrain
	pComponent = m_pCollisionMgr->CloneColObject(CCollisionMgr::COL_TERRAIN);
	m_pTerrainCol = dynamic_cast<CTerrainColl*>(pComponent);
	NULL_CHECK_RETURN(m_pTerrainCol, E_FAIL);
	m_mapComponent.emplace(L"TerrainCol", pComponent);


	return S_OK;
}

void CCubeObj::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CCubeObj::Release(void)
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	if (nullptr != m_pVertex)
	{
		Engine::Safe_Delete_Array(m_pVertex);
	}

	if (nullptr != m_pConvertVertex)
	{
		Engine::Safe_Delete_Array(m_pConvertVertex);
	}

	
}

void CCubeObj::SetTransform(void)
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pmatView);
			if (m_pConvertVertex[i].vPos.z < 1.f)
				m_pConvertVertex[i].vPos.z = 1.f;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pmatProj);
	}

}

CCubeObj* CCubeObj::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CCubeObj*	pInstance = new CCubeObj(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

