#ifndef Trace_h__
#define Trace_h__


#include "GameObject.h"

namespace Engine
{
	class CTimeMgr;
	class CTransform;
	class CVIBuffer;
	class CTexture;
}

class CSphereColBox;
class CCameraObserver;
class CTrace : public Engine::CGameObject
{
private:
	explicit CTrace(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CTrace();
	virtual void	Release();

public:
	virtual HRESULT	Initialize(const VEC3& vPos);
	virtual HRESULT	AddComponent();
	virtual	void	Update();
	virtual	void	Render();

public:
	bool	GetDie() { return m_bDie; }

private:
	void	JumpFunc();
	void	FallFunc();

	void	SetRandomPos();
	void	SetTransform();
	void	Resize();

private:
	Engine::CVIBuffer*	m_pBuffer;
	Engine::CTexture*	m_pTexture;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	DWORD					m_dwVtxCnt;

	Engine::CTimeMgr* m_pTimeMgr;
	const float&	m_fMaxLifeTime = 1.5f;
	const float&	m_fUpMaxTime = 0.2f;
	const float&	m_fDownMaxTime = 0.5f;

	float			m_fLifeTime = 0.f;
	float			m_fDeltaTime = 0.f;

	float			m_fBehavioTime = 0.f;

	int				m_iIndex = 0;
	bool			m_bDie = false;

	CSphereColBox*		m_pShereColBox;
	CCameraObserver*	m_pCameraObserver;

public:
	static CTrace*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos);
};

#endif // !Trace_h__