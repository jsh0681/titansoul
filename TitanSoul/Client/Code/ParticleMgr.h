#ifndef ParticleInfoMgr_h__
#define ParticleInfoMgr_h__

#include "Include.h"
#include "Engine_macro.h"


class CParticleMgr
{
	DECLARE_SINGLETON(CParticleMgr)
private:
	CParticleMgr();
	~CParticleMgr();

public:
	void	AddParticleInfoData(const wstring& wstrKey, PARTICLEINFO* pParticleinfo);
	const	PARTICLEINFO*	GetParticleInfoData(const wstring& wstrKey);
private:
	map<wstring,PARTICLEINFO*>		m_mapParticleInfo;
};

#endif // !ParticleInfoMgr_h__