#ifndef GoliathRHand_h__
#define GoliathRHand_h__

#include "GameObject.h"
#include "CameraObserver.h"
#include "CollisionMgr.h"
#include "TerrainColl.h"
#include "GoliathRShadow.h"
#include "Camera.h"

class CTexAni;
class CSphereColBox;

class CGoliathRHand : public Engine::CGameObject
{
private:
	explicit CGoliathRHand(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CGoliathRHand();

public:
	const float & GetRadius();
	Engine::CTransform* GetInfo() { return m_pInfo; }
	virtual void	Update(void);
	virtual void	Render(void);
	void SetRadius(const float& fSize);
	void SetTarget(D3DXVECTOR3 vTarget) { m_vTarget = vTarget; }
	void SetBodyIndex(int iIndex) { m_iIndex = iIndex; }
	void SetBodyStateKey(wstring strBodyStateKey) { m_wstrBodyStateKey = strBodyStateKey; }
	void SetChangeHand(bool bChangeHand) { m_bChangeHand = bChangeHand; }
	const bool& GetChangeLock() { return m_bChangeLock; }

private:
	bool m_bBodyDead = false;
	void Release(void);
	virtual HRESULT AddComponent(void);
	virtual HRESULT Initialize(void);
	virtual void SetDirection(void);
	virtual void SetTransform(void);
	void		StateKeyChange();
	void Attack(float fDeltaTime);
private:

	CSphereColBox*			m_pSphereBox = nullptr;

public:
	void SetCamera(Engine::CCamera* pCamera) { m_pCamera = pCamera; }
private:
	Engine::CCamera* m_pCamera = nullptr;
private:
	float fFallSpeed = 0.f;
	float fAttackTime = 0.f;
	float fStopTime = 0.f;
	bool m_bChangeLock = false;
	bool m_bIsActivate = false;
	wstring m_wstrBodyStateKey; //GAMEOBJ�� �൵ ������...

	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...
	CCameraObserver*		m_pCameraObserver = nullptr;
	float m_fSpeed;
	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;
	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *					m_pTerrainCol = nullptr;
	D3DXVECTOR3						m_vDestPos;
	D3DXVECTOR3						m_vTarget;
	CGoliathRShadow* m_pShadow = nullptr;


	CTexAni* m_pTexAni = nullptr; 
	bool m_bChangeHand = false;
	int m_iIndex = 0;
	int m_iNextMove = 0;
public:
	static CGoliathRHand*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
};
#endif // GoliathShoulder_h__
