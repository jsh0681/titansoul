#include"stdafx.h"
#include"FireWork.h"

using namespace Engine;



Firework::Firework(D3DXVECTOR3* vOriginPos, int iNumParticles)
{
	m_vOriginPos = *vOriginPos;
	m_fSize = 0.05f;
	m_pVBSize = 2048;
	m_pVBOffset = 0;
	m_pVBBatchSize = 512;

	for (int i = 0; i < iNumParticles; i++)
		AddParticle();
}

Firework::Firework(D3DXVECTOR3* vOriginPos, PARTICLEINFO * pParticleInfo)
{
	m_pParticleInfo = pParticleInfo;
	m_vOriginPos = *vOriginPos;
	m_fSize = CMathMgr::GetRandomFloat(m_pParticleInfo->fSizeMin, m_pParticleInfo->fSizeMax);
	m_pVBSize = 2048;
	m_pVBOffset = 0;
	m_pVBBatchSize = 512;

	for (int i = 0; i < m_pParticleInfo->iNumParticles; i++)
		AddParticle();
}

void Firework::ResetParticle(Engine::tagAttribute* attribute)
{
	if (m_pParticleInfo != nullptr)
	{
		attribute->_isAlive = true;
		attribute->_position = m_vOriginPos;
		float m_fSpeed = CMathMgr::GetRandomFloat(m_pParticleInfo->fSpeedMin, m_pParticleInfo->fSpeedMax);
		D3DXVECTOR3 min = D3DXVECTOR3(-m_pParticleInfo->fFireRangeX, -m_pParticleInfo->fFireRangeY, -m_pParticleInfo->fFireRangeZ);
		D3DXVECTOR3 max = D3DXVECTOR3(m_pParticleInfo->fFireRangeX, m_pParticleInfo->fFireRangeY, m_pParticleInfo->fFireRangeZ);

		CMathMgr::GetRandomVector(
			&attribute->_velocity,
			&min,
			&max);

		// normalize to make spherical
		D3DXVec3Normalize(
			&attribute->_velocity,
			&attribute->_velocity);

		attribute->_velocity *= m_fSpeed;

		attribute->_color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		attribute->_age = 0.0f;
		attribute->_lifeTime = m_pParticleInfo->fLifeTime; // lives for 2 seconds
	}
	else
	{
		attribute->_isAlive = true;
		attribute->_position = m_vOriginPos;
		float m_fSpeed = 1.f;// CMathMgr::GetRandomFloat(0.5, 1);
		D3DXVECTOR3 min = D3DXVECTOR3(30,-30, 0);
		D3DXVECTOR3 max = D3DXVECTOR3(30, 30, 30);

		CMathMgr::GetRandomVector(
			&attribute->_velocity,
			&min,
			&max);

		// normalize to make spherical
		D3DXVec3Normalize(
			&attribute->_velocity,
			&attribute->_velocity);

		attribute->_velocity *= m_fSpeed;

		attribute->_color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		attribute->_age = 0.0f;
		attribute->_lifeTime = 0.5f; // lives for 2 seconds

	}

}

void Firework::Update(float timeDelta)
{
	//AddParticle();
	std::list< tagAttribute>::iterator i;

	for (i = _particles.begin(); i != _particles.end(); i++)
	{
		// only Update living particles
		if (i->_isAlive)
		{
			i->_position += i->_velocity * timeDelta;

			i->_age += timeDelta;
			i->_color -= i->_colorFade;

			if (i->_age > i->_lifeTime) // kill 
				i->_isAlive = false;
		}
	}
}

void Firework::PreRender()
{
	ParticleSystem::PreRender();

	m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);


	//m_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, false);

}

void Firework::PostRender()
{
	ParticleSystem::PostRender();
	
	m_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, true);

}
