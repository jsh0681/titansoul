#ifndef Fade_h__
#define Fade_h__

#include "GameObject.h"


/*!
 * \class CFade
 *
 * \간략정보 대화창
 *
 * \상세정보 
 *
 * \작성자 윤유석
 *
 * \date 1월 18 2019
 *
 */

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTimeMgr;
	class CKeyMgr;
}

class CCameraObserver;
class CFade : public Engine::CGameObject
{
private:
	explicit CFade(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CFade();

public:
	virtual void Update();
	virtual void Render();

private:
	HRESULT	AddComponent();
	virtual HRESULT Initialize();
	virtual void	Release();


public:
	void FadeInStart();
	void FadeOutStart();
	const bool& GetIsOn() { return m_bOn; };
private:
	void			InitVertex();
	void			SetTransform();

private:

	CCameraObserver*	m_pCameraObserver = nullptr;
	D3DXMATRIX			m_matView;
	D3DXMATRIX			m_matProj;
	const D3DXMATRIX*	m_pMatOrtho = nullptr;
	D3DXMATRIX			m_matIdentity;

	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;
	Engine::CTimeMgr*		m_pTimeMgr;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	DWORD					m_dwVtxCnt;

	wstring					m_wstrImgName;

	float					m_fAlpha = 0.f;
	int						m_iAlpha = 0;
	int						m_iTalkMaxSize = 0;
	float					m_fTalkSize = 0;

	bool					m_bOn		= false;
	bool					m_bIsStart = TRUE;


public:
	static		CFade*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
};

#endif // !Fade_h__