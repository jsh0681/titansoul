#include "stdafx.h"

#include "Yeti_Land.h"
#include "Transform.h"
CYeti_Land::CYeti_Land(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti)
	: CYetiState(pGraphicDev, pYeti)
{
}

CYeti_Land::~CYeti_Land()
{
	m_pYeti->Set_HeapRadius(0.0f);
}

HRESULT CYeti_Land::Initialize()
{
	CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Yeti_Land.ogg", CSoundMgr::SOUND_BOSS);
	m_pYeti->Set_HeapRadius(1.0f);
	m_pYeti->SetHeapPos();
	m_pYeti->SetRadius(2.0f);
	//m_pYeti->ShakeCamera();
	//m_pYeti->LandBallEffect(m_pYeti->GetInfo()->m_vPos, 20);
	return S_OK;
}

void CYeti_Land::Update(const float & fTime)
{
	m_fTime += fTime;

	if (m_fTime > 1.f) //일정시간이 지나면...
	{
		if (m_pYeti->GetRollCnt() == 0) //전부다 굴렀으면...
		{
			//Move or Throw지...
			if (!m_pYeti->GetSetFinish()) //한 세트가 끝난게 아니라면..
			{
				m_pYeti->SetRollCnt() = 1;
				m_pYeti->SetThrowCnt() = 2;
				m_pYeti->SetSetFinish(1);
				m_pYeti->SetState(CYeti::YETI_THROW);
			}
			else						 //한세트가 종료되면..
			{
				m_pYeti->SetSetFinish(0);
				m_pYeti->SetRollCnt() = 3;
				m_pYeti->SetThrowCnt() = 4;
				m_pYeti->SetState(CYeti::YETI_MOVE);
			}
		}
		else  //구를게남으면 더굴러 자샤
		{
			m_pYeti->SetState(CYeti::YETI_ROLL);
		}
	}
}

void CYeti_Land::Release()
{
}


CYeti_Land * CYeti_Land::Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti * pYeti)
{
	CYeti_Land* pInstance = new CYeti_Land(pGraphicDev, pYeti);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}