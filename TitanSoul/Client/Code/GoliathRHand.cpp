#include "stdafx.h"
#include "GoliathRHand.h"
#include"Export_Function.h"
#include "Include.h"
#include "TexAni.h"
#include"SphereColBox.h"
#include"StaticCamera2D.h"

CGoliathRHand::CGoliathRHand(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{

}

CGoliathRHand::~CGoliathRHand()
{
	Release();
}




HRESULT CGoliathRHand::Initialize(void)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pCollisionMgr = CCollisionMgr::GetInstance();
	m_pManagement = Engine::Get_Management();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_wstrStateKey = L"GoliathRHandAttack";


	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	return S_OK;
}

HRESULT CGoliathRHand::AddComponent(void)
{
	Engine::CComponent*		pComponent = nullptr;
	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_GoliathRHand");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 1.7f, true);


	m_pInfo->m_vPos.x = 114.5f;
	m_pInfo->m_vPos.y = 0.1f;
	m_pInfo->m_vPos.z = 159.0f;

	m_vDestPos = D3DXVECTOR3(110.f, 0.8f, 161.5f);

	m_pShadow = CGoliathRShadow::Create(m_pGraphicDev);
	m_pShadow->SetHandStateKey(m_wstrStateKey);
	m_pShadow->SetTarget(m_pInfo->m_vPos);


	NULL_CHECK_RETURN(m_pShadow, E_FAIL);


	return S_OK;
}

const float & CGoliathRHand::GetRadius()
{
	return m_pSphereBox->GetRadius();
}

void CGoliathRHand::Update(void)
{
	float fDeltaTime = Engine::Get_TimeMgr()->GetTime();
	Engine::CGameObject::Update();
	m_pShadow->SetTarget(m_pInfo->m_vPos);
	m_pShadow->SetHandStateKey(m_wstrStateKey);
	m_pShadow->Update();
	SetDirection();
	m_pTerrainVertex = m_pManagement->GetTerrainVertex(Engine::LAYER_GAMEOBJECT, L"Terrain");


	StateKeyChange();
	Attack(fDeltaTime);
	SetTransform();
}

void CGoliathRHand::Render(void)
{
	m_pTexAni->Render();
	m_pShadow->Render();
}

void CGoliathRHand::SetRadius(const float & fSize)
{
	m_pSphereBox->SetRadius(fSize);
}

void CGoliathRHand::Release(void)
{
	Engine::Safe_Delete(m_pSphereBox);
	Engine::Safe_Delete(m_pShadow);
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);
}

void CGoliathRHand::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CGoliathRHand::SetTransform(void)
{
	const D3DXMATRIX* pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);
}


void CGoliathRHand::StateKeyChange()
{
	if (m_wstrBodyStateKey == L"GoliathBodySleep")
	{
		return;
	}
	if (m_wstrBodyStateKey == L"GoliathBodyWake")
	{
		m_bIsActivate = true;
	}
	if (m_wstrBodyStateKey == L"GoliathBodyDead"&&!m_bBodyDead)
	{
		m_bIsActivate = false;
		m_bBodyDead = true;
	}

	if (m_bBodyDead)
		m_pSphereBox->SetRadius(0.f);
}


void CGoliathRHand::Attack(float fDeltaTime)
{
	if (m_bIsActivate)
	{
		if (!m_bChangeHand)//오른손은 공격 먼저 
		{

			if (m_pInfo->m_vPos.z > m_vDestPos.z || m_vDestPos.z > m_pInfo->m_vPos.z
				&&m_pInfo->m_vPos.y > m_vDestPos.y || m_vDestPos.y > m_pInfo->m_vPos.y
				&&m_pInfo->m_vPos.x > m_vDestPos.x || m_vDestPos.x > m_pInfo->m_vPos.x)
			{
				if (m_pInfo->m_vPos.z < m_vDestPos.z-5.f)
				{
					m_wstrStateKey = L"GoliathRHandAttack";
				}
				else
				{
					m_wstrStateKey = L"GoliathRHandDefense";
				}
				m_pInfo->m_vDir = m_vDestPos - m_pInfo->m_vPos;//몸의 위치에서 나의 현재 벡터를 뺄셈하고 정규화 해주면 방향이 나온다.
				D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
				m_pInfo->m_vPos += m_pInfo->m_vDir*5.f*fDeltaTime;
			}


		}
		else//true라면 왼손은 방어 오른손은 공격 
		{
			switch (m_iNextMove)
			{
			case 0:
				m_bChangeLock = true;//들어오면 더이상 주먹을 바꾸지 않기 위해서 이걸 안받는다
				m_wstrStateKey = L"GoliathRHandAttack";
				m_pInfo->m_vPos.y += 10.f*fDeltaTime;
				if (m_pInfo->m_vPos.y >= 10.f)
				{
					m_iNextMove = 1;
					break;
				}
				break;
			case 1:
				fAttackTime += Engine::Get_TimeMgr()->GetTime();
				m_pInfo->m_vDir = m_vTarget - m_pInfo->m_vPos;//몸의 위치에서 나의 현재 벡터를 뺄셈하고 정규화 해주면 방향이 나온다.
				D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
				m_pInfo->m_vPos.x += m_pInfo->m_vDir.x * (30.f + fAttackTime*30.f)*fDeltaTime;
				m_pInfo->m_vPos.z += m_pInfo->m_vDir.z *(30.f + fAttackTime*30.f)  *fDeltaTime;

				/*if ()*/

				if (fAttackTime >= 0.6f || m_pInfo->m_vPos.z - 0.5f < m_vTarget.z&&m_vTarget.z < m_pInfo->m_vPos.z + 0.5f
					&&
					m_pInfo->m_vPos.x - 0.5f < m_vTarget.x&&m_vTarget.x < m_pInfo->m_vPos.x + 0.5f)
				{
					fAttackTime = Engine::Get_TimeMgr()->GetTime();
					m_iNextMove = 2;

					break;
				}
				break;
			case 2:
				fFallSpeed += Engine::Get_TimeMgr()->GetTime();
				m_pInfo->m_vDir = m_vTarget - m_pInfo->m_vPos;//몸의 위치에서 나의 현재 벡터를 뺄셈하고 정규화 해주면 방향이 나온다.
				D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
				m_pInfo->m_vPos.y -= (10 + fFallSpeed*80.f) *fDeltaTime;
				if (m_pInfo->m_vPos.y <= 0.2f)
				{
					dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(0.5f, 0.8f, 5.5f);
					m_pInfo->m_vPos.y = 0.2f;
					fStopTime += Engine::Get_TimeMgr()->GetTime();
					if (fStopTime < 0.05f)
						CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Goliath_Hit2.ogg", CSoundMgr::SOUND_BOSS);
					if (fStopTime > 0.2f)
					{
						fStopTime = Engine::Get_TimeMgr()->GetTime();
						fFallSpeed = Engine::Get_TimeMgr()->GetTime();
						m_bChangeLock = false;
						m_iNextMove = 0;
						break;
					}
					break;
				}
				break;
			}

		}
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
	}
	else
	{
		if (m_bBodyDead&&m_pInfo->m_vPos.y >= 0.2f)
			m_pInfo->m_vPos.y -= 10.f*fDeltaTime;
	}

}

CGoliathRHand* CGoliathRHand::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CGoliathRHand* pInstance = new CGoliathRHand(pGraphicDev);
	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	return pInstance;

}
