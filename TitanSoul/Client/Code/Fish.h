#ifndef Fish_h__
#define Fish_h__



#include "Npc.h"
class CPlayerObserver;
class CPlayer;
class CFish : public CNpc
{
	enum C_STATE { IDLE, ATTACK, S_END };
	enum E_EVENT { E_NO, E_HI, E_ENDTALK };

private:
	explicit CFish(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CFish();

private:
	virtual HRESULT Initialize(const VEC3& vPos, CPlayer* pPlayer);
	virtual HRESULT AddComponent();
	virtual void Release();

	virtual HRESULT InitTalkWindow()		override;

private:
	void	SetTransform();
	void	SetTrakingPos();
	void	SceneChange();
	void	CheckEvent();

	void	Event_No();
	void	Event_HI();

	void	TalkAfter(const float& fDis);

public:
	virtual void Update();
	virtual void Render();


public:
	static int						m_iEvent;
	static int						m_iEvent_TalkCnt;

private:
	wstring					m_wstrStateKey[S_END];
	wstring					m_wstrCurStateKey;

	C_STATE					m_eState[Engine::END_COMPARE];

	VEC3					m_vTrakingPos[6];
	Engine::DIRECTION		m_eEventDir[5];

	CPlayer*				m_pPlayer;
	CPlayerObserver*		m_pPlayerObserver = nullptr;



	VEC3					m_vPlayerPos;

	bool					m_bHave = TRUE;
	float					m_fCnt = 0.f;
	bool					m_bBalloon = true;
	const float				m_fBalloonCoolTime = 3.f;
	float					m_fBalloonCool = 0.f;
	const float				m_fBalloonRestCoolTime = 2.f;
	float					m_fBalloonRestTime = 0.f;
	float					m_fBalloneRandTime = 0.f; 
	bool					m_bAttackEvent = false;

	bool					m_bRollEvent = false;
	bool					m_bRunEvent = false;

	int						m_iWeak = 0;
	wstring					m_wstrWeak[6];

public:
	static CFish*	Creare(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, CPlayer* pPlayer);
	static bool	m_bFishInit;

	// CNpc을(를) 통해 상속됨
	virtual HRESULT InitTalkBalloon() override;
};

#endif // !Fish_h__