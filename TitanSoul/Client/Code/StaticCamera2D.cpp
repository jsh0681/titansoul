#include "stdafx.h"
#include "StaticCamera2D.h"
#include "Export_Function.h"
#include "Include.h"
#include "Transform.h"
#include "MathMgr.h"


CStaticCamera2D::CStaticCamera2D(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CCamera(pGraphicDev)
	, m_pTimeMgr(Engine::Get_TimeMgr())
	, m_pInfoSubject(Engine::Get_InfoSubject())
{
}

CStaticCamera2D::~CStaticCamera2D()
{
	Release();
}

HRESULT CStaticCamera2D::Initialize(void)
{
	//데이터 등록하자구! 
	m_pInfoSubject->AddData(CAMERAOPTION::VIEW, &m_matView);
	m_pInfoSubject->AddData(CAMERAOPTION::PROJECTION, &m_matProj);

	m_vEye = m_pInfo->m_vPos;
	m_vEye.y = m_fDistance;
	

	m_vAt = m_pInfo->m_vPos;

	TargetRenewal();
	return S_OK;
}

void CStaticCamera2D::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();

	if (m_bZoomIn)
	{
		m_vEye.y += fTime;

		if (m_vEye.y < m_fZoomY)
		{
			m_vEye.y = m_fZoomY;
		}

	}
	else
		m_vEye.y = m_fDistance;


	if (m_bShakingMode)
		ShakingCamera(fTime);
	else
		TargetRenewal();


	//업데이터
	m_pInfoSubject->Notify(CAMERAOPTION::VIEW);
	m_pInfoSubject->Notify(CAMERAOPTION::PROJECTION);
}

void CStaticCamera2D::Render(void)
{
}

void CStaticCamera2D::Release(void)
{
	m_pInfoSubject->RemoveData(CAMERAOPTION::VIEW, &m_matView);
	m_pInfoSubject->RemoveData(CAMERAOPTION::PROJECTION, &m_matProj);
}

void CStaticCamera2D::SetCameraTarget(const Engine::CTransform * pInfo)
{
	//여기서는 Target의 pInfo와 TimeMgr변수를 처받음.
	m_pInfo = pInfo;
}

void CStaticCamera2D::TargetRenewal()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();

	D3DXVECTOR3 vTemp = m_vEye;

	if (m_bZoomIn)
	{
		m_vEye.y += fTime * m_fZoomYSpd;

		if (m_vEye.y < m_fZoomY)
		{
			m_vEye.y = m_fZoomY;
		}
	}
	else if (m_bZoomOut)
	{
		m_vEye.y -= fTime;

		if (m_vEye.y < m_fDistance)
		{
			m_vEye.y = m_fDistance;
		}
		m_bZoomOut = FALSE;
	}
	else
		vTemp.y = m_pInfo->m_vPos.y;

	if (m_bZoomIn)
		m_vDir = m_vZoomPos - vTemp;
	else
		m_vDir = m_pInfo->m_vPos - vTemp;

	////각도를 구해볼까?
	float fDist = D3DXVec3Length(&m_vDir);

	if (fDist >= 0.1f)
	{
		D3DXVec3Normalize(&m_vDir, &m_vDir);

		if (m_bZoomIn)
		{
			m_vEye += m_vDir *fTime * m_fZoomSpd;
			m_vAt += m_vDir*fTime * m_fZoomSpd;
		}
		else
		{
			m_vEye += m_vDir *fTime * m_fSpeed * m_fPlayerSpd;
			m_vAt += m_vDir*fTime * m_fSpeed * m_fPlayerSpd;
		}



		
	}

	m_fPlayerSpd = 1.f;
	CCamera::SetViewSpaceMatrix(&m_vEye, &m_vAt, &D3DXVECTOR3(0.f, 0.f, 1.f));
}


void CStaticCamera2D::FocusPlayer()
{
	m_vEye = m_pInfo->m_vPos;
	m_vEye.y = m_fDistance;
	m_vAt = m_pInfo->m_vPos;
}

void CStaticCamera2D::ShakingCamera(const float& fTime)
{
	if (!m_bShakingMode)
	{
		return;
	}

	m_fTime -= fTime;
	if (m_fTime < 0.f)
	{
		m_fTime = 0.f;
		m_bShakingMode = FALSE;
		m_fShakingSpd = 0.f;
		m_fShakingPower = 0.f;
		//FocusPlayer();
		return;
	}

	float fXRand = GetRandom<float>(-m_fShakingPower, m_fShakingPower);
	float fZRand = GetRandom<float>(-m_fShakingPower, m_fShakingPower);
	float fYRand = GetRandom<float>(-m_fShakingPower, m_fShakingPower);


	D3DXVECTOR3 vTemp = m_vEye;
	//vTemp.y = m_pInfo->m_vPos.y;

	if (m_bZoomIn)
	{
		m_vDir = m_vZoomPos - vTemp;
	}
	else
		m_vDir = m_pInfo->m_vPos - vTemp;
	m_vDir.y = 0.f;

	////각도를 구해볼까?
	float fDist = D3DXVec3Length(&m_vDir);

	D3DXVec3Normalize(&m_vDir, &m_vDir);

	if (fDist >= 0.1f)
	{
	}

	m_vDir.x += fXRand;
	//m_vDir.y += fZRand;
	m_vDir.z += fYRand;

	m_vEye += m_vDir *fTime * m_fShakingSpd * 3.f;

	m_vAt += m_vDir *fTime * m_fShakingSpd * 3.f;

	CCamera::SetViewSpaceMatrix(&m_vEye, &m_vAt, &D3DXVECTOR3(0.f, 0.f, 1.f));

}

void CStaticCamera2D::ShakingStart(const float & fTime, const float & fPower, const float& fSpd)
{
	m_bShakingMode = TRUE;
	m_fShakingPower = fPower;
	m_fShakingSpd = fSpd;
	m_fTime = fTime;
}

void CStaticCamera2D::ZoomIn(const D3DXVECTOR3 vPos, const float& fSpd, const float& fYSpd)
{
	if (!m_bZoomIn)
	{
		m_bZoomIn = TRUE;
		m_fZoomSpd = fSpd;
		m_fZoomYSpd = fYSpd;
	}
	m_vZoomPos = vPos;

}

void CStaticCamera2D::ZoomOut()
{
	if (m_bZoomIn)
	{
		m_bZoomIn = FALSE;
		m_bZoomOut = TRUE;
		m_fZoomYSpd = 1.f;
	}
}

CStaticCamera2D * CStaticCamera2D::Create(LPDIRECT3DDEVICE9 pGraphicDev, const Engine::CTransform* pTransform)
{
	CStaticCamera2D* pStaticCamera2D = new CStaticCamera2D(pGraphicDev);
	pStaticCamera2D->SetCameraTarget(pTransform);
	pStaticCamera2D->Initialize();
	pStaticCamera2D->SetProjectionMatrix(D3DXToRadian(45.0f), float(WINCX) / WINCY, 1.f, 1000.f);
	
	return pStaticCamera2D;
}
