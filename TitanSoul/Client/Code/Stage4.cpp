#include "stdafx.h"
#include "Stage4.h"

#include "Plane.h"
#include "Player.h"
#include "Arrow.h"
#include "Terrain.h"
#include "StaticCamera2D.h"
#include "Yeti.h"
#include "ShpereCol.h"
#include "SnowBullet.h"
#include "Icicle.h"
#include "Transform.h"
#include "SceneSelector.h"
#include "NonAniEffect.h"
#include "CeremonyEffect.h"
#include "YetiShadow.h"
// Engine
#include "Export_Function.h"

// Client
#include "Include.h"
#include "Display.h"

//Obj
#include "Player.h"
#include"Snow.h"
#include "OrthoCamera.h"


bool CStage4::m_sbStage4_Resource = false;

CStage4::CStage4(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera) :
	Engine::CScene(pGraphicDev),
	m_pManagement(Engine::Get_Management()),
	m_pResourcesMgr(Engine::Get_ResourceMgr()),
	m_pPlayer(pPlayer),
	m_pCamera(pCamera)
{
	m_pDisplay = CDisplay::Create(m_pGraphicDev);
	m_pDisplay->StartFade(CDisplay::FADEIN);
}

CStage4::~CStage4()
{
	Release();
}

HRESULT CStage4::Initialize()
{
	//CSoundMgr::GetInstance()->StopAll();
	CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);

	m_pShpereCol = dynamic_cast<CShpereCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_SHPERE));

	//FAILED_CHECK_RETURN(AddResource(), E_FAIL);

	AddResources();

	FAILED_CHECK_RETURN(Add_Environment_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_GameObject_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_UI_Layer(), E_FAIL);

	SetStartPoint();

	Engine::tagBoundingBox boundingBox;
	boundingBox._max = D3DXVECTOR3(60.0f, 5.0f, 60.0f);
	boundingBox._min = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_pParticle = new Snow(&boundingBox, 1000);
	m_pParticle->Initialize(m_pGraphicDev, L"../Bin/Resources/Texture/Cube/snowflake.dds");

	/*PARTICLEINFO* pParticleInfo = const_cast<PARTICLEINFO*>(CParticleMgr::GetInstance()->GetParticleInfoData(L"ParticleSnowNo"));

	m_pParticle = new Snow(pParticleInfo);*/
	//m_pParticle->Initialize(m_pGraphicDev, L"../Bin/Resources/Texture/Cube/Boss/EyeCube/Attack/0.dds");


	return S_OK;
}

void CStage4::Release()
{
	Engine::Safe_Delete(m_pDisplay);


	Engine::Safe_Delete(m_pParticle);

	Engine::Safe_Delete(m_pShpereCol);


	//눈덩이지우고
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->end();
	CSnowBullet* pSnowBullet = nullptr;
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;
		pSnowBullet = dynamic_cast<CSnowBullet*>(*iter);
		if (pSnowBullet == nullptr) return;

		Engine::Safe_Delete(*iter);
		iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->erase(iter);
	}



	//고드름지우고
	iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->size();
	iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->begin();
	iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->end();
	CIcicle* pIcicle = nullptr;
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;
		pIcicle = dynamic_cast<CIcicle*>(*iter);
		if (pIcicle == nullptr) return;

		Engine::Safe_Delete(*iter);
		iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->erase(iter);
	}


	//이펙트 지우고
	iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->size();
	iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->begin();
	iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->end();
	CNormalEffect* pEffect = nullptr;
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;
		pEffect = dynamic_cast<CNormalEffect*>(*iter);
		if (pEffect == nullptr) return;

		Engine::Safe_Delete(*iter);
		iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->erase(iter);
	}

	iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Yeti")->size();
	iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Yeti")->begin();
	iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Yeti")->end();
	CYeti* pYeti = nullptr;
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;
		pYeti = dynamic_cast<CYeti*>(*iter);
		if (pYeti == nullptr) return;

		Engine::Safe_Delete(*iter);
		iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Yeti")->erase(iter);
	}

	//예티지우고

	if (m_bChangeScene)
	{
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"Player");
		m_mapLayer[Engine::LAYER_UI]->ClearList(L"Camera");
	}
}

HRESULT CStage4::AddResources()
{
	if (m_sbStage4_Resource)
	{
		//m_pDieCheck = TRUE; //보스뒤진상태
		return S_OK;
	}

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Yeti", 30, 30),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_SnowBullet", 7, 7),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Icicle", 10, 10),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_YetiEffect", 10, 10),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_YetiShadow", 10, 10),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_IcicleShadow", 10, 10),
		E_FAIL);


	m_sbStage4_Resource = TRUE;
	return S_OK;
}

HRESULT CStage4::Add_Environment_Layer()
{
	return S_OK;
}

HRESULT CStage4::Add_GameObject_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	//Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	//NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	//Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	//NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	Engine::CGameObject*		pGameObject = nullptr;

	pLayer->AddObject(L"Player", m_pPlayer);
	m_pPlayer->SetScene(this);
	m_pPlayer->CameraCreate();
	m_pPlayer->CreateArrow();



	CArrow* pArrow = m_pArrow = m_pPlayer->Get_ArrowPointer();
	Engine::CGameObject* pObject = dynamic_cast<Engine::CGameObject*>(pArrow);
	pLayer->AddObject(L"Arrow", pObject);

	pGameObject = CTerrainMgr::GetInstance()->Clone(L"MapTerrainStage4");
	m_pTerrain = dynamic_cast<CTerrain*>(pGameObject);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Terrain", pGameObject);


	D3DXVECTOR3 vBossPoint = CTerrainMgr::GetInstance()->GetMapInfo(L"MapTerrainStage4")->vBossPoint;
	vBossPoint.z += 10.f;
	vBossPoint.x += 2.5f;

	CStaticCamera2D* pCamera = dynamic_cast<CStaticCamera2D*>(m_pCamera);
	m_pYeti = CYeti::Create(m_pGraphicDev, pCamera, vBossPoint);
	NULL_CHECK_RETURN(m_pYeti, E_FAIL);

	pLayer->AddObject(L"Yeti", m_pYeti);
	pLayer->AddObject(L"YetiShadow", CYetiShadow::Create(m_pGraphicDev, m_pYeti));

	m_pYeti->SetStage(this);
	m_mapLayer.emplace(Engine::LAYER_GAMEOBJECT, pLayer);


	//눈덩이랑 고드름 생성후 지워주자;

	pGameObject = CSnowBullet::Create(m_pGraphicDev, D3DXVECTOR3(0.f, 0.f, 0.f), D3DXVECTOR3(0.f, 0.f, 0.f), m_pYeti);
	m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"SnowBullet", pGameObject);
	m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"IcicleBullet", pGameObject);
	m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

	Engine::Safe_Delete(pGameObject);

	m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"SnowBullet");
	m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"IcicleBullet");
	m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"Effect");



	//이펙트도 하나 만들고 빠져
	return S_OK;
}

HRESULT CStage4::Add_UI_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	pLayer->AddObject(L"Camera", m_pCamera);

	Engine::CGameObject * pGameObject = COrthoCamera::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"OrthoCamera", pGameObject);


	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);

	return S_OK;
}

void CStage4::CeremonyYeah()
{
	//여기서 일단 세레모니 띄워보자.
	NorEffect Temp;
	Temp.fDisappearSpd = 0.f;
	Temp.fLifeTime = 15.f;
	Temp.fScale = 2.f;
	Temp.fSpd = 1.0f;
	Temp.vAcc = m_pPlayer->Get_Trnasform()->m_vPos; //플레이엉위치넣고
	Temp.vPos = m_pPlayer->Get_ArrowPointer()->Get_Trnasform()->m_vPos;

	float fAngle = 0.f;
	float fRand = 0.f;

	for (int i = 0; i < 14; ++i)
	{
		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;
		fRand = GetRandom<float>(1.f, 2.f);
		Temp.fSpd = fRand;


		Engine::CGameObject* pGameObject = CCeremonyEffect::Create(m_pGraphicDev, Temp, L"Buffer_Effect", L"EffectCeremony4NO", m_pPlayer->Get_Trnasform()->m_matWorld, this, m_pPlayer);
		//m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

		fRand = GetRandom<float>(0.f, 30.f);
		fAngle += 45.f + fRand;
	}
}

void CStage4::SetStartPoint()
{
	D3DXVECTOR3 vStartPos = CTerrainMgr::GetInstance()->GetMapInfo(L"MapTerrainStage4")->vStartingPoint;
	vStartPos.x += 2.5f;
	m_pPlayer->SetPosXZ(vStartPos);
	m_pPlayer->SetDirUp();
	m_pPlayer->SceneChange();
	dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();
}

void CStage4::CheckPlaneColTriger(const int & iTriger)
{
	switch (iTriger)
	{
	case PL_EXIT:

		if (g_BattleMode)
			return;

		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}
		if (m_pDisplay->CheckFadeFinish())
		{
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			m_pPlayer->SetDirDown();
			m_pManagement->SceneChange(CSceneSelector(SC_MAP1, m_pPlayer, m_pCamera));
		}
		break;
	}


}

void CStage4::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	CScene::Update();

	// 충돌검사 함수
	int i = m_pTerrain->CollisionPlane(m_pPlayer);
	m_pTerrain->CollisoinPlanetoArrowBackAgain(m_pPlayer->Get_ArrowPointer(), m_pPlayer);
	ColPlayerMonster(fTime);
	ColMonsterArrow(fTime);
	ColPlaneSnowBullet(fTime);
	ColPlaneIcicleBullet(fTime);
	ColPlaneMonster(fTime);
	RemoveEffect();



	m_pParticle->Update(fTime);
	m_pDisplay->Update();

	CheckPlaneColTriger(i);

}

void CStage4::Render()
{
	m_pParticle->Render();

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000020);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	Engine::CScene::Render();
	m_pDisplay->Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CStage4::BloodMode()
{
	m_pDisplay->StartBlood();
}

CStage4* CStage4::Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	CStage4* pInstance = new CStage4(pGraphicDev, pPlayer, pCamera);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

void CStage4::ColPlayerMonster(const float& fTime)
{
	CPlayer* pPlayer = m_pPlayer;
	CShpereCol* pShpereCol = m_pShpereCol;
	if (pShpereCol->CheckCollision(m_pYeti->GetInfo()->m_vPos, m_pYeti->GetRadius(), pPlayer->Get_Trnasform()->m_vPos, pPlayer->GetRadius()))
	{
		pPlayer->ReverseMove(fTime);
		pPlayer->SetPush(1);
		BloodMode();
	};




	//ㅑ덩이 충돌췍췍
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->end();
	CSnowBullet* pSnowBullet = nullptr;
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pSnowBullet = dynamic_cast<CSnowBullet*>(*iter);
		if (pSnowBullet == nullptr) return;

		if (pShpereCol->CheckCollision(pSnowBullet->Get_Transform()->m_vPos, 1.f, pPlayer->Get_Trnasform()->m_vPos, pPlayer->GetRadius()))
		{
			pPlayer->ReverseMove(fTime);
			pPlayer->SetPush(1);
			BloodMode();
		};
		++iter;
	}
}

void CStage4::ColMonsterArrow(const float& fTime)
{
	CArrow* pArrow = m_pArrow;
	CShpereCol* pShpereCol = m_pShpereCol;
	if (pArrow->Get_ArrowState() == CArrow::A_SHOT || pArrow->Get_ArrowState() == CArrow::A_RECOVERY)
	{
		if (pShpereCol->CheckCollision(pArrow->Get_Trnasform()->m_vPos, pArrow->GetRadius(), m_pYeti->GetInfo()->m_vPos, m_pYeti->GetRadius()))
		{
			if (m_pYeti->GetState() == CYeti::YETI_IDLE) //대기상태 예티를 화살로 맞추면...
			{
				CSoundMgr::GetInstance()->PlayBGM(L"Boss_Yeti.ogg");
				//예티가 살아나고
				m_pYeti->SetState(CYeti::YETI_MOVE);
				g_BattleMode = TRUE;
				m_pArrow->StopArrow();
				return;
				//화살은정지
			}
			else
			{
				m_pArrow->StopArrow();
			}
			//if ((pBoss->GetState() != 1/*점프*/) && (!pBoss->GetHit())/*맞은지 얼마 안됐는지*/)
		};


		D3DXVECTOR3 vYeti = { m_pYeti->GetMatrix()._41, m_pYeti->GetMatrix()._42, m_pYeti->GetMatrix()._43 };
		if (pShpereCol->CheckCollision(pArrow->Get_Trnasform()->m_vPos, pArrow->GetRadius(), vYeti, m_pYeti->Get_HeapRadius()))
		{
			if (m_pYeti->GetState() == CYeti::YETI_LAND)
			{
				m_pYeti->SetState(CYeti::YETI_DIE);
				m_pYeti->SetRadius(0.f);
				g_BattleMode = FALSE;
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BOSS);
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				Engine::Get_TimeMgr()->SlowModeStart(5.f, 0.1f);
				CSoundMgr::GetInstance()->PlayBGM(L"appointments.mp3");
				CSoundMgr::GetInstance()->PlaySoundw(L"Impact.ogg", CSoundMgr::SOUND_BOSS);

				
				CeremonyYeah();
			}
			m_pArrow->StopArrow();
		}
	};
}

void CStage4::ColPlaneSnowBullet(const float & fTime)
{
	//ㅑ덩이 충돌췍췍
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->end();
	CSnowBullet* pSnowBullet = nullptr;
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pSnowBullet = dynamic_cast<CSnowBullet*>(*iter);
		if (pSnowBullet == nullptr) return;

		if (m_pTerrain->CollisionPlaneBullet(pSnowBullet->Get_Transform()->m_vPos))
		{

			Engine::Safe_Delete(*iter);
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"SnowBullet")->erase(iter);
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Yeti_SnowImpact.ogg", CSoundMgr::SOUND_BOSSATTACK);
		}
		else
			++iter;
	}

}

void CStage4::ColPlaneIcicleBullet(const float & fTime)
{
	//ㅑ덩이 충돌췍췍
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->end();
	CIcicle* pIcicleBullet = nullptr;
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pIcicleBullet = dynamic_cast<CIcicle*>(*iter);
		if (pIcicleBullet == nullptr) return;

		if (pIcicleBullet->GetInfo()->m_vPos.y > 10.f)
		{
			++iter;
			continue;
		}

		if (m_pTerrain->CollisionPlaneBullet(pIcicleBullet->GetInfo()->m_vPos))
		{
			Engine::Safe_Delete(*iter);
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->erase(iter);
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Yeti_IcicleLand.ogg", CSoundMgr::SOUND_BOSSATTACK);
			continue;
		}


		if (m_pYeti->GetState() == CYeti::YETI_ROLL)
		{
			if (m_pShpereCol->CheckCollision(m_pYeti->GetInfo()->m_vPos, m_pYeti->GetRadius(), pIcicleBullet->GetInfo()->m_vPos, pIcicleBullet->GetRadius()))
			{
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Yeti_IcicleSmash.ogg", CSoundMgr::SOUND_BOSSATTACK);
				Engine::Safe_Delete(*iter);
				iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->erase(iter);
				continue;
			}
		}


		if (m_pShpereCol->CheckCollision(m_pArrow->Get_Trnasform()->m_vPos, m_pArrow->GetRadius(), pIcicleBullet->GetInfo()->m_vPos, pIcicleBullet->GetRadius()))
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Yeti_IcicleSmash.ogg", CSoundMgr::SOUND_BOSSATTACK);
			Engine::Safe_Delete(*iter);
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"IcicleBullet")->erase(iter);
			continue;
		}

		if (m_pShpereCol->CheckCollision(m_pPlayer->Get_Trnasform()->m_vPos, m_pPlayer->GetRadius(), pIcicleBullet->GetInfo()->m_vPos, pIcicleBullet->GetRadius()))
		{
			if (pIcicleBullet->GetBreak())
			{
				m_pPlayer->ReverseMove(fTime);
				m_pPlayer->SetPush(1);
			}
			else
			{
				BloodMode();
			}
		}


		if (pIcicleBullet->GetInfo()->m_vPos.y < 0.1f)
		{
			pIcicleBullet->BreakIce();

			Engine::CGameObject* pGameObject = nullptr;
			//여기서 이펙트 뿅 고드름 깨질때 터지는 이펙트로
			NorEffect Temp;
			float fRand;

			Temp.fLifeTime = 1.5f;
			Temp.vPos = pIcicleBullet->GetInfo()->m_vPos;
			Temp.fScale = 0.3f;
			Temp.vAcc = { 0.f, 0.f, -1.f };
			Temp.fDisappearSpd = 0.2f;


			//자.!! 여기가 대왕 얼음덩어리
			float fAngle = 0.f;
			//방향은.. 각도방향으로  for문...
			for (int i = 0; i < 13; ++i)
			{
				fRand = GetRandom<float>(1.5f, 10.f);
				Temp.fSpd = fRand;

				Temp.vDir.x = cosf(D3DXToRadian(fAngle));
				Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
				Temp.vDir.y = 0.0f;

				D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

				pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectIcePartNO", 4);
				m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

				float fRand = GetRandom<float>(10.f, 20.f);
				fAngle -= fRand;
			}

			///////// 작은 눈알갱이  바닥에 뿌릴거
			Temp.fLifeTime = 5.f;
			Temp.vPos = pIcicleBullet->GetInfo()->m_vPos;
			fAngle = 0.f;
			Temp.vAcc = { 0.f, 0.f, 0.f };
			Temp.fDisappearSpd = 0.1f;

			//방향은.. 각도방향으로  for문...
			for (int i = 0; i < 15; ++i)
			{
				fRand = GetRandom<float>(4.5f, 8.f);
				Temp.fSpd = fRand;

				Temp.vDir.x = cosf(D3DXToRadian(fAngle));
				Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
				Temp.vDir.y = 0.0f;

				D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

				pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectSnowIceNO", 2);
				m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

				float fRand = GetRandom<float>(20.f, 45.f);
				fAngle -= fRand;
			}

			//약간큰 nice알갱이
			Temp.fLifeTime = 3.f;
			Temp.vPos = pIcicleBullet->GetInfo()->m_vPos;
			fAngle = 0.f;
			Temp.vAcc = { 0.f, 0.f, 1.f };
			Temp.fDisappearSpd = 0.3f;

			//방향은.. 각도방향으로  for문...
			for (int i = 0; i < 10; ++i)
			{
				fRand = GetRandom<float>(1.f, 8.f);
				Temp.fSpd = fRand;

				Temp.vDir.x = cosf(D3DXToRadian(fAngle));
				Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
				Temp.vDir.y = 0.0f;

				D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

				pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectNiceSnowNO", 2);
				m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

				float fRand = GetRandom<float>(10.f, 30.f);
				fAngle -= fRand;
			}


			//여기서 이펙트 만들어줘.




			continue;
		}
		++iter;
	}
}

void CStage4::ColPlanePlayer(const float & fTime)
{
}

void CStage4::ColPlaneArrow(const float & fTime)
{
}

void CStage4::ColPlaneMonster(const float & fTime)
{
	if (m_pYeti->GetState() != CYeti::YETI_ROLL) return;
	if (!m_pYeti->GetOverlap()) return;
	if (m_pYeti->GetJump())
	{
		return;
	}
	if (m_pTerrain->CollisionPlaneBullet(m_pYeti->GetInfo()->m_vPos))
	{
		if (!m_pYeti->GetJump())
		{
			m_pYeti->LandBallEffect(m_pYeti->GetInfo()->m_vPos, 10);
			m_pYeti->SetJump(1);
			m_pYeti->MakeIcicle(m_pYeti->GetInfo()->m_vPos);
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BOSS);
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Yeti_BallImpact.ogg", CSoundMgr::SOUND_BOSSATTACK);
			dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(1.5f, 4.f, 0.8f);


		}
		//이전에는 점프 상태로 변환시켜줄 필요가 있다하네

		///////////////////
		//D3DXVECTOR3 vTemp = m_pYeti->GetInfo()->m_vPos;
		//NorEffect Temp;
		//Temp.fLifeTime = 13.f;
		//Temp.fSpd = 1.f;
		//Temp.vDir = { 0.f, 1.f, 0.f };
		//Temp.vPos = m_pYeti->GetInfo()->m_vPos;

		//m_pEffectMgr->AddEffect(CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_YetiEffect", L"EffectIcePart1NO"));

		////////////////////
		return;

	}
}

void CStage4::ColBulletPlayer(const float & fTime)
{
}

void CStage4::RemoveEffect()
{
	//ㅑ덩이 충돌췍췍
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->end();

	CNormalEffect* pEffect = nullptr;

	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pEffect = dynamic_cast<CNormalEffect*>(*iter);
		if (pEffect == nullptr) return;

		if (pEffect->CheckIsDead())
		{
			//죽여
			Engine::Safe_Delete((*iter));
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->erase(iter);
		}
		else
			++iter;
	}
}


void CStage4::MakeSnowBullet(const D3DXVECTOR3& vPos, const D3DXVECTOR3& vDir)
{
	Engine::CGameObject* pGameObject = nullptr;
	pGameObject = CSnowBullet::Create(m_pGraphicDev, vPos, vDir, m_pYeti);
	m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"SnowBullet", pGameObject);
	CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Yeti_SnowImpact.ogg", CSoundMgr::SOUND_BOSSATTACK);

}

void CStage4::MakeIcicleBullet(const D3DXVECTOR3& vPos)
{
	Engine::CGameObject* pGameObject = nullptr;
	pGameObject = CIcicle::Create(m_pGraphicDev, vPos);
	m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"IcicleBullet", pGameObject);
}

