#pragma once
class CSoundMgr
{
	DECLARE_SINGLETON(CSoundMgr)

public:
	enum SOUNDID { SOUND_BGM, SOUND_BUTTON, SOUND_ARROW, SOUND_BOSSATTACK ,SOUND_PLAYER, SOUND_BOSS, SOUND_EFFECT, SOUND_END };

private:
	explicit CSoundMgr();

	~CSoundMgr();

public:
	void Initialize();
	void Update();
	void PlaySoundw(const TCHAR* pSoundKey, SOUNDID eID);
	void PlayBGM(const TCHAR* pSoundKey);
	void StopSound(SOUNDID eID);
	void StopAll();
	void Release();

private:
	void LoadSoundFile(const char* pFilePath);

private:
	FMOD_SYSTEM*	m_pSystem;
	FMOD_CHANNEL*	m_pChannelArr[SOUND_END];

	map<const TCHAR*, FMOD_SOUND*>	m_MapSound;
};

