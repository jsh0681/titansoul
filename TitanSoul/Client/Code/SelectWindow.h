#ifndef SelectWindow_h__
#define SelectWindow_h__


/*!
 * \class CSelectWindow
 *
 * \간략정보 선택 할 수 있는 창임
 *
 * \상세정보 아직 미완성, 이미지 없음, 버퍼 없음, 선택 할 수 있는게 없음, 뭘 고르고 있는지 볼 수 있는 것도 없음.
 *
 * \작성자 윤유석
 *
 * \date 1월 18 2019
 *
 */



#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTimeMgr;
	class CKeyMgr;
}

class CCameraObserver;
class CSelectObj;
class CSelectWindow : public Engine::CGameObject
{
private:
	explicit CSelectWindow(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CSelectWindow();

private:
	HRESULT	AddComponent();
	virtual HRESULT Initialize(const wstring& wstrTalk1, const wstring& wstrTalk2, const wstring& wstrTalk3);
	virtual void	Release();

public:
	virtual void Update();
	virtual void Render();

private:
	void			InitVertex();
	void			SetTransform();

	void			PushKey();

public:
	int				GetSelectIndex()			{	return m_iSelect;	}
	void			SetOnOff(const bool& bOn)	{ m_bOn = bOn; }
	void			SetIndex(const int& iIndex) { m_iSelect = iIndex; }

private:
	D3DXMATRIX			m_matView;
	D3DXMATRIX			m_matProj;
	const D3DXMATRIX*	m_pMatOrtho = nullptr;
	D3DXMATRIX			m_matIdentity;

	Engine::CVIBuffer*	m_pBuffer;
	Engine::CTexture*	m_pTexture;
	Engine::CTimeMgr*	m_pTimeMgr;
	Engine::CKeyMgr*	m_pKeyMgr;

	Engine::VTXTEX*		m_pVertex = nullptr;
	Engine::VTXTEX*		m_pConvertVertex = nullptr;

	CCameraObserver*	m_pCameraObserver;
	CSelectObj*			m_pSelectObj;

	DWORD				m_dwVtxCnt;

	wstring				m_wstrBufferName;
	wstring				m_wstrImgName;

	LPD3DXFONT			m_pFont = nullptr;

	int					m_iSelect = 0;

	wstring				m_wstrTalk1 = L"";
	wstring				m_wstrTalk2 = L"";
	wstring				m_wstrTalk3 = L"";
	wstring				m_wstrTalkSum = L"";

	RECT				m_tRc;

	bool				m_bOn;

public:
	static CSelectWindow*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const wstring& wstrTalk1, 
											const wstring& wstrTalk2, const wstring& wstrTalk3);
};

#endif // !SelectWindow_h__