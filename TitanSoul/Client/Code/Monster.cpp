#include "stdafx.h"
#include "Monster.h"

#include "Export_Function.h"
#include "Include.h"

CMonster::CMonster(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CGameObject(pGraphicDev)
	, m_pResourcesMgr(Engine::Get_ResourceMgr())
	, m_pInfoSubject(Engine::Get_InfoSubject())
	, m_pCollisionMgr(CCollisionMgr::GetInstance())
	, m_pManagement(Engine::Get_Management())
{

}

CMonster::~CMonster()
{
	Release();
}

void CMonster::SetPos(D3DXVECTOR3& vPos)
{
	m_pInfo->m_vPos = vPos;
}

HRESULT CMonster::Initialize(void)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_RcTex", m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);


	return S_OK;
}
void CMonster::Update(void)
{
	SetDirection();

	m_pTerrainVertex = m_pManagement->GetTerrainVertex(Engine::LAYER_GAMEOBJECT, L"Terrain");
	
	m_pTerrainCol->SetColInfo(&m_pInfo->m_vPos, m_pTerrainVertex);

	Engine::CGameObject::Update();

	D3DXMATRIX		matBill;
	D3DXMatrixIdentity(&matBill);

	/*D3DXMATRIX		matView = *m_pCameraObserver->GetView();

	matBill._11 = matView._11;
	matBill._13 = matView._13;
	matBill._31 = matView._31;
	matBill._33 = matView._33;*/

	matBill = *m_pCameraObserver->GetView();
	ZeroMemory(&matBill.m[3][0], sizeof(D3DXVECTOR3));
	
	D3DXMatrixInverse(&matBill, NULL, &matBill);
	m_pInfo->m_matWorld = matBill * m_pInfo->m_matWorld;

	SetTransform();
}

void CMonster::Render(void)
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_RcTex", m_pConvertVertex);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pTextureCom->Render(0);
	m_pBufferCom->Render();

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);


}


HRESULT CMonster::AddComponent(void)
{
	Engine::CComponent*		pComponent = nullptr;

	// buffer
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_RcTex");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	// texture
	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Texture_Monster");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	// Transform
	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	// Collision Terrain
	pComponent = m_pCollisionMgr->CloneColObject(CCollisionMgr::COL_TERRAIN);
	m_pTerrainCol = dynamic_cast<CTerrainColl*>(pComponent);
	NULL_CHECK_RETURN(m_pTerrainCol, E_FAIL);
	m_mapComponent.emplace(L"TerrainCol", pComponent);


	return S_OK;
}

void CMonster::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CMonster::Release(void)
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	if (nullptr != m_pVertex)
	{
		Engine::Safe_Delete_Array(m_pVertex);
	}

	if (nullptr != m_pConvertVertex)
	{
		Engine::Safe_Delete_Array(m_pConvertVertex);
	}

	
}

void CMonster::SetTransform(void)
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pmatView);
			if (m_pConvertVertex[i].vPos.z < 1.f)
				m_pConvertVertex[i].vPos.z = 1.f;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pmatProj);
	}

}

CMonster* CMonster::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CMonster*	pInstance = new CMonster(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

