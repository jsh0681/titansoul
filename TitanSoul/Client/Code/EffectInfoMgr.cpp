#include "stdafx.h"
#include "EffectInfoMgr.h"

#include "Engine_function.h"

IMPLEMENT_SINGLETON(CEffectInfoMgr)

CEffectInfoMgr::CEffectInfoMgr()
{
}

CEffectInfoMgr::~CEffectInfoMgr()
{
	for_each(m_mapEffectInfo.begin(), m_mapEffectInfo.end(), [](auto& myPair)
	{
		Engine::Safe_Delete(myPair.second);
	});
	m_mapEffectInfo.clear();
}

void CEffectInfoMgr::AddEffectInfoData(const wstring& wstrKey, EFFECTINFO* pEffectInfo)
{
	if (pEffectInfo == nullptr)
		return;

	auto& it_find = m_mapEffectInfo.find(wstrKey);

	if (it_find == m_mapEffectInfo.end())
		m_mapEffectInfo[wstrKey] = pEffectInfo;
}

//************************************
// Method	:  GetTexInfoDataList
// 작성자	:  윤유석
// Date		:  2019/01/03
// Message	:  절대로 반환 받은 List의 값을 지우면 안됨.
// Parameter:  찾는 이미지 데이터의 키 값.
//************************************

const EFFECTINFO* CEffectInfoMgr::GetEffectInfoData(const wstring& wstrKey)
{
	auto& it_find = m_mapEffectInfo.find(wstrKey);

	if (it_find == m_mapEffectInfo.end())
		return nullptr;

	return it_find->second;
}
