#ifndef CubeCol_h__
#define CubeCol_h__

#include "Collision.h"

namespace Obb
{
	struct ST_OBB // OBB구조체
	{
		D3DXVECTOR3 vCenterPos; // 상자 중앙의 좌표
		D3DXVECTOR3 vAxisDir[3]; //상자에 평행한 세 축의 단위벡터
		float  fAxisLen[3]; // 상자의 평행한 세 축의 길이 fAxisLen[n]은 vAxisDir[n]에 각각 대응한다.
	};
}


using namespace Obb;

class CCubeCol : public Engine::CCollision
{
private:
	explicit CCubeCol();
public:
	virtual ~CCubeCol();

public:
	void Release();

	virtual BOOL CheckOBBCollision(tagObbColision* Box1, tagObbColision * Box2);

public:
	static CCubeCol*					Create();
	virtual Engine::CCollision*			Clone();
};
#endif // CubeCol_h__
