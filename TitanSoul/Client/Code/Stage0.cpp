#include "stdafx.h"
#include "Stage0.h"

#include <functional>

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"
#include "ShpereCol.h"
#include "SceneSelector.h"

//Obj
#include "Player.h"
#include "Terrain.h"
#include "Plane.h"
#include "Arrow.h"

#include "Trace.h"
#include "Mucus.h"
#include "Display.h"
#include "Sludge.h"
#include "StaticCamera2D.h"
#include "OrthoCamera.h"
#include "CeremoneyEffect_List.h"
#include "Corver.h"

// static
bool CStage0::m_sbStage0_Resource = false;

CStage0::CStage0(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera) :
	Engine::CScene(pGraphicDev),
	m_pManagement(Engine::Get_Management()),
	m_pResourcesMgr(Engine::Get_ResourceMgr()),
	m_pkeyMgr(Engine::Get_KeyMgr()),
	m_pPlayer(pPlayer),
	m_pCamera(pCamera)
{
	m_pDisplay = CDisplay::Create(m_pGraphicDev);
	m_pDisplay->StartFade(CDisplay::FADEIN);
}

CStage0::~CStage0()
{
	Release();
}

HRESULT CStage0::Initialize()
{
	m_pShpereCol = dynamic_cast<CShpereCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_SHPERE));
	m_pBossList = new list<CSludge*>();

	FAILED_CHECK_RETURN(AddResource(), E_FAIL);

	FAILED_CHECK_RETURN(Add_Environment_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_GameObject_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_UI_Layer(), E_FAIL);

	CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
	CSoundMgr::GetInstance()->PlayBGM(L"Boss_AcidNerveA.ogg");

	SetStartPoint();

	m_sbStage0_Resource = true;

	return S_OK;
}

void CStage0::Release()
{
	Engine::Safe_Delete(m_pDisplay);

	Engine::Safe_Delete(m_pShpereCol);

	m_pBossList->clear();
	Engine::Safe_Delete(m_pBossList);

	if (m_bChangeScene)
	{
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"Player");
		m_mapLayer[Engine::LAYER_UI]->ClearList(L"Camera");
	}

	for (auto& it : CSludge::m_MucusLst)
		Engine::Safe_Delete(it);
	CSludge::m_MucusLst.clear();

	for (auto& it : CSludge::m_TraceLst)
		Engine::Safe_Delete(it);
	CSludge::m_TraceLst.clear();
}

void CStage0::BloodMode()
{
	m_pDisplay->StartBlood();
}

void CStage0::CeremonyYeah()
{
	//여기서 일단 세레모니 띄워보자.
	NorEffect Temp;
	Temp.fDisappearSpd = 0.f;
	Temp.fLifeTime = 15.f;
	Temp.fScale = 2.f;
	Temp.fSpd = 1.0f;
	Temp.vAcc = m_pPlayer->Get_Trnasform()->m_vPos; //플레이엉위치넣고
	Temp.vPos = m_pPlayer->Get_ArrowPointer()->Get_Trnasform()->m_vPos;
	Temp.vPos.y = 0.2f;

	float fAngle = 0.f;
	float fRand = 0.f;

	list<Engine::CGameObject*>* pUpdateList = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	list<Engine::CGameObject*>* pRenderList = m_mapRenderLayer[Engine::LAYER_ALPHA][Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	for (int i = 0; i < 14; ++i)
	{
		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;
		fRand = GetRandom<float>(1.f, 2.f);
		Temp.fSpd = fRand;

		Engine::CGameObject* pGameObject = CCeremoneyEffect_List::Create(m_pGraphicDev, Temp, L"Buffer_Effect", L"EffectCeremony2NO", m_pPlayer->Get_Trnasform()->m_matWorld, pUpdateList, pRenderList, m_pPlayer);
		pUpdateList->push_back(pGameObject);
		pRenderList->push_back(pGameObject);

		fRand = GetRandom<float>(0.f, 30.f);
		fAngle += 45.f + fRand;
	}
}

HRESULT CStage0::AddResource()
{
	if (m_sbStage0_Resource)
	{
		m_bDieCheck = true;
		return S_OK;
	}

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Sludge_Body"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Sludge_Heart"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Sludge_Shadow"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Sludge_Mucus"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Sludge_Trace"), E_FAIL);

	return S_OK;
}

HRESULT CStage0::Add_Environment_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	m_mapLayer.emplace(Engine::LAYER_ENVIRONMENT, pLayer);
	return S_OK;
}

HRESULT CStage0::Add_GameObject_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	Engine::CGameObject*		pGameObject = nullptr;

	pLayer->AddObject(L"Player", m_pPlayer);
	pBlendLayer->AddObject(L"Player", m_pPlayer);
	m_pPlayer->SetScene(this);
	m_pPlayer->CameraCreate();
	m_pPlayer->CreateArrow();

	CArrow* pArrow = m_pArrow = m_pPlayer->Get_ArrowPointer();
	Engine::CGameObject* pObject = dynamic_cast<Engine::CGameObject*>(pArrow);
	pLayer->AddObject(L"Arrow", pObject);
	pBlendLayer->AddObject(L"Arrow", pObject);

	pGameObject = m_pTerrain = CTerrainMgr::GetInstance()->Clone(L"MapTerrainStage0");
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Terrain", pGameObject);
	pNoBlendLayer->AddObject(L"Terrain", pGameObject);

	pGameObject = CCorver::Create(m_pGraphicDev, m_pTerrain, 6);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Corver", pGameObject);
	pBlendLayer->AddObject(L"Corver", pGameObject);


	if (!m_sbStage0_Resource)
	{
		// 안에서 자기를 레이어에 넣음
		const D3DXVECTOR3& vBossPos = CTerrainMgr::GetInstance()->GetMapInfo(L"MapTerrainStage0")->vBossPoint;
		pGameObject = CSludge::Create(m_pGraphicDev, vBossPos, pLayer, pBlendLayer, m_pBossList, m_pCamera);
		NULL_CHECK_RETURN(pGameObject, E_FAIL);
	}

	pLayer->AddObject(L"Effect", pGameObject);
	pBlendLayer->AddObject(L"Effect", pGameObject);

	pLayer->ClearList(L"Effect");
	pBlendLayer->ClearList(L"Effect");

	m_mapLayer.emplace(Engine::LAYER_GAMEOBJECT, pLayer);

	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	return S_OK;
}

HRESULT CStage0::Add_UI_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	pLayer->AddObject(L"Camera", m_pCamera);
	pNoBlendLayer->AddObject(L"Camera", m_pCamera);


	Engine::CGameObject * pGameObject = COrthoCamera::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"OrthoCamera", pGameObject);
	pNoBlendLayer->AddObject(L"OrthoCamera", pGameObject);


	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);

	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	return S_OK;
}

void CStage0::Update()
{
	CScene::Update();
	
	for (auto& it : CSludge::m_MucusLst)
		it->Update();

	for (auto& it : CSludge::m_TraceLst)
		it->Update();

	m_pDisplay->Update();
	RemoveEffect();

	// 충돌후 보스 죽었는지 검사
	CheckDieBoss();
	// 흔적,자국 지우는 함수
	CheckTraceDelete();

	// 충돌검사 함수
	// 벽이랑 먼저함
	ColPlaneArrow();
	ColPlaneMonster();

	// 화살,몬스터 간의 충돌
	ColPlayerMonster();
	ColMonsterArrow();

	// 벽이랑 충돌
	CheckPlaneColTriger(m_pTerrain->CollisionPlane(m_pPlayer));
	if (!CSludge::m_sbSludgeDie)
	{
		if (m_bSoundReply)
		{
			if (m_pBossList->front()->GetSize() == 4)
			{
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"Boss_AcidNerveB.ogg");
				vTempSize = m_pBossList->front()->GetSize();
				m_bSoundReply = false;
			}
			else if (m_pBossList->front()->GetSize() == 3)
			{
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"Boss_AcidNerveC.ogg");
				vTempSize = m_pBossList->front()->GetSize();
				m_bSoundReply = false;
			}
			else if (m_pBossList->front()->GetSize() == 2)
			{
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"Boss_AcidNerveD.ogg");
				vTempSize = m_pBossList->front()->GetSize();
				m_bSoundReply = false;
			}

			else if (m_pBossList->front()->GetSize() == 1)
			{
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"Boss_AcidNerveE.ogg");
				vTempSize = m_pBossList->front()->GetSize();
				m_bSoundReply = false;
			}
		}
	}
}

void CStage0::Render()
{
	for(auto& it : m_mapRenderLayer[Engine::LAYER_NONALPHA])
		it.second->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000045);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	for (auto& it : CSludge::m_MucusLst)
		it->Render();

	for (auto& it : CSludge::m_TraceLst)
		it->Render();

	for (auto& it : m_mapRenderLayer[Engine::LAYER_ALPHA])
			it.second->Render();

	m_pDisplay->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CStage0::SetStartPoint()
{
	D3DXVECTOR3 vStartPos = { 42.f, 0.1f, 25.f };

	if (m_bDieCheck)
		vStartPos.z += 10.f;

	m_pPlayer->SetPosXZ(vStartPos);
	m_pPlayer->SetDirUp();
	m_pPlayer->SceneChange();

	dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();
}

void CStage0::ColPlayerMonster()
{
	if (m_bDieCheck)
		return;

	CPlayer* pPlayer = m_pPlayer;
	CShpereCol* pShpereCol = m_pShpereCol;

	for_each(m_pBossList->begin(), m_pBossList->end(), [&](CSludge* pBoss)
	{
		if (pShpereCol->CheckCollision(pBoss->GetInfo()->m_vPos, pBoss->GetRadius(), pPlayer->Get_Trnasform()->m_vPos, pPlayer->GetRadius()))
		{
			if (pBoss->GetState() != 1/*점프*/)
				BloodMode();
		}
	});
}

void CStage0::ColMonsterArrow()
{
	if (m_bDieCheck)
		return;

	CArrow* pArrow = m_pArrow;
	CShpereCol* pShpereCol = m_pShpereCol;
	for_each(m_pBossList->begin(), m_pBossList->end(), [&pArrow, pShpereCol](CSludge* pBoss)
	{
		if (pArrow->Get_ArrowState() == CArrow::A_SHOT || pArrow->Get_ArrowState() == CArrow::A_RECOVERY)
		{
			if (pShpereCol->CheckCollision(pArrow->Get_Trnasform()->m_vPos, pArrow->GetRadius(), pBoss->GetInfo()->m_vPos, (float)pBoss->GetRadius()))
			{
				if ((pBoss->GetState() != 1/*점프*/) && (!pBoss->GetHit())/*맞은지 얼마 안됐는지*/)
				{
					pBoss->SplitSludge();
					g_BattleMode = true;
				}
			}
		}
	});
}

void CStage0::ColPlaneMonster()
{
	if (m_bDieCheck)
		return;

	CTerrain* pTerrain = m_pTerrain;
	for_each(m_pBossList->begin(), m_pBossList->end(), [&pTerrain](CSludge* pBoss)
	{
		if (pTerrain->CollisionPlaneBullet(pBoss->GetInfo()->m_vPos))
			pBoss->CheckPlaneCol(true);
		else
			pBoss->CheckPlaneCol(false);
	});
}

void CStage0::ColPlaneArrow()
{
	m_pTerrain->CollisoinPlanetoArrowBackAgain(m_pArrow, m_pPlayer);
}

void CStage0::CheckTraceDelete()
{
	for(auto& it = CSludge::m_MucusLst.begin(); it != CSludge::m_MucusLst.end();)
	{
		if ((*it)->GetDie())
		{
			Engine::Safe_Delete((*it));
			it = CSludge::m_MucusLst.erase(it);
		}
		else
			it++;
	}

	for (auto& it = CSludge::m_TraceLst.begin(); it != CSludge::m_TraceLst.end();)
	{
		if ((*it)->GetDie())
		{
			Engine::Safe_Delete((*it));
			it = CSludge::m_TraceLst.erase(it);
		}
		else
			it++;
	}
}

void CStage0::CheckDieBoss()
{
	if (m_bDieCheck)
		return;

	if ((*m_pBossList->begin())->m_sbSludgeDie)
	{
		// 보스 리스트 다 지워줌
		CSludge* pTrueBoss = nullptr;
		for (auto& it = m_pBossList->begin(); it != m_pBossList->end(); ++it)
		{
			if ((*it)->GetTrue())
			{
				pTrueBoss = (*it);
				break;
			}
		}

		m_pArrow->StopArrow();

		auto& it_Layer = m_mapLayer.find(Engine::LAYER_GAMEOBJECT);
		list<Engine::CGameObject*>* pObjLst = it_Layer->second->GetObjList(L"Boss");

		pObjLst->clear();
		pObjLst->push_back(pTrueBoss);

		//auto& it_RenderLayer = m_MapBlendRenderLayer.find(Engine::LAYER_GAMEOBJECT);
		auto& it_RenderLayer = m_mapRenderLayer[Engine::LAYER_ALPHA].find(Engine::LAYER_GAMEOBJECT);
		list<Engine::CGameObject*>* pRenderObjLst = it_RenderLayer->second->GetObjList(L"Boss");

		pRenderObjLst->clear();
		pRenderObjLst->push_back(pTrueBoss);

		for_each(m_pBossList->begin(), m_pBossList->end(), [](CSludge* pBoss)
		{
			if (!pBoss->GetTrue())
				Engine::Safe_Delete(pBoss);
		});
		m_pBossList->clear();
		m_pBossList->push_back(pTrueBoss);

		m_bDieCheck = true;
		g_BattleMode = false;
		CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
		CSoundMgr::GetInstance()->PlayBGM(L"BossDead.mp3");

		Engine::Get_TimeMgr()->SlowModeStart(5.f, 0.1f);
		DWORD temp = BOSSSTAGE0;
		m_pPlayer->SetDBoss(temp);
		CeremonyYeah();
	}
}

void CStage0::CheckPlaneColTriger(const int& iTriger)
{	
	//enum PLANETRIGGER { PL_NORMAL, PL_EXIT, PL_NEXT, PL_BOSS1, PL_BOSS2, PL_BOSS3, PL_BOSS4, PL_END };

	switch (iTriger)
	{
	case PL_EXIT:

		if (!m_bDieCheck)
			return;

		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}

		if (m_pDisplay->CheckFadeFinish())
		{
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			m_pManagement->SceneChange(CSceneSelector(SC_MAP0, m_pPlayer, m_pCamera));
		}
		break;
	}
}

void CStage0::RemoveEffect()
{
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->end();

	CNormalEffect* pEffect = nullptr;

	//list<Engine::CGameObject*>* pRenderList = m_MapBlendRenderLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	list<Engine::CGameObject*>* pRenderList = m_mapRenderLayer[Engine::LAYER_ALPHA][Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pEffect = dynamic_cast<CNormalEffect*>(*iter);
		if (pEffect == nullptr) return;

		if (pEffect->CheckIsDead())
		{
			//죽여
			auto& it_find = find_if(pRenderList->begin(), pRenderList->end(), [pEffect](Engine::CGameObject* pObj)
			{
				if (pObj == pEffect)
					return true;
				return false;
			});

			if (it_find != pRenderList->end())
				pRenderList->erase(it_find);

			Engine::Safe_Delete((*iter));
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->erase(iter);
		}
		else
			++iter;
	}
}

CStage0* CStage0::Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	CStage0* pInstance = new CStage0(pGraphicDev, pPlayer, pCamera);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
