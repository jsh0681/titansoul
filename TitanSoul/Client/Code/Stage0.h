#ifndef Stage0_h__
#define Stage0_h__

/*!
* \class CStage0
*
* \간략정보 슬라임 맵
*
* \상세정보 초기화 완료,
			렌더링 레이어 추가, (블랜딩, 블랜딩 안함, <- Update레이어와 마찬가지의 3가지 계층으로 나누어짐)
			
*
* \작성자 윤유석
*
* \date 1월 6 2019
*
*/

namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CCamera;
	class CLayer;
	class CKeyMgr;
}

#include "Scene.h"

class CDisplay;
class CPlayer;
class CShpereCol;
class CArrow;
class CSludge;
class CTerrain;
class CStage0 : public Engine::CScene
{
private:
	CStage0(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
public:
	virtual ~CStage0();
private:
	virtual HRESULT Initialize();
	virtual void	Release();

	virtual void	BloodMode();
	void			CeremonyYeah();
	void			RemoveEffect();

	HRESULT		AddResource();

	HRESULT		Add_Environment_Layer();
	HRESULT		Add_GameObject_Layer();
	HRESULT		Add_UI_Layer();

	void		SetStartPoint();

	void		ColPlayerMonster();
	void		ColMonsterArrow();
	void		ColPlaneMonster();
	void		ColPlaneArrow();

	void		CheckTraceDelete();
	void		CheckDieBoss();

	void		CheckPlaneColTriger(const int& iTriger);

public:
	virtual void Update();
	virtual void Render();

private:
	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;
	Engine::CCamera*				m_pCamera;
	Engine::CKeyMgr*				m_pkeyMgr;

	CShpereCol*						m_pShpereCol = nullptr;

	CDisplay*						m_pDisplay = nullptr;

	CTerrain*	m_pTerrain;
	CPlayer*	m_pPlayer;
	CArrow*		m_pArrow	= nullptr;
	list<CSludge*>*		m_pBossList = nullptr;

	bool		m_bDieCheck = false;

	bool		m_bChangeScene = false;


	bool m_bSoundReply = true;
	int	 vTempSize = 5;

public:
	static bool			m_sbStage0_Resource;
	static CStage0*		Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
};

#endif //!Stage5_h__