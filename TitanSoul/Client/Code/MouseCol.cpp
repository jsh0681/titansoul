#include "stdafx.h"
#include "MouseCol.h"

#include "Include.h"
#include "Export_Function.h"


CMouseCol::CMouseCol(void)
	: m_pInfoSubject(Engine::Get_InfoSubject())
	, m_pCameraObserver(nullptr)
{

}

CMouseCol::~CMouseCol(void)
{
	Release();
}

void CMouseCol::PickTerrain(D3DXVECTOR3* pOut, const Engine::VTXTEX* pTerrainVtx)
{
	Translation_ViewSpace();

	D3DXMATRIX	matIdentity;
	D3DXMatrixIdentity(&matIdentity);

	Translation_Local(&matIdentity);

	const Engine::VTXTEX*		pVertex = pTerrainVtx;

	float fU, fV, fDist;

	for (int z = 0; z < VTXCNTZ - 1; ++z)
	{
		for (int x = 0; x < VTXCNTX - 1; ++x)
		{
			int		iIndex = z * VTXCNTX + x;

			// 오른쪽 위
			if (D3DXIntersectTri(&pVertex[iIndex + VTXCNTX + 1].vPos, 
								&pVertex[iIndex+ 1].vPos, 
								&pVertex[iIndex + VTXCNTX].vPos,
								&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
			{
				*pOut = pVertex[iIndex + VTXCNTX + 1].vPos +
					(pVertex[iIndex + VTXCNTX].vPos - pVertex[iIndex + VTXCNTX + 1].vPos) * fU +
					(pVertex[iIndex + 1].vPos - pVertex[iIndex + VTXCNTX + 1].vPos) * fV;

				return;
			}

			// 왼쪽 아래
			if (D3DXIntersectTri(&pVertex[iIndex].vPos,
								&pVertex[iIndex + VTXCNTX].vPos,
								&pVertex[iIndex + 1].vPos,
								&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
			{
				*pOut = pVertex[iIndex].vPos +
					(pVertex[iIndex].vPos - pVertex[iIndex + 1].vPos) * fU +
					(pVertex[iIndex + VTXCNTX].vPos - pVertex[iIndex].vPos) * fV;

				return;
			}

		}
	}

}

void CMouseCol::PickObject(D3DXVECTOR3* pOut, 
						const Engine::VTXTEX* pVertex, 
						const D3DXMATRIX* pmatWorld)
{
	Translation_ViewSpace();
	Translation_Local(pmatWorld);

	float fU, fV, fDist;

	if (D3DXIntersectTri(&pVertex[1].vPos,
		&pVertex[2].vPos,
		&pVertex[0].vPos,
		&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
	{
		*pOut = pVertex[1].vPos +
			(pVertex[0].vPos - pVertex[1].vPos) * fU +
			(pVertex[2].vPos - pVertex[1].vPos) * fV;

		return;
	}

	// 왼쪽 아래
	if (D3DXIntersectTri(&pVertex[3].vPos,
		&pVertex[0].vPos,
		&pVertex[2].vPos,
		&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
	{
		*pOut = pVertex[3].vPos +
			(pVertex[3].vPos - pVertex[2].vPos) * fU +
			(pVertex[0].vPos - pVertex[3].vPos) * fV;

		return;
	}
}

HRESULT CMouseCol::Initialize(void)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	return S_OK;
}

void CMouseCol::Translation_ViewSpace(void)
{
	POINT		ptMouse = GetMousePos();

	// 뷰 포트 영역에 있는 마우스를 투영으로 변환 -> 뷰스페이스로 변환

	D3DXMATRIX	matProj = *m_pCameraObserver->GetProj();

	D3DXVECTOR3		vTemp;

	vTemp.x = (FLOAT(ptMouse.x) / (WINCX >> 1) - 1.f) / matProj._11;
	vTemp.y = (FLOAT(-ptMouse.y) / (WINCY >> 1) + 1.f) / matProj._22;
	vTemp.z = 1.f;

	m_vRayPos = D3DXVECTOR3(0.f, 0.f, 0.f);
	m_vRayDir = vTemp - m_vRayPos;
	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);

}


void CMouseCol::Translation_Local(const D3DXMATRIX* pWorld)
{
	// 월드 좌표로 변환
	D3DXMATRIX		matView = *m_pCameraObserver->GetView();
	D3DXMatrixInverse(&matView, 0, &matView);

	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matView);
	D3DXVec3TransformNormal(&m_vRayDir, &m_vRayDir, &matView);

	// 로컬 좌표로 변환

	D3DXMATRIX		matWorld;
	D3DXMatrixInverse(&matWorld, 0, pWorld);

	D3DXVec3TransformCoord(&m_vRayPos, &m_vRayPos, &matWorld);
	D3DXVec3TransformNormal(&m_vRayDir, &m_vRayDir, &matWorld);

}

void CMouseCol::Release(void)
{
	if (0 == (*m_pwRefCnt))
	{
		m_pInfoSubject->UnSubscribe(m_pCameraObserver);
		Engine::Safe_Delete(m_pCameraObserver);	
	}
}

POINT CMouseCol::GetMousePos(void)
{
	POINT		Pt;

	GetCursorPos(&Pt);
	ScreenToClient(g_hWnd, &Pt);

	return Pt;
}

CMouseCol* CMouseCol::Create(void)
{
	CMouseCol*	pInstance = new CMouseCol;

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

Engine::CCollision* CMouseCol::Clone(void)
{
	++(*m_pwRefCnt);

	return new CMouseCol(*this);
}

