#include "stdafx.h"
#include "Goliath.h"
#include "Export_Function.h"
#include "Include.h"
#include "TexAni.h"

#include"GoliathLHand.h"
#include"GoliathRHand.h"
#include"SphereColBox.h"
#include "Arrow.h"
#include"StaticCamera2D.h"


CGoliath::CGoliath(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{
}

CGoliath::~CGoliath()
{
	Release();
}



HRESULT CGoliath::Initialize()
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pCollisionMgr = CCollisionMgr::GetInstance();
	m_pManagement = Engine::Get_Management();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	/////////////////////////////////////////////
	//************** 왼주먹 오른주먹************//

	m_pLHand = CGoliathLHand::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(m_pLHand, E_FAIL);

	m_pRHand = CGoliathRHand::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(m_pRHand, E_FAIL);
	/////////////////////////////////////////////

	m_wstrStateKey = L"GoliathBodySleep";


	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	return S_OK;
}


HRESULT CGoliath::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_Goliath");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);
	//중심 좌표가 되어 각 파트들에 넘겨줄 좌표를 생성할것

	m_pInfo->m_vPos.x = 110.15f;
	m_pInfo->m_vPos.y = -0.672f;
	m_pInfo->m_vPos.z = 158.872f;
	m_pInfo->m_fAngle[Engine::ANGLE_X] = D3DXToRadian(70.f);

	m_pMatWorld = new D3DXMATRIX;
	D3DXMatrixIdentity(m_pMatWorld); 
	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, m_pMatWorld, 1.f, true);
	

	return S_OK;
}

const float & CGoliath::GetRadius()
{
	return m_pSphereBox->GetRadius();
}


void CGoliath::Release(void)
{
	Engine::Safe_Delete(m_pMatWorld);
	Engine::Safe_Delete(m_pSphereBox);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);
	
	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	Engine::Safe_Delete(m_pLHand);
	Engine::Safe_Delete(m_pRHand);

}

void CGoliath::Update(void)
{
	Engine::CGameObject::Update();
	memcpy(m_pMatWorld, &m_pInfo->m_matWorld, sizeof(D3DXMATRIX));
	m_pMatWorld->m[3][1] = 0.f;
	m_pMatWorld->m[3][2] = 162.3f;

	m_iIndex = m_pTexAni->GetIndex();//현재 텍스쳐의 인덱스를 얻어옴
	float fTime = Engine::Get_TimeMgr()->GetTime();

	SetDirection();
	m_pTerrainVertex = m_pManagement->GetTerrainVertex(Engine::LAYER_GAMEOBJECT, L"Terrain");

	m_pLHand->SetBodyIndex(m_iIndex);
	m_pRHand->SetBodyIndex(m_iIndex); 

	m_pLHand->SetTarget(m_pPlayerObserver->GetPlayerTransform()->m_vPos);
	m_pRHand->SetTarget(m_pPlayerObserver->GetPlayerTransform()->m_vPos);

	if (m_pPlayerObserver->GetPlayerTransform()->m_vPos.x < 110.f)//플레이어가 보스 기준 왼쪽에 있을 때.
		m_bChangeHand = false;
	else
		m_bChangeHand = true;

	if (!m_pLHand->GetChangeLock()&&!m_pRHand->GetChangeLock())//락이 걸려있지 않으면
	{
		m_pLHand->SetChangeHand(m_bChangeHand);
		m_pRHand->SetChangeHand(m_bChangeHand);
	}

	//손을 바꿔줌 플레이어 위치에 따라 불값을 넘겨줌 false 면 왼손은 방어 오른손은 공격 true면 왼손은 공격 오른손은 방어 
	
	m_pLHand->SetBodyStateKey(m_wstrStateKey);
	m_pRHand->SetBodyStateKey(m_wstrStateKey);


	m_pLHand->Update();
	m_pRHand->Update();

	Move();
	SetTransform();

}

void CGoliath::Render(void)
{
	m_pTexAni->Render();//몸체

	m_pLHand->Render();//왼팔
	m_pRHand->Render();//오른팔
}



void CGoliath::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CGoliath::SetTransform(void)
{
	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);
}

void CGoliath::Move()
{
	if (m_wstrStateKey == L"GoliathBodyWake")
	{
		switch (m_iIndex)
		{
		case 0:
			m_pInfo->m_vPos.y += 1.2f*Engine::Get_TimeMgr()->GetTime();
			m_pInfo->m_vPos.z += 3.3f*Engine::Get_TimeMgr()->GetTime();

			break;
		case 1:
			m_pInfo->m_vPos.y -= 1.2f*Engine::Get_TimeMgr()->GetTime();
			m_pInfo->m_vPos.z -= 3.3f*Engine::Get_TimeMgr()->GetTime();
			break;
		}
	}
	if (m_bWake)
	{

		CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Goliath_Wake.ogg", CSoundMgr::SOUND_BOSS);
		m_wstrStateKey = L"GoliathBodyWake";
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
		m_bWake = false;
	
	}

	if (m_bDead)
	{	
			m_wstrStateKey = L"GoliathBodyDead";
			m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
			m_bDead = false;
	}

	if ((m_wstrStateKey == L"GoliathBodyWake") && (m_iIndex == 3))
	{
		if (m_bRoar)
		{
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BOSS);
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Goliath_Roar.ogg", CSoundMgr::SOUND_BOSS);
			m_bRoar = false;
		}
	}

	if ((m_wstrStateKey == L"GoliathBodyWake") && (m_iIndex == 4))
	{
		dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(1.3f, 2.0f, 4.5f);
			m_bAttack = true;
			m_wstrStateKey = L"GoliathBodyAttack";
			m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
	}

}

CGoliath* CGoliath::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CGoliath* pInstance = new CGoliath(pGraphicDev);
	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
