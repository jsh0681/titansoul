#ifndef Yeti_h__
#define Yeti_h__

#include "GameObject.h"

namespace Engine
{
	class CTimeMgr;
	class CTransform;
	class CScene;
}
class CStage4;
class CCollisionMgr;
class CCameraObserver;
class CPlayerObserver;
class CSphereColBox;
class CTexAni;
class CYetiState;
class CStaticCamera2D;
class CYeti
	: public Engine::CGameObject
{
private:
	explicit CYeti(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CYeti();

	enum eYETISTATE { YETI_IDLE, YETI_MOVE, YETI_THROW, YETI_ROLL, YETI_LAND, YETI_DIE, YETI_END };
private:
	HRESULT AddComponent();
	HRESULT CreateObj();
	virtual HRESULT Initialize(const D3DXVECTOR3& vPos, CStaticCamera2D* pCamera);
	virtual void Release();
	void	StateKey();
	void	SceneChange();
	void	ScenePointerChange(eYETISTATE eID);
	void	SetTransform();
	


public:
	void	SetHeapPos();// 엉덩이 좌표(월드행렬)설정
	void	ChangeDirection(const float& fAngle); //각도재설정
	void	SettingDirection(const float& fAngle); //방향 pInfo->vDir 설정
	void	Move(const float& fTime, const float & fSpd);//이동
	bool	MovewithAccel(D3DXVECTOR3 vAccel, const float& fTime, const float & fSpd);//역방향엑셀이 끼인 이동 ( 여기선 그냥 점프임)

public:// 이펙트생성
	void	SnowBallEffect(const D3DXVECTOR3& vPos, float fScale, float fRange);
	void    LandBallEffect(const D3DXVECTOR3& vPos, int iCnt);

public:
	virtual void Update();
	virtual void Render();

public:
	virtual void MakeSnowBullet(const float& fDegree);
	virtual void MakeIcicle(const D3DXVECTOR3& vDest);
	void		ShakeCamera();

public:

	Engine::CTransform* GetInfo() { return m_pInfo; }
	const D3DXMATRIX&	GetMatrix() { return m_HeapWorld; };
	const float&		Get_HeapRadius();
	void				Set_HeapRadius(const float& fRadius);
	const float&		GetRadius();
	void				SetRadius(const float& fRadius);
	const eYETISTATE&   GetState() { return m_State[Engine::CUR]; };
	void				SetState(eYETISTATE eID) { m_State[Engine::CUR] = eID; };
	bool				CheckSceneEnd();
	int					GetSceneIndex();
	float				GetPlayerDegree();
	void				SetStage(Engine::CScene* pStage) { m_pStage = pStage; };
	void				SetDir(const D3DXVECTOR3& _vDir);
	void				SetStartVector();
	const D3DXVECTOR3&  GetStartVector() { return m_vStart; };
	const bool&				GetOverlap() { return m_bPreventOverlap; };
	void					SetOverlap(const bool bResult) { m_bPreventOverlap = bResult; };
	const bool&				GetJump() { return m_bIsJumping; };
	void					SetJump(const bool bResult) { m_bIsJumping = bResult; };

	//패턴을 위한 카운트들.
	const int&				GetRollCnt() { return m_iRollCnt; };
	const int&				GetThrowCnt() { return m_iThrowCnt; };
	int&					SetRollCnt() { return m_iRollCnt; };
	int&					SetThrowCnt() { return m_iThrowCnt; };
	const bool&				GetSetFinish() { return m_bSetFinish; };
	void					SetSetFinish(const bool& bResult) { m_bSetFinish = bResult; };
	void					SetDead() { m_bDead = TRUE; };
	
private:
	D3DXMATRIX m_HeapWorld;
	CStaticCamera2D* m_pCamera = nullptr;
	CTexAni* m_pTexAni = nullptr;
	CYetiState* m_pYetiScene = nullptr;
	const Engine::CTransform* m_pTargetInfo = nullptr;
	CCollisionMgr*				m_pCollisionMgr;
	CCameraObserver*			m_pCameraObserver;
	const Engine::CTransform*	m_pTargetTrasform;
	CPlayerObserver*		m_pPlayerObserver;
	Engine::CTimeMgr*			m_pTimeMgr;
	CSphereColBox*			m_pSphereBox = nullptr;
	CSphereColBox*			m_pSphereHeap = nullptr;



	wstring m_wstrYETI_StateKey[YETI_END];
	wstring m_wstrStateKey; //GAMEOBJ에 줘도 괜찮고...

	float		m_fSpeed = 0.f;

	eYETISTATE m_State[Engine::END_COMPARE];
	Engine::DIRECTION m_DIR[Engine::END_COMPARE];


	//예티 패턴 변수
	D3DXVECTOR3	m_vStart; //예티가 구르기 시작할때 지점 (고드름 생산에 쓰일것).
	float		m_fTime = 0.f; //예티의 패턴은 시간값을 기준으로 진행된다.
	int			m_iRand = 0; // 예티의 공격 패턴은 얼마 없지만 랜덤이다.
	bool		m_bPreventOverlap = FALSE; //중복충돌방지.
	bool		m_bIsJumping = FALSE; //예티점픠.

	


	float m_fJumpPow = 0.f;
	float m_fJumpAcc = 0.f;
	//맵
	Engine::CScene*	m_pStage = nullptr; //눈덩이 고드름을 추가하려면 가지고 있어야한다!

	//예티는 4번던지고 3번구르고 2번던지고 1번구르기 이게 한 세트이다.
	int	 m_iRollCnt = 0; //구르기 개수 세자
	int	 m_iThrowCnt = 0;  //던지기 개수세자.
	bool m_bSetFinish = FALSE; //한 세트가 끝났느지 아닌지를 검사. FALSE일시에는 2던지기 1구르기 이후 TRUE로 변환
	bool m_bDead = FALSE;
	float m_fEffectTime = 0.f;
public:
	static CYeti*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CStaticCamera2D* pCamera, const D3DXVECTOR3& vPos);
};

#endif // Yeti_h__
