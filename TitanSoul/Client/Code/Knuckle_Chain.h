#ifndef Knuckle_Chain_h__
#define Knuckle_Chain_h__


#include "Knuckle_Part.h"
class CKnuckle_Chain : public CKnuckle_Part
{
	enum CHAIN_DIR { C_LEFT, C_RIGHT, C_END };
private:
	explicit CKnuckle_Chain(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CKnuckle_Chain();

private:
	virtual HRESULT	Initialize(const VEC3& vPos, const int& iDir, const int& iIndex);
	virtual HRESULT AddComponent() override;
	virtual void	SetTransform() override;
	virtual void	Release();

public:
	virtual void	Update();
	virtual void	Render();

	void			SetBodyPos(const VEC3* vPos);
	void			SetMacePos(const VEC3* vPos);

private:
	void			UpdatePos();
	virtual void	AttackRotation();
	virtual void	AttackJump();

private:
	CHAIN_DIR	m_eDir;
	int			m_iIndex = 0;

	const VEC3*		m_pBodyPos = nullptr;
	const VEC3*		m_pMacePos = nullptr;

public:
	static CKnuckle_Chain*	Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const int& iDir, const int& iIndex);
};
#endif // !Knuckle_Chain_h__