#ifndef BackGround_h__
#define BackGround_h__


/*!
 * \class CBackGround
 *
 * \간략정보 로딩씬 화면
 *
 * \상세정보 
 *
 * \작성자 윤유석
 *
 * \date 1월 5 2019
 *
 */

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
	class CResourcesMgr;
	class CTimeMgr;
	class CInfoSubject;
}

class CCameraObserver;
class CBackGround : public Engine::CGameObject
{
private:
	explicit CBackGround(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CBackGround();

public:
	void		Update(void);
	void		Render(void);

private:
	HRESULT		AddComponent(void);
	HRESULT		Initialize(void);
	void		Release(void);

private:
	void		SetTransform();

private:
	Engine::CResourcesMgr*	m_pResourcesMgr = nullptr;
	Engine::CTimeMgr*		m_pTimeMgr = nullptr;
	Engine::CInfoSubject*	m_pInfoSubject = nullptr;

	Engine::CVIBuffer*		m_pBufferCom;
	Engine::CTexture*		m_pTextureCom;
	Engine::CTransform*		m_pInfo;

	CCameraObserver*		m_pCameraObserver;

	float					m_fFrameCnt = 0.f;
	float					m_fFrameMax = 2.f;

	DWORD					m_dwVtxCnt = 0;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;


public:
	static CBackGround*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
};



#endif // BackGround_h__
