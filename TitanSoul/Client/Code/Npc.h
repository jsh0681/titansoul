#ifndef Npc_h__
#define Npc_h__


#include "GameObject.h"
#include "enum.h"
/*!
* \class CNpc
*
* \간략정보
*
* \상세정보

GameObject에 있는 목록			: Transform, CResourcesMgr, Management, InfoSubject, KeyMgr
이 클래스에서 초기화 하는 목록	:  CResourcesMgr, Management, InfoSubject

이 클래스에서 초기화 하지 않는 목록 : Transform, m_wstrDIr[] <- 이 변수는 방향있으면 방향에 해당하는
애니메이션 이나 이미지 키를 넣어주는 용도.

이 클래스에에서 추가된 항목		: Buffer,TexAni, Direction(enum), m_pVertex, m_pConvertVertex
CTalkBallon(말풍선),CTalkWindow(대화창)
(모든 객체를 지우지도 생성하지도 않음)
(자식클래스에서 해줘야함.)
*
* \작성자 윤유석
*
* \date 1월 17 2019
*
*/

namespace Engine
{
	class CVIBuffer;
	class CKeyMgr;
	class CTimeMgr;
}

class CTexAni;
class CTalkBallon;
class CTalkWindow;
class CSelectWindow;
class CCameraObserver;
class CNpc : public Engine::CGameObject
{
protected:
	explicit CNpc(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CNpc();

protected:
	virtual HRESULT		InitTalkBalloon()	PURE;
	virtual HRESULT		InitTalkWindow()	PURE;

protected:

	Engine::CKeyMgr*	m_pKeyMgr;
	Engine::CTimeMgr*	m_pTimeMgr;
	CTexAni*			m_pTexAni = nullptr;

	CCameraObserver*	m_pCameraObserver = nullptr;

	CTalkBallon*		m_pTalkBalloon	= nullptr;
	CTalkWindow*		m_pTalkWindow	= nullptr;
	CSelectWindow*		m_pSelectWindow = nullptr;
};

#endif // !Npc_h__