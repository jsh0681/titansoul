#ifndef Effect_h__
#define Effect_h__
#include"Component.h"


namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CResourcesMgr;
	class CTimeMgr;
}

class CEffect : public Engine::CComponent
{
protected:
	explicit CEffect(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CEffect(void);
	virtual void Release();

protected:

	virtual HRESULT CreateBuffer(EFFECTINFO* pEffectInfo);
	virtual HRESULT CreateTexture(EFFECTINFO* pEffectInfo);
	virtual void SetTransform(const D3DXMATRIX* pMatWorld,
		const D3DXMATRIX* pMatView, 
		const D3DXMATRIX* pMatProj) PURE;


public:
	virtual void Update(void);
	virtual void Render(void);


protected:
	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	DWORD					m_dwVtxCnt = 0;
	int m_iIndex = 0;
	wstring m_wstrBufferKey = L"";
	EFFECTINFO* m_pEffectInfo = nullptr;
	LPDIRECT3DDEVICE9		m_pGraphicDev;
	Engine::CResourcesMgr*	m_pResourceMgr;
	Engine::CTimeMgr*		m_pTimeMgr;

	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;
};
#endif
