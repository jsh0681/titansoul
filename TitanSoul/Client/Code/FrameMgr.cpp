#include "stdafx.h"
#include "FrameMgr.h"

CFrameMgr::CFrameMgr()
	: m_fDeltaTime(0.f), m_fSecPerFrame(0.f), m_iFps(0), m_szFPS(L""), m_fFpsTime(0.f)
{
	ZeroMemory(&m_CurTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_OldTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_CpuTick, sizeof(LARGE_INTEGER));
}


CFrameMgr::~CFrameMgr()	
{
	
}

void CFrameMgr::InitFrame(float FramePerSec)
{
	m_fSecPerFrame = 1 / FramePerSec;

	QueryPerformanceCounter(&m_CurTime);
	QueryPerformanceCounter(&m_OldTime);
	QueryPerformanceFrequency(&m_CpuTick);
}

bool CFrameMgr::LockFrame()
{
	QueryPerformanceCounter(&m_CurTime);
	QueryPerformanceFrequency(&m_CpuTick);

	m_fDeltaTime += float(m_CurTime.QuadPart - m_OldTime.QuadPart) / m_CpuTick.QuadPart;
	m_OldTime = m_CurTime;

	if (m_fSecPerFrame <= m_fDeltaTime)
	{
		++m_iFps;
		m_fDeltaTime = 0.f;
		return true;
	}

	return false;
}
