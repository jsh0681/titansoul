#ifndef TexAni_h__
#define TexAni_h__

/*!
 * \class CTexAni
 *
 * \간략정보 Component->Animation 을 상속받은 객체.
 *
 * \상세정보 평면에 애니메이션을 출력 할 때 사용함.
 *
 * \작성자 윤유석
 *
 * \date 1월 3 2019
 *
 */

#include "Animation.h"

class CTexAni : public CAnimation
{
private:
	explicit CTexAni(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CTexAni();
	virtual void Release();

private:
	HRESULT	Initialize(const vector<TEXINFO*>* pVecTexInfo, const wstring& wstrBufferKey);

public:
	virtual void Render();
	virtual void SetTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)		override;
	virtual void SetRatioTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)	override;
	
private:
	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

public:
	static CTexAni*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const vector<TEXINFO*>* pVecTexInfo, const wstring& wstrBufferKey);
};

#endif // !TexAni_h__