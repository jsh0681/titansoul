#ifndef CShpereCol_h__
#define CShpereCol_h__

#include "Collision.h"

class CShpereCol : public Engine::CCollision
{
private:
	explicit CShpereCol();

public:
	virtual ~CShpereCol();

public: 
	virtual void Release();
	virtual bool CheckCollision(const VEC3& vDstPos, const float& fDstRadiuss,
							const VEC3& vSrcPos, const float& fSrcRadiusss);
	bool CheckCollision(const D3DXMATRIX & matWorld, const float & fDstRadiuss, const VEC3 & vSrcPos, const float & fSrcRadiusss);
public:
	static CShpereCol*					Create();
	virtual Engine::CCollision*			Clone();
};

#endif // !CShpereCol_h__