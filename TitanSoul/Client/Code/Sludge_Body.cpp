#include "stdafx.h"
#include "Sludge_Body.h"


// Engine
#include "Export_Function.h"

// Client
#include "Include.h"
#include "CameraObserver.h"


CSludge_Body::CSludge_Body(LPDIRECT3DDEVICE9 pGraphicDev) :
	CSludgePart(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject	= Engine::Get_InfoSubject();
	m_pManagement	= Engine::Get_Management();
}


CSludge_Body::~CSludge_Body()
{
	Release();
}

HRESULT CSludge_Body::Initialize(const D3DXVECTOR3& vPos, const int& iIndex)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 4;

	m_pVertex			= new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex	= new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Body", m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos = vPos;
	m_iIndex = iIndex;

	ResetSize();
	InitSize();

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Body", m_pVertex);

	return S_OK;
}

void CSludge_Body::Release()
{
	ResetSize();
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Body", m_pVertex);
}

HRESULT CSludge_Body::AddComponent()
{
	Engine::CComponent*	pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Body");
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"AcidnerveBodyNo");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform",m_pInfo);

	return S_OK;
}

void CSludge_Body::Update()
{
	// 컴포넌트 업데이트
	CGameObject::Update();
	SetTransform();
}

void CSludge_Body::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Body", m_pConvertVertex);

	// 반투명 코드
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(100, 255, 255, 255));

	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);

	m_pTexture->Render(m_iIndex - 1);
	m_pBuffer->Render();

	// 반투명 해제
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}

void CSludge_Body::SetTransform()
{
	const D3DXMATRIX* matView = m_pCameraObserver->GetView();
	const D3DXMATRIX* matProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, matView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, matProj);
}

bool CSludge_Body::IdleFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime)
{
	float fRatioZ = 3.6f;
	float fRatioX = 3.1f;

	if (fPixTime > fSumTime)
	{
		m_pVertex[0].vPos.z -= fPixTime * fDeltaTime * fRatioZ;
		m_pVertex[0].vPos.x -= fPixTime * fDeltaTime * fRatioX;
							  
		m_pVertex[1].vPos.z -= fPixTime * fDeltaTime * fRatioZ;
		m_pVertex[1].vPos.x += fPixTime * fDeltaTime * fRatioX;

		// 아래 정점들, x축만 움직인다.
		m_pVertex[2].vPos.x += fPixTime * fDeltaTime * fRatioX;
		m_pVertex[3].vPos.x -= fPixTime * fDeltaTime * fRatioX;

		return false;
	}
	else if ((fPixTime < fSumTime) && fSumTime < (fPixTime * 2.f))
	{
		m_pVertex[0].vPos.z += fPixTime * fDeltaTime * fRatioZ;
		m_pVertex[0].vPos.x += fPixTime * fDeltaTime * fRatioX;
																 
		m_pVertex[1].vPos.z += fPixTime * fDeltaTime * fRatioZ;
		m_pVertex[1].vPos.x -= fPixTime * fDeltaTime * fRatioX;

		 // 아래 정점들, x축만 움직인다.
		m_pVertex[2].vPos.x -= fPixTime * fDeltaTime * fRatioX;
		m_pVertex[3].vPos.x += fPixTime * fDeltaTime * fRatioX;

		return false;
	}
	else
		return true;
}

bool CSludge_Body::JumpFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime)
{
	float m_fPower = 1.2f;

	if (fSumTime < (fPixTime * 0.8f))
		m_pInfo->m_vPos.y += 10.f * m_fPower * fDeltaTime;

	else if (((fPixTime * 0.8f) < fSumTime) && (fSumTime < fPixTime))
		return false;
	else
		return true;

	return false;
}

bool CSludge_Body::FallFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime)
{
	float m_fPower = 1.3f;

	if (fSumTime < (fPixTime * 0.6f))
		m_pInfo->m_vPos.y -= 10.5f * m_fPower * fDeltaTime;

	else if (((fPixTime * 0.5f) < fSumTime) && (fSumTime < fPixTime))
		m_pInfo->m_vPos.y -= 2.5f * m_fPower * fDeltaTime;

	else
		return true;

	if (m_pInfo->m_vPos.y < 0.1f)
		m_pInfo->m_vPos.y = 0.1f;

	return false;
}

bool CSludge_Body::SplitFunc(const float& fDeltaTime, const float& fSumTime, 
								const float& fPixTime, const bool& bJump)
{
	if (bJump)
		return JumpFunc(fDeltaTime, fSumTime, fPixTime);
	else
		return FallFunc(fDeltaTime, fSumTime, fPixTime);

	return false;
}

void CSludge_Body::ResetSize()
{
	// 정점 초기화
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Body", m_pVertex);

	m_pInfo->m_vScale.z = 1.f;
}

CSludge_Body* CSludge_Body::Create(LPDIRECT3DDEVICE9 pGraphicDev, const D3DXVECTOR3& vPos, const int& iIndex)
{
	CSludge_Body* pInstance = new CSludge_Body(pGraphicDev);

	if (FAILED(pInstance->Initialize(vPos, iIndex)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

void CSludge_Body::InitSize()
{
	if (m_iIndex != 0)
	{
		for (size_t i = 0; i < m_dwVtxCnt; ++i)
		{
			m_pVertex[i].vPos.x *= m_iIndex;
			m_pVertex[i].vPos.z *= m_iIndex;
		}
	}
}
