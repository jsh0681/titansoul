#ifndef TerrainColl_h__
#define TerrainColl_h__

#include "Collision.h"

class CTerrainColl : public Engine::CCollision
{
private:
	CTerrainColl(void);
public:
	virtual ~CTerrainColl(void);

public:
	virtual void Update();
	void		Release(void);
	void		SetColInfo(D3DXVECTOR3* pPos, 
							const Engine::VTXTEX* pTerrainVtx);

private:
	D3DXVECTOR3*				m_pPos;
	const Engine::VTXTEX*		m_pTerrainVtx;

public:
	static	CTerrainColl*			Create(void);
	virtual Engine::CCollision*		Clone(void);

};

#endif // TerrainCol_h__
