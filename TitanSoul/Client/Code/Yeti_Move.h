#ifndef Yeti_Move_h__
#define Yeti_Move_h__

#include "YetiState.h"
class CYeti;
class CYeti_Move
	: public CYetiState
{
public:
	CYeti_Move(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
	virtual ~CYeti_Move();

public:
	virtual HRESULT Initialize();
	virtual void Update(const float& fTime);
	virtual void Release();

public:
	static CYeti_Move*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);

private:
	float m_fTime = 0.f;
	float m_iRand = 0; 

};

#endif // Yeti_Idle_h__
