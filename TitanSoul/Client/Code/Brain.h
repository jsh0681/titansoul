#ifndef Brain_h__
#define Brain_h__
#include "Export_Function.h"
#include "GameObject.h"
#include "Arrow.h"
#include "CollisionMgr.h"
#include "TerrainColl.h"
#include"PlayerObserver.h"

class CSphereColBox;
class CBrainShadow;

namespace Engine
{

	class CVIBuffer;
	class CTexture;
}


class CBrain :public Engine::CGameObject
{
private:
	explicit CBrain(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CBrain(void);

public:
	const float& GetRadius();
	void SetPos(D3DXVECTOR3 vPos) { m_pInfo->m_vPos = vPos; }
	void SetMoving(bool tf) { m_bMove = tf; }
	void		SetTurn(bool bResult) { m_bTurn = bResult; };
	Engine::CTransform* GetInfo() { return m_pInfo; }
	D3DXMATRIX GetWorldMatrix() { return m_pInfo->m_matWorld; }
	void SetDead(bool tf) { m_bDead = tf; }
	bool GetDead() { return m_bDead; }

	void SetRadius(const float& fSize);
	bool GetIsBottom() { return m_bIsBottom; }
private:
	
	bool m_bIsBottom = false;
	bool m_bDead = false;
	float m_fChangeTime = 0.f;
	bool m_bChange = false;
	float m_fDelayTime = 0.f;
	bool m_bTurn = false;
	float m_fDegree = 0.f;
	int m_iAttackPattern = 0;
	float m_fJumpAcc = 0.f;
	float m_fFallAcc = 0.f;
	D3DXVECTOR3 m_vTempDir;
	bool m_bCalculate = true;
	void Move(float fDeltaTime);
	void Release(void);
	virtual HRESULT AddComponent(D3DXVECTOR3 vStartPos);
	virtual HRESULT Initialize(D3DXVECTOR3 vStartPos);

private:
	D3DXVECTOR3 m_vPlayerPos;


	virtual void Update(void);
	virtual void Render(void);
	virtual void SetTransform(void);
	virtual void SetDirection(void);
private:
	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;
private:
	D3DXVECTOR3 m_vTarget;
	CCameraObserver*				m_pCameraObserver = nullptr;
	CPlayerObserver*				m_pPlayerObserver = nullptr;

	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;

	bool m_bMove = false;
	CSphereColBox*			m_pSphereBox = nullptr;

	DWORD						m_dwVtxCnt = 0;
	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;
	int m_iIndex = 0;
private:
	CBrainShadow* m_pBrainShadow = nullptr;

public:

	void StateChange();

	static CBrain*		Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos);
};
#endif // Brain_h__
