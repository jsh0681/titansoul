#include "stdafx.h"
#include "CameraObserver.h"

#include "Export_Function.h"

CCameraObserver::CCameraObserver(void)
	: m_pInfoSubject(Engine::Get_InfoSubject())
{

}

CCameraObserver::~CCameraObserver(void)
{

}

void CCameraObserver::Update(int iFlag)
{
	list<void*>*		pDataList = m_pInfoSubject->GetDataList(iFlag);
	NULL_CHECK(pDataList);

	switch (iFlag)
	{
	case CAMERAOPTION::VIEW:
		m_matView = *((D3DXMATRIX*)pDataList->front());
		break;

	case CAMERAOPTION::PROJECTION:
		m_matProj = *((D3DXMATRIX*)pDataList->front());
		break;

	case CAMERAOPTION::ORTHO:
		m_matOrtho = *((D3DXMATRIX*)pDataList->front());
		break;
	}
}

CCameraObserver* CCameraObserver::Create(void)
{
	return new CCameraObserver;
}

