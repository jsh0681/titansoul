#include "stdafx.h"
#include "StaticCamera.h"

#include "Export_Function.h"
#include "Include.h"
#include "Transform.h"


CStaticCamera::CStaticCamera(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CCamera(pGraphicDev)
	, m_pTimeMgr(Engine::Get_TimeMgr())
	, m_pInfoSubject(Engine::Get_InfoSubject())
{

}

CStaticCamera::~CStaticCamera(void)
{
	Release();
}

void CStaticCamera::SetCameraTarget(const Engine::CTransform* pInfo)
{
	m_pInfo = pInfo;
}

HRESULT CStaticCamera::Initialize(void)
{
	m_pInfoSubject->AddData(CAMERAOPTION::VIEW, &m_matView);
	m_pInfoSubject->AddData(CAMERAOPTION::PROJECTION, &m_matProj);
	
	return S_OK;
}

void CStaticCamera::Update(void)
{
	float	fTime = Engine::Get_TimeMgr()->GetTime();

	if (m_bDownAngle)
	{
		if (m_fAngle > 88.4f)
			m_fAngle -= D3DXToRadian(45.f) * fTime;
		else
			m_fAngle = 88.4f;
		if (m_fDistance > 22.f)
			m_fDistance -= fTime * 5.f;
		else
			m_fDistance = 22.f;
	}
	TargetRenewal();

	m_pInfoSubject->Notify(CAMERAOPTION::VIEW);
	m_pInfoSubject->Notify(CAMERAOPTION::PROJECTION);
}

void CStaticCamera::Release(void)
{
	m_pInfoSubject->RemoveData(CAMERAOPTION::VIEW, &m_matView);
	m_pInfoSubject->RemoveData(CAMERAOPTION::PROJECTION, &m_matProj);
}

void CStaticCamera::KeyCheck(void)
{

}

void CStaticCamera::TargetRenewal(void)
{
	D3DXVECTOR3 vTemp = { 0.f, 0.f, 1.f };
	m_vEye = vTemp* (-1.f);
	D3DXVec3Normalize(&m_vEye, &m_vEye);

	m_vEye *= m_fDistance;

	D3DXVECTOR3	vRight;
	memcpy(&vRight, &m_pInfo->m_matWorld.m[0][0], sizeof(float) * 3);

	D3DXMATRIX	matRotAxis;
	D3DXMatrixRotationAxis(&matRotAxis, &vRight, m_fAngle);
	D3DXVec3TransformNormal(&m_vEye, &m_vEye, &matRotAxis);


	m_vEye += m_pInfo->m_vPos;
	m_vAt = m_pInfo->m_vPos;

	CCamera::SetViewSpaceMatrix(&m_vEye, &m_vAt, &D3DXVECTOR3(0.f, 1.f, 0.f));
	
}

CStaticCamera* CStaticCamera::Create(LPDIRECT3DDEVICE9 pGraphicDev, const Engine::CTransform* pInfo)
{
	CStaticCamera*		pInstance = new CStaticCamera(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	pInstance->SetCameraTarget(pInfo);
	pInstance->SetProjectionMatrix(D3DXToRadian(45.f), float(WINCX) / WINCY, 1.f, 1000.f);
	
	return pInstance;
}

