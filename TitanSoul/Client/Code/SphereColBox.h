#ifndef CSphereColBox_h__
#define CSphereColBox_h__


class CSphereColBox
{
private:
	explicit CSphereColBox(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CSphereColBox();

private:
	HRESULT		Initialize(const D3DXMATRIX* pWorld, const float& fRadius, const bool& bBlending);
	void		Release();
public:
	void		Render();
	const float&	GetRadius()		{ return m_fRadius; }
	void			SetRadius(const float& fRadius);
	
private:
	D3DXMATRIX			m_matIdtt;
	LPDIRECT3DDEVICE9	m_pGraphicDev;
	const D3DXMATRIX*	m_pWorld = nullptr;
	LPD3DXMESH			m_pMesh	 = nullptr;
	float				m_fRadius;
	bool				m_bBlending;

public:
	static CSphereColBox*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const D3DXMATRIX* pWorld,
										const float& fRadius, const bool& bBlending = false);
};

#endif // !CSphereColBox_h__