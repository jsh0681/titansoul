#include "stdafx.h"
#include "Fish.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

//Obj
#include "TexAni.h"
#include "CameraObserver.h"
#include "PlayerObserver.h"

#include "Player.h"

#include "TalkBallon.h"
#include "TalkWindow.h"
#include "SelectWindow.h"

bool CFish::m_bFishInit = false;

int	CFish::m_iEvent = 0;
int	CFish::m_iEvent_TalkCnt = 0;


CFish::CFish(LPDIRECT3DDEVICE9 pGraphicDev) :
	CNpc(pGraphicDev)
{
}

CFish::~CFish()
{
	Release();
}

HRESULT CFish::Initialize(const VEC3& vPos, CPlayer* pPlayer)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_wstrCurStateKey = L"FishIdleDD";
	SetTrakingPos();

	m_pPlayer = pPlayer;

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = vPos;
	//m_pInfo->m_vPos.z += 100.f;
	FAILED_CHECK_RETURN(InitTalkWindow(), E_FAIL);


	if (!m_bFishInit) //두번 이닛되지 않게하자.
	{

	}
	else
	{
		m_pTalkWindow->SetTalkEnd(TRUE);
	}

	m_bFishInit = TRUE;
	return S_OK;
}

HRESULT CFish::AddComponent()
{
	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrCurStateKey), L"Buffer_Fish");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	m_pInfo->m_vScale = { 1.f, 1.f, 1.f };
	return S_OK;
}

void CFish::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();


	m_vPlayerPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;

	CheckEvent();
	SceneChange();
	CGameObject::Update();
	SetTransform();

	m_pTalkWindow->Update();
}

void CFish::Render()
{
	if (m_bHave)
	{
		m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		m_pTexAni->Render();
		m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

		m_pTalkWindow->Render();
	}

}

void CFish::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	Engine::Safe_Delete(m_pTalkWindow);			// 대화창
}


void CFish::SetTrakingPos()
{
	m_vTrakingPos[0] = { 110.f, 0.1f, 63.5f };
	m_vTrakingPos[1] = { 125.f, 0.1f, 63.5f };
	m_vTrakingPos[2] = { 125.f, 0.1f, 70.f };
	m_vTrakingPos[3] = { 110.f, 0.1f, 70.f };
	m_vTrakingPos[4] = { 110.f, 0.1f, 76.f };
	m_vTrakingPos[5] = { 107.f, 0.1f, 76.f };
}



HRESULT CFish::InitTalkWindow()
{
	m_pTalkWindow = CTalkWindow::Create(m_pGraphicDev, L"'물고기'를 획득했다.");
	NULL_CHECK_RETURN(m_pTalkWindow, E_FAIL);
	m_pTalkWindow->SetOnOff(false);

	return S_OK;
}


void CFish::SceneChange()
{
	if (m_eState[Engine::CUR] != m_eState[Engine::PRE])
	{
		switch (m_eState[Engine::CUR])
		{
		case C_STATE::IDLE:
			m_wstrCurStateKey = m_wstrStateKey[IDLE];
			break;

		case C_STATE::ATTACK:
			m_wstrCurStateKey = m_wstrStateKey[ATTACK];
			break;
		}
		m_eState[Engine::PRE] = m_eState[Engine::CUR];
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrCurStateKey));
	}
}

void CFish::CheckEvent()
{
	switch (m_iEvent)
	{
	case CFish::E_NO:
		Event_No();
		break;

	case CFish::E_HI:
		Event_HI();
		break;
	}
}

void CFish::Event_No()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		m_pTalkWindow->SetOnOff(true);
		m_iEvent = E_HI;
	}
}

void CFish::Event_HI()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 2.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				m_pTalkWindow->SetOnOff(FALSE);
				m_pTalkWindow->SetTalkEnd(true);
				m_pPlayer->SetDBoss(FISHBIT);
				m_bHave = FALSE;
			}
		}
	}
}


void CFish::TalkAfter(const float& fDis)
{
	if (m_pTalkWindow->GetOnOff())
	{
		if (fDis > 8.f)
		{
			m_pTalkWindow->SetOnOff(false);
			m_pTalkWindow->SetTalkEnd(true);
			C_STATE::IDLE;
		}
	}
}


void CFish::SetTransform()
{
	const D3DXMATRIX* pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);
	const D3DXMATRIX* pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);
}

CFish* CFish::Creare(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, CPlayer* pPlayer)
{
	CFish* pInstance = new CFish(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos, pPlayer)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

HRESULT CFish::InitTalkBalloon()
{
	return E_NOTIMPL;
}
