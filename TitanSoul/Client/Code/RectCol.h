#ifndef CRectCol_h__
#define CRectCol_h__

/*!
 * \class CRectCol
 *
 * \간략정보 사각형 충돌
 *
 * \상세정보 
 *
 * \작성자 윤유석
 *
 * \date 1월 16 2019
 *
 */

#include "Collision.h"
class CRectCol : public Engine::CCollision
{
private:
	explicit CRectCol();
public:
	virtual ~CRectCol();

public:
	virtual void Release();
	virtual BOOL CheckCollision(const VEC3& vDstPos, const float& fDstRadiuss,
								const VEC3& vSrcPos, const float& fSrcRadiusss, 
								RECT* ColRange);
public:
	static CRectCol*	Create();
	virtual Engine::CCollision*		Clone();
};

#endif // !CRectCol_h__