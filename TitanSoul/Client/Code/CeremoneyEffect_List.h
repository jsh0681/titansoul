#ifndef CeremoneyEffect_List_h__
#define CeremoneyEffect_List_h__



#include "NormalEffect.h"

namespace
{
	class CScene;
}
class CPlayer;
class CPlayerObserver;
class CCeremoneyEffect_List : public CNormalEffect
{
private:
	explicit CCeremoneyEffect_List(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CCeremoneyEffect_List();

private:
	virtual HRESULT Initialize(NorEffect tNorEffect, wstring wstrBuffer, wstring wstrTex, 
								D3DXMATRIX matParent, list<Engine::CGameObject*>* pUpdateList, 
								list<Engine::CGameObject*>* pRenderList, CPlayer* pPlayer);
	virtual void InitVertex();
	virtual void Release() override;

public:
	virtual void Update();
	virtual void Render();

private:
	virtual void SetTransform();

	void CreatePoop();
	void CreateBigPoop(const D3DXVECTOR3& vPos);


private:
	CPlayerObserver*		m_pPlayerObserver = nullptr;
	CPlayer*				m_pPlayer = nullptr;
	bool		m_bLast = FALSE;
	float		m_fpoopTime = 0.f;
	float		m_fDegree = 0.f;
	float		m_fScale = 0.f;
	float		m_fSpeed = 0.f;
	float		m_fSpeed2 = 0.f;
	float		m_fDonwtime = 0.5f;
	float		m_fAngle2 = 0.f; //플레이어에게 향할때 마관광살포 각도.
	bool		m_bDown = FALSE;

	D3DXVECTOR3 m_VStartPos;
	D3DXVECTOR3 m_vDest1;
	D3DXVECTOR3 m_vDest2;
	D3DXVECTOR3 m_vDest3;
	D3DXVECTOR3 m_vDest4;



	D3DXVECTOR3 m_vAccel;
	D3DXVECTOR3 m_vDirTrace; //플레이어 좌표로 뱀처럼 휘면서 추적할 벡터 현재는 vAcc으로 받은 pos를 이용하여 방향백터를 만들것
	D3DXVECTOR3 m_vSMove[2];
	D3DXVECTOR3 m_vAccSMove[2];

	D3DXMATRIX  m_matParent; //플레이어 월드좌표를 넘겨주면 된다 공전할 수 있도록 말이다.
	list<Engine::CGameObject*>* m_pUpdateList = nullptr;
	list<Engine::CGameObject*>* m_pRenderList = nullptr;

	bool		m_bAcc = FALSE;
	bool		m_bAngleDir = FALSE;

	static		int m_iCount;


public:
	static CCeremoneyEffect_List* Create(LPDIRECT3DDEVICE9 pGraphicDev, NorEffect tNorEffect, wstring wstrBuffer, wstring wstrTex, D3DXMATRIX matParent,
									list<Engine::CGameObject*>* pUpdateList, list<Engine::CGameObject*>* pRenderList, CPlayer* pPlayer);
};

#endif // !CeremoneyEffect_List_h__