#include"stdafx.h"
#include"FireButton.h"
#include "Export_Function.h"
#include "Include.h"
#include"Fire.h"

CFireButton::CFireButton(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{

}

CFireButton::~CFireButton(void)
{
	Release();
}

HRESULT CFireButton::Initialize(D3DXVECTOR3 vStartPos)
{

	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();

	m_pCollisionMgr = CCollisionMgr::GetInstance();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);



	
	FAILED_CHECK_RETURN(AddComponent(vStartPos), E_FAIL);

	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_FireButton", m_pVertex);

	return S_OK;
}

HRESULT CFireButton::AddComponent(D3DXVECTOR3 vStartPos)
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_FireButton");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Texture_FireButton");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTextureCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTextureCom);


	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	m_pInfo->m_vPos = vStartPos;

	return S_OK;
}

void CFireButton::Update()
{
	SetDirection();
	float fTime = Engine::Get_TimeMgr()->GetTime();

	Engine::CGameObject::Update();
	StateChange();

	if (m_pFire != nullptr)
		m_pFire->Update();

	SetTransform();
}

void CFireButton::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_FireButton", m_pConvertVertex);

	m_pTextureCom->Render(m_iIndex);
	m_pBufferCom->Render();
	if (m_pFire != nullptr)
		m_pFire->Render();
}



void CFireButton::Release()
{
	if (m_pFire != nullptr)
	{
		Engine::Safe_Delete(m_pFire);
	}

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
		m_pConvertVertex[i] = m_pVertex[i];

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_FireButton", m_pConvertVertex);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}
void CFireButton::StateChange()
{
	if (m_bIsTrample)
	{
		m_iIndex = 1;
		if (m_pFire == nullptr)
		{
			m_pFire = CFire::Create(m_pGraphicDev, D3DXVECTOR3(30.f, 1.0f, 31.0f));
			NULL_CHECK(m_pFire);
		}
	}
	else//�ȹ���
	{
		m_iIndex = 0;
		if (m_pFire != nullptr)
		{
			Engine::Safe_Delete(m_pFire);
		}
	}
}

void CFireButton::SetTransform()
{
	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pMatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pMatProj);
}

void CFireButton::SetDirection(void)
{

	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

CFireButton* CFireButton::Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos)
{
	CFireButton* pInstance = new CFireButton(pGraphicDev);
	if (FAILED(pInstance->Initialize(vStartPos)))
		Engine::Safe_Delete(pInstance);
	
	return pInstance;
}

