#ifndef IceCubeShadow_h__
#define IceCubeShadow_h__

#include "Export_Function.h"
#include "GameObject.h"
#include "CameraObserver.h"

namespace Engine
{

	class CVIBuffer;
	class CTexture;
}


class CBrainShadow : public Engine::CGameObject
{
private:
	explicit CBrainShadow(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CBrainShadow();

public:
	virtual void	Update(void);
	virtual void	Render(void);

public:
	void SetAngleY(float fAngleY) { m_fAngleY = fAngleY; }
	void SetXZ(float fPosX, float fPosZ) {
		m_pInfo->m_vPos.x = fPosX, m_pInfo->m_vPos.z = fPosZ;
	}
	void SetY(float fPosY) { m_fTargetY = fPosY; }

private:
	void Release(void);
	virtual HRESULT AddComponent(D3DXVECTOR3 vStartPos);
	virtual HRESULT Initialize(D3DXVECTOR3 vStartPos);
	virtual void SetDirection(void);
	virtual void SetTransform(void);

private:
	float m_fTargetY = 0.f;
	float  m_fAngleY = 0.f;
	CCameraObserver* m_pCameraObserver = nullptr;
	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;

private:
	DWORD						m_dwVtxCnt = 0;
	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;
public:
	static CBrainShadow*		Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos);
};
#endif // IceCubeShadow_h__
