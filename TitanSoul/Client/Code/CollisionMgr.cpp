#include "stdafx.h"
#include "CollisionMgr.h"

#include "Engine_functor.h"

#include "PlaneCol.h"
#include "ShpereCol.h"
#include "CubeCol.h"
#include "RectCol.h"

IMPLEMENT_SINGLETON(CCollisionMgr)

CCollisionMgr::CCollisionMgr(void)
{

}

CCollisionMgr::~CCollisionMgr()
{
	Release();
}

HRESULT CCollisionMgr::AddColObject(COLLISIONID eCollisionID)
{
	auto	iter = m_mapCollision.find(eCollisionID);

	if (iter != m_mapCollision.end())
	{
		TAGMSG_BOX(L"COLLISION MGR", L"�ߺ�");
		return E_FAIL;
	}

	Engine::CCollision*		pCollision = nullptr;

	switch (eCollisionID)
	{
	case COL_MOUSE:
		pCollision = CMouseCol::Create();
		break;
	case COL_TERRAIN:
		pCollision = CTerrainColl::Create();
		break;
	case COL_PLANE:
		pCollision = CPlaneCol::Create();
		break;
	case COL_SHPERE:
		pCollision = CShpereCol::Create();
		break;
	case COL_CUBE:
		pCollision = CCubeCol::Create();
		break;
	case COL_RECT:
		pCollision = CRectCol::Create();
		break;
	}

	NULL_CHECK_RETURN(pCollision, E_FAIL);

	m_mapCollision.emplace(eCollisionID, pCollision);

	return S_OK;
}

Engine::CCollision* CCollisionMgr::CloneColObject(COLLISIONID eCollisionID)
{
	auto	iter = m_mapCollision.find(eCollisionID);
	if (iter == m_mapCollision.end())
		return nullptr;

	return iter->second->Clone();
}

void CCollisionMgr::Release(void)
{
	for_each(m_mapCollision.begin(), m_mapCollision.end(), Engine::CDeleteMap());
	m_mapCollision.clear();
}

