#ifndef EffectInfoMgr_h__
#define EffectInfoMgr_h__

/*!
 * \class EffectInfoMgr_h__
 *
 * \brief 
 *		  Singleton.(stdafx.h에 올라가있음)
 *		  EffectInfo를 반환하는 매니져
 *
 *		 MainApp Release에서 지워짐.
 *		 GetTexInfoDataList의 경우 얕은 복사로 List를 반환하기 떄문에 Delete를 하면 안됨.

 * \수정자 정성호
 * \수정일 1월 5 2019
 * \수정 내용 : 
 */


#include "Include.h"
#include "Engine_macro.h"


class CEffectInfoMgr
{
	DECLARE_SINGLETON(CEffectInfoMgr)
private:
	CEffectInfoMgr();
	~CEffectInfoMgr();

public:
	void	AddEffectInfoData(const wstring& wstrKey, EFFECTINFO* pTexinfo);
	const	EFFECTINFO*	GetEffectInfoData(const wstring& wstrKey);
private:
	map<wstring,EFFECTINFO*>		m_mapEffectInfo;
};

#endif // !EffectInfoMgr_h__