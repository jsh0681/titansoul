#ifndef FireButton_h__
#define FireButton_h__

#include"GameObject.h"
#include"CollisionMgr.h"
#include "Export_Function.h"

#include"TerrainColl.h"
#include"CameraObserver.h"


namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CScene;
}
class CFire;

class CFireButton : public Engine::CGameObject
{
private:
	explicit CFireButton(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	~CFireButton(void);

	virtual void Render();
	virtual void Update();
	virtual void Release();
	void StateChange();
	CFire* GetFire() { return m_pFire; }
private:
	virtual HRESULT			Initialize(D3DXVECTOR3 vStartPos);
	virtual HRESULT			AddComponent(D3DXVECTOR3 vStartPos);

	CCollisionMgr*					m_pCollisionMgr = nullptr;
private:
	CCameraObserver*				m_pCameraObserver = nullptr;

	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;

	int m_iIndex = 0;
	DWORD						m_dwVtxCnt = 0;
	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;
	CFire* m_pFire = nullptr;
public:
	void SetIsTrample(bool tf) { m_bIsTrample = tf; }
	bool& GetIsTrample() { return m_bIsTrample; }
	int GetIndex() { return m_iIndex; }
	Engine::CTransform* GetInfo() { return m_pInfo; }
	
	

private:

	bool m_bIsTrample = false; //�������� �P����

private:

	virtual	void		SetDirection(void);
	virtual void		SetTransform();


	

public:
	static CFireButton* Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos);
};
#endif // FireButton_h__