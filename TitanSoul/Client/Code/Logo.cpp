#include "stdafx.h"
#include "Logo.h"

#include "Export_Function.h"
#include "SceneSelector.h"

#include "OrthoCamera.h"

#include "BackGround.h"
#include "LogoButton.h"
#include "LogoArrow.h"
#include "Animal.h"


CLogo::CLogo(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CScene(pGraphicDev)
	, m_pManagement(Engine::Get_Management())
	, m_pResourcesMgr(Engine::Get_ResourceMgr()),
	m_pKeyMgr(Engine::Get_KeyMgr())
{
	for (int i = 0; i < 3; ++i)
		m_pButton[i] = nullptr;

	for (int i = 0; i < 2; ++i)
		m_pArrow[i] = nullptr;
}

CLogo::~CLogo(void)
{
	Release();
}

HRESULT CLogo::Initialize(void)
{
	FAILED_CHECK_RETURN(InitResource(), E_FAIL);			// 백그라운드, Texture, Buffer 생성

	m_pTextureInfoMgr = CTextureInfoMgr::GetInstance();
	m_pEffectInfoMgr = CEffectInfoMgr::GetInstance();
	m_pTerrainMgr = CTerrainMgr::GetInstance();
	m_pParticleMgr = CParticleMgr::GetInstance();

	m_pThread = new thread([&]() {StartLoading(); });

	FAILED_CHECK_MSG(Add_Environment_Layer(), L"Environment 초기화 실패");
	FAILED_CHECK_MSG(Add_GameObject_Layer(), L"GameObject 초기화 실패");
	FAILED_CHECK_MSG(Add_UI_Layer(), L"UI 초기화 실패");		// Background 객체 생성(일단은 막아뒀음)

	CSoundMgr::GetInstance()->PlayBGM(L"Logo_Credit.ogg");

	return S_OK;
}

void CLogo::Update(void)
{
	Engine::CScene::Update();

	if (m_iPercent >= 100)
	{
		for (int i = 0; i < 3; ++i)
			m_pButton[i]->Update();

		for (int i = 0; i < 2; ++i)
			m_pArrow[i]->Update();
	}

	m_pAnimal->Update();

	if (!m_bPercentEnd)
	{
		m_iPercent = int(float(m_AtomLoadingCount) / m_AtomDataCount * 100);

		if (m_pAnimal == nullptr)
			return;
		D3DXVECTOR3 vPos = { 0.f,0.f,0.f };
		vPos = m_pAnimal->GetPos();

		if (m_iPercent != 0)
			vPos.x = (m_iPercent * 8.f) - 350.f;

		m_pAnimal->SetPos(vPos);

		if (m_iPercent == 100)
			m_bPercentEnd = true;
	}

	if (m_AtomReadFileFlag && m_bPercentEnd)
	{
		m_pKeyMgr->KeyCheck();

		if (m_pKeyMgr->KeyDown(KEY_UP))
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Logo_Move.ogg", CSoundMgr::SOUND_BUTTON);
			--m_iButtonIndex;

			if (m_iButtonIndex < 0)
				m_iButtonIndex = 2;

			for (int i = 0; i < 3; ++i)
				m_pButton[i]->SetIndex(0);

			m_pButton[m_iButtonIndex]->SetIndex(1);

			for (int i = 0; i < 2; ++i)
				m_pArrow[i]->SetY((m_iButtonIndex + 1) * -20.f);
		}
		else if (m_pKeyMgr->KeyDown(KEY_DOWN))
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Logo_Move.ogg", CSoundMgr::SOUND_BUTTON);
			++m_iButtonIndex;

			if (m_iButtonIndex > 2)
				m_iButtonIndex = 0;

			for (int i = 0; i < 3; ++i)
				m_pButton[i]->SetIndex(0);

			m_pButton[m_iButtonIndex]->SetIndex(1);

			for (int i = 0; i < 2; ++i)
				m_pArrow[i]->SetY((m_iButtonIndex + 1) * -20.f);
		}

		else if (m_pKeyMgr->KeyDown(KEY_X))
		{
			switch (m_iButtonIndex)
			{
			case 0:
				CSoundMgr::GetInstance()->PlaySoundw(L"Logo_PlayGame.ogg", CSoundMgr::SOUND_BUTTON);
				m_pManagement->SceneChange(CSceneSelector(SC_MAP0_ST, nullptr, nullptr));
				break;
			case 1:
				break;
			case 2:
				PostQuitMessage(0);
				break;
			}
		}
	}
}

void CLogo::Render(void)
{
	m_pGraphicDev->SetRenderState(D3DRS_LIGHTING, FALSE);

	Engine::CScene::Render();

	if (m_iPercent >= 100)
	{
		for (int i = 0; i < 3; ++i)
			m_pButton[i]->Render();

		for (int i = 0; i < 2; ++i)
			m_pArrow[i]->Render();
	}

	m_pAnimal->Render();
}

void CLogo::Release(void)
{
	CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
	for_each(m_ImgPathLst.begin(), m_ImgPathLst.end(), Engine::Safe_Delete<IMGPATH*>);
	for_each(m_DataPathLst.begin(), m_DataPathLst.end(), Engine::Safe_Delete<DATAPATH*>);

	for (int i = 0; i < 3; ++i)
		Engine::Safe_Delete(m_pButton[i]);

	for (int i = 0; i< 2; ++i)
		Engine::Safe_Delete(m_pArrow[i]);

	Engine::Safe_Delete(m_pAnimal);

	m_ImgPathLst.clear();
	m_DataPathLst.clear();

	m_pTextureInfoMgr = nullptr;
	m_pEffectInfoMgr = nullptr;
	m_pParticleMgr = nullptr;

	if (m_pThread)
		ThreadDelete();
}

HRESULT CLogo::InitResource()
{
	// 버퍼 추가    배경, 동물, 버튼(화살표,시작,옵션,나가기, Plane)
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_BackGround"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_Animal"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_LogoArrow"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_LogoButton"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Plane"),
		E_FAIL);



	//	텍스쳐 추가   배경, 동물, 버튼(화살표,시작,옵션,나가기)
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::TEX_NORMAL,
		L"Texture_Logo",
		L"../Bin/Resources/Texture/Tex/Logo/Background/No/%d.png",
		2),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::TEX_NORMAL,
		L"Texture_Animal",
		L"../Bin/Resources/Texture/Tex/Logo/Animal/No/%d.png",
		10),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::TEX_NORMAL,
		L"Texture_LogoArrow",
		L"../Bin/Resources/Texture/Tex/Logo/Button/Arrow/%d.png",
		6),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::TEX_NORMAL,
		L"Texture_Start",
		L"../Bin/Resources/Texture/Tex/Logo/Button/Start/%d.png",
		2),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::TEX_NORMAL,
		L"Texture_Option",
		L"../Bin/Resources/Texture/Tex/Logo/Button/Option/%d.png",
		2),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::TEX_NORMAL,
		L"Texture_Quit",
		L"../Bin/Resources/Texture/Tex/Logo/Button/Quit/%d.png",
		2),
		E_FAIL);

	// Ui 버퍼 추가
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_TalkBalloon"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_TalkWindow"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_Blood"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_Fade"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_SelectObj"),
		E_FAIL);

	CCollisionMgr::GetInstance()->AddColObject(CCollisionMgr::COL_MOUSE);
	CCollisionMgr::GetInstance()->AddColObject(CCollisionMgr::COL_TERRAIN);
	CCollisionMgr::GetInstance()->AddColObject(CCollisionMgr::COL_PLANE);
	CCollisionMgr::GetInstance()->AddColObject(CCollisionMgr::COL_SHPERE);
	CCollisionMgr::GetInstance()->AddColObject(CCollisionMgr::COL_CUBE);

	return S_OK;
}

HRESULT CLogo::Add_GameObject_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);


	Engine::CGameObject*		pGameObject = nullptr;

	pGameObject = CBackGround::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"BackGround", pGameObject);
	pNoBlendLayer->AddObject(L"BackGround", pGameObject);

	m_mapLayer.emplace(Engine::LAYER_GAMEOBJECT, pLayer);

	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	return S_OK;
}

HRESULT CLogo::Add_UI_Layer(void)
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);


	Engine::CGameObject*		pGameObject = nullptr;

	pGameObject = COrthoCamera::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"OrthoCamera", pGameObject);
	pNoBlendLayer->AddObject(L"OrthoCamera", pGameObject);

	m_pButton[0] = CLogoButton::Create(m_pGraphicDev, L"Texture_Start", 1, D3DXVECTOR3(0.f, -20.f, -0.1f));
	NULL_CHECK_RETURN(m_pButton[0], E_FAIL);

	m_pButton[1] = CLogoButton::Create(m_pGraphicDev, L"Texture_Option", 0, D3DXVECTOR3(0.f, -40.f, -0.1f));
	NULL_CHECK_RETURN(m_pButton[1], E_FAIL);

	m_pButton[2] = CLogoButton::Create(m_pGraphicDev, L"Texture_Quit", 0, D3DXVECTOR3(0.f, -60.f, -0.1f));
	NULL_CHECK_RETURN(m_pButton[2], E_FAIL);

	m_pArrow[0] = CLogoArrow::Create(m_pGraphicDev, 0, D3DXVECTOR3(-40.f, -20.f, -0.1f));
	NULL_CHECK_RETURN(pGameObject, E_FAIL);

	m_pArrow[1] = CLogoArrow::Create(m_pGraphicDev, 1, D3DXVECTOR3(40.f, -20.f, -0.1f));
	NULL_CHECK_RETURN(m_pArrow[1], E_FAIL);

	m_pAnimal = CAnimal::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(m_pAnimal, E_FAIL);


	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);

	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	return S_OK;
}

CLogo* CLogo::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CLogo*	pInstance = new CLogo(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

//**********************************************************************
//				로딩 시작

//************************************
// Method	:  StartLoading
// 작성자	:  윤유석
// Date		:  2019/01/03
// Message	:  이중잠금으로 안전한 사용이 가능하게 만듬. (MutexLock, Flag 사용)
//			   ImgPath
//			   DataPath
//			   TextureLoad
//			   DataLoad
//************************************
void CLogo::StartLoading()
{
	if (!m_AtomStartFlag)
	{
		m_AtomStartFlag = true;
		unique_lock<mutex> lock(m_StartMutex);

		ImgPathFindLoad();				// Texture  경로를 얻어올 수 있는 이미지 데이터 생성
		DataPathFindLoad();				// Data		파일 경로 얻음

		LoadTexture();					// Texture	만들기
		LoadData();

		m_AtomReadFileFlag = true;
		m_AtomStartFlag = false;

		lock.unlock();
	}
}

void CLogo::ImgPathFindLoad()
{
	wifstream fin;

	fin.open("../../Data/PathFind/Texture/ImgPath.txt");

	if (fin.fail())
	{
		MessageBox(g_hWnd, L"ImgFind Read Fail", L"Error", MB_OK);
		return;
	}

	TCHAR szTypeKey[MAX_STR] = L"";
	TCHAR szObjKey[MAX_STR] = L"";
	TCHAR szStateKey[MAX_STR] = L"";
	TCHAR szDirkey[MAX_STR] = L"";
	TCHAR szCount[MIN_STR] = L"";
	TCHAR szPath[MAX_STR] = L"";

	IMGPATH* pImgPath = nullptr;

	while (true)
	{
		fin.getline(szTypeKey, MAX_STR, '|');
		fin.getline(szObjKey, MAX_STR, '|');
		fin.getline(szStateKey, MAX_STR, '|');
		fin.getline(szDirkey, MAX_STR, '|');
		fin.getline(szCount, MIN_STR, '|');
		fin.getline(szPath, MAX_STR);

		if (fin.eof())	// EOF에 도달하면 break
			break;

		pImgPath = new IMGPATH;
		pImgPath->wstrType = szTypeKey;
		pImgPath->wstrObjKey = szObjKey;
		pImgPath->wstrStateKey = szStateKey;
		pImgPath->wstrDirKey = szDirkey;
		pImgPath->iCount = _ttoi(szCount);
		pImgPath->wstrPath = szPath;

		m_ImgPathLst.emplace_back(pImgPath);
		m_AtomDataCount.fetch_add(1);
	}
	fin.close();
}

void CLogo::DataPathFindLoad()
{
	wifstream fin;

	fin.open("../../Data/PathFind/Data/DataPath.txt");

	if (fin.fail())
	{
		MessageBox(g_hWnd, L"DataFind Read Fail", L"Error", MB_OK);
		return;
	}

	TCHAR szTypeKey[MAX_STR] = L"";
	TCHAR szObjKey[MAX_STR] = L"";
	TCHAR szStateKey[MAX_STR] = L"";
	TCHAR szName[MAX_STR] = L"";
	TCHAR szPath[MAX_STR] = L"";

	DATAPATH* pDataPath = nullptr;

	while (true)
	{
		fin.getline(szTypeKey, MAX_STR, '|');
		fin.getline(szObjKey, MAX_STR, '|');
		fin.getline(szStateKey, MAX_STR, '|');
		fin.getline(szName, MAX_STR, '|');
		fin.getline(szPath, MAX_STR);

		if (fin.eof())
			break;

		pDataPath = new DATAPATH;
		pDataPath->wstrType = szTypeKey;
		pDataPath->wstrObjKey = szObjKey;
		pDataPath->wstrStateKey = szStateKey;
		pDataPath->wstrFileName = szName;
		pDataPath->wstrPath = szPath;

		m_DataPathLst.emplace_back(pDataPath);
		m_AtomDataCount.fetch_add(1);
	}
	fin.close();
}

HRESULT CLogo::AddPlaneBuffer()
{
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Plane"), E_FAIL);
	return S_OK;
}

void CLogo::LoadTexture()
{
	wstring strComblined = L"";
	for (auto& pImgPath : m_ImgPathLst)
	{
		strComblined = pImgPath->wstrObjKey + pImgPath->wstrStateKey + pImgPath->wstrDirKey;

		if (pImgPath->wstrType == L"Tex")
			m_pResourcesMgr->AddTexture(m_pGraphicDev, Engine::RESOURCE_STATIC, Engine::TEX_NORMAL, strComblined, pImgPath->wstrPath, pImgPath->iCount);
		else if (pImgPath->wstrType == L"Cube")
			m_pResourcesMgr->AddTexture(m_pGraphicDev, Engine::RESOURCE_STATIC, Engine::TEX_CUBE, strComblined, pImgPath->wstrPath, pImgPath->iCount);

		m_AtomLoadingCount.fetch_add(1);
	}
}

void CLogo::LoadData()
{
	wstring strCombliend = L"";

	for (auto& pDataPath : m_DataPathLst)
	{
		if (pDataPath->wstrType == L"Animation")
			LoadAnimation(pDataPath);
		else if (pDataPath->wstrType == L"Effect")
			LoadEffect(pDataPath);
		else if (pDataPath->wstrType == L"Map")
			LoadMap(pDataPath);
		else if (pDataPath->wstrType == L"Particle")
			LoadParticle(pDataPath);

		m_AtomLoadingCount.fetch_add(1);
	}
}

void CLogo::LoadAnimation(DATAPATH* pDataPath)
{
	if (m_pTextureInfoMgr == nullptr)
	{
		MessageBox(g_hWnd, L"TextureInfoMgr Nullptr", L"Error", MB_OK);
		return;
	}

	HANDLE hFile = CreateFile(pDataPath->wstrPath.c_str(), GENERIC_READ, 0, 0, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Animation Load Failed", L"Error", MB_OK);
		return;
	}

	DWORD	dwByte = 0;
	TEXINFO TexInfo;
	TEXINFO* pTexInfo = nullptr;

	int		iCount = 0;
	TCHAR	szBuffer[MIN_STR] = L"";

	ReadFile(hFile, &iCount, sizeof(int), &dwByte, nullptr);
	ReadFile(hFile, &TexInfo.iType, sizeof(int), &dwByte, nullptr);
	ReadFile(hFile, &szBuffer, sizeof(szBuffer), &dwByte, nullptr);
	ReadFile(hFile, &TexInfo.fRatioHeight, sizeof(float), &dwByte, nullptr);
	ReadFile(hFile, &TexInfo.fRatioWidth, sizeof(float), &dwByte, nullptr);

	TexInfo.wstrTexKey = szBuffer;
	m_pTextureInfoMgr->SetVecReserve(pDataPath->wstrFileName, iCount);			// 크기를 미리 확정 지어놓음.

	for (int i = 0; i < iCount; ++i)
	{
		ReadFile(hFile, &TexInfo.iNum, sizeof(int), &dwByte, nullptr);
		ReadFile(hFile, &TexInfo.iNextNum, sizeof(int), &dwByte, nullptr);
		ReadFile(hFile, &TexInfo.fDelay, sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &TexInfo.vScale, sizeof(float) * 3, &dwByte, nullptr);
		ReadFile(hFile, &TexInfo.vRotation, sizeof(float) * 3, &dwByte, nullptr);
		ReadFile(hFile, &TexInfo.vMove, sizeof(float) * 3, &dwByte, nullptr);

		pTexInfo = new TEXINFO(TexInfo);
		m_pTextureInfoMgr->AddTexInfoData(pDataPath->wstrFileName, pTexInfo);
	}
	CloseHandle(hFile);
}

void CLogo::LoadEffect(DATAPATH* pDataPath)
{
	if (m_pEffectInfoMgr == nullptr)
	{
		MessageBox(g_hWnd, L"EffectInfoMgr Nullptr", L"Error", MB_OK);
		return;
	}

	HANDLE hFile = CreateFile(pDataPath->wstrPath.c_str(), GENERIC_READ, 0, 0, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Effect Load Failed", L"Error", MB_OK);
		return;
	}

	DWORD	dwByte = 0;
	EFFECTINFO* pEffectInfo = new EFFECTINFO;

	TCHAR szBuf[MAX_STR] = L"";


	ReadFile(hFile, szBuf, sizeof(szBuf), &dwByte, nullptr);
	pEffectInfo->wstrResourceKey = szBuf;

	ReadFile(hFile, szBuf, sizeof(szBuf), &dwByte, nullptr);
	pEffectInfo->wstrFilePath = szBuf;

	ReadFile(hFile, &(pEffectInfo->wCnt), sizeof(WORD), &dwByte, nullptr);
	ReadFile(hFile, &(pEffectInfo->fRatioWidth), sizeof(float), &dwByte, nullptr);
	ReadFile(hFile, &(pEffectInfo->fRatioHeight), sizeof(float), &dwByte, nullptr);

	ReadFile(hFile, szBuf, sizeof(szBuf), &dwByte, nullptr);
	pEffectInfo->wstrBufferKey = szBuf;

	ReadFile(hFile, &(pEffectInfo->eBufferType), sizeof(Engine::BUFFERTYPE), &dwByte, nullptr);

	m_pEffectInfoMgr->AddEffectInfoData(pDataPath->wstrFileName, pEffectInfo);
	CloseHandle(hFile);
}



void CLogo::LoadParticle(DATAPATH* pDataPath)
{
	if (m_pParticleMgr == nullptr)
	{
		MessageBox(g_hWnd, L"ParticleMgr Nullptr", L"Error", MB_OK);
		return;
	}

	HANDLE hFile = CreateFile(pDataPath->wstrPath.c_str(), GENERIC_READ, 0, 0, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Effect Load Failed", L"Error", MB_OK);
		return;
	}

	DWORD	dwByte = 0;
	PARTICLEINFO* pParticleInfo = new PARTICLEINFO;
	while (true)
	{
		ReadFile(hFile, &(pParticleInfo->bCheckFire), sizeof(bool), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fFirePosX), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fFirePosY), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fFirePosZ), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fFireRangeX), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fFireRangeY), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fFireRangeZ), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fSizeMax), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fSizeMin), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fColorFade), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fLifeTime), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fSpeedMax), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fSpeedMin), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->bCheckShow), sizeof(bool), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fBoundingBoxX), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fBoundingBoxY), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->fBoundingBoxZ), sizeof(float), &dwByte, nullptr);
		ReadFile(hFile, &(pParticleInfo->iNumParticles), sizeof(int), &dwByte, nullptr);

		if (0 == dwByte)
		{
			break;
		}
	}
	m_pParticleMgr->AddParticleInfoData(pDataPath->wstrFileName, pParticleInfo);
	CloseHandle(hFile);
}


void CLogo::LoadMap(DATAPATH* pDataPath)
{
	if (m_pTerrainMgr == nullptr)
	{
		MessageBox(g_hWnd, L"TerrainMgr Nullptr", L"Error", MB_OK);
		return;
	}

	HANDLE hFile = CreateFile(pDataPath->wstrPath.c_str(), GENERIC_READ, 0, 0, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Terrain Load Failed", L"Error", MB_OK);
		return;
	}

	if (pDataPath->wstrObjKey == L"Terrain")
		LoadMapData(hFile);
	else if (pDataPath->wstrObjKey == L"Plane")
		LoadPlaneData(hFile, pDataPath);
	else if (pDataPath->wstrObjKey == L"Brick")
		LoadBrickData(hFile, pDataPath);

	CloseHandle(hFile);
}

void CLogo::LoadMapData(HANDLE& hFile)
{
	DWORD	dwByte = 0;
	MAPINFO* pMapInfo = new MAPINFO;
	TCHAR	szKey[MID_STR] = L"";

	ReadFile(hFile, szKey, sizeof(szKey), &dwByte, nullptr);
	pMapInfo->wstrMapBufferKey = szKey;
	ReadFile(hFile, szKey, sizeof(szKey), &dwByte, nullptr);
	pMapInfo->wstrMapKey = szKey;
	ReadFile(hFile, szKey, sizeof(szKey), &dwByte, nullptr);
	pMapInfo->wstrMapTexKey = szKey;

	ReadFile(hFile, &pMapInfo->vStartingPoint, sizeof(D3DXVECTOR3), &dwByte, nullptr);
	ReadFile(hFile, &pMapInfo->vResurrectionPoint, sizeof(D3DXVECTOR3), &dwByte, nullptr);

	ReadFile(hFile, &pMapInfo->vDestination, sizeof(D3DXVECTOR3), &dwByte, nullptr);
	ReadFile(hFile, &pMapInfo->vBossPoint, sizeof(D3DXVECTOR3), &dwByte, nullptr);

	ReadFile(hFile, &pMapInfo->fSizeX, sizeof(float), &dwByte, nullptr);
	ReadFile(hFile, &pMapInfo->fSizeZ, sizeof(float), &dwByte, nullptr);
	ReadFile(hFile, &pMapInfo->iIndex, sizeof(int), &dwByte, nullptr);

	m_pTerrainMgr->CreateTerrain(pMapInfo);
}

void CLogo::LoadPlaneData(HANDLE& hFile, DATAPATH* pDataPath)
{
	DWORD	dwByte = 0;
	PLANEINFO* pPlaneInfo = nullptr;
	TCHAR	szKey[MID_STR] = L"";
	list<PLANEINFO*> PlaneInfoLst;

	wstring wstrMapKey = pDataPath->wstrType + L"Terrain" + pDataPath->wstrStateKey + pDataPath->wstrFileName;
	// MapTerrainMap0
	// MapTerrainStage0

	int iCount = 0;

	ReadFile(hFile, &iCount, sizeof(int), &dwByte, nullptr);

	for (int i = 0; i < iCount; ++i)
	{
		pPlaneInfo = new PLANEINFO;

		ReadFile(hFile, szKey, sizeof(szKey), &dwByte, nullptr);
		ReadFile(hFile, szKey, sizeof(szKey), &dwByte, nullptr);

		ReadFile(hFile, &pPlaneInfo->vVertex0, sizeof(D3DXVECTOR3), &dwByte, nullptr);
		ReadFile(hFile, &pPlaneInfo->vVertex1, sizeof(D3DXVECTOR3), &dwByte, nullptr);
		ReadFile(hFile, &pPlaneInfo->vVertex2, sizeof(D3DXVECTOR3), &dwByte, nullptr);
		ReadFile(hFile, &pPlaneInfo->vVertex3, sizeof(D3DXVECTOR3), &dwByte, nullptr);

		ReadFile(hFile, &pPlaneInfo->iOption, sizeof(int), &dwByte, nullptr);
		ReadFile(hFile, &pPlaneInfo->iTrigger, sizeof(int), &dwByte, nullptr);

		ReadFile(hFile, &pPlaneInfo->fY, sizeof(float), &dwByte, nullptr);

		PlaneInfoLst.push_back(pPlaneInfo);
	}
	m_pTerrainMgr->CreatePlane(wstrMapKey, PlaneInfoLst);
}

void CLogo::LoadBrickData(HANDLE & hFile, DATAPATH * pDataPath)
{
	DWORD	dwByte = 0;
	CUBEINFO* pCubeInfo = nullptr;
	TCHAR	szKey[MID_STR] = L"";
	list<CUBEINFO*> CubeInfolist;

	wstring wstrMapKey = pDataPath->wstrType + L"Brick" + pDataPath->wstrStateKey + pDataPath->wstrFileName;

	int iCount = 0;

	ReadFile(hFile, &iCount, sizeof(int), &dwByte, nullptr);

	for (int i = 0; i < iCount; ++i)
	{
		pCubeInfo = new CUBEINFO;
		ReadFile(hFile, pCubeInfo, sizeof(CUBEINFO), &dwByte, nullptr);
		CubeInfolist.push_back(pCubeInfo);
	}

	m_pTerrainMgr->CreateBrick(CubeInfolist);
}

void CLogo::ThreadDelete()
{
	if (m_pThread->joinable())
		m_pThread->join();
	Engine::Safe_Delete(m_pThread);
}

//				로딩 끝
//**********************************************************************
