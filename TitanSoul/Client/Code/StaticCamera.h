#ifndef StaticCamera_h__
#define StaticCamera_h__

#include "Camera.h"

namespace Engine
{
	class CTimeMgr;
	class CTransform;
	class CInfoSubject;
}

class CStaticCamera : public Engine::CCamera
{
private:
	explicit	CStaticCamera(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual		~CStaticCamera(void);

public:
	void		SetCameraTarget(const Engine::CTransform* pInfo);
	void		SetDownAngle() { m_bDownAngle = TRUE; };
public:
	virtual		HRESULT	Initialize(void);
	virtual		void	Update(void);
	virtual		void	Release(void);

private:
	void		KeyCheck(void);
	void		TargetRenewal(void);

private:
	Engine::CTimeMgr*			m_pTimeMgr = nullptr;
	Engine::CInfoSubject*		m_pInfoSubject = nullptr;
	const Engine::CTransform*	m_pInfo = nullptr;

	float						m_fDistance = 22.5f;
	float						m_fSpeed = 10.f;
	float						m_fAngle = 89.5f;

	bool						m_bDownAngle = FALSE;
public:
	static CStaticCamera*		Create(LPDIRECT3DDEVICE9 pGraphicDev, 
										const Engine::CTransform* pInfo);

};


#endif // StaticCamera_h__
