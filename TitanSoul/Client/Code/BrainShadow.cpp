#include "stdafx.h"
#include "BrainShadow.h"
#include"Export_Function.h"
#include "Include.h"



CBrainShadow::CBrainShadow(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{

}

CBrainShadow::~CBrainShadow()
{

	Release();
}

HRESULT CBrainShadow::Initialize(D3DXVECTOR3 vStartPos)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);


	FAILED_CHECK_RETURN(AddComponent(vStartPos), E_FAIL);

	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_BrainShadow", m_pVertex);
	return S_OK;
}

HRESULT CBrainShadow::AddComponent(D3DXVECTOR3 vStartPos)
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_BrainShadow");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Texture_Shadow");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTextureCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTextureCom);


	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);


	m_pInfo->m_vPos.y = 0.1f;


	return S_OK;
}





void CBrainShadow::Update(void)
{
	float fTime = Engine::Get_TimeMgr()->GetTime();

	Engine::CGameObject::Update();


	float fScale = 0;

	D3DXMATRIX matScale;
	fScale = m_fTargetY / 10;

	m_pInfo->m_vScale = D3DXVECTOR3(1.f - fScale, 1.f - fScale, 1.f - fScale);
	m_pInfo->m_vPos.y = 0.1f;
	m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(m_fAngleY);



	SetDirection();

	SetTransform();
}

void CBrainShadow::Render(void)
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_BrainShadow", m_pConvertVertex);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(100, 255, 255, 255));

	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pTextureCom->Render(0);
	m_pBufferCom->Render();

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

}

void CBrainShadow::Release(void)
{

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);


	for (size_t i = 0; i < m_dwVtxCnt; ++i)
		m_pConvertVertex[i] = m_pVertex[i];

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_IceCubeShadow", m_pConvertVertex);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);

}

void CBrainShadow::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

}

void CBrainShadow::SetTransform(void)
{
	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pMatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pMatProj);
}


CBrainShadow* CBrainShadow::Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos)
{
	CBrainShadow* pInstance = new CBrainShadow(pGraphicDev);
	if (FAILED(pInstance->Initialize(vStartPos)))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}
