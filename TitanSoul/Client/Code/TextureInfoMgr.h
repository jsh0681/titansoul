#ifndef TextureInfoMgr_h__
#define TextureInfoMgr_h__

/*!
 * \class CTextureInfoMgr
 *
 * \brief 
 *		  Singleton.(stdafx.h에 올라가있음)
 *		  TexInfo를 반환하는 매니져
 *
 *		 MainApp Release에서 지워짐.
 *		 GetTexInfoDataList의 경우 얕은 복사로 List를 반환하기 떄문에 Delete를 하면 안됨.

 * \수정자 윤유석
 * \수정일 1월 3 2019
 * \수정 내용 : 
 */


#include "Include.h"
#include "Engine_macro.h"


class CTextureInfoMgr
{
	DECLARE_SINGLETON(CTextureInfoMgr)
private:
	CTextureInfoMgr();
	~CTextureInfoMgr();

public:
	void	AddTexInfoData(const wstring& wstrKey, TEXINFO* pTexinfo);
	const vector<TEXINFO*>*	GetTexInfoDataVec(const wstring& wstrKey);
	void	SetVecReserve(const wstring& wstrkey, const int& iSize);

private:
	map<wstring,vector<TEXINFO*>>		m_mapTexInfo;
};

#endif // !TextureInfoMgr_h__s