#ifndef Cat_h__
#define Cat_h__



#include "Npc.h"
class CPlayerObserver;
class CPlayer;
class CCat : public CNpc
{
	enum C_STATE { IDLE, ATTACK, S_END };
	enum E_EVENT { E_NO, E_HI, E_FISH, E_SLUDGE, E_GOLIATH, E_QAYIN, E_EYE, E_ENDTALK };

private:
	explicit CCat(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CCat();

private:
	virtual HRESULT Initialize(const VEC3& vPos, CPlayer* pPlayer);
	virtual HRESULT AddComponent();
	virtual void Release();

	virtual HRESULT InitTalkBalloon()		override;
	virtual HRESULT InitTalkWindow()		override;

private:
	void	ReturnIdle();
	void	SetTransform();
	void	SetStateKey();
	void	SetTrakingPos();
	void	ShowTalkBalloon();

	void	SceneChange();
	void	CheckEvent();

	void	Event_No();
	void	Event_Hi();
	void	Event_Fish();
	void	Event_SLUDGE();
	void	Event_GOLIATH();
	void	Event_QAYIN();
	void	Event_EYE();
	void	Event_ENDTALK();

	void	TalkAfter(const float& fDis);

public:
	virtual void Update();
	virtual void Render();


public:
	static int						m_iEvent;
	static int						m_iEvent_TalkCnt;

private:
	wstring					m_wstrStateKey[S_END];
	wstring					m_wstrCurStateKey;

	C_STATE					m_eState[Engine::END_COMPARE];

	VEC3					m_vTrakingPos[6];
	Engine::DIRECTION		m_eEventDir[5];

	CPlayer*				m_pPlayer;
	CPlayerObserver*		m_pPlayerObserver = nullptr;



	VEC3					m_vPlayerPos;

	float					m_fCnt = 0.f;
	bool					m_bBalloon = true;
	const float				m_fBalloonCoolTime = 3.f;
	float					m_fBalloonCool = 0.f;
	const float				m_fBalloonRestCoolTime = 2.f;
	float					m_fBalloonRestTime = 0.f;
	float					m_fBalloneRandTime = 0.f; 
	bool					m_bAttackEvent = false;

	bool					m_bRollEvent = false;
	bool					m_bRunEvent = false;

	int						m_iWeak = 0;
	wstring					m_wstrWeak[6];

public:
	static CCat*	Creare(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, CPlayer* pPlayer);
	static bool	m_bCatInit;
};

#endif // !Cat_h__