#ifndef MouseCol_h__
#define MouseCol_h__

#include "Collision.h"
#include "CameraObserver.h"

namespace Engine
{
	class CInfoSubject;
}

class CMouseCol : public Engine::CCollision
{
private:
	CMouseCol(void);

public:
	virtual ~CMouseCol(void);

public:
	void			PickTerrain(D3DXVECTOR3* pOut, const Engine::VTXTEX* pTerrainVtx);
	void			PickObject(D3DXVECTOR3* pOut, const Engine::VTXTEX* pVertex, const D3DXMATRIX* pmatWorld);
	void			Release(void);

private:
	HRESULT			Initialize(void);
	void			Translation_ViewSpace(void);
	void			Translation_Local(const D3DXMATRIX* pWorld);


private:
	Engine::CInfoSubject*			m_pInfoSubject;
	CCameraObserver*				m_pCameraObserver;
	D3DXVECTOR3						m_vRayPos;
	D3DXVECTOR3						m_vRayDir;

public:
	static POINT		GetMousePos(void);
	static CMouseCol*	Create(void);
	virtual Engine::CCollision*		Clone(void);

};

#endif // MouseCol_h__
