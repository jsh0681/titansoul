#include "stdafx.h"
#include "Stage2.h"


// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

//Obj
#include "Player.h"
#include "Arrow.h"
#include "Terrain.h"
#include "Plane.h"
#include "Knuckle.h"
#include "ShpereCol.h"
#include "FirePlane.h"
#include "Display.h"
#include "StaticCamera2D.h"
#include "SceneSelector.h"
#include "OrthoCamera.h"
#include "CeremoneyEffect_List.h"

// static
bool CStage2::m_sbStage2_Resource = false;
bool CStage2::m_sbBossDie = false;

CStage2::CStage2(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera) :
	Engine::CScene(pGraphicDev),
	m_pManagement(Engine::Get_Management()),
	m_pResourcesMgr(Engine::Get_ResourceMgr()),
	m_pkeyMgr(Engine::Get_KeyMgr()),
	m_pTimeMgr(Engine::Get_TimeMgr()),
	m_pPlayer(pPlayer),
	m_pCamera(pCamera)
{
	m_pDisplay = CDisplay::Create(m_pGraphicDev);
	m_pDisplay->StartFade(CDisplay::FADEIN);
}

CStage2::~CStage2()
{
	Release();
}

HRESULT CStage2::Initialize()
{
	m_vecFirePlane.reserve(m_iPlaneSize);

	FAILED_CHECK_RETURN(AddResource(), E_FAIL);

	FAILED_CHECK_RETURN(Add_Environment_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_GameObject_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_UI_Layer(), E_FAIL);

	SetStartPoint();
	
	g_BattleMode = false;
	m_pSphereCol = dynamic_cast<CShpereCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_SHPERE));

	CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
	
	return S_OK;
}

void CStage2::Release()
{
	Engine::Safe_Delete(m_pDisplay);

	Engine::Safe_Delete(m_pSphereCol);

	m_vecFirePlane.clear();
	m_vecFirePlane.shrink_to_fit();

	if (m_bChangeScene)
	{
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"Player");
		m_mapLayer[Engine::LAYER_UI]->ClearList(L"Camera");
	}
}

HRESULT CStage2::AddResource()
{
	if (m_sbStage2_Resource)
	{
		m_bDieCheck = true;
		return S_OK;
	}

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC, 
		Engine::BUFFER_RCTEX_Z,	L"Buffer_Knuckle_Body"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Knuckle_Mace"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Knuckle_Chain"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_Knuckle_Shadow"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_FirePlane"), E_FAIL);
	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev, Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z, L"Buffer_FireEffect"), E_FAIL);

	m_sbStage2_Resource = true;

	return S_OK;
}

HRESULT CStage2::Add_Environment_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	m_mapLayer.emplace(Engine::LAYER_ENVIRONMENT, pLayer);
	return S_OK;
}

HRESULT CStage2::Add_GameObject_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	Engine::CGameObject*		pGameObject = nullptr;

	pLayer->AddObject(L"Player", m_pPlayer);
	m_pPlayer->SetScene(this);
	m_pPlayer->CameraCreate();
	m_pPlayer->CreateArrow();
	pBlendLayer->AddObject(L"Player", m_pPlayer);

	CArrow* pArrow = m_pArrow = m_pPlayer->Get_ArrowPointer();
	Engine::CGameObject* pObject = dynamic_cast<Engine::CGameObject*>(pArrow);
	pLayer->AddObject(L"Arrow", pObject);
	pBlendLayer->AddObject(L"Arrow", pObject);

	pGameObject = m_pTerrain = CTerrainMgr::GetInstance()->Clone(L"MapTerrainStage2");
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Terrain", pGameObject);
	pNoBlendLayer->AddObject(L"Terrain", pGameObject);

	if (!m_sbBossDie)
	{
		VEC3 vBossPos = CTerrainMgr::GetInstance()->GetTerrain(L"MapTerrainStage2")->GetDataInfo()->vBossPoint;
		pGameObject = m_pKnuckle = CKnuckle::Create(m_pGraphicDev, vBossPos);
		NULL_CHECK_RETURN(pGameObject, E_FAIL);
		pLayer->AddObject(L"Boss", pGameObject);
		pBlendLayer->AddObject(L"Boss", pGameObject);
	}
	// �� �Ʒ�
	VEC3 vPos = { 28.f, 0.08f, 30.f };
	m_vecFirePlane.emplace_back(CFirePlane::Create(m_pGraphicDev, vPos, L"Buffer_FirePlane"));
	pGameObject = m_vecFirePlane.back();
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FirePlane", pGameObject);
	pBlendLayer->AddObject(L"FirePlane", pGameObject);

	vPos = { 43.f,0.08f, 30.f };
	m_vecFirePlane.emplace_back(CFirePlane::Create(m_pGraphicDev, vPos, L"Buffer_FirePlane"));
	pGameObject = m_vecFirePlane.back();
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FirePlane", pGameObject);
	pBlendLayer->AddObject(L"FirePlane", pGameObject);

	// �� ����
	vPos = { 28.f, 0.08f, 50.f };
	m_vecFirePlane.emplace_back(CFirePlane::Create(m_pGraphicDev, vPos, L"Buffer_FirePlane"));
	pGameObject = m_vecFirePlane.back();
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FirePlane", pGameObject);
	pBlendLayer->AddObject(L"FirePlane", pGameObject);

	vPos = { 43.f ,0.08f, 50.f };
	m_vecFirePlane.emplace_back(CFirePlane::Create(m_pGraphicDev, vPos, L"Buffer_FirePlane"));
	pGameObject = m_vecFirePlane.back();
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FirePlane", pGameObject);
	pBlendLayer->AddObject(L"FirePlane", pGameObject);

	// ������ ��
	vPos = { 38.f ,0.08f, 44.5f };
	m_vecFirePlane.emplace_back(CFirePlane::Create(m_pGraphicDev, vPos, L"Buffer_FirePlane"));
	pGameObject = m_vecFirePlane.back();
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FirePlane", pGameObject);
	pBlendLayer->AddObject(L"FirePlane", pGameObject);

	vPos = { 24.f ,0.08f, 40.2f };
	m_vecFirePlane.emplace_back(CFirePlane::Create(m_pGraphicDev, vPos, L"Buffer_FirePlane"));
	pGameObject = m_vecFirePlane.back();
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FirePlane", pGameObject);
	pBlendLayer->AddObject(L"FirePlane", pGameObject);

	vPos = { 42.f ,0.08f, 39.1f };
	m_vecFirePlane.emplace_back(CFirePlane::Create(m_pGraphicDev, vPos, L"Buffer_FirePlane"));
	pGameObject = m_vecFirePlane.back();
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FirePlane", pGameObject);
	pBlendLayer->AddObject(L"FirePlane", pGameObject);

	vPos = { 28.4f ,0.08f, 37.f };
	m_vecFirePlane.emplace_back(CFirePlane::Create(m_pGraphicDev, vPos, L"Buffer_FirePlane"));
	pGameObject = m_vecFirePlane.back();
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FirePlane", pGameObject);
	pBlendLayer->AddObject(L"FirePlane", pGameObject);


	pLayer->AddObject(L"Effect", pGameObject);
	pBlendLayer->AddObject(L"Effect", pGameObject);

	pLayer->ClearList(L"Effect");
	pBlendLayer->ClearList(L"Effect");

	m_mapLayer.emplace(Engine::LAYER_GAMEOBJECT, pLayer);

	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	return S_OK;
}

HRESULT CStage2::Add_UI_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	pLayer->AddObject(L"Camera", m_pCamera);
	pNoBlendLayer->AddObject(L"Camera", m_pCamera);


	Engine::CGameObject * pGameObject = COrthoCamera::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"OrthoCamera", pGameObject);
	pNoBlendLayer->AddObject(L"OrthoCamera", pGameObject);

	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);

	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_UI, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_UI, pBlendLayer);

	return S_OK;
}

void CStage2::Update()
{
	CScene::Update();

	m_pDisplay->Update();
	RemoveEffect();

	// ���� �̺�Ʈ
	if (!m_bDieCheck)
	{
		CheckJumpEnd();
		CheckCameraShaking();
		CheckFirePlaneCol();
	}

	// �÷��̾� �� ȭ���̶� �ٸ� �͵��̶� �浹
	CheckMaceCol();			// �÷��̾� �� ȭ���̶� ö��� �浹
	CollPlaneArrow();		// ���̶� ȭ���̶�

	CheckBodyCol();			// ���� ���̶� �÷��̾�� �浹
	CheckMonsterCol();		// ���Ͷ� ȭ���̶� �浹 �ϴ°�

	CheckPlaneColTriger(m_pTerrain->CollisionPlane(m_pPlayer));	// �÷��̾�� ���̶� �浹
}

void CStage2::Render()
{
	for (auto& it : m_mapRenderLayer[Engine::LAYER_NONALPHA])
		it.second->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000045);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	for (auto& it : m_mapRenderLayer[Engine::LAYER_ALPHA])
		it.second->Render();

	m_pDisplay->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CStage2::SetStartPoint()
{
	const D3DXVECTOR3& vStartPos = { 36.f, 0.1f, 25.f };

	m_pPlayer->SetPosXZ(vStartPos);
	m_pPlayer->SetDirUp();
	m_pPlayer->SceneChange();
	dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();
}

void CStage2::BloodMode()
{
	m_pDisplay->StartBlood();
}

void CStage2::CeremonyYeah()
{
	//���⼭ �ϴ� ������� �������.
	NorEffect Temp;
	Temp.fDisappearSpd = 0.f;
	Temp.fLifeTime = 15.f;
	Temp.fScale = 2.f;
	Temp.fSpd = 1.0f;
	Temp.vAcc = m_pPlayer->Get_Trnasform()->m_vPos; //�÷��̾���ġ�ְ�
	Temp.vPos = m_pPlayer->Get_ArrowPointer()->Get_Trnasform()->m_vPos;
	Temp.vPos.y = 0.2f;

	float fAngle = 0.f;
	float fRand = 0.f;

	list<Engine::CGameObject*>* pUpdateList = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	list<Engine::CGameObject*>* pRenderList = m_mapRenderLayer[Engine::LAYER_ALPHA][Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	for (int i = 0; i < 14; ++i)
	{
		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;
		fRand = GetRandom<float>(1.f, 2.f);
		Temp.fSpd = fRand;

		Engine::CGameObject* pGameObject = CCeremoneyEffect_List::Create(m_pGraphicDev, Temp, L"Buffer_Effect", L"EffectCeremony2NO", m_pPlayer->Get_Trnasform()->m_matWorld, pUpdateList, pRenderList, m_pPlayer);
		pUpdateList->push_back(pGameObject);
		pRenderList->push_back(pGameObject);

		fRand = GetRandom<float>(0.f, 30.f);
		fAngle += 45.f + fRand;
	}
}

void CStage2::RemoveEffect()
{	
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->end();

	CNormalEffect* pEffect = nullptr;

	list<Engine::CGameObject*>* pRenderList = m_mapRenderLayer[Engine::LAYER_ALPHA][Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pEffect = dynamic_cast<CNormalEffect*>(*iter);
		if (pEffect == nullptr) return;

		if (pEffect->CheckIsDead())
		{
			//�׿�
			auto& it_find = find_if(pRenderList->begin(), pRenderList->end(), [pEffect](Engine::CGameObject* pObj)
			{
				if (pObj == pEffect)
					return true;
				return false;
			});

			if (it_find != pRenderList->end())
				pRenderList->erase(it_find);

			Engine::Safe_Delete((*iter));
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->erase(iter);
		}
		else
			++iter;
	}
}

void CStage2::CheckMonsterCol()
{
	if (m_bDieCheck)
		return;

	if (!m_bFirstCol)
	{
		if (m_pSphereCol->CheckCollision(m_pArrow->Get_Trnasform()->m_vPos, m_pArrow->GetRadius(),
			m_pKnuckle->GetInfo()->m_vPos, m_pKnuckle->GetRadius()))
		{
			CSoundMgr::GetInstance()->PlayBGM(L"Boss_Knuckle.ogg");
			m_bFirstCol = true;
			g_BattleMode = true;
			m_pKnuckle->SetState(1);			// ����
			m_pKnuckle->SetInvincibility(true);	// ��񵿾� ������ ��
			m_pArrow->StopArrow();
		}
	}
	else
	{
		if (m_pArrow->Get_ArrowState() == CArrow::A_SHOT || m_pArrow->Get_ArrowState() == CArrow::A_RECOVERY)	
		{
			if (m_pSphereCol->CheckCollision(m_pArrow->Get_Trnasform()->m_vPos, m_pArrow->GetRadius(),
				m_pKnuckle->GetPos(), m_pKnuckle->GetRadius()))
			{
				m_pArrow->StopArrow();
				return;

			}
		}

		if (m_pArrow->Get_ArrowState() == CArrow::A_SHOT || m_pArrow->Get_ArrowState() == CArrow::A_RECOVERY)			// ȭ���� ��� �ֳ� ���� �ֳ� Ȯ�� �� �浹�˻�
		{
			if (m_pSphereCol->CheckCollision(m_pArrow->Get_Trnasform()->m_vPos, m_pArrow->GetRadius(),
				m_pKnuckle->GetDiePos(), m_pKnuckle->GetWeakRadius()))
			{
				if (m_pKnuckle->GetInvincibility())
				{
					m_pArrow->StopArrow();
					return;
				}
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"BossDead.mp3");
				m_pArrow->StopArrow();
				m_pKnuckle->SetDie(true);
				g_BattleMode = false;				// Ŭ������
				m_sbBossDie = true;
				m_bDieCheck = true;

				Engine::Get_TimeMgr()->SlowModeStart(5.f, 0.1f);
				DWORD temp = BOSSSTAGE2;
				m_pPlayer->SetDBoss(temp);
				CeremonyYeah();
			}
		}
	}
}

void CStage2::CheckFirePlaneCol()
{
	if (m_bDieCheck)
		return;

	for (auto it : m_vecFirePlane)
	{
		if (it->GetEruption())
		{
			if (m_pSphereCol->CheckCollision(m_pPlayer->Get_Trnasform()->m_vPos,
				m_pPlayer->GetRadius(), it->GetPos(), it->GetRadius()))
			{
				BloodMode();
			}
		}

		if (m_pSphereCol->CheckCollision(m_pKnuckle->GetPos(),
			m_pKnuckle->GetRadius(), it->GetPos(), it->GetRadius()))
		{
			it->OnEruption();
		}
	}
}

void CStage2::CheckJumpEnd()
{
	if (m_pKnuckle->GetJumpEnd())			// ���� ���ִ� �̺�Ʈ
	{
		for (auto& it : m_vecFirePlane)
			it->OnEruption();

		m_bJumpEvent = true;
		m_fJumpEventCount = 0.f;
	}
	else									// �˾Ƽ� ����
	{
		if (m_bJumpEvent)
		{
			m_fJumpEventCount += m_pTimeMgr->GetTime();
			if (m_fJumpEventCount > 3.5f)
			{
				m_bJumpEvent = false;

				for (auto& it : m_vecFirePlane)
					it->OffEruption();

				m_fJumpEventCount = 0.f;
			}
		}
		else
		{
			for (auto& it : m_vecFirePlane)
			{
				if(it->GetEruption() && (!it->GetLife()))			// ���� �հ� �ִ°� ����ִ� �ð��� ������ ����
					it->OffEruption();
			}

		}
	}
}

void CStage2::CheckBodyCol()
{
	if (m_bDieCheck)
		return;

	if (m_pSphereCol->CheckCollision(m_pPlayer->Get_Trnasform()->m_vPos, m_pPlayer->GetRadius(),
		m_pKnuckle->GetPos(), m_pKnuckle->GetRadius()))
	{
		const VEC3& vPlayerPos = m_pPlayer->Get_Trnasform()->m_vPos;
		VEC3 vPush = vPlayerPos - m_pKnuckle->GetPos();
		vPush.y = 0.f;
		vPush *= 0.1f;

		m_pPlayer->SetPushPos(vPush);
	}
}

void CStage2::CheckMaceCol()
{
	if (m_bDieCheck)
		return;

	for (int i = 0; i < 2; ++i)
	{
		// �÷��̾�� ���� ö��� �浹
		if (m_pSphereCol->CheckCollision(m_pPlayer->Get_Trnasform()->m_vPos, m_pPlayer->GetRadius(),
			m_pKnuckle->GetMacePos(i), m_pKnuckle->GetMaceRadius(i)))
		{
			BloodMode();
		}

		// ȭ���̶� ö���
		if (m_pArrow->Get_ArrowState() == CArrow::A_SHOT || m_pArrow->Get_ArrowState() == CArrow::A_RECOVERY)
		{
			if (m_pSphereCol->CheckCollision(m_pArrow->Get_Trnasform()->m_vPos, m_pArrow->GetRadius(),
				m_pKnuckle->GetMacePos(i), m_pKnuckle->GetMaceRadius(i)))
			{
				m_pArrow->StopArrow();
			}
		}

	}

}

void CStage2::CollPlaneArrow()
{
	m_pTerrain->CollisoinPlanetoArrowBackAgain(m_pArrow, m_pPlayer);
}

void CStage2::CheckPlaneColTriger(const int& iTriger)
{
	switch (iTriger)
	{
	case PL_EXIT:

		if (!m_bDieCheck)
			return;

		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}

		if (m_pDisplay->CheckFadeFinish())
		{
			m_pPlayer->SetDirDown();
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			m_pManagement->SceneChange(CSceneSelector(SC_MAP0, m_pPlayer, m_pCamera));
		}
		break;
	}
}

void CStage2::CheckCameraShaking()
{
	if (m_pKnuckle->GetAttackEnd())
		dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(0.7f, 1.5f, 1.3f);
	else if (m_pKnuckle->GetJumpEnd())	
		dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(1.f, 2.2f, 1.7f);
}

CStage2* CStage2::Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	CStage2* pInstance = new CStage2(pGraphicDev, pPlayer, pCamera);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
