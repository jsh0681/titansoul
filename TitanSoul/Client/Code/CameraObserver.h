#ifndef CameraObserver_h__
#define CameraObserver_h__

#include "Observer.h"

namespace Engine
{
	class CInfoSubject;
}

class CCameraObserver : public Engine::CObserver
{
private:
	CCameraObserver(void);
public:
	virtual ~CCameraObserver(void);

public:
	const D3DXMATRIX*		GetView(void) { return &m_matView; }
	const D3DXMATRIX*		GetProj(void) { return &m_matProj; }
	const D3DXMATRIX*		GetOrtho(void) { return &m_matOrtho; }

public:
	virtual void Update(int iFlag);

private:
	Engine::CInfoSubject*	m_pInfoSubject;

	D3DXMATRIX				m_matView;
	D3DXMATRIX				m_matProj;
	D3DXMATRIX				m_matOrtho;

public:
	static CCameraObserver*		Create(void);

};



#endif // CameraObserver_h__
