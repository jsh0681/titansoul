#include "stdafx.h"
#include "ParticleMgr.h"

#include "Engine_function.h"

IMPLEMENT_SINGLETON(CParticleMgr)

CParticleMgr::CParticleMgr()
{
}

CParticleMgr::~CParticleMgr()
{
	for_each(m_mapParticleInfo.begin(), m_mapParticleInfo.end(), [](auto& myPair)
	{
		Engine::Safe_Delete(myPair.second);
	});
	m_mapParticleInfo.clear();
}

void CParticleMgr::AddParticleInfoData(const wstring& wstrKey, PARTICLEINFO* pEffectInfo)
{
	if (pEffectInfo == nullptr)
		return;

	auto& it_find = m_mapParticleInfo.find(wstrKey);

	if (it_find == m_mapParticleInfo.end())
		m_mapParticleInfo[wstrKey] = pEffectInfo;
}

//************************************
// Method	:  GetTexInfoDataList
// 작성자	:  윤유석
// Date		:  2019/01/03
// Message	:  절대로 반환 받은 List의 값을 지우면 안됨.
// Parameter:  찾는 이미지 데이터의 키 값.
//************************************

const PARTICLEINFO* CParticleMgr::GetParticleInfoData(const wstring& wstrKey)
{
	auto& it_find = m_mapParticleInfo.find(wstrKey);

	if (it_find == m_mapParticleInfo.end())
		return nullptr;

	return it_find->second;
}
