#ifndef TerrainMgr_h__
#define TerrainMgr_h__

/*!
* \class CTerrainMgr
*
* \간략정보 지형을 관리하는 매니져
*
* \상세정보 지형 추가,불러오기
*			 바닥 추가, 불러오기
*			 여기서 Plane리스트를 가지고 있음, 지워줌
*			 Terrain Release에서 지워줌
*
* \작성자 윤유석
*
* \date 1월 7 2019
*
*/

class CTerrain;
class CPlane;
class CTerrainMgr
{
	DECLARE_SINGLETON(CTerrainMgr)
private:
	CTerrainMgr();
	~CTerrainMgr();

	void Release();

public:
	HRESULT		CreateTerrain(MAPINFO* pMapInfo);
	CTerrain*	Clone(const wstring& wstrTerrainName);

	HRESULT		CreatePlane(const wstring& wstrTerrainName, list<PLANEINFO*>& pPlaneInfoLst);
	HRESULT		CreateBrick(list<CUBEINFO*>& pCubeInfoLst);


	CTerrain*				GetTerrain(const wstring& wstrTerrainName);
	const list<CPlane*>*	GetPlane(const wstring& wstrTerrainName);
	const MAPINFO*			GetMapInfo(const wstring& wstrTerrainName);
	const list<CUBEINFO*>* GetpCubeInfoList() { return &m_pCubeInfoList; };


private:
	void	SetPlaneInfoText(PLANEINFO* pPlaneInfo);
	HRESULT	CreateTerrainBuffer(const wstring& wstrBufferKey, const int& iHeight, const int& iWidth);

private:
	LPDIRECT3DDEVICE9			m_pGraphicDev;
	map<wstring, CTerrain*>		m_mapTerrain;
	map<wstring, list<CPlane*>*> m_mapPlane;
	list<CUBEINFO*> m_pCubeInfoList;
};

#endif // !TerrainMgr_h__