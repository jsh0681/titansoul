#include "stdafx.h"
#include "Display.h"

#include "Export_Function.h"

#include "Include.h"
#include "Blood.h"
#include "Fade.h"

CDisplay::CDisplay(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement	= Engine::Get_Management();
	m_pInfoSubject	= Engine::Get_InfoSubject();
	m_pTimeMgr		= Engine::Get_TimeMgr();
}


CDisplay::~CDisplay()
{
	Release();
}

HRESULT CDisplay::Initialize()
{
	m_pFade = CFade::Create(m_pGraphicDev);
	m_pBlood = CBlood::Create(m_pGraphicDev);

	return S_OK;
}

void CDisplay::Release()
{
	Engine::Safe_Delete(m_pBlood);
	Engine::Safe_Delete(m_pFade);
}

void CDisplay::Update()
{
	m_pBlood->Update();
	m_pFade->Update();
}

void CDisplay::Render()
{
	m_pBlood->Render();
	m_pFade->Render();
}

void CDisplay::StartFade(FADEEFFECT dID)
{
	switch (dID)
	{
	case FADEIN:
		m_pFade->FadeInStart();
		break;

	case FADEOUT:
		m_pFade->FadeOutStart();
		break;
	}
}

void CDisplay::StartBlood()
{
	m_pBlood->BloodStart();
}

bool CDisplay::CheckFadeFinish()
{
	bool bCheck = m_pFade->GetIsOn();;
	return !bCheck;
}

CDisplay * CDisplay::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CDisplay* pInstance = new CDisplay(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}
