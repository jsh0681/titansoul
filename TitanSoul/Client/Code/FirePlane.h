#ifndef FirePlane_h__
#define FirePlane_h__

#include "GameObject.h"



/*!
 * \class CFirePlane
 *
 * \�������� ���� ����Ʈ�� �մ� �ٴ�
 *
 * \������ 
 *
 * \�ۼ��� ������
 *
 * \date 1�� 16 2019
 *
 */


namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTimeMgr;
}

class CCameraObserver;
class CAniEffect;
class CSphereColBox;
class CFirePlane : public Engine::CGameObject
{
private:
	explicit CFirePlane(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CFirePlane();

private:
	HRESULT		AddComponent();
	HRESULT		CreateObj();

	virtual HRESULT	Initialize(const VEC3& vPos, const wstring& wstrBuffer);
	virtual void	Release();

public:
	virtual void	Update();
	virtual void	Render();

private:
	void			ResetBuffer();
	void			SetTransform();

	void			CheckLifeTime();

public:
	void			OnEruption();
	void			OffEruption(); 

	const VEC3&			GetPos();

	const float&	GetRadius()			{ return m_fRadius; }
	const bool&		GetEruption()		{ return m_bOn; }
	const bool&		GetLife()			{ return m_bLife; }

private:
	const int				m_iMaxFireEffect = 15;
	const float				m_fMaxLifeTIme = 3.5f;

private:
	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;
	Engine::CTimeMgr*		m_pTimeMgr;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	DWORD					m_dwVtxCnt;

	wstring					m_wstrBufferName;

	CSphereColBox*			m_pColBox = nullptr;;
	CCameraObserver*		m_pCameraObserver;
	list<CAniEffect*>		m_FireEffectList;
	NorEffect				m_NoEffect;

	float					m_fLimit = 0.f;
	float					m_fRadius = 0.4f;

	float					m_fLifeTime = 0.f;
	
	bool					m_bOn = false;
	bool					m_bLife = false;

public:
	static CFirePlane*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const wstring& wstrBuffer);
};

#endif //!FirePlane_h__