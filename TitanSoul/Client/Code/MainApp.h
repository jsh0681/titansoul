#ifndef MainApp_h__
#define MainApp_h__

#include "Include.h"

namespace Engine
{
	class CGraphicDev;
	class CManagement;
}

class CMainApp
{
private: // 생성자, 소멸자
	explicit CMainApp(void);
public:
	~CMainApp(void);

public:		// public 함수, 변수
	void	Update(void);
	void	Render(void);
protected:	// protected 함수, 변수
private:	// protected 함수, 변수
	void	Release(void);
	HRESULT	Initialize(void);

private:
	Engine::CGraphicDev*			m_pGraphicDev = nullptr;
	Engine::CManagement*			m_pManagement = nullptr;
	LPDIRECT3DDEVICE9				m_pDevice = nullptr;



public: // static 생성함수
	static CMainApp*		Create(void);

};

#endif // MainApp_h__

