#include "stdafx.h"
#include "PlaneCol.h"


CPlaneCol::CPlaneCol()
{
	ZeroMemory(&m_vDis, sizeof(D3DXVECTOR3));
}


CPlaneCol::~CPlaneCol()
{
	Release();
}

void CPlaneCol::Update()
{
	float	fRatioX = (m_pPos->x - m_pPlaneVtx[0].vPos.x);
	float	fRatioZ = (m_pPlaneVtx[0].vPos.z - m_pPos->z);

	// 오른쪽 위
	if (fRatioX > fRatioZ)
	{
		D3DXPlaneFromPoints(&m_Plane,
			&m_pPlaneVtx[0].vPos,
			&m_pPlaneVtx[1].vPos,
			&m_pPlaneVtx[2].vPos);
	}
	// 왼쪽 아래
	else
	{
		D3DXPlaneFromPoints(&m_Plane,
			&m_pPlaneVtx[0].vPos,
			&m_pPlaneVtx[2].vPos,
			&m_pPlaneVtx[3].vPos);
	}
}

void CPlaneCol::Release()
{
}

void CPlaneCol::SetColInfo(D3DXVECTOR3* pPos, const Engine::VTXTEX* pPlaneVtx)
{
	m_pPos = pPos;
	m_pPlaneVtx = pPlaneVtx;
}

const D3DXVECTOR3& CPlaneCol::GetColVec()
{
	m_vDis.x = (-m_Plane.b * m_pPos->y - m_Plane.c * m_pPos->z - m_Plane.d) / m_Plane.a;
	m_vDis.y = (-m_Plane.a * m_pPos->x - m_Plane.c * m_pPos->z - m_Plane.d) / m_Plane.b;
	m_vDis.z = (-m_Plane.a * m_pPos->x - m_Plane.b * m_pPos->y - m_Plane.d) / m_Plane.c;
	return m_vDis;
}

float CPlaneCol::GetColDisX()
{
	return move(-m_Plane.b * m_pPos->y - m_Plane.c * m_pPos->z - m_Plane.d) / m_Plane.a;
}

float CPlaneCol::GetColDisY()
{
	return move(-m_Plane.a * m_pPos->x - m_Plane.c * m_pPos->z - m_Plane.d) / m_Plane.b;
}

float CPlaneCol::GetColDisZ()
{
	return move(-m_Plane.a * m_pPos->x - m_Plane.b * m_pPos->y - m_Plane.d) / m_Plane.c;
}

CPlaneCol* CPlaneCol::Create()
{
	return new CPlaneCol;
}

Engine::CCollision* CPlaneCol::Clone()
{
	(*m_pwRefCnt);
	return new CPlaneCol(*this);
}
