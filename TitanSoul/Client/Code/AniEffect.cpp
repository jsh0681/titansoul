#include "stdafx.h"
#include "AniEffect.h"

#include "CameraObserver.h"
#include "TexAni.h"

CAniEffect::CAniEffect(LPDIRECT3DDEVICE9 pGraphicDev) :
	CNormalEffect(pGraphicDev)
{
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement = Engine::Get_Management();
	m_pTimeMgr = Engine::Get_TimeMgr();
}

CAniEffect::~CAniEffect()
{
	Release();
}

HRESULT CAniEffect::Initialize(const NorEffect& tNorEffect, const wstring& wstrBuffer, const wstring& wstrAni)
{
	if (GetRandom<int>(0, 1) == 0)
		m_bAngleDir = false;
	else
		m_bAngleDir = true;

	m_wstrBufferKey = wstrBuffer;
	m_wstrTextureKey = wstrAni;
	memcpy(&m_tEffect, &tNorEffect, sizeof(NorEffect));

	m_iIndex = 0;

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vDir = m_tEffect.vDir;
	m_pInfo->m_vPos = m_tEffect.vPos;

	m_vAccel = -m_pInfo->m_vDir;

	m_fScale = m_tEffect.fScale;
	m_pInfo->m_vScale = { m_tEffect.fScale, m_tEffect.fScale, m_tEffect.fScale };

	m_fLifeTime = m_tEffect.fLifeTime;
	m_fSpeed = m_tEffect.fSpd;
	return S_OK;
}

HRESULT CAniEffect::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferKey);
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);

	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrTextureKey), m_wstrBufferKey);
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

void CAniEffect::Release(void)
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CAniEffect::SetTransform()
{
	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);
}

void CAniEffect::Update()
{
	float fTime = m_pTimeMgr->GetTime();

	m_fLifeTime -= fTime;
	if (m_fLifeTime <= 0.f)
	{
		m_bIsDead = TRUE;
		return;
	}

	switch (m_eOption)
	{
	case CAniEffect::UP:
		Up(fTime);
		break;
	case CAniEffect::UPDOWN:
		UpDown(fTime);
		break;
	}

	if (m_bAngleDir)
		m_fDegree += 0.5f;
	else
		m_fDegree -= 0.5f;

	m_pInfo->m_fAngle[1] = D3DXToRadian(m_fDegree);

	CGameObject::Update();
	SetTransform();
}

void CAniEffect::Render()
{
	m_pTexAni->Render();
}

void CAniEffect::ResetEffect()
{
	m_pInfo->m_vDir = m_tEffect.vDir;
	m_pInfo->m_vPos = m_tEffect.vPos;

	m_vAccel = -m_pInfo->m_vDir;

	m_fScale = m_tEffect.fScale;
	m_pInfo->m_vScale = { m_tEffect.fScale, m_tEffect.fScale, m_tEffect.fScale };

	m_fLifeTime = m_tEffect.fLifeTime;
	m_fSpeed = m_tEffect.fSpd;

	m_bIsDead = false;
	m_bAcc = false;
}

void CAniEffect::InitVertex()
{
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pVertex);
}

void CAniEffect::Up(const float& fDelata)
{
	m_fScale += fDelata * m_tEffect.fDisappearSpd;
	m_pInfo->m_vScale = { m_fScale, m_fScale ,m_fScale };

	m_pInfo->m_vPos += m_pInfo->m_vDir * fDelata * m_fSpeed;
}

void CAniEffect::UpDown(const float& fDelata)
{
	if (m_bAcc)
	{
		m_pInfo->m_vPos += m_pInfo->m_vDir * fDelata* m_fSpeed;
		m_fSpeed += fDelata * m_fSpeed2;

		m_fScale -= fDelata * m_tEffect.fDisappearSpd;
		if (m_fScale < 0.f)
			m_fScale = 0.00f;
		m_pInfo->m_vScale = { m_fScale, m_fScale ,m_fScale };
	}
	else
	{
		m_pInfo->m_vDir += m_vAccel * fDelata;

		m_fScale += fDelata * m_tEffect.fDisappearSpd;
		m_pInfo->m_vScale = { m_fScale, m_fScale ,m_fScale };

		D3DXVECTOR3 vTemp = m_pInfo->m_vDir - m_vAccel;
		float fLength = D3DXVec3Length(&vTemp);
		if (fLength < 1.1f)
		{
			m_pInfo->m_vDir = m_tEffect.vAcc;
			m_fSpeed2 = m_fSpeed;
			m_fSpeed = 0.f;
			m_bAcc = TRUE;
		}
		else
			m_pInfo->m_vPos += m_pInfo->m_vDir * fDelata * m_fSpeed;
	}
}

CAniEffect* CAniEffect::Create(LPDIRECT3DDEVICE9 pGraphicDev, const NorEffect& tNorEffect, const wstring& wstrBuffer, const wstring& wstrAni)
{
	CAniEffect* pInstance = new CAniEffect(pGraphicDev);
	if (FAILED(pInstance->Initialize(tNorEffect, wstrBuffer, wstrAni)))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}