#include "stdafx.h"
#include "LogoArrow.h"

#include "Export_Function.h"
#include "Include.h"

#include "CameraObserver.h"

CLogoArrow::CLogoArrow(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev),
	m_pResourcesMgr(Engine::Get_ResourceMgr()),
	m_pTimeMgr(Engine::Get_TimeMgr()),
	m_pInfoSubject(Engine::Get_InfoSubject())
{
}


CLogoArrow::~CLogoArrow()
{
	Release();
}

HRESULT CLogoArrow::AddComponent(void)
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_LogoArrow");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Texture_LogoArrow");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTextureCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTextureCom);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

HRESULT CLogoArrow::Initialize(const int& ArrowCnt, const D3DXVECTOR3& vPos)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_LogoArrow", m_pVertex);

	m_iArrowCnt = ArrowCnt;
	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);


	m_pInfo->m_vScale = { 8.f, 8.f,1.f };
	m_pInfo->m_vPos = vPos;

	return S_OK;
}

void CLogoArrow::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CLogoArrow::Update()
{
	CGameObject::Update();
	SetTransform();
}

void CLogoArrow::Render()
{
	// 알파블랜딩
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_LogoArrow", m_pConvertVertex);
	m_pTextureCom->Render(m_iArrowCnt);
	m_pBufferCom->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CLogoArrow::SetTransform()
{
	D3DXMATRIX matView;
	const D3DXMATRIX* matOrtho = m_pCameraObserver->GetOrtho();

	D3DXMatrixIdentity(&matView);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &matView);
		if (m_pConvertVertex[i].vPos.z < 1.f)
			m_pConvertVertex[i].vPos.z = 1.f;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, matOrtho);
	}
}

void CLogoArrow::SetY(const float& fY)
{
	m_pInfo->m_vPos.y = fY;
}


CLogoArrow* CLogoArrow::Create(LPDIRECT3DDEVICE9 pGraphicDev, const int& ArrowCnt, const D3DXVECTOR3& vPos)
{
	CLogoArrow* pInstance = new CLogoArrow(pGraphicDev);

	if (FAILED(pInstance->Initialize(ArrowCnt, vPos)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
