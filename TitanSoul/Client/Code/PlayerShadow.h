#ifndef PlayerShadow_h__
#define PlayerShadow_h__

#include "Export_Function.h"
#include "GameObject.h"
#include "CameraObserver.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
}


class CPlayerShadow : public Engine::CGameObject
{
private:
	explicit CPlayerShadow(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CPlayerShadow();

public:
	virtual void	Update(void);
	virtual void	Render(void);

public:
	void SetXZ(float fPosX, float fPosZ) {
		m_pInfo->m_vPos.x = fPosX, m_pInfo->m_vPos.z = fPosZ;
	}

private:
	void Release(void);
	virtual HRESULT AddComponent(D3DXVECTOR3 vStartPos);
	virtual HRESULT Initialize(D3DXVECTOR3 vStartPos);
	virtual void SetDirection(void);
	virtual void SetTransform(void);

private:
	CCameraObserver* m_pCameraObserver = nullptr;
	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;

private:
	DWORD						m_dwVtxCnt = 0;
	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;
public:
	static CPlayerShadow*		Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos);
};
#endif // PlayerShadow_h__
