#ifndef TalkBollon_h__
#define TalkBollon_h__

#include "GameObject.h"


/*!
 * \class CTalkBallon
 *
 * \간략정보 말풍선
 *
 * \상세정보 
 *
 * \작성자 윤유석
 *
 * \date 1월 18 2019
 *
 */



namespace Engine
{
	class CVIBuffer;
	class CTexture;
}

class CCameraObserver;
class CTalkBallon : public Engine::CGameObject
{
private:
	explicit CTalkBallon(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CTalkBallon();

public:
	virtual void Update();
	virtual void Render();

private:
	HRESULT	AddComponent();
	virtual HRESULT Initialize(VEC3& vPos, VEC3& vSize, const int& iIndex);
	virtual void	Release();

private:
	void			InitVertex();
	void			SetTransform();

public:
	void			SetIndex(const int& iIndex) { m_iIndex = iIndex; }
	void			SetPos(const VEC3& vPos);
	void			SetScale(const VEC3 vScale);
	void			SetOnOff(const bool& bOn) { m_bOn = bOn; }

private:
	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;

	Engine::VTXTEX*			m_pVertex = nullptr;
	Engine::VTXTEX*			m_pConvertVertex = nullptr;

	CCameraObserver*		m_pCameraObserver;

	DWORD					m_dwVtxCnt;
	int						m_iIndex;

	wstring					m_wstrImgName;
	bool					m_bOn = false;

public:
	static CTalkBallon*		Create(LPDIRECT3DDEVICE9 pGraphicDev, VEC3& vPos, VEC3& vSize, const int& iIndex = 0);
};

#endif // !TalkBollon_h__
