#ifndef Knuckle_Mace_h__
#define Knuckle_Mace_h__


#include "Knuckle_Part.h"


class CKnuckle_Mace : public CKnuckle_Part
{
	enum MACE_DIR { M_LEFT, M_RIGHT, M_END };
private:
	explicit CKnuckle_Mace(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CKnuckle_Mace();

private:
	virtual HRESULT	Initialize(const VEC3& vPos, const int& iDir);
	virtual HRESULT AddComponent() override;
	virtual void	SetTransform() override;
	virtual void	Release();

public:
	virtual void	Update();
	virtual void	Render();

public:
	void	SetLength(const float& fLength)		{ m_fLength = fLength; }
	void	SetAttack(const bool& bAttack)		{ m_bAttack = bAttack; }
	void	SetBodyPos(const VEC3& vPos)		{ m_vBodyPos = vPos; }

	void	ResetAccel()						{ m_fAccel = 1.f; }
	D3DXMATRIX*	GetWorldMat();
	const float&	GetRadius()					{ return m_fRadius; }

private:
	virtual void		Body_Rot();
	virtual void		AttackFunc();
	virtual void		AttackRotation();
	virtual void		AttackJump();

private:
	MACE_DIR		m_eMaceDir;
	float			m_fLength = 0.f;
	bool			m_bAttack = false;
	bool			m_bAttackEnd = false;
	VEC3			m_vBodyPos;
	float			m_fAccel = 0.f;
	bool			m_bUp = true;
	float			m_fRadius = 0.9f;

public:
	static CKnuckle_Mace*	Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const int& iDir);
};
#endif // !Knuckle_Mace_h__