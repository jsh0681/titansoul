#include "stdafx.h"
#include "Knuckle_Body.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"

CKnuckle_Body::CKnuckle_Body(LPDIRECT3DDEVICE9 pGraphicDev) :
	CKnuckle_Part(pGraphicDev)
{
}


CKnuckle_Body::~CKnuckle_Body()
{
	Release();
}

HRESULT CKnuckle_Body::Initialize(const VEC3& vPos, const int& iIndex)
{
	m_dwVtxCnt = 4;
	m_eDir = (BODY_DIRECTION)iIndex;
	m_wstrBufferName = L"Buffer_Knuckle_Body";

	m_wstrDir[DD] = L"KnuckleBodyDD";
	m_wstrDir[DL] = L"KnuckleBodyDL";
	m_wstrDir[DR] = L"KnuckleBodyDR";
	m_wstrDir[LL] = L"KnuckleBodyLL";
	m_wstrDir[RR] = L"KnuckleBodyRR";
	m_wstrDir[UL] = L"KnuckleBodyUL";
	m_wstrDir[UR] = L"KnuckleBodyUR";
	m_wstrDir[UU] = L"KnuckleBodyUU";

	// 부모 release에서 지워줌(카메라, 버텍스, 컴포넌트)
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = vPos;
	m_pInfo->m_vScale = { 1.8f, 1.f, 1.8f };

	ResetBuffer();

	return S_OK;
}

HRESULT CKnuckle_Body::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferName);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	// 텍스처를 한번에 받아둠(방향마다)
	for (size_t i = 0; i < D_END; ++i)
	{
		m_pTextureArr[i] = dynamic_cast<Engine::CTexture*>(m_pResourcesMgr->Clone(
			Engine::RESOURCE_STATIC, m_wstrDir[i]));
	}
	pComponent = m_pTexture = m_pTextureArr[m_eDir];
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

void CKnuckle_Body::SetTransform()
{
	const D3DXMATRIX* matView = m_pCameraObserver->GetView();
	const D3DXMATRIX* matProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, matView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, matProj);
}

void CKnuckle_Body::Update()
{
	switch (m_eState)
	{
	case CKnuckle_Part::WAKE_UP:
		Walk_UpFunc();
		break;
	case CKnuckle_Part::CHECKBACK:
		break;
	case CKnuckle_Part::ATTACK:
		break;
	case CKnuckle_Part::BODY_ROT:
		Body_Rot();
		break;
	case CKnuckle_Part::ATTACK_ROT:
		AttackRotation();
		break;
	case CKnuckle_Part::ATTACK_JUMP:
		AttackJump();
		break;
	}
	CKnuckle_Part::Update();
	SetTransform();
}

void CKnuckle_Body::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pConvertVertex);

	m_pTexture->Render(0);
	m_pBuffer->Render();
}

void CKnuckle_Body::SetDirection(const int& iDir)
{
	m_eDir = (BODY_DIRECTION)iDir;
	m_pTexture = m_pTextureArr[m_eDir];
}

void CKnuckle_Body::Walk_UpFunc()
{
	m_pInfo->m_vPos += m_pInfo->m_vDir * m_fDeltaTime * 1.3f;
}

void CKnuckle_Body::Body_Rot()
{
	m_pInfo->m_vDir = m_vTargetPos - m_pInfo->m_vPos;

	float fLength = D3DXVec3Length(&m_pInfo->m_vDir);
	if (fLength < 3.f)
		return;

	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
	m_pInfo->m_vPos += m_pInfo->m_vDir * m_fDeltaTime * fLength * 3.f;
}

void CKnuckle_Body::AttackRotation()
{
	m_pInfo->m_vDir = m_vTargetPos - m_pInfo->m_vPos;
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
	m_pInfo->m_vPos += m_pInfo->m_vDir * m_fDeltaTime * 4.f;
}

void CKnuckle_Body::AttackJump()
{
	m_fBehavioTime += m_fDeltaTime;
	if (m_fBehavioTime < (m_fEndTime * 0.6f))
	{
		m_pInfo->m_vPos.y += 3.2f * m_fDeltaTime * 1.2f;
		m_pInfo->m_vPos.z += 2.f * m_fDeltaTime * 1.3f;
	}
	else if (m_fBehavioTime < (m_fEndTime * 0.8f))
	{
		m_pInfo->m_vPos.y -= 3.2f * m_fDeltaTime * 2.3f;
		m_pInfo->m_vPos.z -= 2.f * m_fDeltaTime * 1.6f;

		if (m_pInfo->m_vPos.y < 0.1f)
			m_pInfo->m_vPos.y = 0.1f;
	}
	else
	{
		m_pInfo->m_vPos.y -= 3.2f * m_fDeltaTime * 6.2f;
		m_pInfo->m_vPos.z -= 2.f * m_fDeltaTime * 1.6f;


		if (m_pInfo->m_vPos.y < 0.1f)
			m_pInfo->m_vPos.y = 0.1f;
	}
}

void CKnuckle_Body::Release()
{
	ResetBuffer();

	m_pTexture = m_pTextureArr[DD];
	for (size_t i = DL; i < D_END; ++i)
		Engine::Safe_Delete(m_pTextureArr[i]);
}

CKnuckle_Body* CKnuckle_Body::Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const int& iIndex/*방향*/)
{
	CKnuckle_Body* pInstance = new CKnuckle_Body(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos, iIndex)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}