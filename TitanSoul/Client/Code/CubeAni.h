#ifndef CubeAni_h__
#define CubeAni_h__

/*!
* \class CTexAni
*
* \간략정보 Component->Animation 을 상속받은 객체.
*
* \상세정보 큐브에 애니메이션을 출력 할 때 사용함.
*
* \작성자 윤유석
*
* \date 1월 3 2019
*
*/

#include "Animation.h"

class CCubeAni : public CAnimation
{
private:
	explicit CCubeAni(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CCubeAni();
	virtual void Release();

	Engine::VTXCUBE* GetVertex() { return m_pVertex; };
	Engine::VTXCUBE* GetConvertVertext() { return m_pConvertVertex; }
private:
	HRESULT	Initialize(const vector<TEXINFO*>* pVecTexInfo, const wstring& wstrBufferKey);

public:
	virtual void Render();
	virtual void SetTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)		override;
	virtual void SetRatioTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)	override;

private:
	Engine::VTXCUBE*		m_pVertex = nullptr;
	Engine::VTXCUBE*		m_pConvertVertex = nullptr;

public:
	static CCubeAni*	Create(LPDIRECT3DDEVICE9 pGraphicDev, const vector<TEXINFO*>* pVecTexInfo, const wstring& wstrBufferKey);
};

#endif // !CubeAni_h__