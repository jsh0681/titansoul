#ifndef Yeti_Idle_h__
#define Yeti_Idle_h__

#include "YetiState.h"
class CYeti;
class CYeti_Idle
	: public CYetiState
{
public:
	CYeti_Idle(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
	virtual ~CYeti_Idle();

public:
	virtual HRESULT Initialize();
	virtual void Update(const float& fTime);
	virtual void Release();

public:
	static CYeti_Idle*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);

};

#endif // Yeti_Idle_h__
