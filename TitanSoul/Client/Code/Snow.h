#ifndef Snow_h__
#define Snow_h__
#include"ParticleSystem.h"
#include "Export_Function.h"
#include"CameraObserver.h"

namespace Engine
{
	class CInfoSubject;
	
};


class Snow : public Engine::ParticleSystem
{
	
public:
	Snow(PARTICLEINFO* pParticleInfo);
	Snow(Engine::tagBoundingBox* boundingBox, int numParticles);
	void ResetParticle(Engine::tagAttribute* attribute);
	void Update(float timeDelta);
private:
	PARTICLEINFO* m_pParticleInfo = nullptr;
};
#endif // Snow_h__
