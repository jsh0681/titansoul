#ifndef Sludge_h__
#define Sludge_h__

/*!
 * \class CSludge
 *
 * \간략정보 슬라임 몬스터
 *
 * \상세정보			자기가 가지고 있을 여러 부품들을 관리하는 객체(본체임)
						받아온 레이어에 자신이 생성 될떄마다 자신을 추가해줌, Stage0에서 해주지 않음.
						받아온 list에 자신을 추가해줌. 이 리스트에도 자신을 추가해줌.
						static List들은 씬에서 모두 지워줌
 *
 * \작성자 윤유석
 *
 * \date 1월 8 2019
 *
 */

#include "GameObject.h"

namespace Engine
{
	class CLayer;
	class CTimeMgr;
	class CTransform;
	class CCamera;
}

class CSludgePart;
class CPlayerObserver;
class CSphereColBox;
class CMucus;
class CTrace;
class CStaticCamera2D;
class CSludge : public Engine::CGameObject
{
	enum SL_SIZE { SL_VERYSMALL = 1, SL_QUARTER, SL_HALF, SL_NORMAL, SL_BIG, SL_HEART, SL_END };
	enum SL_STATE { SL_IDLE, SLS_JUMP, SLS_FALL, SLS_SPLIT };
	enum SL_OBJ { SHADOW, HEART, BODY, SLO_END };
private:
	explicit CSludge(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CSludge();

private:
	HRESULT AddComponent();
	HRESULT CreateObj();
	virtual HRESULT Initialize(const bool& bTrue, const SL_SIZE& eStat, 
								const D3DXVECTOR3& vPos, Engine::CLayer* pUpdateLayer, 
								Engine::CCamera* pCamera, Engine::CLayer* pRenderLayer, 
								list<CSludge*>* pSludgeLst);
	virtual void Release();

public:
	virtual void Update();
	virtual void Render();

public:
	Engine::CTransform* GetInfo()			{ return m_pInfo; }
	int		GetSize()						{ return m_eSize; }
	int		GetState()						{ return m_eState; }

	void	SplitSludge();
	void	CheckPlaneCol(const bool& bCheck) { m_bPlaneCol = bCheck; }

	const float&		GetRadius();
	const bool&			GetHit()	{	return m_bHit;	};
	const bool&			GetTrue()	{ return m_bTrue; }

	const float&		GetBodyY();

private:
	void	BehavioFunc();
	void	BehavioEndFunc();
	void	Traking();
	void	SplitResizeAndCreate();
	void	SetRandomDir();
	void	HitTimeCounting();

	void	ShakingCamera();

	void	SludgeMove(const float& fDelta);

	void	CreateMucus();
	void	CreateTrace();

private:
	Engine::CLayer*				m_pUpdateLayer = nullptr;
	Engine::CLayer*				m_pRenderLayer = nullptr;
	Engine::CTimeMgr*			m_pTimeMgr;
	const Engine::CTransform*	m_pTargetTrasform;

	list<CSludge*>*			m_pSludgeList = nullptr;
	CStaticCamera2D*		m_pCamera = nullptr;

	bool		m_bTrue		= true;
	SL_SIZE		m_eSize		= SL_BIG;
	SL_STATE	m_eState	= SL_IDLE;
	CPlayerObserver*		m_pPlayerObserver;
	CSphereColBox*			m_pSphereBox = nullptr;

	CSludgePart*	m_pPart[SLO_END];


	const float& m_fUpDownTime = 0.4f;

	const float& m_fMaxIdleTime = 1.f;

	const float& m_fMaxJumpTime = 0.5f;
	const float& m_fMaxFallTime = 0.6f;

	const float& m_fSplitJumpTime = 0.4f;
	const float& m_fSplitFallTime = 0.25f;

	const float& m_fMaxHitTime = 0.8f;

	float		m_fBehavioTime	= 0.f;
	float		m_fSpeed		= 0.f;
	
	//bool		m_bFirstFrame = false;
	bool		m_BehavioEnd	= false;
	bool		m_bSplitUp		= true;

	bool		m_bHit = false;
	float		m_iHitTime = 0.f;
	bool		m_bPlaneCol = false;

public:
	static CSludge*	Create(LPDIRECT3DDEVICE9 pGraphicDev, const D3DXVECTOR3& vPos, 
							Engine::CLayer* pUpdateLayer, Engine::CLayer* pRenderLayer, 
							list<CSludge*>* pSludgeLst, Engine::CCamera* pCamera, 
							const bool& bTrue = true, const SL_SIZE& eSize = SL_BIG);

	static bool		m_sbSludgeDie;			// 완전히 죽었을 경우 사용함.
	static list<CMucus*>	m_MucusLst;
	static list<CTrace*>	m_TraceLst;
};

#endif // !Sludge_h__