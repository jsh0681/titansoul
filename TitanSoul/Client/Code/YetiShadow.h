#pragma once
#ifndef YetiShadow_h__
#define YetiShadow_h__

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
}
class CYeti;
class CCameraObserver;
class CYetiShadow
	: public Engine::CGameObject
{
public:
	CYetiShadow(LPDIRECT3DDEVICE9 pGraphicDev);
	virtual ~CYetiShadow();

	HRESULT Initialize(CYeti* pYeti);
	void	Update();
	void	Render();
	HRESULT	AddComponent();
	void	SetTransform();
	void	Release();

private:
	CYeti*						m_pYeti = nullptr;
	CCameraObserver*			m_pCameraObserver = nullptr;

	Engine::CVIBuffer*				m_pBufferCom = nullptr;
	Engine::CTexture*				m_pTextureCom = nullptr;
	Engine::CResourcesMgr*			m_pResourcesMgr = nullptr;
	Engine::CInfoSubject*			m_pInfoSubject = nullptr;

	Engine::VTXTEX*					m_pVertex = nullptr;
	Engine::VTXTEX*					m_pConvertVertex = nullptr;

	DWORD							m_dwVtxCnt = 4;


public:
	static CYetiShadow*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
};


#endif

