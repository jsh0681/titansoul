#ifndef Monster_h__
#define Monster_h__

#include "GameObject.h"
#include "CameraObserver.h"
#include "CollisionMgr.h"
#include "TerrainColl.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
	class CResourcesMgr;
	class CInfoSubject;
	class CManagement;
	
}

class CMonster : public Engine::CGameObject
{
private:
	explicit CMonster(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CMonster();

public:
	void		SetPos(D3DXVECTOR3& vPos);;

public:
	void		Update(void);
	void		Render(void);

private:
	HRESULT		AddComponent(void);
	HRESULT		Initialize(void);
	void		SetDirection(void);
	void		Release(void);
	void		SetTransform(void);

private:
	Engine::CTransform*				m_pInfo			= nullptr;
	Engine::CVIBuffer*				m_pBufferCom	= nullptr;
	Engine::CTexture*				m_pTextureCom	= nullptr;
	Engine::CResourcesMgr*			m_pResourcesMgr = nullptr;
	Engine::CInfoSubject*			m_pInfoSubject = nullptr;
	Engine::CManagement*			m_pManagement = nullptr;

	CCameraObserver*				m_pCameraObserver = nullptr;

	Engine::VTXTEX*					m_pVertex = nullptr;
	Engine::VTXTEX*					m_pConvertVertex = nullptr;

	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *m_pTerrainCol = nullptr;
	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;


	DWORD							m_dwVtxCnt;

public:
	static CMonster*		Create(LPDIRECT3DDEVICE9 pGraphicDev);



};



#endif // Monster_h__
