#include "stdafx.h"
#include "Map0.h"

#include "Player.h"
#include "Include.h"
#include "Export_Function.h"
#include "SceneSelector.h"
#include "CollisionMgr.h"
#include "Arrow.h"
#include"Goliath.h"
#include"ShpereCol.h"
#include "StaticCamera2D.h"
#include "OrthoCamera.h"
#include"Snow.h"
#include "CeremonyEffect.h"
#include "CeremoneyEffect_List.h"
#include "FireWork.h"
#include "Corver.h"
#include "Cat.h"
#include "Fish.h"

#include "Tutorial.h"
#include "Display.h"
#include "NonAniEffect.h"

bool CMap0::m_sbMap0_Resource = false;

CMap0::CMap0(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CScene(pGraphicDev)
	, m_pManagement(Engine::Get_Management())
	, m_pResourcesMgr(Engine::Get_ResourceMgr())
{
	m_pDisplay = CDisplay::Create(m_pGraphicDev);
	m_pDisplay->StartFade(CDisplay::FADEIN);
}

CMap0::~CMap0(void)
{
	Release();
}

HRESULT CMap0::Initialize(void)
{
	AddResources();

	m_pShpereCol = dynamic_cast<CShpereCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_SHPERE));

	FAILED_CHECK_MSG(Add_Environment_Layer(), L"Environment 초기화 실패");
	FAILED_CHECK_MSG(Add_GameObject_Layer(), L"GameObject 초기화 실패");
	FAILED_CHECK_MSG(Add_UI_Layer(), L"UI 초기화 실패");
	

	CSoundMgr::GetInstance()->PlayBGM(L"Map0.ogg");


	//PARTICLEINFO* pParticleInfo = const_cast<PARTICLEINFO*>(CParticleMgr::GetInstance()->GetParticleInfoData(L"ParticleFireworkNo"));

	//D3DXVECTOR3 origin(110.0f, 10.0f, 160.0f);
	//m_pParticle = new Firework(&origin , pParticleInfo);
	//m_pParticle->Initialize(m_pGraphicDev, L"../Bin/Resources/Texture/Cube/water.bmp");

	
	D3DXVECTOR3 origin(110.0f, 10.0f, 160.0f);
	//m_pParticle = new Firework(&origin,10);
	//m_pParticle->Initialize(m_pGraphicDev, L"../Bin/Resources/Texture/Cube/water.bmp");

	return S_OK;
}

HRESULT CMap0::Initialize(CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	m_pPlayer = pPlayer;
	m_pCamera = pCamera;

	m_pPlayer->Get_Trnasform()->m_vPos = m_pPlayer->GetSavePos();
	dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();

	m_pShpereCol = dynamic_cast<CShpereCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_SHPERE));

	FAILED_CHECK_MSG(Add_Environment_Layer(), L"Environment 초기화 실패");
	FAILED_CHECK_MSG(Add_GameObject_Layer(), L"GameObject 초기화 실패");
	FAILED_CHECK_MSG(Add_UI_Layer(), L"UI 초기화 실패");

	CSoundMgr::GetInstance()->PlayBGM(L"Map0.ogg");
	return S_OK;
}

void CMap0::Update(void)
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	Engine::CScene::Update();

	int iTriger = m_pTerrain->CollisionPlane(m_pPlayer);
	m_pTerrain->CollisoinPlanetoArrowBackAgain(m_pPlayer->Get_ArrowPointer(), m_pPlayer);
	m_pPlayer->Get_ArrowPointer()->Set_PressingRecovery();
	ColPlayerMonster(fTime);
	ColMonsterArrow(fTime);
	RemoveEffect();

	m_pDisplay->Update();

	if (iTriger != 0)
	{
		Triger(iTriger);
	}
}

void CMap0::Render(void)
{
	for (auto& it : m_mapRenderLayer[Engine::LAYER_NONALPHA])
		it.second->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000040);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	for (auto& it : m_mapRenderLayer[Engine::LAYER_ALPHA])
		it.second->Render();

	m_pDisplay->Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

void CMap0::Release(void)
{
	Engine::Safe_Delete(m_pDisplay);
	Engine::Safe_Delete(m_pShpereCol);
	if (m_bChangeScene)
	{
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"Player");
		m_mapLayer[Engine::LAYER_UI]->ClearList(L"Camera");
	}
}

void CMap0::RemoveEffect(void)
{
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->end();

	CNormalEffect* pEffect = nullptr;

	list<Engine::CGameObject*>* pRenderList = m_mapRenderLayer[Engine::LAYER_ALPHA][Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pEffect = dynamic_cast<CNormalEffect*>(*iter);
		if (pEffect == nullptr) return;

		if (pEffect->CheckIsDead())
		{
			//죽여
			auto& it_find = find_if(pRenderList->begin(), pRenderList->end(), [pEffect](Engine::CGameObject* pObj)
			{
				if (pObj == pEffect)
					return true;
				return false;
			});

			if (it_find != pRenderList->end())
				pRenderList->erase(it_find);

			Engine::Safe_Delete((*iter));
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->erase(iter);
		}
		else
			++iter;
	}
}

void CMap0::FireArrowEffect(const D3DXVECTOR3 & vPos)
{
	NorEffect Temp;
	Temp.vPos = vPos;
	Temp.vPos.x += GetRandom<float>(-0.1f, 0.1f);
	Temp.vDir = D3DXVECTOR3(0.f, 0.f, 1.f);
	Temp.vAcc = D3DXVECTOR3(0.f, 0.f, 0.f);
	Temp.fSpd = 1.f;
	Temp.fScale = 0.5f;
	Temp.fLifeTime = 2.f;
	Temp.fDisappearSpd = 0.2f;

	m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_Effect", L"EffectFireArrowEffectNO", 8));
}

void CMap0::Triger(int _iID)
{
	D3DXVECTOR3 vTemp;
	switch (_iID)
	{
	case PL_EXIT:
		break;

	case PL_NEXT:
		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}
		if (m_pDisplay->CheckFadeFinish())
		{
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			vTemp = { 110.f, 0.1f, 119.0f };
			m_pPlayer->SavePos(vTemp);
			m_pPlayer->SetDirDown();
			m_pManagement->SceneChange(CSceneSelector(SC_MAP1, m_pPlayer, m_pCamera));
		}
		break;

	case PL_BOSS1: //라임슬
		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}
		if (m_pDisplay->CheckFadeFinish())
		{
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			vTemp = { 110.f, 0.1f, 119.0f };
			m_pPlayer->SavePos(vTemp);
			m_pPlayer->SetDirUp();
			m_pManagement->SceneChange(CSceneSelector(SC_STAGE0, m_pPlayer, m_pCamera));
		}
		break;

	case PL_BOSS2: // 아이큐브
		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}
		if (m_pDisplay->CheckFadeFinish())
		{
			vTemp = { 94.f, 0.1f, 127.8f };
			m_pPlayer->SavePos(vTemp);
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			m_pPlayer->SetDirUp();
			m_pManagement->SceneChange(CSceneSelector(SC_STAGE1, m_pPlayer, m_pCamera));
		}
		break;

	case PL_BOSS3: //철퇴보이
		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);

		}
		if (m_pDisplay->CheckFadeFinish())
		{
			m_bChangeScene = true;
			vTemp = { 126.f, 0.1f, 128.0f };
			m_pPlayer->SavePos(vTemp);
			m_pPlayer->CameraRelease();
			m_pPlayer->SetDirUp();
			m_pManagement->SceneChange(CSceneSelector(SC_STAGE2, m_pPlayer, m_pCamera));
		}
		break;
	}
}

HRESULT CMap0::AddResources(void)
{
	if (m_sbMap0_Resource) return S_OK;

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Effect", 10, 10),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Player", 5, 5),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Arrow", 5, 5),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_ArrowEffect", 30, 30),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX,
		L"Buffer_Goliath", 36, 36),
		E_FAIL);


	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_GoliathLHand", 18, 18),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_GoliathRHand", 18, 18),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_GoliathLShadow", 18, 18),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_GoliathRShadow", 18, 18),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Tutorial"),
		E_FAIL);


	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Cat"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_Fish"),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::BUFFER_RCTEX_Z,
		L"Buffer_PlayerShadow",5,5),
		E_FAIL);

	FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
		Engine::RESOURCE_STATIC,
		Engine::TEX_NORMAL,
		L"Texture_PlayerShadow",
		L"../Bin/Resources/Texture/Tex/Shadow/Player/Body/%d.png",
		1),
		E_FAIL);



	m_sbMap0_Resource = TRUE;
	return S_OK;
}

HRESULT CMap0::Add_Environment_Layer(void)
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CGameObject*		pGameObject = nullptr;

	m_mapLayer.emplace(Engine::LAYER_ENVIRONMENT, pLayer);

	return S_OK;
}

HRESULT CMap0::Add_GameObject_Layer(void)
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);


	Engine::CGameObject*		pGameObject = nullptr;

	if (m_pPlayer == nullptr)
	{
		pGameObject = m_pPlayer = CPlayer::Create(m_pGraphicDev);
		NULL_CHECK_RETURN(pGameObject, E_FAIL);
		pLayer->AddObject(L"Player", pGameObject);
		pBlendLayer->AddObject(L"Player", pGameObject);
		dynamic_cast<CPlayer*>(pGameObject)->SetScene(this);

		CArrow* pArrow = m_pArrow = dynamic_cast<CPlayer*>(pGameObject)->Get_ArrowPointer();
		Engine::CGameObject* pObject = dynamic_cast<Engine::CGameObject*>(pArrow);
		pArrow->SetScene(this);
		pLayer->AddObject(L"Arrow", pObject);
		pBlendLayer->AddObject(L"Arrow", pObject);
	}
	else
	{
		pLayer->AddObject(L"Player", m_pPlayer);
		pBlendLayer->AddObject(L"Player", m_pPlayer);
		m_pPlayer->SetScene(this);
		m_pPlayer->CameraCreate();
		m_pPlayer->CreateArrow();

		CArrow* pArrow = m_pArrow = m_pPlayer->Get_ArrowPointer();
		Engine::CGameObject* pObject = dynamic_cast<Engine::CGameObject*>(pArrow);
		pLayer->AddObject(L"Arrow", pObject);
		pBlendLayer->AddObject(L"Arrow", pObject);
	}

	pGameObject = m_pTerrain = CTerrainMgr::GetInstance()->Clone(L"MapTerrainMap0");
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Terrain", pGameObject);
	pNoBlendLayer->AddObject(L"Terrain", pGameObject);

	pGameObject = CCorver::Create(m_pGraphicDev, m_pTerrain, 2);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Corver", pGameObject);
	pBlendLayer->AddObject(L"Corver", pGameObject);


	pGameObject = m_pGoliath = CGoliath::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Goliath", pGameObject);
	pBlendLayer->AddObject(L"Goliath", pGameObject);

	m_pLHand = m_pGoliath->GetLHand();
	m_pRHand = m_pGoliath->GetRHand();

	pGameObject = CTutorial::Creare(m_pGraphicDev, { 110.f, 0.09f, 50.f }, m_pPlayer);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Tutorial", pGameObject);
	pBlendLayer->AddObject(L"Tutorial", pGameObject);



	pGameObject = CCat::Creare(m_pGraphicDev, { 108.05f, 0.09f, 115.226f }, m_pPlayer);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Cat", pGameObject);
	pBlendLayer->AddObject(L"Cat", pGameObject);

	if ((m_pPlayer->GetDBoss() & FISHBIT) == 0)
	{
		pGameObject = CFish::Creare(m_pGraphicDev, { 128.f, 0.09f, 100.f }, m_pPlayer);
		NULL_CHECK_RETURN(pGameObject, E_FAIL);
		pLayer->AddObject(L"Cat", pGameObject);
		pBlendLayer->AddObject(L"Cat", pGameObject);
	}


	pLayer->AddObject(L"Effect", pGameObject);
	pBlendLayer->AddObject(L"Effect", pGameObject);

	pLayer->ClearList(L"Effect");
	pBlendLayer->ClearList(L"Effect");

	m_mapLayer.emplace(Engine::LAYER_GAMEOBJECT, pLayer);
	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	return S_OK;
}

HRESULT CMap0::Add_UI_Layer(void)
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	Engine::CGameObject*		pGameObject = nullptr;

	auto	iter = m_mapLayer.find(Engine::LAYER_GAMEOBJECT);
	if (iter == m_mapLayer.end())
		return E_FAIL;

	const Engine::CTransform*		pTransform =  dynamic_cast<const Engine::CTransform*>
		(iter->second->GetComponent(L"Player", L"Transform"));
	NULL_CHECK_RETURN(pTransform, E_FAIL);

	if (m_pCamera == nullptr)
	{
		pGameObject = m_pCamera = CStaticCamera2D::Create(m_pGraphicDev, pTransform);
		NULL_CHECK_RETURN(pGameObject, E_FAIL);
		pLayer->AddObject(L"Camera", pGameObject);
		pNoBlendLayer->AddObject(L"Camera", pGameObject);
		m_pPlayer->SetCamera(dynamic_cast<CStaticCamera2D*>(m_pCamera));
	}
	else
	{
		pLayer->AddObject(L"Camera", m_pCamera);
		pNoBlendLayer->AddObject(L"Camera", pGameObject);
	}

	pGameObject = COrthoCamera::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"OrthoCamera", pGameObject);
	pNoBlendLayer->AddObject(L"OrthoCamera", pGameObject);

	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);

	m_pGoliath->SetCamera(m_pCamera);
	m_pLHand->SetCamera(m_pCamera);
	m_pRHand->SetCamera(m_pCamera);


	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);
	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_UI, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_UI, pBlendLayer);

	return S_OK;
}

CMap0* CMap0::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CMap0*	pInstance = new CMap0(pGraphicDev); 

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

CMap0* CMap0::Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	CMap0*	pInstance = new CMap0(pGraphicDev);

	if (FAILED(pInstance->Initialize(pPlayer, pCamera)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}


void CMap0::CeremonyYeah()
{
	NorEffect Temp;
	Temp.fDisappearSpd = 0.f;
	Temp.fLifeTime = 15.f;
	Temp.fScale = 2.f;
	Temp.fSpd = 1.0f;
	Temp.vAcc = m_pPlayer->Get_Trnasform()->m_vPos; //플레이엉위치넣고
	Temp.vPos = m_pPlayer->Get_ArrowPointer()->Get_Trnasform()->m_vPos;
	Temp.vPos.y = 0.2f;

	float fAngle = 0.f;
	float fRand = 0.f;

	list<Engine::CGameObject*>* pUpdateList = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	list<Engine::CGameObject*>* pRenderList = m_mapRenderLayer[Engine::LAYER_ALPHA][Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect");
	for (int i = 0; i < 14; ++i)
	{
		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;
		fRand = GetRandom<float>(1.f, 2.f);
		Temp.fSpd = fRand;

		Engine::CGameObject* pGameObject = CCeremoneyEffect_List::Create(m_pGraphicDev, Temp, L"Buffer_Effect", L"EffectCeremony2NO", m_pPlayer->Get_Trnasform()->m_matWorld, pUpdateList, pRenderList, m_pPlayer);
		pUpdateList->push_back(pGameObject);
		pRenderList->push_back(pGameObject);

		fRand = GetRandom<float>(0.f, 30.f);
		fAngle += 45.f + fRand;
	}
}

void CMap0::ColPlayerMonster(const float& fTime)
{
	CPlayer* pPlayer = m_pPlayer;
	CShpereCol* pShpereCol = m_pShpereCol;
	if (pShpereCol->CheckCollision(*m_pGoliath->GetWorldMatrix(), m_pGoliath->GetRadius(), pPlayer->Get_Trnasform()->m_vPos, pPlayer->GetRadius()))
	{
		pPlayer->ReverseMove(fTime);
		pPlayer->SetPush(1);
	};

	if (pShpereCol->CheckCollision(m_pLHand->GetInfo()->m_vPos, m_pLHand->GetRadius(), pPlayer->Get_Trnasform()->m_vPos, pPlayer->GetRadius()))
	{
		pPlayer->ReverseMove(fTime);
		pPlayer->SetPush(1);
		BloodMode();
	};

	if (pShpereCol->CheckCollision(m_pRHand->GetInfo()->m_vPos, m_pRHand->GetRadius(), pPlayer->Get_Trnasform()->m_vPos, pPlayer->GetRadius()))
	{
		pPlayer->ReverseMove(fTime);
		pPlayer->SetPush(1);
		BloodMode();
	};
}
void CMap0::BloodMode()
{
	m_pDisplay->StartBlood();
}


void CMap0::ColMonsterArrow(const float& fTime)
{
	CArrow* pArrow = m_pArrow;
	CShpereCol* pShpereCol = m_pShpereCol;
	if (pArrow->Get_ArrowState() == CArrow::A_SHOT || pArrow->Get_ArrowState() == CArrow::A_RECOVERY)
	{
		if (pShpereCol->CheckCollision(*m_pGoliath->GetWorldMatrix(), (float)m_pGoliath->GetRadius(), pArrow->Get_Trnasform()->m_vPos, pArrow->GetRadius()))
		{
			if (!m_pGoliath->GetDead() && m_pGoliath->GetAttack()&&i==0)
			{
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"Map0.ogg");
				i++;
				m_pGoliath->SetDead();
				g_BattleMode = FALSE;
				Engine::Get_TimeMgr()->SlowModeStart(5.f, 0.1f);
				DWORD temp = BOSSMAP0;
				m_pPlayer->SetDBoss(temp);
				CeremonyYeah();
				m_pLHand->SetRadius(0.f);
				m_pRHand->SetRadius(0.f);
			}
		}

		if (pShpereCol->CheckCollision(pArrow->Get_Trnasform()->m_vPos, pArrow->GetRadius(), m_pLHand->GetInfo()->m_vPos, (float)m_pLHand->GetRadius()))
		{
			if (!m_pGoliath->GetWake() && !m_pGoliath->GetAttack()&&j == 0)
			{
				j++;
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"Boss_Goliath.ogg");
				m_pGoliath->SetWake();
			}
			m_pArrow->StopArrow();
		}

		if (pShpereCol->CheckCollision(pArrow->Get_Trnasform()->m_vPos, pArrow->GetRadius(), m_pRHand->GetInfo()->m_vPos, (float)m_pRHand->GetRadius()))
		{
			if (!m_pGoliath->GetWake() && !m_pGoliath->GetAttack()&& j == 0)
			{
				j++;
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"Boss_Goliath.ogg");
				m_pGoliath->SetWake();
			}
			m_pArrow->StopArrow();
		}
	}
}
