#include "stdafx.h"
#include "IceCube.h"
#include "Export_Function.h"
#include "Include.h"
#include"CubeAni.h"
#include<cmath>
#include"Arrow.h"
#include"StaticCamera2D.h"
#include"IceCubeShadow.h"
#include "SphereColBox.h"

CIceCube::CIceCube(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{

}

CIceCube::~CIceCube(void)
{
	Release();
}


HRESULT CIceCube::Initialize()
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();
	m_pCollisionMgr = CCollisionMgr::GetInstance();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_wstrStateKey = L"BossIceCubeIdle";

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pIceCubeShadow = CIceCubeShadow::Create(m_pGraphicDev, m_pInfo->m_vPos);
	NULL_CHECK_RETURN(m_pIceCubeShadow, E_FAIL);

	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 3.5f, true);

	return S_OK;
}

HRESULT CIceCube::AddComponent(void)
{
	Engine::CComponent* pComponent = nullptr;

	m_pCubeAni = CCubeAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_IceCube");
	NULL_CHECK_RETURN(m_pCubeAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pCubeAni);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	m_pInfo->m_vPos = D3DXVECTOR3(30.0f, 1.0f, 30.9f);
	m_vBackPos = D3DXVECTOR3(30.0f, 1.0f, 30.9f);



	m_pVertex = m_pCubeAni->GetVertex();
	m_pConvertVertex = m_pCubeAni->GetConvertVertext();

	m_pMatWorld = &(m_pInfo->m_matWorld);
	m_pMatView = const_cast<D3DXMATRIX*>(m_pCameraObserver->GetView());
	m_pMatProj = const_cast<D3DXMATRIX*>(m_pCameraObserver->GetProj());
	return S_OK;
}


void CIceCube::Release()
{
	Engine::Safe_Delete(m_pSphereBox);
	Engine::Safe_Delete(m_pIceCubeShadow);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

}

void CIceCube::StopIceCube()
{
	m_bCalculate = true;
	m_iAttackPattern = 0;
}

void CIceCube::SetTransform()
{
	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pCubeAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);

}

float CIceCube::CalculateSpeed(float fDeltaTime)
{
	m_fSpeed *= m_fFriction;
	return m_fSpeed;
}

void CIceCube::Dash(float fDeltaTime)
{
	m_fDashTime += Engine::Get_TimeMgr()->GetTime();
	m_pInfo->m_vPos += m_pInfo->m_vDir * CalculateSpeed(fDeltaTime);
	if (m_fDashTime > 1.f)
	{
		m_iAttackPattern++;//1증가
		m_fDashTime = Engine::Get_TimeMgr()->GetTime();
		return;
	}
}
void CIceCube::Jump(float fDeltaTime)
{
}

void CIceCube::Move(float fDeltaTime)
{
	if (m_bCalculate)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"Boss_IceCube_Dash.ogg", CSoundMgr::SOUND_BOSS);
		m_iCollisionCount = 0;
		m_vPlayerPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;//플레이어 좌표를 받음
		m_vTempDir = m_vPlayerPos - m_pInfo->m_vPos;
		D3DXVec3Normalize(&m_vTempDir, &m_vTempDir);
		m_fSpeed = 29.f;
		m_bCalculate = false;
	}
	else
	{
		{
			switch (m_iAttackPattern)
			{
			case 0:
			case 1:
			case 2:
				m_fDashTime += Engine::Get_TimeMgr()->GetTime();
				m_pInfo->m_vPos.x += m_vTempDir.x*CalculateSpeed(fDeltaTime)*fDeltaTime;
				m_pInfo->m_vPos.z += m_vTempDir.z*CalculateSpeed(fDeltaTime)*fDeltaTime;
				m_pInfo->m_vPos.y = 1.0f;
				if (m_fDashTime > 4.5f)
				{
					m_iAttackPattern++;//1증가
					m_fDashTime = 0.f;
					m_bCalculate = true;
				}
				break;
			case 3:
				m_fJumpAcc += Engine::Get_TimeMgr()->GetTime();
				if (m_fJumpAcc < 0.03f)
					CSoundMgr::GetInstance()->PlaySoundw(L"Boss_IceCube_Jump.mp3", CSoundMgr::SOUND_BOSS);
				m_pInfo->m_vPos.x += m_vTempDir.x*5.f*fDeltaTime;
				m_pInfo->m_vPos.z += m_vTempDir.z*5.f*fDeltaTime;
				m_pInfo->m_vPos.y += 10.f*fDeltaTime;
				if (m_pInfo->m_vPos.y >= 10.f)
				{
					m_fJumpAcc = 0.f;
					m_iAttackPattern = 4;
				}
				break;
			case 4:
				m_pInfo->m_vPos.x += m_vTempDir.x*3.f*fDeltaTime;
				m_pInfo->m_vPos.z += m_vTempDir.z*3.f*fDeltaTime;
				m_pInfo->m_vPos.y -= (12.f*fDeltaTime);
				if (m_pInfo->m_vPos.y <= 1.0f)
				{
					m_pInfo->m_vPos.y = 1.0f;
					m_fJumpTime += Engine::Get_TimeMgr()->GetTime();
					m_bIsBottom = true;
					if (m_fJumpTime > 0.1f)
						m_bIsBottom = false;
					if (m_fJumpTime > 1.0f)
					{
						m_fJumpTime = 0.f;
						m_iAttackPattern = 0;
						m_bCalculate = true;
					}
				}
				break;
			}
		}
	}
	m_fAngleY += 10.f*fDeltaTime;

}
bool CIceCube::PositionLock()
{
	if (17.f < m_pInfo->m_vPos.x &&m_pInfo->m_vPos.x < 43.f &&
		16.5f < m_pInfo->m_vPos.z &&m_pInfo->m_vPos.z < 42.f)
		return true;
	else
		return false;
}
void CIceCube::ReverseMove()
{
	float fDeltaTime = Engine::Get_TimeMgr()->GetTime();

	m_pInfo->m_vPos.x -= m_vTempDir.x*CalculateSpeed(fDeltaTime)*fDeltaTime;
	m_pInfo->m_vPos.z -= m_vTempDir.z*CalculateSpeed(fDeltaTime)*fDeltaTime;


}

void CIceCube::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	m_iIndex = m_pCubeAni->GetIndex();

	D3DXMatrixIdentity(&m_pInfo->m_matWorld);

	vRight = D3DXVECTOR3(1.f, 0.f, 0.f);
	vUp = D3DXVECTOR3(0.f, 1.f, 0.f);
	vLook = D3DXVECTOR3(0.f, 0.f, 1.f);
	vPos = D3DXVECTOR3(0.f, 0.f, 0.f);

	// scale
	vRight.x *= m_pInfo->m_vScale.x;
	vUp.y *= m_pInfo->m_vScale.y;
	vLook.z *= m_pInfo->m_vScale.z;


	// rotate

	CMathMgr::MyRotationX(&vRight, &vRight, m_pInfo->m_fAngle[Engine::ANGLE_X]);
	CMathMgr::MyRotationX(&vUp, &vUp, m_pInfo->m_fAngle[Engine::ANGLE_X]);
	CMathMgr::MyRotationX(&vLook, &vLook, m_pInfo->m_fAngle[Engine::ANGLE_X]);

	CMathMgr::MyRotationY(&vRight, &vRight, m_pInfo->m_fAngle[Engine::ANGLE_Y]);
	CMathMgr::MyRotationY(&vUp, &vUp, m_pInfo->m_fAngle[Engine::ANGLE_Y]);
	CMathMgr::MyRotationY(&vLook, &vLook, m_pInfo->m_fAngle[Engine::ANGLE_Y]);

	CMathMgr::MyRotationZ(&vRight, &vRight, m_pInfo->m_fAngle[Engine::ANGLE_Z]);
	CMathMgr::MyRotationZ(&vUp, &vUp, m_pInfo->m_fAngle[Engine::ANGLE_Z]);
	CMathMgr::MyRotationZ(&vLook, &vLook, m_pInfo->m_fAngle[Engine::ANGLE_Z]);

	// trans
	vPos = m_pInfo->m_vPos;

	Engine::CPipeline::MakeTransformMatrix(&m_pInfo->m_matWorld, &vRight, &vUp, &vLook, &vPos);



	StateChange();
	
		if (m_bAttack)
		{
			if (PositionLock())
				Move(fTime);
			else
			{
				m_vPlayerPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;//플레이어 좌표를 받음
				m_vTempDir = m_vPlayerPos - m_pInfo->m_vPos;
				D3DXVec3Normalize(&m_vTempDir, &m_vTempDir);
				m_pInfo->m_vPos.y = 1.f;
				m_pInfo->m_vPos.x += m_vTempDir.x*3.f*fTime;
				m_pInfo->m_vPos.z += m_vTempDir.z*3.f*fTime;

			}
		}
	

	SetRotateAngle();
	SetDirection();
	m_pIceCubeShadow->SetXZ(m_pInfo->m_vPos.x, m_pInfo->m_vPos.z);
	m_pIceCubeShadow->SetY(m_pInfo->m_vPos.y);
	m_pIceCubeShadow->SetAngleY(m_fAngleY);

	m_pIceCubeShadow->Update();
	m_vOnePos = m_pInfo->m_vPos;
	m_vOnePos.y = 1.f;
	SetTransform();
}


void CIceCube::Render()
{
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(100, 255, 255, 255));

	m_pCubeAni->Render();
	m_pIceCubeShadow->Render();

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);// 투명 해제
}


void CIceCube::SetDirection(void)
{
	//D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	//D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CIceCube::StateChange()
{
	if (m_bWake)
	{
		m_bWake = false;
		m_bAttack = true;
	}

	if (m_bDead)
	{
		m_bAttack = false;
	}
}

void CIceCube::SetRotateAngle()
{
	m_pInfo->m_fAngle[Engine::ANGLE_Y] = D3DXToRadian(m_fAngleY);
}

void CIceCube::SpreadPlayer(D3DXVECTOR3 vPlayerPos, D3DXVECTOR3 vEyeCubePos, float fRange)
{
}

CIceCube* CIceCube::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CIceCube* pInstance = new CIceCube(pGraphicDev);
	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}




void CIceCube::Reflect(const D3DXVECTOR3 & _vDir, const D3DXVECTOR3& _vPoint)
{
	m_iCollisionCount++;
	if (m_iCollisionCount > 3)
		return;
	m_vStart = m_pInfo->m_vPos;
	m_pInfo->m_vPos -= m_vTempDir * Engine::Get_TimeMgr()->GetTime() * 3.f;

	////법선검사
	//m_pInfo->m_vDir = _vDir;
	//m_fSpeed *= 0.5f;
	//m_vAccel = -m_pInfo->m_vDir;

	//return;
	//번선 _vDir;

	m_fSpeed *= 0.85f;

	m_vTempDir = m_vTempDir - ((_vDir * (D3DXVec3Dot(&_vDir, &m_vTempDir))) * 2);
	D3DXVec3Normalize(&m_vTempDir, &m_vTempDir);
	m_vAccel = -m_vTempDir;
	m_pInfo->m_vDir = m_vTempDir;

	

	//방향을 구해주자.

	float fCos = D3DXVec3Dot(&m_vTempDir, &g_vRight);

	fCos = acosf(fCos);
	fCos = D3DXToDegree(fCos);

	if (m_pInfo->m_vPos.z < m_pInfo->m_vPos.z + m_vTempDir.z)
		fCos *= -1.f;


	if (fCos < -180.f || fCos > 180.f)
	{
		StopIceCube();
		return;
	}
	m_fJumpTime += Engine::Get_TimeMgr()->GetTime();

	if (m_fJumpTime < 0.05f)
	{
		if (m_fSpeed > 8.f)
		{
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BOSS);
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_IceCube_FCol.ogg", CSoundMgr::SOUND_BOSS);
		}
		else
		{
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BOSS);
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_IceCube_SCol.ogg", CSoundMgr::SOUND_BOSS);
		}
	}

	m_bIsBottom = true;
	if (m_fJumpTime < 0.01f)
	{
		m_bIsBottom = false;
		m_fJumpTime = 0.f;
	}
}