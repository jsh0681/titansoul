#include "stdafx.h"
#include "PlayerObserver.h"

#include "Include.h"
#include "Transform.h"
#include "Export_Function.h"

CPlayerObserver::CPlayerObserver(void)
	: m_pInfoSubject(Engine::Get_InfoSubject())
{
	m_matTransform = Engine::CTransform::Create(g_vLook);
}

CPlayerObserver::~CPlayerObserver(void)
{
	Engine::Safe_Delete(m_matTransform);
}

void CPlayerObserver::Update(int iFlag)
{
	list<void*>*		pDataList = m_pInfoSubject->GetDataList(iFlag);
	NULL_CHECK(pDataList);

	switch (iFlag)
	{
	case CAMERAOPTION::TRANSFORM:
		for(int i=0; i<Engine::ANGLE_END; ++i)
			*m_matTransform = *((Engine::CTransform*)(pDataList->front()));
		break;
	}
}

CPlayerObserver* CPlayerObserver::Create(void)
{
	return new CPlayerObserver;
}

