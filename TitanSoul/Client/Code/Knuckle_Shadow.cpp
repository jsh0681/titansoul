#include "stdafx.h"
#include "Knuckle_Shadow.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"

CKnuckle_Shadow::CKnuckle_Shadow(LPDIRECT3DDEVICE9 pGraphicDev) :
	CKnuckle_Part(pGraphicDev)
{
}


CKnuckle_Shadow::~CKnuckle_Shadow()
{
	Release();
}

HRESULT CKnuckle_Shadow::Initialize(const VEC3& vPos, const float& fSize)
{
	m_dwVtxCnt = 4;
	m_fSize = fSize;

	m_wstrBufferName = L"Buffer_Knuckle_Shadow";

	// 부모에서 지워줌(카메라, 버텍스, 컴포넌트)
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = vPos;

	m_pInfo->m_vScale = { m_fSize , 1.f, m_fSize };

	ResetBuffer();

	return S_OK;
}

HRESULT CKnuckle_Shadow::AddComponent()
{
	Engine::CComponent* pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferName);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"KnuckleShadowNo");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

void CKnuckle_Shadow::SetTransform()
{
	const D3DXMATRIX* matView = m_pCameraObserver->GetView();
	const D3DXMATRIX* matProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, matView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, matProj);
}

void CKnuckle_Shadow::Update()
{
	UpdatePos();

	CKnuckle_Part::Update();
	SetTransform();
}

void CKnuckle_Shadow::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pConvertVertex);

	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);

	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(150, 255, 255, 255));

	m_pTexture->Render(0);
	m_pBuffer->Render();


	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}

void CKnuckle_Shadow::SetTargetPos(const VEC3* vPos)
{
	if (vPos != nullptr)
		m_pTargetVec = vPos;
}

void CKnuckle_Shadow::Release()
{
	m_pTargetVec = nullptr;
	ResetBuffer();
}

void CKnuckle_Shadow::Small(const float& fDeltaTime, const float& fSpeed)
{
	m_pInfo->m_vScale.x -= fDeltaTime * fSpeed;
	m_pInfo->m_vScale.z -= fDeltaTime * fSpeed;
}

void CKnuckle_Shadow::Big(const float& fDeltaTime, const float& fSpeed)
{
	m_pInfo->m_vScale.x += fDeltaTime * fSpeed;
	m_pInfo->m_vScale.z += fDeltaTime * fSpeed;
}

void CKnuckle_Shadow::Normal()
{
	m_pInfo->m_vScale.x = m_fSize;
	m_pInfo->m_vScale.z = m_fSize;
}

void CKnuckle_Shadow::UpdatePos()
{
	if (m_pTargetVec == nullptr)
		return;

	memcpy(&m_pInfo->m_vPos, m_pTargetVec, sizeof(float) * 3);
	m_pInfo->m_vPos.y = BOSS_Y;
}

CKnuckle_Shadow* CKnuckle_Shadow::Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const float& fSize)
{
	CKnuckle_Shadow* pInstance = new CKnuckle_Shadow(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos, fSize)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
