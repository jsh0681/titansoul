#ifndef Yeti_Land_h__
#define Yeti_Land_h__

#include "YetiState.h"
class CYeti;
class CYeti_Land
	: public CYetiState
{
public:
	CYeti_Land(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
	virtual ~CYeti_Land();

public:
	virtual HRESULT Initialize();
	virtual void Update(const float& fTime);
	virtual void Release();

public:
	static CYeti_Land*	Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti);
private:
	float	m_fTime = 0.f;


};

#endif // Yeti_Land_h__
