#ifndef Stage1_h__
#define Stage1_h__

/*!
* \class CStage1
*
* \간략정보 눈큐브 맵
*
* \상세정보 초기화 완료
*
* \작성자 윤유석
*
* \date 1월 6 2019
*
*/

#include "Scene.h"
#include "Player.h"
#include "Terrain.h"

#include "StaticCamera.h"
#include "StaticCamera2D.h"

namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CCamera;
}

class CDisplay;
class CCubeCol;
class CShpereCol;
class CTerrain;
class CEyeCube;
class CEyeCubeRaser;

class CPlayer;
class CStage1 :	public Engine::CScene
{
private:
	explicit CStage1(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
public:
	virtual ~CStage1();

private:
	virtual HRESULT Initialize();
	virtual void	Release();

	HRESULT		Add_Environment_Layer();
	HRESULT		Add_GameObject_Layer();
	void SetStartPoint();
	HRESULT		Add_UI_Layer();


public:
	virtual void Update();
	virtual void Render();
private:
	CTerrain*						m_pTerrain = nullptr;
	CShpereCol*						m_pShpereCol = nullptr;
	CCubeCol*						m_pCubeCol = nullptr;


	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;
	Engine::CCamera*				m_pCamera;

	CDisplay*						m_pDisplay = nullptr;
	CEyeCube*						m_pEyeCube;
	CPlayer*						m_pPlayer;
	CArrow*							m_pArrow = nullptr;
	tagObbColision* m_tPlayer;
	tagObbColision* m_tArrow;
	tagObbColision* m_tEyeCube;


	bool m_bChangeScene = false;
	int i = 0;
public:
	static CStage1*		Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
	void ColPlayerMonster(const float& fTime);
	void ColMonsterArrow(const float& fTime);

	void CheckPlaneColTriger(const int & iTriger);
	void BloodMode();
	void CeremonyYeah();
	void RemoveEffect();
};

#endif // !Stage1_h__