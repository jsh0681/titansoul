#ifndef GoliathRShadow_h__
#define GoliathRShadow_h__

#include "GameObject.h"
#include "CameraObserver.h"
#include "CollisionMgr.h"
#include "TerrainColl.h"


class CTexAni;

class CGoliathRShadow : public Engine::CGameObject
{
private:
	explicit CGoliathRShadow(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CGoliathRShadow();

public:
	virtual void	Update(void);
	virtual void	Render(void);
	void SetTarget(D3DXVECTOR3 vTarget) { m_vTarget = vTarget; }
	void SetBodyIndex(int iIndex) { m_iIndex = iIndex; }
	void SetHandStateKey(wstring strHandStateKey) { m_wstrHandStateKey = strHandStateKey; }
	void SetChangeHand(bool bChangeHand) { m_bChangeHand = bChangeHand; }
	const bool& GetChangeLock() { return m_bChangeLock; }

private:
	void Release(void);
	virtual HRESULT AddComponent(void);
	virtual HRESULT Initialize(void);
	virtual void SetDirection(void);
	virtual void SetTransform(void);
	void		StateKeyChange();

private:
	bool m_bChangeLock = false;
	bool m_bIsActivate = false;
	wstring m_wstrHandStateKey; //GAMEOBJ�� �൵ ������...

	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...
	CCameraObserver*		m_pCameraObserver = nullptr;
	float m_fSpeed;
	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;
	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *					m_pTerrainCol = nullptr;
	D3DXVECTOR3						m_vDestPos;
	D3DXVECTOR3						m_vTarget;


	CTexAni* m_pTexAni = nullptr;
	bool m_bChangeHand = false;
	int m_iIndex = 0;
	int m_iNextMove = 0;
public:
	static CGoliathRShadow*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
};
#endif // GoliathRShadow_h__
