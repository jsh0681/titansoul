#ifndef Sludge_Shadow_h__
#define Sludge_Shadow_h__


#include "SludgePart.h"


class CSludge_Shadow : public CSludgePart
{
private:
	explicit CSludge_Shadow(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CSludge_Shadow();
private:
	virtual HRESULT Initialize(const D3DXVECTOR3& vPos, const int& iIndex);
	virtual HRESULT AddComponent()				override;

	virtual void	Update();
	virtual void	Render();

	virtual void	Release();

public:
	virtual void SetTransform()					override;
	virtual void ResetSize()					override;
	virtual void InitSize()						override;

private:
	virtual bool	IdleFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	JumpFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	FallFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	SplitFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime, const bool& bJump) override;

public:
	static CSludge_Shadow*	Create(LPDIRECT3DDEVICE9 pGraphicDev, const D3DXVECTOR3& vPos, const int& iIndex);
};

#endif // !Sludge_Shadow_h__