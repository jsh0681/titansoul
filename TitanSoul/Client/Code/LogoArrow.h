#ifndef LogoArrow_h__
#define LogoArrow_h__


/*!
* \class CLogoArrow
*
* \간략정보 LogoButton을 고를 수 있게 도와주는 클래스
*
* \상세정보
*
* \작성자 윤유석
*
* \date 1월 5 2019
*
*/

#include "GameObject.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CTransform;
	class CResourcesMgr;
	class CTimeMgr;
	class CInfoSubject;
}

class CCameraObserver;
class CLogoArrow : public Engine::CGameObject
{
private:
	explicit CLogoArrow(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CLogoArrow();

private:
	HRESULT	AddComponent();
	virtual HRESULT	Initialize(const int& ArrowCnt, const D3DXVECTOR3& vPos);
	virtual void	Release();

public:
	virtual void Update()	override;
	virtual void Render()	override;

private:
	void		SetTransform();

public:
	void		SetY(const float& fY);

private:
	Engine::CResourcesMgr*		m_pResourcesMgr = nullptr;
	Engine::CTimeMgr*			m_pTimeMgr = nullptr;
	Engine::CInfoSubject*		m_pInfoSubject = nullptr;

	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;
	Engine::CTransform*			m_pInfo;

	CCameraObserver*			m_pCameraObserver = nullptr;

	int							m_iArrowCnt = 0;

	DWORD						m_dwVtxCnt = 0;

	Engine::VTXTEX*				m_pVertex = nullptr;
	Engine::VTXTEX*				m_pConvertVertex = nullptr;


public:
	static	CLogoArrow*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const int& ArrowCnt,const D3DXVECTOR3& vPos);
};

#endif // !LogoArrow_h__