#ifndef Stage2_h__
#define Stage2_h__

/*!
* \class CStage2
*
* \�������� ö�� ��
*
* \������ �ʱ�ȭ �Ϸ�
*
* \�ۼ��� ������
*
* \date 1�� 6 2019
*
*/

/*!
 * \class CStage2
 *
 * \brief 
 *
 * \������ ������
 * \������ 1�� 12 2019
 * \���� ���� : ö�� �۾� ��
 */

namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CCamera;
	class CLayer;
	class CKeyMgr;
	class CTimeMgr;
}

#include "Scene.h"

class CPlayer;
class CArrow;
class CShpereCol;
class CKnuckle;
class CFirePlane;
class CTerrain;
class CDisplay;
class CStage2 : public Engine::CScene
{
private:
	explicit CStage2(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
public:
	virtual ~CStage2();

private:
	const int&		m_iPlaneSize = 10;

private:
	virtual HRESULT Initialize();
	virtual void	Release();

	HRESULT		AddResource();

	HRESULT		Add_Environment_Layer();
	HRESULT		Add_GameObject_Layer();
	HRESULT		Add_UI_Layer();

	void		SetStartPoint();

private:
	virtual void	BloodMode();
	void			CeremonyYeah();
	void			RemoveEffect();

	void		CheckMonsterCol();
	void		CheckFirePlaneCol();
	void		CheckJumpEnd();
	void		CheckBodyCol();
	void		CheckMaceCol();
	void		CollPlaneArrow();
	void		CheckPlaneColTriger(const int& iTriger);
	void		CheckCameraShaking();

public:
	virtual void Update();
	virtual void Render();

private:
	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;
	Engine::CCamera*				m_pCamera;
	Engine::CKeyMgr*				m_pkeyMgr;
	Engine::CTimeMgr*				m_pTimeMgr;

	CPlayer*	m_pPlayer;
	CDisplay*	m_pDisplay = nullptr;
	CArrow*		m_pArrow = nullptr;
	CTerrain*	m_pTerrain = nullptr;
	CKnuckle*	m_pKnuckle = nullptr;
	CShpereCol*	m_pSphereCol = nullptr;

	vector<CFirePlane*> m_vecFirePlane;

	bool		m_bDieCheck = false;
	bool		m_bFirstCol = false;
	bool		m_bChangeScene = false;

	bool		m_bJumpEvent = false;
	float		m_fJumpEventCount = 0.f;

public:
	static bool			m_sbStage2_Resource;
	static bool			m_sbBossDie;
	static CStage2*		Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
};

#endif // !Stage2_h__