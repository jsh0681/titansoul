#include "stdafx.h"
#include "SelectObj.h"


// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"


CSelectObj::CSelectObj(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement = Engine::Get_Management();
	m_pInfoSubject = Engine::Get_InfoSubject();
}

CSelectObj::~CSelectObj()
{
	Release();
}

void CSelectObj::Update()
{
	CGameObject::Update();
	SetTransform();
}

void CSelectObj::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBuffer, m_pConvertVertex);

	m_pTexture->Render(0);
	m_pBuffer->Render();
}

HRESULT CSelectObj::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBuffer);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrTexture);
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);
	return S_OK;
}

HRESULT CSelectObj::Initialize(const VEC3& vPos)
{
	m_wstrBuffer = L"Buffer_SelectObj";
	m_wstrTexture = L"ArrowDefaultDD";

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBuffer, m_pVertex);
	InitVertex();

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos = vPos;
	m_pInfo->m_vScale = { 20.f,20.f,1.f };

	return S_OK;
}

void CSelectObj::Release()
{
	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CSelectObj::SetTransform()
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
}

void CSelectObj::InitVertex()
{
	m_pVertex[0].vPos = { -1.f,  1.f,  0.f };
	m_pVertex[1].vPos = { 1.f,   1.f,  0.f };
	m_pVertex[2].vPos = { 1.f,  -1.f,  0.f };
	m_pVertex[3].vPos = { -1.f, -1.f,  0.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBuffer, m_pVertex);
}


void CSelectObj::SetPos(const VEC3& vPos)
{
	m_pInfo->m_vPos = vPos;
}

void CSelectObj::SetPosY(const float& fY)
{
	m_pInfo->m_vPos.y = fY;
}

const float& CSelectObj::GetPosY()
{
	return m_pInfo->m_vPos.y;
}

const VEC3& CSelectObj::GetPos()
{
	return m_pInfo->m_vPos;
}

CSelectObj* CSelectObj::Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos)
{
	CSelectObj* pInstance = new CSelectObj(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
