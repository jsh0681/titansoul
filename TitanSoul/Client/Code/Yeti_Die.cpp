#include "stdafx.h"
#include "Yeti_Die.h"

CYeti_Die::CYeti_Die(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti)
	:CYetiState(pGraphicDev, pYeti)
{
}

CYeti_Die::~CYeti_Die()
{
}

HRESULT CYeti_Die::Initialize()
{
	return S_OK;
}

void CYeti_Die::Update(const float & fTime)
{
	if (m_pYeti->CheckSceneEnd())
	{
		m_pYeti->SetDead();
	}
}

void CYeti_Die::Release()
{
}

CYeti_Die * CYeti_Die::Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti * pYeti)
{
	CYeti_Die* pInstance = new CYeti_Die(pGraphicDev, pYeti);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
