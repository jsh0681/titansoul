#include "stdafx.h"
#include "Yeti_Throw.h"
#include "SnowBullet.h"
CYeti_Throw::CYeti_Throw(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti)
	:CYetiState(pGraphicDev, pYeti)
{
}

CYeti_Throw::~CYeti_Throw()
{
}

HRESULT CYeti_Throw::Initialize()
{
	m_fDegree = m_pYeti->GetPlayerDegree();

	m_fDegree -= 45.f;

	return S_OK;
}

void CYeti_Throw::Update(const float & fTime)
{
	//�����̸� ���� ���ȴ����?
	//��������� ��ȯ��
	if (m_pYeti->GetThrowCnt() == 0)
	{
		m_pYeti->SetState(CYeti::YETI_ROLL);
		return;
	}

	if (m_pYeti->GetSceneIndex() == 1)
	{
		m_bPreventOverlap = TRUE;
	}

	//���� end�϶�...
	if (m_pYeti->CheckSceneEnd())
	{
		if (m_bPreventOverlap)
		{
			m_pYeti->MakeSnowBullet(m_fDegree);
			m_bPreventOverlap = FALSE;
			m_fDegree += 15.f;
			--m_pYeti->SetThrowCnt();
		}
	}


}

void CYeti_Throw::Release()
{
}

CYeti_Throw * CYeti_Throw::Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti * pYeti)
{
	CYeti_Throw* pInstance = new CYeti_Throw(pGraphicDev, pYeti);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}