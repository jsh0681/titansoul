#include "stdafx.h"


#include "Yeti_Idle.h"
#include "Transform.h"


CYeti_Idle::CYeti_Idle(LPDIRECT3DDEVICE9 pGraphicDev, CYeti* pYeti)
	:CYetiState(pGraphicDev, pYeti)
{
}

CYeti_Idle::~CYeti_Idle()
{
}

HRESULT CYeti_Idle::Initialize()
{
	return S_OK;
}

void CYeti_Idle::Update(const float & fTime)
{

}

void CYeti_Idle::Release()
{
}

CYeti_Idle * CYeti_Idle::Create(LPDIRECT3DDEVICE9 pGraphicDev, CYeti * pYeti)
{
	CYeti_Idle* pInstance = new CYeti_Idle(pGraphicDev, pYeti);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}