#include "stdafx.h"
#include "Player.h"

#include "Display.h"
#include "Export_Function.h"
#include "Include.h"
#include "TexAni.h"
#include "Engine_constant.h"
#include "Arrow.h"
#include "SphereColBox.h"
#include"PlayerShadow.h"

#include "SphereColBox.h"
#include "StaticCamera2D.h"
CPlayer::CPlayer(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CGameObject(pGraphicDev)

{

}

CPlayer::~CPlayer()
{
	Release();
}

HRESULT CPlayer::Initialize(void)
{
	m_pResourcesMgr = (Engine::Get_ResourceMgr());
	m_pInfoSubject = (Engine::Get_InfoSubject());
	m_pCollisionMgr = (CCollisionMgr::GetInstance());
	m_pManagement = (Engine::Get_Management());
	m_pKeyMgr = Engine::Get_KeyMgr();


	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);


	m_wstrStateKey = L"PlayerReleverDD";
	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	KeySetting();

	m_pInfo->m_vPos.y = 0.1f;
	m_pInfo->m_vPos.x = 110.f;
	m_pInfo->m_vPos.z = 13.7f;




	m_pArrow = CArrow::Create(m_pGraphicDev, m_pInfo);

	m_State[Engine::PRE] = P_RELEVER;
	m_State[Engine::CUR] = P_RELEVER;

	m_pSphereColBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 0.3f, TRUE);

	m_pShadow = CPlayerShadow::Create(m_pGraphicDev, m_pInfo->m_vPos);
	//m_pDisplay = CDisplay::Create(m_pGraphicDev);
	return S_OK;
}
void CPlayer::KeySetting(void)
{
	m_wstrP_StateKey[P_IDLE] = L"PlayerIdle";
	m_wstrP_StateKey[P_MOVE] = L"PlayerMove";
	m_wstrP_StateKey[P_RUN] = L"PlayerRun";
	m_wstrP_StateKey[P_ROLL] = L"PlayerRoll";
	m_wstrP_StateKey[P_ATTACK] = L"PlayerAttack";
	m_wstrP_StateKey[P_RECOVERY] = L"PlayerRecovery";
	m_wstrP_StateKey[P_SWIM] = L"PlayerSwim";
	m_wstrP_StateKey[P_RELEVER] = L"PlayerRelever";
	m_wstrP_StateKey[P_DANCE] = L"PlayerDance";
	m_wstrP_StateKey[P_DANCE2] = L"PlayerDance2";
	m_wstrP_StateKey[P_CLIMB] = L"PlayerClimb";
	m_wstrP_StateKey[P_PUSH] = L"PlayerPush";
	m_wstrP_StateKey[P_DIE] = L"PlayerDie";
}

void CPlayer::Update(void)
{
	m_pInfoSubject->Notify(CAMERAOPTION::TRANSFORM);

	fTime = Engine::Get_TimeMgr()->GetTime();

	if (m_dAct == NO_ACT)
	{
		if (!g_SystemMode)
		{
			KeyInput(fTime);
		}
		SceneChange();
		if (!g_SystemMode)
		{
			if (!m_bCollision)
				Move(fTime);
			else
				ReverseRoll(fTime);
		}
		else
		{
			if (m_bChangeStage)
			{
				if (m_State[Engine::CUR] != P_RELEVER)
					m_pInfo->m_vPos += m_pInfo->m_vDir * fTime * 0.3f;
			}
		}
	}
	else 
	{
		PlayerReset();
		m_DIR[Engine::CUR] = Engine::DD;
		if (m_dAct == DANCE1)
		{
			m_State[Engine::CUR] = P_DANCE;
		}
		else if (m_dAct == DANCE2)
		{
			if(g_2DMode)
				m_pCamera->ZoomIn(m_pInfo->m_vPos, 2.f, 0.001f);
			m_pInfo->m_vPos.y += fTime * 0.5f;
			m_State[Engine::CUR] = P_DANCE2;
		}
		else if (m_dAct == ENDDANCE)
		{
			m_fDanceTime += fTime;
			m_pInfo->m_vPos.y -= fTime * 2.f;
			if (g_2DMode)
				m_pCamera->ZoomOut();
			m_State[Engine::CUR] = P_DANCE;


			if (g_2DMode)
			{
				if (m_pInfo->m_vPos.y < 0.1f)
				{
					m_fDanceTime = 0.f;
					m_pInfo->m_vPos.y = 0.1f;
					m_dAct = NO_ACT;
					CSoundMgr::GetInstance()->StopAll();
				}
			}
			else
			{
				if (m_pInfo->m_vPos.y < 1.f)
				{
					m_fDanceTime = 0.f;
					m_pInfo->m_vPos.y = 1.0f;
					m_dAct = NO_ACT;
					CSoundMgr::GetInstance()->StopAll();

				}
			}

		}
		SceneChange();
	}
	m_pTerrainVertex = m_pManagement->GetTerrainVertex(Engine::LAYER_GAMEOBJECT, L"Terrain");

	//m_pTerrainCol->SetColInfo(&m_pInfo->m_vPos, m_pTerrainVertex);


	if (m_pArrow->Get_Ready())
	{
		m_pArrow->ResetData();
		m_bArrow = TRUE;
		m_bAim = FALSE;
		m_bGetArrow = TRUE;
		if (m_bPressingKey)
		{
			//c 키를 누르고 있는 상태였다면 
			m_bBanZoom = TRUE;
		}
		CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_ARROW);
		CSoundMgr::GetInstance()->PlaySoundw(L"Arrow_Pickup.ogg", CSoundMgr::SOUND_ARROW);

		
	}
	Engine::CGameObject::Update();

	if (!g_2DMode)
	{
		if (m_bTurn)
		{
			m_pInfo->m_vPos.y = 1.f;


			if (m_fDegree > -90.f)
				m_fDegree -= fTime * 60.f;

			m_pInfo->m_fAngle[0] = D3DXToRadian(m_fDegree);
		}
	}
	m_pShadow->SetXZ(m_pInfo->m_vPos.x, m_pInfo->m_vPos.z - 0.5f);
	m_pShadow->Update();

	SetTransform();

	//m_pDisplay->Update();
}

void CPlayer::Render(void)
{
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000088);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	m_pTexAni->Render();
	m_pShadow->Render();

	//m_pSphereColBox->Render();
	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

	//m_pDisplay->Render();

}


HRESULT CPlayer::AddComponent(void)
{
	Engine::CComponent*		pComponent = nullptr;

	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_Player");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	// Transform
	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	m_pInfoSubject->AddData(CAMERAOPTION::TRANSFORM, m_pInfo);

	//// Collision Terrain
	//pComponent = m_pCollisionMgr->CloneColObject(CCollisionMgr::COL_TERRAIN);
	//m_pTerrainCol = dynamic_cast<CTerrainColl*>(pComponent);
	//NULL_CHECK_RETURN(m_pTerrainCol, E_FAIL);
	//m_mapComponent.emplace(L"TerrainCol", pComponent);

	//// Collision Mouse
	//pComponent = m_pCollisionMgr->CloneColObject(CCollisionMgr::COL_MOUSE);
	//m_pMouseCol = dynamic_cast<CMouseCol*>(pComponent);
	//NULL_CHECK_RETURN(m_pMouseCol, E_FAIL);
	//m_mapComponent.emplace(L"MouseCol", pComponent);

	return S_OK;
}


void CPlayer::KeyInput(const float& _fTime)
{
	m_bChangeStage = FALSE;
	m_bMove = FALSE;
	m_fRollCool -= _fTime;
	if (m_fRollCool <= 0.f)
	{
		m_fRollCool = 0.f;
		m_bCoolTime = FALSE;
	}

	if (m_bRoll)
	{
		m_pCamera->SetCameraSpeed(2.0f);

		if (m_pTexAni->CheckAniEnd())
		{
			m_bRoll = FALSE;
			m_bCollision = FALSE;
		}
		return;
	}


	if (!m_bBanZoom && m_pKeyMgr->KeyPressing(KEY_C) && !m_bSwim)
	{

		m_bPressingKey = TRUE;
		if (m_bArrow) //화살이 있는상태라면...
		{
			if (g_2DMode)
				m_pCamera->ZoomIn(m_pInfo->m_vPos + m_pInfo->m_vDir * 7.f, 30.f ,1.f);

			m_bAim = TRUE;
			if (m_fSpeed > 0.f)
				m_fRollCool = m_fSpeed;

			m_fRollCool -= fTime * m_fAcceleration;
			m_fSpeed = 0.f;
			m_fGauge += fTime;

			if (m_bFirstAim)
			{
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_ARROW);
				CSoundMgr::GetInstance()->PlaySoundw(L"Arrow_Charge.ogg", CSoundMgr::SOUND_ARROW);

				m_pArrow->SetTargetDirection(m_DIR[Engine::CUR], 1);
				m_pArrow->Update();
				m_bFirstAim = FALSE;
			}
			//m_pArrow->SetTargetDirection(m_DIR[Engine::CUR], 1);
			m_pArrow->SetTargetDirection(m_DIR[Engine::CUR]);
			//m_pArrow->SettingStartPoint();

			m_State[Engine::CUR] = P_ATTACK;

			if (!m_pArrow->GetValid())
			{
			}
			m_pArrow->SetValid(1);
			m_pArrow->Gauge(1, fTime);
		}
		else //화살이 없는 상태라면...
		{


			if (m_bFirstRecovery) //화살을 쏘고 첫번째로 회수버튼을 누른경우.
			{
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_ARROW);
				CSoundMgr::GetInstance()->PlaySoundw(L"Arrow_Retrieval.ogg", CSoundMgr::SOUND_ARROW);

				if (m_pArrow->Get_DirectLength())
				{
					//각도 구해서...
					D3DXVECTOR3 vDir = m_pArrow->Get_Trnasform()->m_vPos - m_pInfo->m_vPos;
					D3DXVec3Normalize(&vDir, &vDir);

					float fCos = D3DXVec3Dot(&vDir, &g_vRight);

					fCos = acosf(fCos);
					fCos = D3DXToDegree(fCos);

					if (m_pInfo->m_vPos.z > m_pArrow->Get_Trnasform()->m_vPos.z)
						fCos *= -1.f;

					DirectChange(fCos);

					m_pArrow->Set_Angle(fCos);

					m_bArrow = m_pArrow->Recovery(fTime, m_bFirstRecovery);

					m_bFirstRecovery = FALSE;
				}
				else
				{
					int i = 0;
				}
			}
			else
			{
				m_State[Engine::CUR] = P_RECOVERY;
				m_bArrow = m_pArrow->Recovery(fTime, m_bFirstRecovery);

			}
			m_State[Engine::CUR] = P_RECOVERY;
			m_fSpeed = 0.f;

			//화살 - 나.
			D3DXVECTOR3 vDir = m_pArrow->Get_Trnasform()->m_vPos - m_pInfo->m_vPos;
			D3DXVec3Normalize(&vDir, &vDir);


			if (g_2DMode)
				m_pCamera->ZoomIn(m_pInfo->m_vPos + vDir * 7.f, 30.f, 1.f);

			return;
		}
	}
	else
	{
		CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUNDID::SOUND_ARROW);

		if (m_bBanZoom && m_pKeyMgr->KeyUp(KEY_C))
		{
			m_bBanZoom = FALSE;
		}
		if (m_bPressingKey) //C키가 눌렸던 상황이라면?
		{
			m_bPressingKey = FALSE;
			m_bFirstRecovery = TRUE;
			m_bFirstAim = TRUE;

			if (m_bAim)
			{
				bool bResult = m_pArrow->Shout(); //화살을 쏘는데 성공했느닞 실패했는지 검사 성공하면 FALSE 반환이야~!!!
				m_bArrow = (bResult);
				m_bAim = FALSE;
				if (m_bArrow) //실패하면
				{
					m_pArrow->ResetData();
				}

			}
		}
		if (g_2DMode)
			m_pCamera->ZoomOut();

	}


	if (m_pKeyMgr->KeyDown(KEY_X) && !m_bCoolTime && !m_bSwim)
	{
		//잠깐!!!

		m_State[Engine::CUR] = P_ROLL;
		m_bCoolTime = TRUE;
		m_fSpeed = 11.0f;
		m_bRoll = TRUE;

		m_fRollCool = 0.8f;

		//화살은초기화
		if (m_bArrow) //화살이 있으면
		{
			m_bArrow = TRUE;
			m_bAim = FALSE;
			m_pArrow->SetValid(0);
			m_pArrow->ResetData();
			m_bFirstAim = TRUE;
		}
		CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_PLAYER);
		CSoundMgr::GetInstance()->PlaySoundw(L"Player_Roll.ogg", CSoundMgr::SOUND_PLAYER);

		return;
	}


	if (Engine::Get_KeyMgr()->KeyDown(KEY_S))
	{
		m_bArrow = TRUE;
		m_bAim = FALSE;
		m_pArrow->SetValid(0);
		m_pArrow->ResetData();
	}


	m_fRoot2 = 1.f;


	if (!m_bRoll)
	{
		if (m_pKeyMgr->KeyPressing(KEY_UP))
		{
			if (m_pKeyMgr->KeyPressing(KEY_RIGHT))//UR
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::UR;
				m_pInfo->m_vDir = g_vRight + g_vLook;
				m_fRoot2 = g_R2;
			}
			else if (m_pKeyMgr->KeyPressing(KEY_LEFT))//UL
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::UL;
				m_pInfo->m_vDir = -g_vRight + g_vLook;
				m_fRoot2 = g_R2;

			}
			else//UU
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::UU;
				m_pInfo->m_vDir = g_vLook;

			}
			m_bMove = TRUE;

		}
		else if (m_pKeyMgr->KeyPressing(KEY_DOWN))
		{
			if (m_pKeyMgr->KeyPressing(KEY_RIGHT))//DR
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::DR;
				m_pInfo->m_vDir = -g_vLook + g_vRight;
				m_fRoot2 = g_R2;


			}
			else if (m_pKeyMgr->KeyPressing(KEY_LEFT))//DL
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::DL;
				m_pInfo->m_vDir = -g_vLook - g_vRight;
				m_fRoot2 = g_R2;

			}
			else//DD
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::DD;
				m_pInfo->m_vDir = -g_vLook;

			}
			m_bMove = TRUE;

		}
		else if (m_pKeyMgr->KeyPressing(KEY_RIGHT))
		{
			if (m_pKeyMgr->KeyPressing(KEY_UP))//UR
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::UR;
				m_pInfo->m_vDir = g_vLook + g_vRight;
				m_fRoot2 = g_R2;

			}
			else if (m_pKeyMgr->KeyPressing(KEY_DOWN))//DR
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::DR;
				m_pInfo->m_vDir = -g_vLook + g_vRight;
				m_fRoot2 = g_R2;

			}
			else//DD
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::RR;
				m_pInfo->m_vDir = g_vRight;

			}
			m_bMove = TRUE;
		}
		else if (m_pKeyMgr->KeyPressing(KEY_LEFT))
		{
			if (m_pKeyMgr->KeyPressing(KEY_UP))//LR
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::UL;
				m_pInfo->m_vDir = g_vLook - g_vRight;
				m_fRoot2 = g_R2;

			}
			else if (m_pKeyMgr->KeyPressing(KEY_DOWN))//DL
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::DL;
				m_pInfo->m_vDir = -g_vLook - g_vRight;
				m_fRoot2 = g_R2;

			}
			else//DD
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::LL;
				m_pInfo->m_vDir = -g_vRight;
			}
			m_bMove = TRUE;
		}
	}


	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);


	if (m_bAim)
	{
		m_pArrow->SetTargetDirection(m_DIR[Engine::CUR]);

		//if (m_fRollCool <= 0.f)
		//{
		//	m_fRollCool = 0.f;
		//	m_bCoolTime = FALSE;
		//}

		return;
	}

	if (m_bMove)
	{
		m_fSoundTime += fTime;


		m_State[Engine::CUR] = P_MOVE;

		m_fSpeed = 5.f;

		if (m_pKeyMgr->KeyPressing(KEY_X)) //달리기모드
		{
			m_State[Engine::CUR] = P_RUN;
			m_fSpeed = 6.0f;
			if (g_2DMode)
				m_pCamera->SetCameraSpeed(1.0f);

			if (m_fSoundTime > 0.3f)
			{
				m_fSoundTime = 0.f;
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_PLAYER);
				CSoundMgr::GetInstance()->PlaySoundw(L"Player_Stone_Step1.ogg", CSoundMgr::SOUND_PLAYER);
			}
		}
		else
		{
			if (m_fSoundTime > 0.4f)
			{
				m_fSoundTime = 0.f;
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_PLAYER);
				CSoundMgr::GetInstance()->PlaySoundw(L"Player_Stone_Step1.ogg", CSoundMgr::SOUND_PLAYER);
			}
		}



		m_fMoveCool = 3.f;

		if (m_bPush)
			m_State[Engine::CUR] = P_PUSH;
		if (m_bSwim)
			m_State[Engine::CUR] = P_SWIM;
		if (m_bClimb)
			m_State[Engine::CUR] = P_CLIMB;
	}
	else
	{
		m_bPush = FALSE;
		m_bClimb = FALSE;
		m_State[Engine::CUR] = P_MOVE;
		m_fSpeed = 0.f;
		if (m_fMoveCool <= 0.f)
		{
			m_fMoveCool = 0.f;
			//m_bCoolTime = FALSE;
			m_bCollision = FALSE; //구르는것도 초기화

			m_State[Engine::CUR] = P_IDLE;
		}
		else
			m_fMoveCool -= fTime * m_fAcceleration;


	}
	//pPlayer->StopReverseRoll();
	m_bCollision = FALSE;
	m_bPush = FALSE;

	if (m_bSwim)
		m_State[Engine::CUR] = P_SWIM;
	else
	{
	}


}

void CPlayer::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);
}

void CPlayer::Release(void)
{
	Engine::Safe_Delete(m_pShadow);
	//Engine::Safe_Delete(m_pDisplay);
	Engine::Safe_Delete(m_pSphereColBox);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);
}

void CPlayer::SetTransform(void)
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pmatView, pmatProj);
}

void CPlayer::Move(const float & _fTime)
{
	if (!m_bCurseOn)
		m_pInfo->m_vPos += m_pInfo->m_vDir * _fTime * m_fSpeed / m_fRoot2 * m_fCollisionSpd;
	else
	{
		m_pInfo->m_vPos += m_pInfo->m_vDir * _fTime * m_fSpeed / m_fRoot2 * m_fCollisionSpd*-1;
	}
}

void CPlayer::ReverseMove(const float & _fTime)
{
	if (!m_bCurseOn)
		m_pInfo->m_vPos -= m_pInfo->m_vDir * _fTime * m_fSpeed / m_fRoot2 * m_fCollisionSpd;
	else
	{
		m_pInfo->m_vPos -= m_pInfo->m_vDir * _fTime * m_fSpeed / m_fRoot2 * m_fCollisionSpd*-1;
	}
}

void CPlayer::ReverseRoll(const float & _fTime)
{
	if (!m_bCollision)
		m_bCollision = TRUE;
	if (!m_bCurseOn)
		m_pInfo->m_vPos -= m_pInfo->m_vDir * _fTime * m_fSpeed / m_fRoot2 * 0.25f;
	else
		m_pInfo->m_vPos -= m_pInfo->m_vDir * _fTime * m_fSpeed / m_fRoot2 * m_fCollisionSpd*-1;
}

void CPlayer::SceneChange()
{
	if (m_State[Engine::PRE] != m_State[Engine::CUR])
	{
		m_wstrStateKey = m_wstrP_StateKey[m_State[Engine::CUR]];
		m_wstrStateKey = m_wstrStateKey + g_wstrDir[m_DIR[Engine::CUR]];
		m_State[Engine::PRE] = m_State[Engine::CUR];
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));

	}

	if (m_DIR[Engine::PRE] != m_DIR[Engine::CUR])
	{
		m_wstrStateKey.pop_back();
		m_wstrStateKey.pop_back();

		m_wstrStateKey = m_wstrStateKey + g_wstrDir[m_DIR[Engine::CUR]];
		m_DIR[Engine::PRE] = m_DIR[Engine::CUR];
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
	}
}

void CPlayer::SetDirUp()
{
	m_DIR[Engine::CUR] = Engine::DIRECTION::UU;
	m_pInfo->m_vDir = { 0.f, 0.f, 1.f };
	m_bChangeStage = TRUE;
}

void CPlayer::SetDirDown()
{
	m_DIR[Engine::CUR] = Engine::DIRECTION::DD;
	m_pInfo->m_vDir = { 0.f, 0.f, -1.f };
	m_bChangeStage = TRUE;


}



void CPlayer::GoUpStairs(Engine::DIRECTION _dDir, float _fDirection)
{

	float fTime = Engine::Get_TimeMgr()->GetTime();
	m_pInfo->m_vPos.z += fTime * m_fSpeed / m_fRoot2 * _fDirection;

}

void CPlayer::SetDBoss(DWORD dID)
{
	m_dBoss |= dID;
}

void CPlayer::SetPosXZ(const D3DXVECTOR3& vPos)
{
	m_pInfo->m_vPos.x = vPos.x;
	m_pInfo->m_vPos.z = vPos.z;
}

void CPlayer::CreateArrow()
{
	m_pArrow = CArrow::Create(m_pGraphicDev, m_pInfo);
}

const int CPlayer::Get_AnimationIndex()
{
	return m_pTexAni->GetIndex();
}

void CPlayer::Return2DMode()
{
	m_pInfo->m_vPos.y = 0.1f;
	m_fDegree = 0.f;
	m_pInfo->m_fAngle[0] = D3DXToRadian(m_fDegree);

	m_pArrow->SetY(0.2f);
}

void CPlayer::SetPushPos(const VEC3& vPush)
{
	m_pInfo->m_vPos += vPush;
}

void CPlayer::DirectChange(const float & _fAngle)
{
	if (-22.5f <= _fAngle && _fAngle <= 22.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::RR;
	}
	else if (22.5f <= _fAngle && _fAngle <= 67.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UR;
	}
	else if (67.5f <= _fAngle  && _fAngle <= 112.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UU;

	}
	else if (112.5f <= _fAngle && _fAngle <= 157.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::UL;
	}
	else if (-157.5f <= _fAngle && _fAngle <= -112.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DL;

	}
	else if (-112.5f <= _fAngle  && _fAngle <= -67.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DD;

	}
	else if (-67.5 <= _fAngle  && _fAngle <= -22.5f)
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::DR;
	}
	else
	{
		m_DIR[Engine::CUR] = Engine::DIRECTION::LL;
	}
}

void CPlayer::RollVan()
{
	m_bRoll = FALSE;
	m_bCollision = FALSE;
}

void CPlayer::PlayerReset()
{
	m_bMove = FALSE; //움직이는가?
	m_bRoll = FALSE; //구르는가?
	m_bAim = FALSE; // 
	m_bArrow = TRUE; //화살이 있는가?
	m_bTurn = FALSE;
	m_bBanZoom = FALSE;
	m_bCurseOn = FALSE;
	m_bCoolTime = FALSE;
	m_bFirstRecovery = TRUE; //첫번째 회수. 꾹누르고 있다가 떼고 다시 누르는 경우는 FALSE줘야함
	m_bPressingKey = FALSE; // C버튼을 누르고있는상태 안누른상태를 구분하기 위해서임.
	m_bFirstAim = TRUE; //줌을 처음 할때ㅣ...
	m_bRecovery = FALSE; //리커버리중...
	m_bGetArrow = FALSE; //리커버리하여 화살을 처음 먹은 상태.;
	m_bPush = FALSE; //미는상태...
	m_bCollision = FALSE; //구르는도중 충돌하는상태.
	m_bSwim = FALSE; //수영하는상태
	m_bClimb = FALSE; //줄타는 상태
}

const float & CPlayer::GetRadius()
{
	return m_pSphereColBox->GetRadius();
}

void CPlayer::CameraRelease()
{

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	m_vSavePos;
	D3DXVECTOR3 vTemp = m_vSavePos;

}

void CPlayer::CameraCreate()
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);
}


void CPlayer::Rolling(const float& _fTime)
{

}

void CPlayer::Aiming(const float& _fTime)
{
	if (m_State[Engine::CUR] != P_ATTACK) return;

	if (m_bArrow)	//화살이 있는 경우...
	{
		if (m_pKeyMgr->KeyPressing(KEY_UP))
		{
			if (m_pKeyMgr->KeyPressing(KEY_RIGHT))//UR
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::UR;
			}
			else if (m_pKeyMgr->KeyPressing(KEY_LEFT))//UL
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::UL;
			}
			else//UU
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::UU;
			}
		}
		else if (m_pKeyMgr->KeyPressing(KEY_DOWN))
		{
			if (m_pKeyMgr->KeyPressing(KEY_RIGHT))//DR
				m_DIR[Engine::CUR] = Engine::DIRECTION::DR;
			else if (m_pKeyMgr->KeyPressing(KEY_LEFT))//DL
				m_DIR[Engine::CUR] = Engine::DIRECTION::DL;
			else//DD
				m_DIR[Engine::CUR] = Engine::DIRECTION::DD;
		}
		else if (m_pKeyMgr->KeyPressing(KEY_RIGHT))
		{
			if (m_pKeyMgr->KeyPressing(KEY_UP))//UR
				m_DIR[Engine::CUR] = Engine::DIRECTION::UR;
			else if (m_pKeyMgr->KeyPressing(KEY_DOWN))//DR
				m_DIR[Engine::CUR] = Engine::DIRECTION::DR;
			else//DD
				m_DIR[Engine::CUR] = Engine::DIRECTION::RR;
		}
		else if (m_pKeyMgr->KeyPressing(KEY_LEFT))
		{
			if (m_pKeyMgr->KeyPressing(KEY_UP))//LR
				m_DIR[Engine::CUR] = Engine::DIRECTION::UL;
			else if (m_pKeyMgr->KeyPressing(KEY_DOWN))//DL
				m_DIR[Engine::CUR] = Engine::DIRECTION::DL;
			else//DD
			{
				m_DIR[Engine::CUR] = Engine::DIRECTION::LL;
			}
		}

		m_pArrow->SetTargetDirection(m_DIR[Engine::CUR]);
	}
	else			//화살이 없는 경우...
	{

	}
}

void CPlayer::StopReverseRoll()
{
	m_bCollision = FALSE;
	
}

CPlayer* CPlayer::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CPlayer*	pInstance = new CPlayer(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

void CPlayer::SetY(float y)
{
	m_pInfo->m_vPos.y = y;
}

