#ifndef CKnuckle_Shadow_h__
#define CKnuckle_Shadow_h__

/*!
 * \class CKnuckle_Shadow
 *
 * \�������� ö��� ���� ���� �׸���.
 *
 * \������ 
 *
 * \�ۼ��� ������
 *
 * \date 1�� 13 2019
 *
 */

#include "Knuckle_Part.h"
class CKnuckle_Shadow :  public CKnuckle_Part
{
private:
	explicit CKnuckle_Shadow(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CKnuckle_Shadow();

private:
	virtual HRESULT	Initialize(const VEC3& vPos, const float& fSize);
	virtual HRESULT AddComponent() override;
	virtual void	SetTransform() override;
	virtual void	Release();

public:
	virtual void	Update();
	virtual void	Render();

	void			SetTargetPos(const VEC3* vPos);

public:
	void			Small(const float& fDeltaTime,const float& fSpeed);		// �۾���
	void			Big(const float& fDeltaTime, const float& fSpeed);		// Ŀ��
	void			Normal();

	void			UpdatePos();

private:

private:
	float			m_fSize = 0.f;
	const VEC3*		m_pTargetVec = nullptr;

public:
	static CKnuckle_Shadow*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const float& fSize);

};

#endif // !CKnuckle_Shadow_h__