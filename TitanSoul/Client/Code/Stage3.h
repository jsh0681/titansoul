#ifndef Stage3_h__
#define Stage3_h__

/*!
* \class CStage3
*
* \간략정보 얼음뇌 맵
*
* \상세정보 초기화 완료
*
* \작성자 윤유석
*
* \date 1월 6 2019
*
*/


namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CCamera;
}
#include"Brain.h"
#include "Fire.h"
#include "Scene.h"
#include "StaticCamera.h"
#include "Player.h"
#include "Terrain.h"
#include "StaticCamera2D.h"
#include "IceCube.h"

class CDisplay;
class CShpereCol;
class CCubeCol;
class CPlayer;
class CFireButton;

class CStage3 : public Engine::CScene
{
private:
	explicit CStage3(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
public:
	virtual ~CStage3();

private:
	virtual HRESULT CreateBrick(); //추가
	virtual void TimeStartBrick(); //추가
	virtual HRESULT Initialize();
	virtual void	Release();

	HRESULT		Add_Environment_Layer();
	HRESULT		Add_GameObject_Layer();
	HRESULT		Add_UI_Layer();

public:
	enum FIREBUTTON { LEFT, RIGHT, UP, DOWN };
public:
	virtual void Update();
	virtual void Render();
	virtual void FireArrowEffect(const D3DXVECTOR3& vPos);

private:
	D3DXMATRIX matWorld ;
	CBrain*		m_pBrain = nullptr;
	CTerrain*						m_pTerrain = nullptr;
	CCubeCol*						m_pCubeCol = nullptr;
	CFireButton* m_pFireButton[4] = { nullptr, };

	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;
	Engine::CCamera*				m_pCamera;
	CSphereColBox*				m_pSphereBox = nullptr;
	CIceCube*						m_pIceCube;
	CPlayer*						m_pPlayer;
	CArrow*							m_pArrow = nullptr;
	CFire*							m_pFire = nullptr;
	bool							m_bIsFire[4] = {false,};

	tagObbColision* m_tPlayer;
	tagObbColision* m_tArrow;
	tagObbColision* m_tIceCube;
	tagObbColision* m_tFireButton[4];
	tagObbColision* m_tFire;
	CShpereCol*						m_pShpereCol = nullptr;
	CDisplay*	m_pDisplay = nullptr;


	bool m_bChangeScene = false;


public:
	static CStage3*		Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
	static bool			m_sbStage3_Resource;

	void SetStartPoint();
public:

	void ColPlayerMonster(const float& fTime);
	void ColMonsterArrow(const float& fTime);
	void ColMonsterFireButton(const float& fTime);
	void IcuCubeSplatter();
	void BrainSplatter();
	void RemoveEffect();
	void CheckPlaneColTriger(const int & iTriger);
	void SphereColPlayerMonster(const float & fTime);
	void SphereColMonsterArrow(const float & fTime);
	void ColArrowFire(const float & fTime);
	void CeremonyYeah();
	void BloodMode();
};

#endif // !Stage3_h__

