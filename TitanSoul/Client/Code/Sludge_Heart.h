#ifndef Sludge_Heart_h__
#define Sludge_Heart_h__


#include "SludgePart.h"

namespace Engine
{
	class CTimeMgr;
}

class CSludge_Heart : public CSludgePart
{
private:
	explicit CSludge_Heart(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CSludge_Heart();
private:
	virtual HRESULT Initialize(const D3DXVECTOR3& vPos, const int& iIndex, const bool& bSolo);
	virtual HRESULT AddComponent()				override;

	virtual void	Update();
	virtual void	Render();

	virtual void	Release();

public:
	virtual void SetTransform()					override;
	virtual void ResetSize()					override;
	virtual void InitSize()						override;

	void	SetDie();
	void	SetSolo()			{ m_bSolo = true;}

private:
	virtual bool	IdleFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	JumpFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	FallFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime) override;
	virtual bool	SplitFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime, const bool& bJump) override;

private:
	Engine::CTimeMgr*	m_pTimeMgr;

	float m_fIndexAni = 0;
	float m_fMaxAniTime = 1.f;
	int m_iMaxIndexAni = 3;
	int m_iAniCount = 0;
	bool m_bDie		= false;
	bool m_bSolo	= false;

public:
	static CSludge_Heart*	Create(LPDIRECT3DDEVICE9 pGraphicDev, const D3DXVECTOR3& vPos, 
										const int& iIndex, const bool& bSolo);
};

#endif // !Sludge_Heart_h__