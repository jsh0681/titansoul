#include "stdafx.h"
#include "RcEffect.h"

#include "Export_Function.h"


CRcEffect::CRcEffect(LPDIRECT3DDEVICE9 pGraphicDev) :
	CEffect(pGraphicDev)
{

}

CRcEffect::~CRcEffect()
{

	Release();
}

void CRcEffect::Release()
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
		m_pConvertVertex[i] = m_pVertex[i];

	m_pResourceMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_pEffectInfo->wstrBufferKey, m_pConvertVertex);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

HRESULT CRcEffect::Initialize(EFFECTINFO* pEffectInfo)
{
	m_pEffectInfo = pEffectInfo;

	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourceMgr->EngineToClient(Engine::RESOURCE_STATIC, m_pEffectInfo->wstrBufferKey, m_pVertex);
	
	
	if (CreateBuffer(m_pEffectInfo) == E_FAIL)
		return E_FAIL;
	if (CreateTexture(m_pEffectInfo) == E_FAIL)
		return E_FAIL;

	return S_OK;
}

void CRcEffect::Render()
{
	m_pResourceMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_pEffectInfo->wstrBufferKey, m_pConvertVertex);
	CEffect::Render();
}

void CRcEffect::SetTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatWorld);


		//Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatView);
		//if (m_pConvertVertex[i].vPos.z < 1.f)
		//	m_pConvertVertex[i].vPos.z = 1.f;
		//
		//Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, pMatProj);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, pMatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pMatProj);
}



CRcEffect* CRcEffect::Create(LPDIRECT3DDEVICE9 pGraphicDev, EFFECTINFO* pEffectInfo)
{
	CRcEffect* pInstance = new CRcEffect(pGraphicDev);

	if (FAILED(pInstance->Initialize(pEffectInfo)))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}