#ifndef FireWork_h__
#define FireWork_h__
#include"ParticleSystem.h"
#include "Export_Function.h"


class Firework : public Engine::ParticleSystem
{
public:

	Firework(D3DXVECTOR3* vOriginPos, PARTICLEINFO * pParticleInfo);
	Firework(D3DXVECTOR3* vOriginPos, int iNumParticles);
	//시작 위치와 개수 
	void ResetParticle(Engine::tagAttribute* attribute);
	void Update(float timeDelta);
	void PreRender();
	void PostRender();
private:
	PARTICLEINFO* m_pParticleInfo;
};
#endif // FireWork_h__
