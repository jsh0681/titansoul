#ifndef CubeBrick_h__
#define CubeBrick_h__

#include "GameObject.h"

namespace Engine
{
	class CTimeMgr;
	class CVIBuffer;
	class CTexture;
}
class CStage3;
class CCameraObserver;
class CCubeBrick
	: public Engine::CGameObject
{
private:
	explicit CCubeBrick(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CCubeBrick();

public:
	virtual void InitVertex();
	virtual HRESULT AddComponent();
	virtual HRESULT Initialize(CUBEINFO* pCubeInfo, const float _fSpd, const int iNum);
	virtual void Update(void);
	virtual void Render(void);
	virtual void Release(void);

public:
	void SetTransform();

public:
	void Set_Stage(CStage3* pStage) { m_pStage3 = pStage; };
	void Set_FallingStart() { m_bFallingStart = TRUE; };

public:
	static CCubeBrick* Create(LPDIRECT3DDEVICE9 pGraphicDev, CUBEINFO* pCubeInfo , const float _fSpd, const int iNum);

private:
	CUBEINFO* m_pCubeInfo = nullptr;  //삭제는 TerrainMgr에서...
	Engine::CResourcesMgr*		m_pResourcesMgr = nullptr;
	Engine::CTimeMgr*			m_pTimeMgr = nullptr;
	Engine::CInfoSubject*		m_pInfoSubject = nullptr;
	CCameraObserver*			m_pCameraObserver;

	Engine::CVIBuffer*			m_pBufferCom;
	Engine::CTexture*			m_pTextureCom;

	Engine::VTXCUBE*			m_pVertex = nullptr;
	Engine::VTXCUBE*			m_pConvertVertex = nullptr;

	CStage3*					m_pStage3 = nullptr;
	DWORD m_dwVtxCnt = 8;

	int m_iNum = 0;
	float m_fSpeed = 0.f;
	bool m_bFallingStart = FALSE;
	bool m_bTimeFinish = FALSE;
	float m_fOriginY = 0.f;

	float m_fFallTime = 0.f;
	
};

#endif // CubeBrick_h__
