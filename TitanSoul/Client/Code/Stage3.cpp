#include "stdafx.h"
#include "Stage3.h"

// Engine
#include "Export_Function.h"
#include"SceneSelector.h"

// Client
#include "Include.h"
#include "CubeBrick.h"
#include "NonAniEffect.h"
#include "StaticCamera.h"
#include "AlphaAniEffect.h"
#include "CeremonyEffect.h"
#include "CeremoneyEffect_List.h"
#include "Display.h"
//Colision
#include"CubeCol.h"
#include"ShpereCol.h"

//Obj
#include "Player.h"
#include "Plane.h"
#include "Arrow.h"
#include "FireButton.h"
#include "Fire.h"
#include "StaticCamera2D.h"
#include "SphereColBox.h"
#include "OrthoCamera.h"


bool CStage3::m_sbStage3_Resource = false;
CStage3::CStage3(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera) :
	Engine::CScene(pGraphicDev),
	m_pManagement(Engine::Get_Management()),
	m_pResourcesMgr(Engine::Get_ResourceMgr()),
	m_pPlayer(pPlayer),
	m_pCamera(pCamera)
{
	m_pDisplay = CDisplay::Create(m_pGraphicDev);
	m_pDisplay->StartFade(CDisplay::FADEIN);
}

CStage3::~CStage3()
{
	Release();
}


HRESULT CStage3::CreateBrick()
{
	int iNum = 0;
	float fSpd = 15.f;
	Engine::CGameObject* pGameObject = nullptr;
	for (auto& pCube : *(CTerrainMgr::GetInstance()->GetpCubeInfoList()))
	{
		pCube->vPos.x -= 1.f;
		pCube->vCenter.x -= 1.f;
		pGameObject = CCubeBrick::Create(m_pGraphicDev, pCube, fSpd, iNum);
		dynamic_cast<CCubeBrick*>(pGameObject)->Set_Stage(this);
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Brick", pGameObject);
		++iNum;
	}
	return S_OK;
}

void CStage3::TimeStartBrick()
{
	//카메라도...

	dynamic_cast<CStaticCamera*>(m_pCamera)->SetDownAngle();
	m_pBrain->SetTurn(1);
	m_pPlayer->SetTurn(1);

	for (auto& pBrick : *m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Brick"))
	{
		dynamic_cast<CCubeBrick*>(pBrick)->Set_FallingStart();
	}
}

HRESULT CStage3::Initialize()
{
	if (!m_sbStage3_Resource)
	{
		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_CUBETEX,
			L"Buffer_Brick", 5, 5),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_CUBETEX,
			L"Buffer_IceCube", 1, 1, 1),
			E_FAIL);


		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_RCTEX_Z,
			L"Buffer_FireButton", 7, 7),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_RCTEX,
			L"Buffer_Fire", 10, 10),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::TEX_NORMAL,
			L"Texture_FireButton",
			L"../Bin/Resources/Texture/Tex/IceCube/Tool/Button/%d.png",
			2),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_RCTEX_Z,
			L"Buffer_Brain", 15, 15),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::TEX_NORMAL,
			L"Texture_BrainIdle",
			L"../Bin/Resources/Texture/Tex/IceCube/Brain/Idle/%d.png",
			8),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::TEX_NORMAL,
			L"Texture_BrainAttack",
			L"../Bin/Resources/Texture/Tex/IceCube/Brain/Attack/%d.png",
			8),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_RCTEX_Z,
			L"Buffer_IceCubeShadow", 20, 20),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_RCTEX_Z,
			L"Buffer_BrainShadow", 15, 15),
			E_FAIL);


		FAILED_CHECK_RETURN(m_pResourcesMgr->AddTexture(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::TEX_NORMAL,
			L"Texture_Shadow",
			L"../Bin/Resources/Texture/Tex/IceCube/Brain/Shadow/%d.png",
			2),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_RCTEX_Z,
			L"Buffer_IceCubeEffect", 10, 10),
			E_FAIL);

		FAILED_CHECK_RETURN(m_pResourcesMgr->AddBuffer(m_pGraphicDev,
			Engine::RESOURCE_STATIC,
			Engine::BUFFER_RCTEX_Z,
			L"Buffer_BrainEffect", 10, 10),
			E_FAIL);
	}


	SetStartPoint();

	m_pCubeCol = dynamic_cast<CCubeCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_CUBE));
	m_pShpereCol = dynamic_cast<CShpereCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_SHPERE));

	FAILED_CHECK_RETURN(Add_Environment_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_GameObject_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_UI_Layer(), E_FAIL);


	m_tPlayer = new tagObbColision;
	m_tIceCube = new tagObbColision;
	m_tArrow = new tagObbColision;
	m_tFire = new tagObbColision;

	for (int i = 0; i < 4; i++)
	{
		m_tFireButton[i] = new tagObbColision;
	}


	g_2DMode = FALSE;
	//3D카메라를 생성하고...
	Safe_Delete(m_pCamera);
	m_pCamera = CStaticCamera::Create(m_pGraphicDev, m_pPlayer->Get_Trnasform());

	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	pLayer->AddObject(L"Camera", m_pCamera);

	Engine::CGameObject * pGameObjectz = COrthoCamera::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObjectz, E_FAIL);
	pLayer->AddObject(L"OrthoCamera", pGameObjectz);


	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);
	//m_pPlayer->SetCamera(m_pCamera);
	//m_pPlayer->Set3DCamera(m_pCamera);

	D3DXMatrixIdentity(&matWorld);
	matWorld.m[3][0] = 30.f;
	matWorld.m[3][1] = 1.0f;
	matWorld.m[3][2] = 31.f;

	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, &matWorld, 1.f, true);

	m_sbStage3_Resource = TRUE;
	CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
	return S_OK;
}

void CStage3::Release()
{
	Engine::Safe_Delete(m_pDisplay);

	if (m_bChangeScene)
	{
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"Player");
		m_mapLayer[Engine::LAYER_UI]->ClearList(L"Camera");
	}

	//이펙트 지우고
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->end();
	CNormalEffect* pEffect = nullptr;
	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;
		pEffect = dynamic_cast<CNormalEffect*>(*iter);
		if (pEffect == nullptr) return;

		Engine::Safe_Delete(*iter);
		iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->erase(iter);
	}

	Engine::Safe_Delete_Array(m_tFireButton[0]);
	Engine::Safe_Delete_Array(m_tFireButton[1]);
	Engine::Safe_Delete_Array(m_tFireButton[2]);
	Engine::Safe_Delete_Array(m_tFireButton[3]);

	Engine::Safe_Delete(m_pSphereBox);
	Engine::Safe_Delete(m_tFire);
	Engine::Safe_Delete(m_tPlayer);
	Engine::Safe_Delete(m_tArrow);
	Engine::Safe_Delete(m_tIceCube);
	Engine::Safe_Delete(m_pCubeCol);
	Engine::Safe_Delete(m_pShpereCol);
}

HRESULT CStage3::Add_Environment_Layer()
{
	return S_OK;
}

HRESULT CStage3::Add_GameObject_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CGameObject*		pGameObject = nullptr;

	pLayer->AddObject(L"Player", m_pPlayer);
	m_pPlayer->SetScene(this);
	m_pPlayer->CameraCreate();
	m_pPlayer->CreateArrow();

	CArrow* pArrow = m_pArrow = m_pPlayer->Get_ArrowPointer();
	Engine::CGameObject* pObject = dynamic_cast<Engine::CGameObject*>(pArrow);
	pLayer->AddObject(L"Arrow", pObject);
	m_pArrow->SetScene(this);

	pGameObject = m_pTerrain = CTerrainMgr::GetInstance()->Clone(L"MapTerrainStage3");
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Terrain", pGameObject);


	pGameObject = m_pIceCube = CIceCube::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"IceCube", pGameObject);

	pGameObject = m_pFireButton[LEFT] = CFireButton::Create(m_pGraphicDev, D3DXVECTOR3(19.85f, 0.05f, 31.0f));
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FireButtonLeft", pGameObject);

	pGameObject = m_pFireButton[RIGHT] = CFireButton::Create(m_pGraphicDev, D3DXVECTOR3(40.2f, 0.05f, 31.0f));
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FireButtonRight", pGameObject);

	pGameObject = m_pFireButton[UP] = CFireButton::Create(m_pGraphicDev, D3DXVECTOR3(30.f, 0.05f, 39.15f));
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FireButtonUp", pGameObject);

	pGameObject = m_pFireButton[DOWN] = CFireButton::Create(m_pGraphicDev, D3DXVECTOR3(30.f, 0.05f, 22.85f));
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"FireButtonDown", pGameObject);

	pGameObject = m_pBrain = CBrain::Create(m_pGraphicDev, m_pIceCube->GetInfo()->m_vPos);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Brain", pGameObject);


	pLayer->AddObject(L"Effect", pGameObject);
	pLayer->ClearList(L"Effect");

	m_mapLayer.emplace(Engine::LAYER_GAMEOBJECT, pLayer);

	CreateBrick();

	return S_OK;
}

HRESULT CStage3::Add_UI_Layer()
{
	return S_OK;
}

void CStage3::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	Engine::CScene::Update();
	int i = m_pTerrain->CollisionPlane(m_pPlayer);
	//m_pTerrain->CollisoinPlanetoArrow(m_pPlayer->Get_ArrowPointer(), m_pPlayer);

	m_pTerrain->CollisoinPlanetoArrowBackAgain(m_pPlayer->Get_ArrowPointer(), m_pPlayer);

	if (g_BattleMode)
		m_pArrow->SetY(1.f);

	for (int i = 0; i < 4; i++)
		m_bIsFire[i] = m_pFireButton[i]->GetIsTrample();

	for (int i = 0; i < 4; i++)
	{
		if (m_pFire == nullptr)
		{
			if (m_bIsFire[i] == true)
			{
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_IceCube_FireLoop.ogg", CSoundMgr::SOUND_BOSSATTACK);
				m_pFire = m_pFireButton[i]->GetFire();
			}
		}
	}

	if (!m_bIsFire[0] && !m_bIsFire[1] && !m_bIsFire[2] && !m_bIsFire[3])//네개가 다 꺼져있으면 m_pFire객체를 날린다. 
	{
		CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BOSSATTACK);
		m_pFire = nullptr;
	}
	if (m_pFire != nullptr)
		ColArrowFire(fTime);//화살과 불의 충돌

	if ( m_pBrain != nullptr && m_pIceCube == nullptr) 
	{
		SphereColMonsterArrow(fTime);
		SphereColPlayerMonster(fTime);
	}


	if (m_pIceCube != nullptr)
	{
		m_pTerrain->CollisoinPlanetoIceCube(m_pIceCube, m_pPlayer);
		D3DXVECTOR3 vTemp = m_pIceCube->GetInfo()->m_vPos;
		vTemp.y += 1.0f;
		m_pBrain->SetPos(vTemp);
		ColPlayerMonster(fTime);
		ColMonsterFireButton(fTime);
		ColMonsterArrow(fTime);
	}
	else
		m_pBrain->SetMoving(true);

	if (m_pIceCube != nullptr)
	{
		if (m_pIceCube->GetIsBottom())
			IcuCubeSplatter();
	}

	if (m_pBrain->GetIsBottom())
	{
		BrainSplatter();
	}

	RemoveEffect();
	m_pDisplay->Update();

	CheckPlaneColTriger(i);
}

void CStage3::Render()
{
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000045);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	m_pDisplay->Render();
	Engine::CScene::Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	
	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CStage3::FireArrowEffect(const D3DXVECTOR3 & vPos)
{
	NorEffect Temp;
	Temp.vPos = vPos;
	Temp.vDir = D3DXVECTOR3(0.f, 1.f, 0.f);
	Temp.vAcc = D3DXVECTOR3(0.f, 0.f, 0.f);
	Temp.fSpd = 1.f;
	Temp.fScale = 0.5f;
	Temp.fLifeTime = 2.f;
	Temp.fDisappearSpd = 0.8f;

	m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_Effect", L"EffectFireArrowEffectNO", 8));
}

CStage3* CStage3::Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	CStage3* pInstance = new CStage3(pGraphicDev, pPlayer, pCamera);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

void CStage3::SetStartPoint()
{
	const D3DXVECTOR3& vStartPos = CTerrainMgr::GetInstance()->GetMapInfo(L"MapTerrainStage3")->vStartingPoint;
	m_pPlayer->SetPosXZ(D3DXVECTOR3(30.0f, 0.1f, 22.09f));

	m_pPlayer->SetDirUp();
	m_pPlayer->SceneChange();

	dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();
}

void CStage3::ColPlayerMonster(const float& fTime)
{
	m_tPlayer->fAxisLen[0] = 0.25f;
	m_tPlayer->fAxisLen[1] = 0.25f;
	m_tPlayer->fAxisLen[2] = 0.25f;
	m_tPlayer->vAxisDir[0] = D3DXVECTOR3(1.f, 0.f, 0.f);
	m_tPlayer->vAxisDir[1] = D3DXVECTOR3(0.f, 1.f, 0.f);
	m_tPlayer->vAxisDir[2] = D3DXVECTOR3(0.f, 0.f, 1.f);
	m_tPlayer->vCenterPos = m_pPlayer->Get_Trnasform()->m_vPos;

	m_tIceCube->fAxisLen[0] = 2.03f;
	m_tIceCube->fAxisLen[1] = 2.03f;
	m_tIceCube->fAxisLen[2] = 2.03f;
	m_tIceCube->vAxisDir[0] = m_pIceCube->GetRight();
	m_tIceCube->vAxisDir[1] = m_pIceCube->GetUp();
	m_tIceCube->vAxisDir[2] = m_pIceCube->GetLook();
	m_tIceCube->vCenterPos = m_pIceCube->GetInfo()->m_vPos;

	if (m_pCubeCol->CheckOBBCollision(m_tPlayer, m_tIceCube))
	{
		m_pPlayer->ReverseMove(fTime);
		m_pPlayer->SetPush(1);
		BloodMode();
	}
}

void CStage3::ColMonsterArrow(const float& fTime)
{
	if (m_pArrow->Get_ArrowState() == CArrow::A_SHOT || m_pArrow->Get_ArrowState() == CArrow::A_RECOVERY)
	{
		m_tArrow->fAxisLen[0] = 0.1f;
		m_tArrow->fAxisLen[1] = 0.1f;
		m_tArrow->fAxisLen[2] = 0.1f;
		m_tArrow->vAxisDir[0] = D3DXVECTOR3(1.f, 0.f, 0.f);
		m_tArrow->vAxisDir[1] = D3DXVECTOR3(0.f, 1.f, 0.f);
		m_tArrow->vAxisDir[2] = D3DXVECTOR3(0.f, 0.f, 1.f);
		m_tArrow->vCenterPos = m_pArrow->Get_Trnasform()->m_vPos;

		m_tIceCube->fAxisLen[0] = 2.03f;
		m_tIceCube->fAxisLen[1] = 2.03f;
		m_tIceCube->fAxisLen[2] = 2.03f;
		m_tIceCube->vAxisDir[0] = m_pIceCube->GetRight();
		m_tIceCube->vAxisDir[1] = m_pIceCube->GetUp();
		m_tIceCube->vAxisDir[2] = m_pIceCube->GetLook();
		m_tIceCube->vCenterPos = m_pIceCube->GetInfo()->m_vPos;

		if (m_pCubeCol->CheckOBBCollision(m_tArrow, m_tIceCube))
		{
			if (!m_pIceCube->GetWake() && !m_pIceCube->GetAttack())//자고있다면
			{
				g_BattleMode = true;
				TimeStartBrick();

				CSoundMgr::GetInstance()->PlayBGM(L"Boss_IceCube.ogg");
				m_pIceCube->SetWake();//깨워준다
			}
			m_pArrow->StopArrow();

			if (m_pArrow->Get_IsFire() == true)
			{
				auto& it_find = m_mapLayer.find(Engine::LAYER_GAMEOBJECT);

				if (it_find == m_mapLayer.end())
					return;

				it_find->second->SafeDeleteObjList(L"IceCube");
				m_pIceCube = nullptr;
			}
		}
	}
}

void CStage3::ColMonsterFireButton(const float& fTime)
{
	for (int i = 0; i < 4; i++)
	{
		m_tFireButton[i]->fAxisLen[0] = 0.35f;
		m_tFireButton[i]->fAxisLen[1] = 0.35f;
		m_tFireButton[i]->fAxisLen[2] = 0.35f;
		m_tFireButton[i]->vAxisDir[0] = D3DXVECTOR3(1.f, 0.f, 0.f);
		m_tFireButton[i]->vAxisDir[1] = D3DXVECTOR3(0.f, 1.f, 0.f);
		m_tFireButton[i]->vAxisDir[2] = D3DXVECTOR3(0.f, 0.f, 1.f);
		m_tFireButton[i]->vCenterPos = m_pFireButton[i]->GetInfo()->m_vPos;

		m_tIceCube->fAxisLen[0] = 2.03f;
		m_tIceCube->fAxisLen[1] = 2.03f;
		m_tIceCube->fAxisLen[2] = 2.03f;
		m_tIceCube->vAxisDir[0] = m_pIceCube->GetRight();
		m_tIceCube->vAxisDir[1] = m_pIceCube->GetUp();
		m_tIceCube->vAxisDir[2] = m_pIceCube->GetLook();
		m_tIceCube->vCenterPos = m_pIceCube->GetInfo()->m_vPos;

		if (m_pCubeCol->CheckOBBCollision(m_tIceCube, m_tFireButton[i]))
		{
			m_pFireButton[i]->SetIsTrample(true);
		}
		else
		{
			m_pFireButton[i]->SetIsTrample(false);
		}
	}
}

void CStage3::IcuCubeSplatter()
{
	Engine::CGameObject* pGameObject = nullptr;

	NorEffect Temp;
	float fRand;
	Temp.fScale = 0.35f;


	//자.!! 여기가 대왕 얼음덩어리
	float fAngle = 0.f;
	//방향은.. 각도방향으로  for문...

	Temp.fLifeTime = 2.f;
	Temp.vPos = m_pBrain->GetInfo()->m_vPos;
	Temp.vPos.y = m_pIceCube->GetInfo()->m_vPos.y-0.9f;

	fAngle = 0.f;
	Temp.vAcc = { 0.f, 1.f, 0.f };
	Temp.fDisappearSpd = 0.5f;

	//방향은.. 각도방향으로  for문...
	for (int i = 0; i < 3; ++i)
	{
		fRand = GetRandom<float>(4.5f, 8.f);
		Temp.fSpd = fRand;

		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_IceCubeEffect", L"EffectSnowIceNO", 2);
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

		float fRand = GetRandom<float>(0.f, 360.f);
		fAngle -= fRand;
	}


	fAngle = 0.f;
	Temp.fDisappearSpd = 0.3f;

	//방향은.. 각도방향으로  for문...
	for (int i = 0; i < 3; ++i)
	{
		fRand = GetRandom<float>(1.f, 8.f);
		Temp.fSpd = fRand;

		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_IceCubeEffect", L"EffectNiceSnowNO", 2);
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

		float fRand = GetRandom<float>(0.f, 360.f);
		fAngle -= fRand;
	}
}

void CStage3::BrainSplatter()
{
	Engine::CGameObject* pGameObject = nullptr;

	NorEffect Temp;
	float fRand;


	//자.!! 여기가 대왕 얼음덩어리
	float fAngle = 0.f;
	Temp.fScale = 0.4f;

	///////// 작은 눈알갱이  바닥에 뿌릴거
	Temp.fLifeTime = 3.f;
	Temp.vPos = m_pBrain->GetInfo()->m_vPos;
	Temp.vPos.y = 0.2f;

	fAngle = 0.f;
	Temp.vAcc = { 0.f, -1.f, 0.f };
	Temp.fDisappearSpd = -0.4f;

	//방향은.. 각도방향으로  for문...
	for (int i = 0; i < 3; ++i)
	{
		fRand = GetRandom<float>(1.f, 6.f);
		Temp.fSpd = fRand;

		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.y = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));

		float fRand = GetRandom<float>(0.f, 360.f);
		fAngle -= fRand;

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CAlphaAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_BrainEffect", L"EffectBrainEffectNo", 6);
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);
	}

	//방향은.. 각도방향으로  for문...
	for (int i = 0; i < 3; ++i)
	{
		fRand = GetRandom<float>(1.f, 6.f);
		Temp.fSpd = fRand;

		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.y = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));;

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CAlphaAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_BrainEffect", L"EffectBrainEffectNo", 6);
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

		float fRand = GetRandom<float>(0.f, 360.f);
		fAngle -= fRand;
	}
}

void CStage3::RemoveEffect()
{
	int iSize = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->size();
	auto& iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->begin();
	auto& iter_end = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->end();

	CNormalEffect* pEffect = nullptr;

	for (int i = 0; i < iSize; ++i)
	{
		if (iter == iter_end) return;

		pEffect = dynamic_cast<CNormalEffect*>(*iter);
		if (pEffect == nullptr) return;

		if (pEffect->CheckIsDead())
		{
			//죽여
			Engine::Safe_Delete((*iter));
			iter = m_mapLayer[Engine::LAYER_GAMEOBJECT]->GetObjList(L"Effect")->erase(iter);
		}
		else
			++iter;
	}
}

void CStage3::CheckPlaneColTriger(const int & iTriger)
{
	switch (iTriger)
	{
	case PL_EXIT:
		if (g_BattleMode) return;

		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}
		if (m_pDisplay->CheckFadeFinish())
		{
			m_pPlayer->SetDirDown();
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			Engine::Safe_Delete(m_pCamera);
			m_pCamera = CStaticCamera2D::Create(m_pGraphicDev, m_pPlayer->Get_Trnasform());
			dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();
			m_pPlayer->SetCamera(dynamic_cast<CStaticCamera2D*>(m_pCamera));
			g_2DMode = TRUE;
			m_pPlayer->Return2DMode();

			m_pManagement->SceneChange(CSceneSelector(SC_MAP1, m_pPlayer, m_pCamera));
		}
		break;
	}
}

void CStage3::SphereColPlayerMonster(const float& fTime)
{
	if (m_pShpereCol->CheckCollision(m_pBrain->GetInfo()->m_matWorld, (float)m_pBrain->GetRadius(), m_pPlayer->Get_Trnasform()->m_vPos, m_pPlayer->GetRadius()))
	{
		m_pPlayer->ReverseMove(fTime);
		m_pPlayer->SetPush(1);
		BloodMode();
	};
}

void CStage3::SphereColMonsterArrow(const float& fTime)
{
	if (m_pArrow->Get_ArrowState() == CArrow::A_SHOT || m_pArrow->Get_ArrowState() == CArrow::A_RECOVERY)
	{
		if (m_pShpereCol->CheckCollision(m_pBrain->GetInfo()->m_matWorld, (float)m_pBrain->GetRadius(), m_pArrow->Get_Trnasform()->m_vPos, m_pArrow->GetRadius()))
		{
			if (!m_pBrain->GetDead())
			{	
				m_pArrow->StopArrow();
				m_pBrain->SetDead(true);
				g_BattleMode = FALSE;
				m_pBrain->SetRadius(0.f);
				Engine::Get_TimeMgr()->SlowModeStart(5.f, 0.1f);
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
				CSoundMgr::GetInstance()->PlayBGM(L"BossDead.mp3");
				CeremonyYeah();
			}

		}
	}
}

void CStage3::ColArrowFire(const float& fTime)
{
	if (m_pArrow->Get_ArrowState() == CArrow::A_SHOT || m_pArrow->Get_ArrowState() == CArrow::A_RECOVERY)
	{
		if (m_pShpereCol->CheckCollision(matWorld, (float)m_pSphereBox->GetRadius(), m_pArrow->Get_Trnasform()->m_vPos, m_pArrow->GetRadius()))
		{
			m_pArrow->Set_Fire(true);
		}
	}
}

void CStage3::CeremonyYeah()
{
	//여기서 일단 세레모니 띄워보자.
	NorEffect Temp;
	Temp.fDisappearSpd = 0.f;
	Temp.fLifeTime = 15.f;
	Temp.fScale = 2.f;
	Temp.fSpd = 1.0f;
	Temp.vAcc = m_pPlayer->Get_Trnasform()->m_vPos; //플레이엉위치넣고
	Temp.vPos = m_pPlayer->Get_ArrowPointer()->Get_Trnasform()->m_vPos;

	float fAngle = 0.f;
	float fRand = 0.f;

	for (int i = 0; i < 14; ++i)
	{
		Temp.vDir.x = cosf(D3DXToRadian(fAngle));
		Temp.vDir.z = -sinf(D3DXToRadian(fAngle));
		Temp.vDir.y = 0.0f;
		fRand = GetRandom<float>(1.f, 2.f);
		Temp.fSpd = fRand;


		Engine::CGameObject* pGameObject = CCeremonyEffect::Create(m_pGraphicDev, Temp, L"Buffer_Effect", L"EffectCeremony2NO", m_pPlayer->Get_Trnasform()->m_matWorld, this, m_pPlayer);
		//m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->AddObject(L"Effect", pGameObject);

		fRand = GetRandom<float>(0.f, 30.f);
		fAngle += 45.f + fRand;
	}
}

void CStage3::BloodMode()
{
	m_pDisplay->StartBlood();
}