#include"stdafx.h"
#include"Brain.h"
#include"Export_Function.h"
#include"TexAni.h"
#include"SphereColBox.h"
#include"Arrow.h"
#include"StaticCamera2D.h"
#include"BrainShadow.h"

CBrain::CBrain(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{

}

CBrain::~CBrain(void)
{
	Release();
}

const float & CBrain::GetRadius()
{
	return m_pSphereBox->GetRadius();
}

void CBrain::SetRadius(const float & fSize)
{
	m_pSphereBox->SetRadius(fSize);
}

HRESULT CBrain::Initialize(D3DXVECTOR3 vStartPos)
{

	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	FAILED_CHECK_RETURN(AddComponent(vStartPos), E_FAIL);	
	
	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Brain", m_pVertex);

	m_pBrainShadow = CBrainShadow::Create(m_pGraphicDev, m_pInfo->m_vPos);
	NULL_CHECK_RETURN(m_pBrainShadow, E_FAIL);

	return S_OK;
}

HRESULT CBrain::AddComponent(D3DXVECTOR3 vStartPos)
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Brain");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Texture_BrainIdle");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTextureCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTextureCom);


	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);
	//중심 좌표가 되어 각 파트들에 넘겨줄 좌표를 생성할것

	m_pInfo->m_vPos.x = vStartPos.x;
	m_pInfo->m_vPos.y = 2.f;
	m_pInfo->m_vPos.z = vStartPos.z;



	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 1.f, true);
	return S_OK;
}

void CBrain::Update(void)
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	
	Engine::CGameObject::Update();
	if (m_bMove && !m_bDead)//아이스큐브가 죽고 뇌만 살아있을때 하는 움직임
	{
		Move(fTime);	
		
		m_pBrainShadow->SetXZ(m_pInfo->m_vPos.x, m_pInfo->m_vPos.z);
		m_pBrainShadow->SetY(m_pInfo->m_vPos.y);
		m_pBrainShadow->Update();
	}
	StateChange();
	SetDirection();

	if (!g_2DMode)
	{
		if (m_bTurn)
		{
			if (m_fDegree > -90.f)
				m_fDegree -= fTime * 60.f;

			m_pInfo->m_fAngle[Engine::ANGLE_X] = D3DXToRadian(m_fDegree);
		}
	}
	SetTransform();
}


void CBrain::Move(float fDeltaTime)
{
	m_vPlayerPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;//플레이어 좌표를 받음
	m_vTempDir = m_vPlayerPos - m_pInfo->m_vPos;
	D3DXVec3Normalize(&m_vTempDir, &m_vTempDir);
	if (m_bCalculate)
	{
		m_pInfo->m_vPos.y = 1.1f;
		m_bChange = false;
		m_bCalculate = false;
	}
	else
	{
		switch (m_iAttackPattern)
		{
		case 0:
			m_fJumpAcc += Engine::Get_TimeMgr()->GetTime();
			m_pInfo->m_vPos.x += m_vTempDir.x*5.f*fDeltaTime;
			m_pInfo->m_vPos.z += m_vTempDir.z*5.f*fDeltaTime;
			m_pInfo->m_vPos.y += 12.f*fDeltaTime - m_fJumpAcc*0.15f;
			if (m_pInfo->m_vPos.y >= 8.f)
			{
				m_fJumpAcc = 0;
				m_iAttackPattern = 1;
			}
			break;

		case 1:
			if (m_pInfo->m_vPos.y > 1.3f)
			{
				m_fFallAcc += Engine::Get_TimeMgr()->GetTime();
				m_pInfo->m_vPos.x += m_vTempDir.x*3.f*fDeltaTime;
				m_pInfo->m_vPos.z += m_vTempDir.z*3.f*fDeltaTime;
				m_pInfo->m_vPos.y -= 17.f*fDeltaTime - m_fFallAcc*0.1f;

			}
			else if (m_pInfo->m_vPos.y <= 1.3f)
			{
				
				m_bIsBottom = true;
				m_pInfo->m_vPos.y = 1.3f;

				m_fDelayTime += Engine::Get_TimeMgr()->GetTime();
				if(m_fDelayTime>0.1f)
					m_bIsBottom = false;

				if(m_fDelayTime>0.5f)
				{
					m_bChange = true;
					m_fDelayTime = Engine::Get_TimeMgr()->GetTime();
					m_fFallAcc = Engine::Get_TimeMgr()->GetTime();
					m_iAttackPattern = 0;
					m_bCalculate = true;
				}
			}
			break;
		}
	}
}

void CBrain::Render(void)
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Brain", m_pConvertVertex);
	
	m_pTextureCom->Render(m_iIndex);
	m_pBufferCom->Render();

	if (m_pBrainShadow != nullptr)
	{
		m_pBrainShadow->Render();
	}
}

void CBrain::SetTransform(void)
{
	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pMatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pMatProj);
}

void CBrain::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

}

void CBrain::Release(void)
{
	Engine::Safe_Delete(m_pBrainShadow);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete(m_pSphereBox);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
		m_pConvertVertex[i] = m_pVertex[i];

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Brain", m_pConvertVertex);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);


}

void CBrain::StateChange()
{
	m_vPlayerPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;
	m_pInfo->m_vDir = m_vPlayerPos - m_pInfo->m_vPos;
	D3DXVec3Normalize(&m_pInfo->m_vDir,&m_pInfo->m_vDir);//방향을 구한다.
	float fCosTheta = D3DXVec3Dot(&m_pInfo->m_vDir, &g_vRight);
	//x축양의 방향을 기준으로 플레이어가 위치한 코사인 세타 값 = 각도를 구할수있다.   
	fCosTheta = acosf(fCosTheta);//세타값만 빼옴  = 라디안.
	fCosTheta = D3DXToDegree(fCosTheta);//우리가 아는 그 각도로 변환이 완료되었음. 
	
	if (m_vPlayerPos.z < m_pInfo->m_vPos.z)
		fCosTheta *= -1;//z값이 아래면 음수로 만든다 각도를 

	if (-22.5f <= fCosTheta && fCosTheta <= 22.5f)
		m_iIndex = 2;
	else if (22.5f <= fCosTheta && fCosTheta <= 67.5f)
		m_iIndex = 7;
	else if (67.5f <= fCosTheta  && fCosTheta <= 112.5f)
		m_iIndex = 3;
	else if (112.5f <= fCosTheta && fCosTheta <= 157.5f)
		m_iIndex = 6;
	else if (-157.5f <= fCosTheta && fCosTheta <= -112.5f)
		m_iIndex = 5;
	else if (-112.5f <= fCosTheta  && fCosTheta <= -67.5f)
		m_iIndex = 1;
	else if (-67.5 <= fCosTheta  && fCosTheta <= -22.5f)
		m_iIndex = 4;
	else
		m_iIndex = 0;
}
CBrain* CBrain::Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos)
{
	CBrain* pInstance = new CBrain(pGraphicDev);
	if (FAILED(pInstance->Initialize(vStartPos)))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}
