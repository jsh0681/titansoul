#ifndef Logo_h__
#define Logo_h__

/*!
* \class CLogo
*
* \간략정보 멀티쓰레드 사용으로 로딩
*
* \상세정보 Texture 불러오기, Data불러와서 Buffer 및 객체 생성
*
*			Manager
*				Engine-> CManagement, CResourcesMgr
*			Obj
*				BackGround				: CScene::Release 에서 지워줌
*			Thread
m_pThread							: CLogo::Release 에서 지워줌
list<IMGPATH*> m_ImgPathLst			: CLogo::Release 에서 지워줌
list<DATAPATH*>	m_DataPathLst		: CLogo::Release 에서 지워줌


Buffer
Plane 버퍼를 InitResource에서 만들어줌
*
*
* \작성자 윤유석
*
* \date 1월 3 2019
*
*/

/*!
* \class CLogo
*
* \brief
*
* \수정자 윤유석
* \수정일 1월 8 2019
* \수정 내용 : Map 데이터 추가, Plane 데이터 추가
			
* \수정자 윤유석
* \수정일 1월 10 2019
* \수정 내용 : ColliosionMgr 맴버 생성 InitResource에서


* \수정일 1월 17 2019
* \수정 내용 : Ui버퍼 추가
*/

#include <thread>
#include <atomic>
#include <mutex>
#include <fstream>

#include "Include.h"
#include "Scene.h"

namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CKeyMgr;
};

class CParticleMgr;
class CTextureInfoMgr;
class CEffectInfoMgr;
class CTerrainMgr;
class CLogoButton;
class CLogoArrow;
class CAnimal;
class CLogo : public Engine::CScene
{
private:
	explicit CLogo(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CLogo(void);

public:
	virtual HRESULT Initialize(void)	override;
	virtual void	Update(void)		override;
	virtual void	Render(void)		override;
	virtual void	Release(void)		override;

private:
	HRESULT		InitResource();

private:
	HRESULT		Add_Environment_Layer(void) { return S_OK; }
	HRESULT		Add_GameObject_Layer(void);
	HRESULT		Add_UI_Layer(void);

private:
	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;
	Engine::CKeyMgr*				m_pKeyMgr;

private:
	CLogoButton*		m_pButton[3];
	CLogoArrow*			m_pArrow[2];
	CAnimal*			m_pAnimal = nullptr;

	int					m_iButtonIndex = 0;
	int					m_iPercent = 0;
	bool				m_bPercentEnd = false;

public:
	static CLogo*	Create(LPDIRECT3DDEVICE9 pGraphicDev);

private:
	//*********************************************************//
	//						Loading
	void	StartLoading();
	void	ImgPathFindLoad();
	void	DataPathFindLoad();

	HRESULT	AddPlaneBuffer();

	void	LoadTexture();
	void	LoadData();

	void	LoadAnimation(DATAPATH* pDataPath);
	void	LoadEffect(DATAPATH* pDataPath);
	void	LoadParticle(DATAPATH * pDataPath);
	void	LoadMap(DATAPATH* pDataPath);

	void	LoadMapData(HANDLE& hFile);
	void	LoadPlaneData(HANDLE& hFile, DATAPATH* pDataPath);
	void	LoadBrickData(HANDLE& hFile, DATAPATH* pDataPath); //이거추가

	void	ThreadDelete();

private:
	list<IMGPATH*>	m_ImgPathLst;
	list<DATAPATH*>	m_DataPathLst;

	CParticleMgr*		m_pParticleMgr = nullptr;
	CTextureInfoMgr*	m_pTextureInfoMgr = nullptr;
	CEffectInfoMgr*		m_pEffectInfoMgr = nullptr;
	CTerrainMgr*		m_pTerrainMgr = nullptr;

private:
	atomic<int>	m_AtomDataCount = 0;
	atomic<int>	m_AtomLoadingCount = 0;
	atomic<bool> m_AtomStartFlag = false;
	atomic<bool> m_AtomReadFileFlag = false;
	thread*		m_pThread = nullptr;

	mutex	m_StartMutex;
};


#endif // Logo_h__
