#ifndef CKnuckle_Body_h__
#define CKnuckle_Body_h__


#include "Knuckle_Part.h"

namespace Engine
{
	class CTexture;
}

class CKnuckle_Body : public CKnuckle_Part
{
	enum BODY_DIRECTION { DD, DL, DR, LL, RR, UL, UR, UU, D_END };
private:
	explicit CKnuckle_Body(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CKnuckle_Body();

private:
	virtual HRESULT	Initialize(const VEC3& vPos, const int& iIndex);
	virtual HRESULT AddComponent() override;
	virtual void	SetTransform() override;
	virtual void	Release();

public:
	virtual void	Update();
	virtual void	Render();

public:
	void			SetDirection(const int& iDir);
	void			SetLength(const float& fLength) { m_fLength = fLength; }

private:
	virtual	void		Walk_UpFunc();
	virtual void		Body_Rot();
	virtual void		AttackRotation();
	virtual void		AttackJump();

private:
	Engine::CTexture*	m_pTextureArr[D_END];
	BODY_DIRECTION		m_eDir = BODY_DIRECTION::DD;
	wstring				m_wstrDir[D_END];
	float				m_fLength = 0.f;

public:
	static CKnuckle_Body*	Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const int& iIndex = DD/*����*/);
};

#endif // !CKnuckle_Body_h__