#include "stdafx.h"
#include "Sludge_Shadow.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"
#include "CameraObserver.h"


CSludge_Shadow::CSludge_Shadow(LPDIRECT3DDEVICE9 pGraphicDev) :
	CSludgePart(pGraphicDev)
{
}


CSludge_Shadow::~CSludge_Shadow()
{
	Release();
}

HRESULT CSludge_Shadow::Initialize(const D3DXVECTOR3& vPos, const int& iIndex)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Shadow", m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos = vPos;
	m_iIndex = iIndex;

	ResetSize();
	InitSize();

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Shadow", m_pVertex);

	return S_OK;
}

void CSludge_Shadow::Release()
{
	ResetSize();
}

HRESULT CSludge_Shadow::AddComponent()
{
	Engine::CComponent*	pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Shadow");
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"AcidnerveShadowNo");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

void CSludge_Shadow::Update()
{
	// 컴포넌트 업데이트
	CSludgePart::Update();
	SetTransform();
	
}

void CSludge_Shadow::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Shadow", m_pConvertVertex);

	// 반투명 코드
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);

	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(100, 255, 255, 255));

	if (m_iIndex > 5)
		m_iIndex = 5;

	m_pTexture->Render(m_iIndex - 1);
	m_pBuffer->Render();
	// 반투명 해제
	//m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}


void CSludge_Shadow::SetTransform()
{
	const D3DXMATRIX* matView = m_pCameraObserver->GetView();
	const D3DXMATRIX* matProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, matView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, matProj);
}

bool CSludge_Shadow::IdleFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime)
{
	float fRatio = 0.7f;
	if (fSumTime < fPixTime)
	{
		m_pInfo->m_vScale.z += fPixTime * fDeltaTime * fRatio;
		m_pInfo->m_vScale.x += fPixTime * fDeltaTime * fRatio;
		return false;
	}
	else if ((fPixTime < fSumTime) && fSumTime < (fPixTime * 2.f))
	{
		m_pInfo->m_vScale.z -= fPixTime * fDeltaTime * fRatio;
		m_pInfo->m_vScale.x -= fPixTime * fDeltaTime * fRatio;
		return false;
	}
	else
		return true;

	return false;
}

bool CSludge_Shadow::JumpFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime)
{
	if (fSumTime < fPixTime)
	{
		m_pInfo->m_vScale.z -= fPixTime * fDeltaTime * 0.9f;
		m_pInfo->m_vScale.x -= fPixTime * fDeltaTime * 0.7f;
		return false;
	}
	return true;
}

bool CSludge_Shadow::FallFunc(const float& fDeltaTime, const float& fSumTime, const float& fPixTime)
{
	if (fSumTime < fPixTime)
	{
		m_pInfo->m_vScale.z += fPixTime * fDeltaTime * 0.9f;
		m_pInfo->m_vScale.x += fPixTime * fDeltaTime * 0.7f;
		return false;
	}
	return false;
}

bool CSludge_Shadow::SplitFunc(const float& fDeltaTime, const float& fSumTime, 
								const float& fPixTime, const bool& bJump)
{
	if (bJump)
		return JumpFunc(fDeltaTime, fSumTime, fPixTime);
	else
		return FallFunc(fDeltaTime, fSumTime, fPixTime);

	return false;
}

void CSludge_Shadow::ResetSize()
{
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };

	m_pInfo->m_vScale.z = 1.5f;
	m_pInfo->m_vScale.x = 1.f;

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Sludge_Shadow", m_pVertex);
}

void CSludge_Shadow::InitSize()
{
	m_pInfo->m_vScale.z *= m_iIndex;
	m_pInfo->m_vScale.x *= m_iIndex;
}

CSludge_Shadow* CSludge_Shadow::Create(LPDIRECT3DDEVICE9 pGraphicDev, const D3DXVECTOR3& vPos, const int& iIndex)
{
	CSludge_Shadow* pInstance = new CSludge_Shadow(pGraphicDev);

	if (FAILED(pInstance->Initialize(vPos, iIndex)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}