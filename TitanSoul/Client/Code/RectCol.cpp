#include "stdafx.h"
#include "RectCol.h"


CRectCol::CRectCol()
{
}


CRectCol::~CRectCol()
{
	Release();
}

void CRectCol::Release()
{
}

BOOL CRectCol::CheckCollision(const VEC3& vDstPos, const float& fDstRadiuss, const VEC3& vSrcPos, const float& fSrcRadiusss, RECT* ColRange)
{
	//float fRcLeft_1		= vDstPos.x - fDstRadiuss;
	//float fRcRight_1	= vDstPos.x + fDstRadiuss; 
	//float fRcTop_1		= vDstPos.z + fDstRadiuss;
	//float fRcBottom_1	= vDstPos.z - fDstRadiuss;

	//float fRcLeft_2		= vSrcPos.x - fSrcRadiusss;
	//float fRcRight_2	= vSrcPos.x + fSrcRadiusss;
	//float fRcTop_2		= vSrcPos.z + fSrcRadiusss;
	//float fRcBottom_2	= vSrcPos.z - fSrcRadiusss;

	RECT rc1 = { LONG(vDstPos.x - fDstRadiuss), LONG(vDstPos.z + fDstRadiuss), 
					LONG(vDstPos.x + fDstRadiuss), LONG(vDstPos.z - fDstRadiuss) };
	RECT rc2 = { LONG(vSrcPos.x - fSrcRadiusss), LONG(vSrcPos.z + fSrcRadiusss), 
					LONG(vSrcPos.x + fSrcRadiusss), LONG(vSrcPos.z - fSrcRadiusss) };
	
	if (IntersectRect(&rc1, &rc2, ColRange))
		return true;
	else
		return false;
	return false;
}

CRectCol* CRectCol::Create()
{
	return new CRectCol;
}

Engine::CCollision* CRectCol::Clone()
{
	(*m_pwRefCnt)++;
	return new CRectCol(*this);
}