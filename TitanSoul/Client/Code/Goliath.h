#ifndef Goliath_h__
#define Goliath_h__
#include "Export_Function.h"
#include "GameObject.h"
#include "CameraObserver.h"

#include "PlayerObserver.h"

#include "CollisionMgr.h"
#include "TerrainColl.h"

#include"GoliathLHand.h"
#include"GoliathRHand.h"
#include "Camera.h"

class CSphereColBox;

namespace Engine
{
	class CScene;
}
class CTexAni;

class CGoliath : public Engine::CGameObject
{
private:
	explicit CGoliath(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CGoliath();

public:


	const float & GetRadius();

	Engine::CTransform* GetInfo() { return m_pInfo; }
	D3DXMATRIX* GetWorldMatrix() {return m_pMatWorld;}

	CGoliathLHand* GetLHand() { return m_pLHand; }
	CGoliathRHand* GetRHand() { return m_pRHand; } 

	virtual void	Update(void);
	virtual void	Render(void);
	virtual		void SetTransform(void);
	void		SetTarget(D3DXVECTOR3 vTarget) { m_vTarget = vTarget; }
	virtual		void SetDirection(void);
	void SetWake() { m_bWake = true; }
	void SetDead() { m_bDead = true; }
	bool GetWake() { return m_bWake; }
	bool GetDead() { return m_bDead; }
	bool GetAttack() { return m_bAttack; }
private:
	void		Release(void);
	bool		m_bRoar = false;
	virtual		HRESULT AddComponent();
	virtual		HRESULT Initialize();
	void		Move();

private:
	bool m_bWake = false;
	bool m_bDead = false;
	bool m_bAttack = false;
	bool m_bChangeHand = false;

	D3DXVECTOR3 m_vTarget;
	CCameraObserver*				m_pCameraObserver = nullptr;

	CPlayerObserver*				m_pPlayerObserver = nullptr;

	float							m_fSpeed;

	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;
	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *					m_pTerrainCol = nullptr;


	CSphereColBox*			m_pSphereBox = nullptr;
	D3DXMATRIX* m_pMatWorld = nullptr;


public:
	void SetCamera(Engine::CCamera* pCamera) { m_pCamera = pCamera; }
private:
	Engine::CCamera* m_pCamera = nullptr;

	D3DXVECTOR3						m_vDestPos;
public:
	
	static CGoliath*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
private:
	wstring m_wstrStateKey; //GAMEOBJ�� �൵ ������...
	CTexAni* m_pTexAni = nullptr;

	int m_iIndex = 0;
	CGoliathLHand* m_pLHand = nullptr;
	CGoliathRHand* m_pRHand = nullptr;
private:
	Engine::CLayer* m_pLayer = nullptr;
};
#endif // Goliath_h__


