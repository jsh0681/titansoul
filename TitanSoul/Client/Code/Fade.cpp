#include "stdafx.h"
#include "Fade.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"

#define MAXALPHA 230
CFade::CFade(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement = Engine::Get_Management();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pTimeMgr = Engine::Get_TimeMgr();
}


CFade::~CFade()
{
	Release();
}

void CFade::Update()
{
	float fTime = m_pTimeMgr->GetTime();
	if (m_bOn)
	{
		if(m_bIsStart) //�����̸� ���� �������� �����
		{
			m_fAlpha += fTime * 75.f;
			m_iAlpha = (int)m_fAlpha;

			if (m_iAlpha >= MAXALPHA)
			{
				m_iAlpha = MAXALPHA;
				m_bOn = FALSE;
			}
		}
		else
		{
			m_fAlpha -= fTime * 75.f;
			m_iAlpha = (int)m_fAlpha;
			if (m_iAlpha <= 0)
			{
				m_iAlpha = 0;
				m_bOn = FALSE;
				g_SystemMode = FALSE;
			}
		}

		CGameObject::Update();
		SetTransform();

	}
}

void CFade::Render()
{
	if (m_bOn)
	{
		m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Fade", m_pConvertVertex);

		m_pGraphicDev->GetTransform(D3DTS_VIEW, &m_matView);
		m_pGraphicDev->GetTransform(D3DTS_PROJECTION, &m_matProj);

		m_pMatOrtho = m_pCameraObserver->GetOrtho();
		m_pGraphicDev->SetTransform(D3DTS_VIEW, &m_matIdentity);
		m_pGraphicDev->SetTransform(D3DTS_PROJECTION, m_pMatOrtho);


		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
		m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(m_iAlpha, 255, 255, 255));

		m_pTexture->Render(0);
		m_pBuffer->Render();

		m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

		m_pGraphicDev->SetTransform(D3DTS_VIEW, &m_matView);
		m_pGraphicDev->SetTransform(D3DTS_PROJECTION, &m_matProj);

	}
}

HRESULT CFade::AddComponent()
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_Fade");
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrImgName);
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

HRESULT CFade::Initialize()
{
	m_wstrImgName = L"UiDefaultFade";

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	D3DXMatrixIdentity(&m_matIdentity);

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];


	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_Fade", m_pVertex);
	InitVertex();

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = { 0.f, 0.f, 0.f };
	m_pInfo->m_vScale = { 800.f, 600.f, 0.f };

	return S_OK;
}

void CFade::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CFade::FadeOutStart()
{
	//��� -> �� -> ����� Scene�� Start
	//������ �������� ������Ƿ� FadeOut ȿ���̴�.
	if (!m_bOn)
	{
		m_bOn = TRUE;
		m_fAlpha = 0.f;
		m_iAlpha = 0;
		m_bIsStart = TRUE;
		g_SystemMode = TRUE;
	}
}

void CFade::FadeInStart()
{
	//�� -> ��� �� �ο��� Scene�� End
	//������ �������� ��ο����Ƿ� FadeInȿ���̴�.
	if (!m_bOn)
	{
		m_bOn = TRUE;
		m_fAlpha = MAXALPHA;
		m_iAlpha = MAXALPHA;
		m_bIsStart = FALSE;
		g_SystemMode = TRUE;
	}
}


void CFade::InitVertex()
{
	m_pVertex[0].vPos = { -1.f,  1.f,  0.f };
	m_pVertex[1].vPos = { 1.f,   1.f,  0.f };
	m_pVertex[2].vPos = { 1.f,  -1.f,  0.f };
	m_pVertex[3].vPos = { -1.f, -1.f,  0.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_Fade", m_pVertex);
}

void CFade::SetTransform()
{
	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
}


CFade* CFade::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CFade* pInstance = new CFade(pGraphicDev);
	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	return pInstance;
}