#include "stdafx.h"
#pragma once
class CFrameMgr
{
public:
	CFrameMgr();
	~CFrameMgr();

public:
	void InitFrame(float FramePerSec);
	bool LockFrame();

private:
	LARGE_INTEGER	m_CurTime;
	LARGE_INTEGER	m_OldTime;
	LARGE_INTEGER	m_CpuTick;	// CPU 진동수 (1초 주기로 진동하는 횟수)
	float			m_fDeltaTime;
	float			m_fSecPerFrame;	// 1 / 60 == 0.016

	int				m_iFps;
	TCHAR			m_szFPS[MIN_STR];
	float			m_fFpsTime;
};

