#include "stdafx.h"
#include "CeremonyEffect.h"

#include "CameraObserver.h"
#include "Scene.h"
#include "NonAniEffect.h"
#include "PlayerObserver.h"
#include "Player.h"

int CCeremonyEffect::m_iCount;

CCeremonyEffect::CCeremonyEffect(LPDIRECT3DDEVICE9 pGraphicDev)
	:CNormalEffect(pGraphicDev)
{
}


CCeremonyEffect::~CCeremonyEffect()
{
	Release();
}


HRESULT CCeremonyEffect::Initialize(NorEffect tNorEffect, wstring wstrBuffer, wstring wstrTex, D3DXMATRIX matParent, Engine::CScene* pStage, CPlayer* pPlayer)
{
	++m_iCount;
	//1번방향은 각도로 받은 방향벡터 방향벡터 그대로 받고 엑설으로 값을 더하자 기존과 같음.

	// 그 다음은 플레이어 포스값을 받고... 뱀처럼 휘면서 추적하자. 기존의 acc에 플레이어 포스를 넣고 dir을 pos처럼 사용하면 된다.
	// 그다음 공전은... 일단 매트릭스 받아놓고 나중에 생각 ㅇㅇ

	m_pScene = pStage;
	m_matParent = matParent;
	m_pPlayer = pPlayer;

	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pResourcesMgr = Engine::Get_ResourceMgr();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_dwVtxCnt = 4;
	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_tEffect = tNorEffect;
	m_wstrBufferKey = wstrBuffer;
	m_wstrTextureKey = wstrTex;
	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pVertex);
	InitVertex();

	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferKey);
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	m_mapComponent.emplace(L"Buffer", m_pBufferCom);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, wstrTex);
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	m_mapComponent.emplace(L"Texture", m_pTextureCom);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	m_pInfo->m_vDir = m_tEffect.vDir;
	m_pInfo->m_vPos = m_tEffect.vPos;

	m_vAccel = -m_pInfo->m_vDir;

	m_fScale = m_tEffect.fScale;
	m_pInfo->m_vScale = { m_tEffect.fScale, m_tEffect.fScale ,m_tEffect.fScale };
	//버텍스 초기화 해주고

	m_fLifeTime = m_tEffect.fLifeTime;
	m_fSpeed = m_tEffect.fSpd;



	//v1 = p2
	//	v2 = p3
	//	t1 = (p3 - p1) / 2

	//	t2 = (p4 - p2) / 2


	m_VStartPos = m_pInfo->m_vPos; // 2
	m_vDest1 = m_pInfo->m_vDir * 3.f;
	m_vDest1 = m_pInfo->m_vDir * 3.f;

	m_vDest2 = m_tEffect.vAcc; // 3
	m_vDest3 += m_vDest2 + m_pInfo->m_vDir * 5.f;

	return S_OK;
}

void CCeremonyEffect::InitVertex()
{
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[0].vTex = { 0, 0 };

	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[1].vTex = { 1, 0 };

	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[2].vTex = { 1, 1 };

	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };
	m_pVertex[3].vTex = { 0, 1 };
}

void CCeremonyEffect::Release(void)
{
	--m_iCount;
	if (m_iCount < 3)
	{
		m_iCount = 0;
		m_pPlayer->SetAct(CPlayer::ENDDANCE);
	}

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	if (nullptr != m_pVertex)
	{
		Engine::Safe_Delete_Array(m_pVertex);
	}

	if (nullptr != m_pConvertVertex)
	{
		Engine::Safe_Delete_Array(m_pConvertVertex);
	}
}

void CCeremonyEffect::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	D3DXMATRIX matRev;
	D3DXMATRIX matworld;

	m_tEffect.vAcc = m_pPlayerObserver->GetPlayerTransform()->m_vPos;
	m_matParent = m_pPlayerObserver->GetPlayerTransform()->m_matWorld;
	m_pInfo->m_fAngle[Engine::ANGLE_Y] = m_pPlayerObserver->GetPlayerTransform()->m_fAngle[Engine::ANGLE_Y];
	//m_vDest3 = m_pPlayerObserver->GetPlayerTransform()->m_vPos;

	m_fpoopTime += fTime;



	if (m_bLast)
	{
		m_pPlayer->SetAct(CPlayer::ACTOPTION::DANCE2);

		m_pInfo->m_vPos += m_vAccel * fTime * 0.3f;

		float fLength = D3DXVec3Length(&D3DXVECTOR3(m_pInfo->m_vPos - m_vAccel));

		if (fLength < 0.3f)
		{
			m_bIsDead = TRUE;
		}
		m_fAngle2 += 1.f * m_fSpeed;
		m_fSpeed += fTime * 3.f;
		D3DXMatrixRotationY(&matRev, -D3DXToRadian(m_fAngle2));

		Engine::CGameObject::Update();

		matworld = m_pInfo->m_matWorld * matRev * m_matParent;
		m_pInfo->m_matWorld = matworld;

		if (m_fpoopTime > 0.07f)
		{
			m_fpoopTime = 0.f;
			CreateBigPoop(D3DXVECTOR3(matworld._41, matworld._42 + 1.f, matworld._43));
		}

		SetTransform();
		return;
	}


	if (m_fpoopTime > 0.07f)
	{
		m_fpoopTime = 0.f;
		CreatePoop();
	}

	if (m_bAcc)
	{
		m_pPlayer->SetAct(CPlayer::ACTOPTION::DANCE1);

		m_fDonwtime += fTime;
		if (m_fDonwtime > 1.f)
		{
			m_fDonwtime = 0.f;
			m_bDown = !m_bDown;
			m_fSpeed = 1.5f;
			m_vSMove[0] = m_vAccSMove[1];
			m_vSMove[1] = m_vAccSMove[0];

		}
		m_pInfo->m_vPos += m_pInfo->m_vDir * fTime* 3.f;

		if (m_bDown)
		{
			m_pInfo->m_vPos += m_vSMove[0] * fTime* m_fSpeed;
			m_vSMove[0] += m_vAccSMove[0] * fTime;

		}
		else
		{
			m_pInfo->m_vPos += m_vSMove[1] * fTime* m_fSpeed;
			m_vSMove[1] += m_vAccSMove[1] * fTime;

		}

		m_fSpeed -= fTime;

		//m_fSpeed += fTime * m_fSpeed2;

		m_fScale -= fTime * m_tEffect.fDisappearSpd;
		if (m_fScale < 0.f)
		{
			m_fScale = 0.00f;
		}
		m_pInfo->m_vScale = { m_fScale, m_fScale ,m_fScale };


		//공전준비
		D3DXVECTOR3 temp = m_pInfo->m_vPos - m_tEffect.vAcc;
		float fLength = D3DXVec3Length(&temp);
		if (fLength < 3.f)
		{
			//공전시작핮.
			m_pInfo->m_vPos = m_tEffect.vAcc - m_pInfo->m_vPos;
			m_vAccel = -m_pInfo->m_vPos;
			D3DXVec3Normalize(&m_vAccel, &m_vAccel);
			m_fAngle2 = 0.f;
			m_fSpeed = 0.f;
			m_fLifeTime = 0.f;
			m_bLast = TRUE;
		}

	}
	else
	{
		D3DXVECTOR3 Temp;

		m_pInfo->m_vDir += m_vAccel * fTime * 0.1f;

		D3DXVECTOR3 vTemp = m_pInfo->m_vDir - m_vAccel;
		float fLength = D3DXVec3Length(&vTemp);
		if (fLength < 1.1f)
		{
			m_pInfo->m_vDir = m_tEffect.vAcc - m_pInfo->m_vPos;
			D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

			//각도를구해서
			float fcos = D3DXVec3Dot(&m_pInfo->m_vDir, &g_vRight);
			//x축양의 방향을 기준으로 플레이어가 위치한 코사인 세타 값 = 각도를 구할수있다.   
			fcos = acosf(fcos);//세타값만 빼옴  = 라디안.
			fcos = D3DXToDegree(fcos);//우리가 아는 그 각도로 변환이 완료되었음. 

			if (m_tEffect.vAcc.z > m_pInfo->m_vPos.z)
			{
				fcos *= -1;//z값이 아래면 음수로 만든다 각도를 
			}

			m_vSMove[0].x = cosf(D3DXToRadian(fcos + 90.f));
			m_vSMove[0].y = 0.f;
			m_vSMove[0].z = -sinf(D3DXToRadian(fcos + 90.f));
			D3DXVec3Normalize(&m_vSMove[0], &m_vSMove[0]);
			m_vAccSMove[0] = -m_vSMove[0];

			m_vSMove[1].x = cosf(D3DXToRadian(fcos - 90.f));
			m_vSMove[1].y = 0.f;
			m_vSMove[1].z = -sinf(D3DXToRadian(fcos - 90.f));
			D3DXVec3Normalize(&m_vSMove[1], &m_vSMove[1]);
			m_vAccSMove[1] = -m_vSMove[1];



			if (GetRandom<int>(0, 1) == 1)
			{
				m_bDown = TRUE;
			}
			else
				m_bDown = FALSE;

			m_fSpeed2 = m_fSpeed;
			m_fSpeed = 0.f;
			m_fSpeed = 1.5f;
			m_bAcc = TRUE;
		}
		else
		{
			m_pInfo->m_vPos += m_pInfo->m_vDir * fTime * m_fSpeed;
		}


	}

	if (m_bAngleDir)
	{
		m_fDegree += 0.5f;
	}
	else
		m_fDegree -= 0.5f;

	m_pInfo->m_fAngle[1] = D3DXToRadian(m_fDegree);

	Engine::CGameObject::Update();
	SetTransform();
}

void CCeremonyEffect::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pConvertVertex);


	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
	m_pGraphicDev->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB(180, 255, 255, 255));

	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);


	//m_pTextureCom->Render(0);
	//m_pBufferCom->Render();

	m_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);



	//m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	//m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	//m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_MAX);
	//m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	//m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);



}

void CCeremonyEffect::SetTransform()
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);


}

void CCeremonyEffect::CreatePoop()
{
	Engine::CGameObject* pGameObject = nullptr;
	//여기서 이펙트 뿅 고드름 깨질때 터지는 이펙트로
	NorEffect Temp;
	float fRand;

	Temp.fLifeTime = 1.5f;
	Temp.vPos = m_pInfo->m_vPos;
	Temp.fScale = 0.7f;
	Temp.vAcc = { 0.f, 0.f, 0.f };
	Temp.fDisappearSpd = 0.5f;

	//방향은.. 각도방향으로  for문...
	for (int i = 0; i < 2; ++i)
	{
		Temp.vPos = m_pInfo->m_vPos;
		fRand = GetRandom<float>(0.5f, 0.5f);
		//Temp.vPos.x = Temp.vPos.x + fRand;
		//Temp.vPos.z = Temp.vPos.z + fRand;
		//Temp.vPos.z -= 0.5f;

		Temp.vDir = { 0.f, 0.f, 0.f };

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_Effect", m_wstrTextureKey, 5);
		m_pScene->ObjListAdd(Engine::LAYER_GAMEOBJECT, L"Effect", pGameObject);
	}
}

void CCeremonyEffect::CreateBigPoop(const D3DXVECTOR3& vPos)
{
	Engine::CGameObject* pGameObject = nullptr;
	//여기서 이펙트 뿅 고드름 깨질때 터지는 이펙트로
	NorEffect Temp;

	Temp.fLifeTime = 1.0f;
	Temp.vPos = vPos;
	Temp.fScale = 0.7f;
	Temp.vAcc = { 0.f, 0.f, 0.f };
	Temp.fDisappearSpd = 0.5f;

	//방향은.. 각도방향으로  for문...
	for (int i = 0; i < 2; ++i)
	{
		Temp.vPos;

		Temp.vDir = { 0.f, 0.f, 0.f };

		D3DXVec3Normalize(&Temp.vDir, &Temp.vDir);

		pGameObject = CNonAniEffect::Create(m_pGraphicDev, Temp, L"Buffer_Effect", m_wstrTextureKey, 5);
		m_pScene->ObjListAdd(Engine::LAYER_GAMEOBJECT, L"Effect", pGameObject);
	}

}

CCeremonyEffect * CCeremonyEffect::Create(LPDIRECT3DDEVICE9 pGraphicDev, NorEffect tNorEffect, wstring wstrBuffer, wstring wstrTex, D3DXMATRIX matParent, Engine::CScene* pStage, CPlayer* pPlayer)
{
	CCeremonyEffect*	pInstance = new CCeremonyEffect(pGraphicDev);

	if (FAILED(pInstance->Initialize(tNorEffect, wstrBuffer, wstrTex, matParent, pStage, pPlayer)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

