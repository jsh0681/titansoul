#include "stdafx.h"
#include "TerrainColl.h"

#include "Include.h"
#include "Export_Function.h"

CTerrainColl::CTerrainColl(void)
{

}

CTerrainColl::~CTerrainColl(void)
{
	Release();
}

void CTerrainColl::Update()
{
	// ax + by + cz + d = 0;

	// by = -ax - cz - d;

	// y = (-ax - cz - d) / b;

	int	iIndex = int(m_pPos->z / VTXITV) * VTXCNTX + int(m_pPos->x / VTXITV);

	float	fRatioX = (m_pPos->x - m_pTerrainVtx[iIndex + VTXCNTX].vPos.x) / VTXITV;
	float	fRatioZ = (m_pTerrainVtx[iIndex + VTXCNTX].vPos.z - m_pPos->z) / VTXITV;

	D3DXPLANE	Plane;

	// 오른쪽 위
	if (fRatioX > fRatioZ)
	{
		D3DXPlaneFromPoints(&Plane, 
			&m_pTerrainVtx[iIndex + VTXCNTX].vPos,
			&m_pTerrainVtx[iIndex + VTXCNTX + 1].vPos,
			&m_pTerrainVtx[iIndex + 1].vPos);
	}
	// 왼쪽 아래
	else
	{
		D3DXPlaneFromPoints(&Plane,
			&m_pTerrainVtx[iIndex + VTXCNTX].vPos,
			&m_pTerrainVtx[iIndex + 1].vPos,
			&m_pTerrainVtx[iIndex].vPos);
	}

	m_pPos->y = (-Plane.a * m_pPos->x - Plane.c * m_pPos->z - Plane.d) / Plane.b;
	m_pPos->y += 1.f;

}

void CTerrainColl::Release(void)
{

}

void CTerrainColl::SetColInfo(D3DXVECTOR3* pPos, 
							const Engine::VTXTEX* pTerrainVtx)
{
	m_pPos = pPos;
	m_pTerrainVtx = pTerrainVtx;
}

CTerrainColl* CTerrainColl::Create(void)
{
	return new CTerrainColl;
}

Engine::CCollision* CTerrainColl::Clone(void)
{
	++(*m_pwRefCnt);

	return new CTerrainColl(*this);
}

