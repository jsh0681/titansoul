#ifndef Stage4_h__
#define Stage4_h__

/*!
* \class CStage4
*
* \간략정보 예티 맵
*
* \상세정보 초기화 완료
*
* \작성자 윤유석
*
* \date 1월 6 2019
*
*/


namespace Engine
{
	class CManagement;
	class CResourcesMgr;
	class CCamera;
}


#include "Scene.h"
#include"ParticleSystem.h"
class CTerrain;
class CShpereCol;
class CPlayer;
class CArrow;
class CYeti;
class CDisplay;

class CStage4 : public Engine::CScene
{
private:
	explicit CStage4(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
public:
	virtual ~CStage4();

private:
	virtual HRESULT Initialize();
	virtual void	Release();

	HRESULT		AddResources();
	HRESULT		Add_Environment_Layer();
	HRESULT		Add_GameObject_Layer();
	HRESULT		Add_UI_Layer();

	void		CeremonyYeah();
	void		SetStartPoint();
	void		CheckPlaneColTriger(const int& iTriger);


public:
	virtual void Update();
	virtual void Render();
	virtual void BloodMode();
	void ColPlayerMonster(const float& fTime);
	void ColMonsterArrow(const float& fTime);
	void ColPlaneSnowBullet(const float& fTime);
	void ColPlaneIcicleBullet(const float& fTime);
	void ColPlanePlayer(const float& fTime);
	void ColPlaneArrow(const float& fTime);
	void ColPlaneMonster(const float& fTime);
	void ColBulletPlayer(const float& fTime);
	void RemoveEffect();


public:
	void MakeSnowBullet(const D3DXVECTOR3& vPos, const D3DXVECTOR3& vDir);
	void MakeIcicleBullet(const D3DXVECTOR3& vPos);

private:
	Engine::CManagement*			m_pManagement;
	Engine::CResourcesMgr*			m_pResourcesMgr;
	Engine::CCamera*				m_pCamera;

	//파티클
	Engine::ParticleSystem* m_pParticle;

	//이펙트

	CTerrain*	m_pTerrain = nullptr;
	CPlayer*	m_pPlayer = nullptr;
	CArrow*		m_pArrow = nullptr;
	CYeti*		m_pYeti = nullptr;
	CDisplay*	m_pDisplay = nullptr;

	bool		m_pDieCheck = false;
	bool		m_bChangeScene = false;



	//충돌체크
	CShpereCol*						m_pShpereCol = nullptr;

public:
	static bool			m_sbStage4_Resource;
	static CStage4*		Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera);
};

#endif // !Stage4_h__