#ifndef Animation_h__
#define Animation_h__

/*!
 * \class CAnimation
 *
 * \간략정보 AniTex와 AniCube의 부모가 되는 클래스
 *
 * \상세정보 컴포넌트 클래스 상속함, Component의 Update 재정의했음.
 *
 * \작성자 윤유석
 *
 * \date 1월 3 2019
 *
 */

#include "Component.h"

namespace Engine
{
	class CVIBuffer;
	class CTexture;
	class CResourcesMgr;
	class CTimeMgr;
}

class CAnimation : public Engine::CComponent
{
protected:
	explicit CAnimation(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CAnimation();
	virtual void Release();

protected:
	virtual HRESULT CreateBuffer(const wstring& wstrBufferKey);
	virtual HRESULT CreateTexture(const vector<TEXINFO*>* pVecTexInfo);

public:
	virtual void Update();
	virtual void Render();
	
	bool CheckAniEnd()					{ return m_bEnd; }

public:
	virtual void SetTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj) PURE;
	virtual void SetRatioTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj) PURE;
	virtual void SetVecTexInfo(const vector<TEXINFO*>* pVecInfo);

	virtual const D3DXVECTOR3&	GetScale();
	virtual const D3DXVECTOR3&	GetRotation();
	virtual const D3DXVECTOR3&	GetMove();
	virtual const int&			GetIndex();

protected:
	LPDIRECT3DDEVICE9		m_pGraphicDev;
	Engine::CResourcesMgr*	m_pResourceMgr;
	Engine::CTimeMgr*		m_pTimeMgr;

	Engine::CVIBuffer*		m_pBuffer;
	Engine::CTexture*		m_pTexture;

	bool m_bEnd				= false;


	const vector<TEXINFO*>*	m_pVecTexInfo = nullptr;

	DWORD					m_dwVtxCnt	= 0;
	float					fTimer		= 0.f;
	int						m_iIndex	= 0;
	wstring					m_wstrBufferKey = L"";
};

#endif // !Animation_h__