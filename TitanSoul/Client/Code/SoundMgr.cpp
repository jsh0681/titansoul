#include "stdafx.h"
#include "SoundMgr.h"

IMPLEMENT_SINGLETON(CSoundMgr)

CSoundMgr::CSoundMgr()
{
}


CSoundMgr::~CSoundMgr()
{
	Release();
}

void CSoundMgr::Initialize()
{
	FMOD_System_Create(&m_pSystem);
	FMOD_System_Init(m_pSystem, SOUND_END, FMOD_INIT_NORMAL, nullptr);

	LoadSoundFile("../../Sound/*.*");
}

void CSoundMgr::Update()
{
	FMOD_System_Update(m_pSystem);
}

void CSoundMgr::PlaySoundw(const TCHAR* pSoundKey, SOUNDID eID)
{
	auto& iter_find = find_if(m_MapSound.begin(), m_MapSound.end(),
		[&pSoundKey](auto& MyPair)
	{
		return !lstrcmp(pSoundKey, MyPair.first);
	});

	if (m_MapSound.end() == iter_find)
		return;

	FMOD_System_PlaySound(m_pSystem, FMOD_CHANNEL_FREE, iter_find->second, FALSE,
		&m_pChannelArr[eID]);	
}

void CSoundMgr::PlayBGM(const TCHAR* pSoundKey)
{
	auto& iter_find = find_if(m_MapSound.begin(), m_MapSound.end(),
		[&pSoundKey](auto& MyPair)
	{
		return !lstrcmp(pSoundKey, MyPair.first);
	});

	if (m_MapSound.end() == iter_find)
		return;

	FMOD_System_PlaySound(m_pSystem, FMOD_CHANNEL_FREE, iter_find->second, FALSE,
		&m_pChannelArr[SOUND_BGM]);

	FMOD_Channel_SetMode(m_pChannelArr[SOUND_BGM], FMOD_LOOP_NORMAL);
}

void CSoundMgr::StopSound(SOUNDID eID)
{
	FMOD_Channel_Stop(m_pChannelArr[eID]);
}

void CSoundMgr::StopAll()
{
	for(int i = 0; i < SOUND_END; ++i)
		FMOD_Channel_Stop(m_pChannelArr[i]);
}

void CSoundMgr::Release()
{
	for_each(m_MapSound.begin(), m_MapSound.end(),
		[](auto& MyPair)
	{
		delete[] MyPair.first;
		FMOD_Sound_Release(MyPair.second);
	});

	m_MapSound.clear();

	FMOD_System_Release(m_pSystem);
	FMOD_System_Close(m_pSystem);
}

void CSoundMgr::LoadSoundFile(const char* pFilePath)
{
	_finddata_t fd = {};

	// pFilePath: "../Sound/*.*"
	int handle = _findfirst(pFilePath, &fd);

	if (0 == handle)
		return;

	int iLength = strlen(pFilePath);

	int i = 0;

	for (; i < iLength; ++i)
	{
		if ('*' == pFilePath[i])
			break;
	}

	char szRelative[256] = "";
	char szFullPath[256] = "";

	memcpy(szRelative, pFilePath, i);

	int result = 0;

	while (-1 != result)
	{
		strcpy_s(szFullPath, szRelative);
		strcat_s(szFullPath, fd.name);

		FMOD_SOUND* pSound = nullptr;

		FMOD_RESULT res = FMOD_System_CreateSound(m_pSystem, szFullPath,
			FMOD_HARDWARE, nullptr, &pSound);

		if (FMOD_OK == res)
		{
			int iBuffSize = strlen(fd.name) + 1;
			TCHAR* pSoundKey = new TCHAR[iBuffSize];

			MultiByteToWideChar(CP_ACP, 0, fd.name, iBuffSize, pSoundKey, iBuffSize);

			m_MapSound.insert({ pSoundKey, pSound });
		}

		result = _findnext(handle, &fd);
	}

	_findclose(handle);
	FMOD_System_Update(m_pSystem);
}
