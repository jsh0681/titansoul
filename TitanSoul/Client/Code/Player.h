#ifndef Player_h__
#define Player_h__

#include "GameObject.h"
#include "CameraObserver.h"
#include "PlayerObserver.h"

#include "CollisionMgr.h"
#include "TerrainColl.h"
#include "PlayerShadow.h"
namespace Engine
{
	class CScene;
	class CKeyMgr;
}

class CDisplay;
class CArrow;
class CTexAni;
class CSphereColBox;
class CSphereColBox;
class CStaticCamera2D;
class CStaticCamera;

class CPlayer : public Engine::CGameObject
{
private:
	explicit CPlayer(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CPlayer();
	enum ACTOPTION { NO_ACT, DANCE1, DANCE2, ENDDANCE};

public:
	void		Update(void);
	void		Render(void);

private:
	HRESULT		AddComponent(void);
	HRESULT		Initialize(void);
	void		KeySetting(void);

	void		Release(void);
	void		SetDirection(void);
	void		SetTransform(void);

	void		KeyInput(const float& _fTime);
	void		Move(const float& _fTime);
	void		Rolling(const float& _fTime);
	void		Aiming(const float& _fTime);


public:
	void		StopReverseRoll();
	void		SetAct(ACTOPTION iAcop) { m_dAct = iAcop; };
	void		SetCurse(bool bCurseOn) { m_bCurseOn = bCurseOn; }
	void					SavePos(const D3DXVECTOR3& _vPos) { m_vSavePos = _vPos; };
	const D3DXVECTOR3&		GetSavePos() { return m_vSavePos; };

public:
	void		ReverseMove(const float& _fTime);
	void		ReverseRoll(const float& _fTime);
	void		CreateArrow();
	const bool&	Get_Collision() { return m_bCollision; };
	
	CArrow*		Get_ArrowPointer() { return m_pArrow; };
	bool		GetHaveArrow() { return m_bArrow; }; //화살이 플레이어가 가지고 있는지 없는지;
	P_STATE& Get_PlayerState() { return m_State[Engine::CUR]; };
	Engine::DIRECTION& Get_PlayerDirect() { return m_DIR[Engine::CUR]; };
	const int	Get_AnimationIndex(); //애니메이션 인덱스가 몇번인지 반환.

	void		Return2DMode();
	void		Set_CollisionSpeed(const float& _fSpd) { m_fCollisionSpd = _fSpd; };
	void		Set_DoubleSpeed(const float& _fSpd) { m_fCollisionSpd = _fSpd; };

	void		CollisionSpeed_Default() { m_fCollisionSpd = 1.f; };
	void		GoUpStairs(Engine::DIRECTION _dDir, float _fDirection);

	DWORD GetDBoss() { return m_dBoss; };
	void		SetDBoss(DWORD dID);
	void		Set3DCamera(CStaticCamera* pCamera) { m_p3DCamera = pCamera; };
	void		SetTurn(bool bResult) { m_bTurn = bResult; };
	void		SetCamera(CStaticCamera2D* pCamera) { m_pCamera = pCamera; };
	void		SetPosXZ(const D3DXVECTOR3& vPos);
	void		SceneChange();
	void		SetDirUp();
	void		SetDirDown();
	void		SetChageStage() { m_bChangeStage = TRUE; };
	void		SetPlayerState(P_STATE _dwState) { m_State[Engine::CUR] = _dwState; };
	void		SetPush(const bool& _bResult) { m_bPush = _bResult; };
	void		SetPushPos(const VEC3& vPush);
	void		SetGetArrow() { m_bGetArrow = TRUE; };
	void		DirectChange(const float& _fAngle);
	void		RollVan();
	void		PlayerReset();
	void		CameraRelease();
	void		CameraCreate();
	void		SetSwim(const bool& bResult) { m_bSwim = bResult; };
	const bool&	GetSwim() { return m_bSwim; };
	const float&		GetRadius();
public:
	Engine::CTransform* Get_Trnasform() { return m_pInfo; };
private:
	CPlayerShadow* m_pShadow = nullptr;
	CCameraObserver*				m_pCameraObserver = nullptr;
	CPlayerObserver*				m_pPlayerObserver = nullptr;
	CSphereColBox*					m_pSphereColBox = nullptr;
	CStaticCamera2D*				m_pCamera = nullptr;
	CStaticCamera*					m_p3DCamera = nullptr;
	CDisplay*						m_pDisplay = nullptr;
	float							m_fSpeed = 0.f;
	const Engine::VTXTEX*			m_pTerrainVertex = nullptr;
	CCollisionMgr*					m_pCollisionMgr = nullptr;
	CTerrainColl *					m_pTerrainCol = nullptr;
	CMouseCol*						m_pMouseCol = nullptr;
	D3DXVECTOR3						m_vDestPos;
	D3DXVECTOR3						m_vSavePos;
	bool							m_bMove = FALSE; //움직이는가?
	bool							m_bRoll = FALSE; //구르는가?
	bool							m_bAim = FALSE; // 
	bool							m_bArrow = TRUE; //화살이 있는가?
													 //애니메이션

	float							m_fSoundTime = 0.f;
	float							m_fDegree = 0.f;
	bool							m_bTurn = FALSE;
	bool							m_bBanZoom = FALSE;
	ACTOPTION						m_dAct = NO_ACT;
public:
	static CPlayer*		Create(LPDIRECT3DDEVICE9 pGraphicDev);

	void SetScene(Engine::CScene* _pScene) { m_pScene = _pScene; };

private:
	Engine::CScene*					m_pScene = nullptr;
	Engine::CKeyMgr* m_pKeyMgr = nullptr;
	CTexAni* m_pTexAni = nullptr;
	P_STATE m_State[Engine::END_COMPARE];
	wstring m_wstrP_StateKey[END_PLAYER];
	wstring m_wstrStateKey; //GAMEOBJ에 줘도 괜찮고...

							//Move
	bool m_bCurseOn = FALSE;
	bool m_bCoolTime = FALSE;
	float fTime = 0.f;
	float m_fRoot2 = 0.f;
	float m_fAcceleration = 30.f;
	float m_fMoveCool = 3.f;
	float m_fRollCool = 0.f;


	//Arrow
	CArrow* m_pArrow = nullptr;
	float  m_fGauge = 0.f; //화살 파워 게이지.

	bool	m_bChangeStage = FALSE;
	bool	m_bFirstRecovery = TRUE; //첫번째 회수. 꾹누르고 있다가 떼고 다시 누르는 경우는 FALSE줘야함
	bool	m_bPressingKey = FALSE; // C버튼을 누르고있는상태 안누른상태를 구분하기 위해서임.
	bool	m_bFirstAim = TRUE; //줌을 처음 할때ㅣ...
	bool	m_bRecovery = FALSE; //리커버리중...
	bool	m_bGetArrow = FALSE; //리커버리하여 화살을 처음 먹은 상태.;

								 //Collision
	float  m_fCollisionSpd = 1.f; // 충돌로 인한 스피드 변경시 이 값을 조정하여 곱해준다.
	float  m_fDoubleSpd = 1.f;
	float m_fDanceTime = 0.f;
	//Push
	bool	m_bPush = FALSE; //미는상태...
	bool	m_bCollision = FALSE; //구르는도중 충돌하는상태.

								  //swiming
	bool	m_bSwim = FALSE; //수영하는상태

							 //Climb
	bool	m_bClimb = FALSE; //줄타는 상태

	float	m_fCeremonyTime = 0.f;

	DWORD	m_dBoss = 0;
public:
	void SetY(float  y);
};

#endif // Player_h__
