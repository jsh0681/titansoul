#include "stdafx.h"
#include "Map1.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"
#include "ShpereCol.h"
#include "Player.h"
#include "Arrow.h"
#include "ParticleSystem.h"
#include "ParticleMgr.h"
#include "Snow.h"
#include "Terrain.h"
#include "SceneSelector.h"
#include "Display.h"
//Obj
#include "Player.h"
#include "StaticCamera2D.h"
#include "OrthoCamera.h"
#include "Corver.h"

bool CMap1::m_sbMap1_Resource = false;
CMap1::CMap1(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera) :
	Engine::CScene(pGraphicDev),
	m_pPlayer(pPlayer),
	m_pCamera(pCamera),
	m_pManagement(Engine::Get_Management()),
	m_pResourcesMgr(Engine::Get_ResourceMgr())
{
	m_pDisplay = CDisplay::Create(m_pGraphicDev);
	m_pDisplay->StartFade(CDisplay::FADEIN);
}

CMap1::~CMap1()
{
	Release();
}

HRESULT CMap1::Initialize()
{

	m_pShpereCol = dynamic_cast<CShpereCol*>(CCollisionMgr::GetInstance()->CloneColObject(CCollisionMgr::COL_SHPERE));

	//FAILED_CHECK_RETURN(AddResource(), E_FAIL);

	AddResources();

	FAILED_CHECK_RETURN(Add_Environment_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_GameObject_Layer(), E_FAIL);
	FAILED_CHECK_RETURN(Add_UI_Layer(), E_FAIL);

	if(!m_sbMap1_Resource)
		SetStartingPoint();
	else
	{
		m_pPlayer->Get_Trnasform()->m_vPos = m_pPlayer->GetSavePos();
		dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();
	}
		
	Engine::tagBoundingBox boundingBox;
	boundingBox._max = D3DXVECTOR3(180.0f, 5.0f, 180.0f);
	boundingBox._min = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_pParticle = new Snow(&boundingBox, 1000);
	m_pParticle->Initialize(m_pGraphicDev, L"../Bin/Resources/Texture/Cube/snowflake.dds");

	/*PARTICLEINFO* pParticleInfo = const_cast<PARTICLEINFO*>(CParticleMgr::GetInstance()->GetParticleInfoData(L"ParticleSnowNo"));

	m_pParticle = new Snow(pParticleInfo);*/
	//m_pParticle->Initialize(m_pGraphicDev, L"../Bin/Resources/Texture/Cube/Boss/EyeCube/Attack/0.dds");

	m_sbMap1_Resource = TRUE;
	CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
	CSoundMgr::GetInstance()->PlayBGM(L"Map1.ogg");
	return S_OK;

}

void CMap1::Release()
{
	Engine::Safe_Delete(m_pDisplay);

	Engine::Safe_Delete(m_pParticle);
	Engine::Safe_Delete(m_pShpereCol);
	if (m_bChangeScene)
	{
		m_mapLayer[Engine::LAYER_GAMEOBJECT]->ClearList(L"Player");
		m_mapLayer[Engine::LAYER_UI]->ClearList(L"Camera");
	}
}

HRESULT CMap1::Add_Environment_Layer()
{
	return S_OK;
}

HRESULT CMap1::Add_GameObject_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	Engine::CGameObject*		pGameObject = nullptr;

	pLayer->AddObject(L"Player", m_pPlayer);
	pBlendLayer->AddObject(L"Player", m_pPlayer);
	m_pPlayer->SetScene(this);
	m_pPlayer->CameraCreate();
	m_pPlayer->CreateArrow();


	CArrow* pArrow = m_pArrow = m_pPlayer->Get_ArrowPointer();
	Engine::CGameObject* pObject = dynamic_cast<Engine::CGameObject*>(pArrow);
	pLayer->AddObject(L"Arrow", pObject);
	pBlendLayer->AddObject(L"Arrow", pObject);

	pGameObject = CTerrainMgr::GetInstance()->Clone(L"MapTerrainMap1");
	m_pTerrain = dynamic_cast<CTerrain*>(pGameObject);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Terrain", pGameObject);
	pNoBlendLayer->AddObject(L"Terrain", pGameObject);

	pGameObject = CCorver::Create(m_pGraphicDev, m_pTerrain, 3);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"Corver", pGameObject);
	pBlendLayer->AddObject(L"Corver", pGameObject);


	m_mapLayer.emplace(Engine::LAYER_GAMEOBJECT, pLayer);
	m_mapRenderLayer[Engine::LAYER_NONALPHA].emplace(Engine::LAYER_GAMEOBJECT, pNoBlendLayer);
	m_mapRenderLayer[Engine::LAYER_ALPHA].emplace(Engine::LAYER_GAMEOBJECT, pBlendLayer);

	//이펙트도 하나 만들고 빠져
	return S_OK;
}

HRESULT CMap1::Add_UI_Layer()
{
	Engine::CLayer*			pLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pLayer, E_FAIL);

	Engine::CLayer*			pNoBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pNoBlendLayer, E_FAIL);

	Engine::CLayer*			pBlendLayer = Engine::CLayer::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pBlendLayer, E_FAIL);

	pLayer->AddObject(L"Camera", m_pCamera);
	pNoBlendLayer->AddObject(L"Camera", m_pCamera);

	Engine::CGameObject * pGameObject = COrthoCamera::Create(m_pGraphicDev);
	NULL_CHECK_RETURN(pGameObject, E_FAIL);
	pLayer->AddObject(L"OrthoCamera", pGameObject);
	pNoBlendLayer->AddObject(L"OrthoCamera", pGameObject);

	m_mapLayer.emplace(Engine::LAYER_UI, pLayer);
	return S_OK;
}

void CMap1::AddResources()
{
}

void CMap1::SetStartingPoint()
{
	const D3DXVECTOR3& vStartPos = CTerrainMgr::GetInstance()->GetMapInfo(L"MapTerrainMap1")->vStartingPoint;
	m_pPlayer->SetPosXZ(vStartPos);
	m_pPlayer->SceneChange();
	dynamic_cast<CStaticCamera2D*>(m_pCamera)->FocusPlayer();
}

int CMap1::ColPlanePlayer()
{
	int iResult = m_pTerrain->CollisionPlane(m_pPlayer);
	return iResult;
}

void CMap1::ColPlaneArrow()
{
	m_pTerrain->CollisoinPlanetoArrowBackAgain(m_pArrow, m_pPlayer);
}

void CMap1::CheckPlaneColTriger(const int& iResult)
{
	D3DXVECTOR3 vTemp;
	//enum PLANETRIGGER { PL_NORMAL, PL_EXIT, PL_NEXT, PL_BOSS1, PL_BOSS2, PL_BOSS3, PL_BOSS4, PL_END };
	switch (iResult)
	{
	case PL_EXIT:
		break;

	case PL_BOSS2: // 예티보이
		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}
		if (m_pDisplay->CheckFadeFinish())
		{
			m_bChangeScene = true;
			m_pPlayer->CameraRelease();
			vTemp = { 69.f, 0.1f, 98.0f };
			m_pPlayer->SavePos(vTemp);
			m_pPlayer->SetDirUp();
			m_pManagement->SceneChange(CSceneSelector(SC_STAGE4, m_pPlayer, m_pCamera));
		}
		break;

	case PL_BOSS4: //아이스큐브
		if (!g_SystemMode)
		{
			m_pDisplay->StartFade(CDisplay::FADEOUT);
			m_pPlayer->SetPlayerState(P_MOVE);
		}
		if (m_pDisplay->CheckFadeFinish())
		{
			m_bChangeScene = true;
			vTemp = { 133.f, 0.1f, 74.0f };
			m_pPlayer->SavePos(vTemp);
			m_pPlayer->CameraRelease();
			m_pPlayer->SetDirUp();
			m_pManagement->SceneChange(CSceneSelector(SC_STAGE3, m_pPlayer, m_pCamera));
		}
		break;
	}

}

void CMap1::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	CScene::Update();

	// 충돌검사 함수
	int i = m_pTerrain->CollisionPlaneMap1(m_pPlayer);
	ColPlaneArrow();

	m_pParticle->Update(fTime);

	m_pDisplay->Update();

	CheckPlaneColTriger(i);
}

void CMap1::Render()
{
	m_pParticle->Render();

	for (auto& it : m_mapRenderLayer[Engine::LAYER_NONALPHA])
		it.second->Render();

	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pGraphicDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	m_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAREF, 0x00000040);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	for (auto& it : m_mapRenderLayer[Engine::LAYER_ALPHA])
		it.second->Render();

	m_pDisplay->Render();

	m_pGraphicDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

	m_pGraphicDev->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	m_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

CMap1* CMap1::Create(LPDIRECT3DDEVICE9 pGraphicDev, CPlayer* pPlayer, Engine::CCamera* pCamera)
{
	CMap1* pInstance = new CMap1(pGraphicDev, pPlayer, pCamera);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
