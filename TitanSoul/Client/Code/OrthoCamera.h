#ifndef OrthoCamera_h__
#define OrthoCamera_h__


#include "Camera.h"

namespace Engine
{
	class CInfoSubject;
}

class COrthoCamera : public Engine::CCamera
{
private:
	explicit COrthoCamera(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~COrthoCamera();

public:
	void	SetCameraTransform(const D3DXVECTOR3& vPos);

private:
	virtual HRESULT	Initialize();
	virtual void	Update(void);
	virtual void	Release();

private:
	void	SetOrthoMatrix(const float& fFovY, const float& fHeight, const float& fWidth);

private:
	Engine::CInfoSubject*	m_pInfoSubject;

	D3DXMATRIX	m_matOrtho;

public:
	static COrthoCamera*	Create(LPDIRECT3DDEVICE9 pGraphicDev);
};

#endif // !OrthoCamera_h__