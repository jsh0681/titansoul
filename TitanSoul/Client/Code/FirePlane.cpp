#include "stdafx.h"
#include "FirePlane.h"


// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

// Obj
#include "CameraObserver.h"
#include "AniEffect.h"
#include "SphereColBox.h"

CFirePlane::CFirePlane(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pManagement = Engine::Get_Management();
	m_pTimeMgr = Engine::Get_TimeMgr();
}

CFirePlane::~CFirePlane()
{
	Release();
}

HRESULT CFirePlane::AddComponent()
{
	Engine::CComponent*	pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferName);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer",pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Stage2FireParticlePlane");
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

HRESULT CFirePlane::CreateObj()
{
	return S_OK;
}

HRESULT CFirePlane::Initialize(const VEC3& vPos, const wstring& wstrBuffer)
{
	m_dwVtxCnt = 4;
	m_wstrBufferName = wstrBuffer;

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	FAILED_CHECK_RETURN(CreateObj(), E_FAIL);
	
	m_pInfo->m_vPos = vPos;
	m_pInfo->m_vScale = { 0.7f, 1.f, 0.7f };

	m_NoEffect.fLifeTime = 1.6f;
	m_NoEffect.fDisappearSpd = 0.3f;
	m_NoEffect.fScale = 0.1f;
	m_NoEffect.fSpd = 6.f;
	m_NoEffect.vAcc = { 0.f,0.f, 0.f }; //������� ������ ����
	m_NoEffect.vDir = { 0.f,0.f, 1.f };
	m_NoEffect.vPos = m_pInfo->m_vPos;
	m_NoEffect.vPos.y += 0.13f;
	m_NoEffect.vPos.z += 0.6f;

	for (int i = 0; i < m_iMaxFireEffect; ++i)
	{
		m_NoEffect.vPos.x = GetRandom<float>(-0.3f, 0.3f) + m_pInfo->m_vPos.x;

		m_FireEffectList.emplace_back(CAniEffect::Create(m_pGraphicDev, m_NoEffect,
			L"Buffer_FireEffect", L"Stage2FireParticleParticle"));
	}

	m_pColBox = CSphereColBox::Create(m_pGraphicDev, &m_pInfo->m_matWorld, 0.4f, true);
	ResetBuffer();

	return S_OK;
}

void CFirePlane::Release()
{
	for (auto& it : m_FireEffectList)
		Engine::Safe_Delete(it);
	m_FireEffectList.clear();

	Engine::Safe_Delete(m_pColBox);

	ResetBuffer();

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CFirePlane::Update()
{
	CGameObject::Update();
	SetTransform();

	if (m_bOn)
	{
		m_fLimit += m_pTimeMgr->GetTime() * 3.7f;
		m_fLifeTime += m_pTimeMgr->GetTime();

		if ((int)m_fLimit > m_iMaxFireEffect)
			m_fLimit = (float)m_iMaxFireEffect;

		int i = 0;
		for (auto& it : m_FireEffectList)
		{
			it->Update();
			if (it->CheckIsDead())
				it->ResetEffect();

			++i;
			if (i > (int)m_fLimit)
				break;
		}
	}

	CheckLifeTime();
}

void CFirePlane::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pConvertVertex);

	m_pTexture->Render(0);
	m_pBuffer->Render();

	if (m_bOn)
	{
		int i = 0;
		for (auto& it : m_FireEffectList)
		{
			it->Render();
			if (it->CheckIsDead())
				it->ResetEffect();

			++i;
			if (i > (int)m_fLimit)
				break;
		}
	}
}

void CFirePlane::ResetBuffer()
{
	// ���� �ʱ�ȭ
	m_pVertex[0].vPos = { -1.f, 0.f, 1.f };
	m_pVertex[1].vPos = { 1.f, 0.f, 1.f };
	m_pVertex[2].vPos = { 1.f, 0.f, -1.f };
	m_pVertex[3].vPos = { -1.f, 0.f, -1.f };

	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferName, m_pVertex);
}

void CFirePlane::SetTransform()
{
	const D3DXMATRIX* pMatView = m_pCameraObserver->GetView();
	const D3DXMATRIX* pMatProj = m_pCameraObserver->GetProj();

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}
	m_pGraphicDev->SetTransform(D3DTS_VIEW, pMatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pMatProj);
}

void CFirePlane::CheckLifeTime()
{
	if (m_fLifeTime > m_fMaxLifeTIme)
		m_bLife = false;
}

void CFirePlane::OnEruption()
{
	m_bOn = true;
	m_bLife = true;

	CSoundMgr::GetInstance()->PlaySoundw(L"Boss_Knuckle_Eruption.ogg", CSoundMgr::SOUND_BOSS);
	for (auto& it : m_FireEffectList)
		it->ResetEffect();

	m_fLimit = 0.f;
	m_fLifeTime = 0.f;
}

void CFirePlane::OffEruption()
{
	m_bOn = false;
	m_fLifeTime = 0.f;
}

const VEC3& CFirePlane::GetPos()
{
	return m_pInfo->m_vPos;
}

CFirePlane* CFirePlane::Create(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, const wstring& wstrBuffer)
{
	CFirePlane* pInstance = new CFirePlane(pGraphicDev);

	if (FAILED(pInstance->Initialize(vPos, wstrBuffer)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
