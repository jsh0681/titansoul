#include "stdafx.h"
#include "Corver.h"

#include "Export_Function.h"

#include "Include.h"

#include "Terrain.h"
#include "CameraObserver.h"

CCorver::CCorver(LPDIRECT3DDEVICE9 pGraphicDev) :
	Engine::CGameObject(pGraphicDev)
{
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pResourcesMgr = Engine::Get_ResourceMgr();
}

CCorver::~CCorver()
{
	Release();
}

HRESULT CCorver::Initialize(CTerrain* pTerrain, const int& iIndex)
{
	m_dwVtxCnt = 4;
	m_iIndex = iIndex;

	m_pTerrain = pTerrain;

	m_wstrBufferKey = m_pTerrain->GetDataInfo()->wstrMapBufferKey;
	m_wstrTexKey = m_pTerrain->GetDataInfo()->wstrMapTexKey;

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pVertex);
	
	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vPos.x += (m_pTerrain->GetDataInfo()->fSizeX / 2.f);
	m_pInfo->m_vPos.z += (m_pTerrain->GetDataInfo()->fSizeZ / 2.f);
	m_pInfo->m_vPos.y = 0.11f;

	return S_OK;
}

void CCorver::Release()
{
	m_pTerrain = nullptr;
	InitVertex();

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

HRESULT CCorver::AddComponent()
{
	Engine::CComponent*	pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrBufferKey);
	m_pBuffer = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBuffer, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, m_wstrTexKey);
	m_pTexture = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pTexture, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

void CCorver::Update()
{
	CGameObject::Update();
	SetTransform();
}

void CCorver::Render()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pConvertVertex);

	m_pTexture->Render(m_iIndex);
	m_pBuffer->Render();
}

void CCorver::SetTransform()
{
	const D3DXMATRIX*		pmatView = m_pCameraObserver->GetView();
	NULL_CHECK(pmatView);

	const D3DXMATRIX*		pmatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pmatProj);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
	}

	m_pGraphicDev->SetTransform(D3DTS_VIEW, pmatView);
	m_pGraphicDev->SetTransform(D3DTS_PROJECTION, pmatProj);
}

void CCorver::InitVertex()
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, m_wstrBufferKey, m_pVertex);
}

CCorver* CCorver::Create(LPDIRECT3DDEVICE9 pGraphicDev, CTerrain* pTerrain, const int& iIndex)
{
	CCorver* pInstance = new CCorver(pGraphicDev);
	if (FAILED(pInstance->Initialize(pTerrain, iIndex)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}
