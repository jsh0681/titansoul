#include "stdafx.h"
#include "Tutorial.h"

// Engine
#include "Export_Function.h"

// Client
#include "Include.h"

//Obj
#include "TexAni.h"
#include "CameraObserver.h"
#include "PlayerObserver.h"

#include "Player.h"

#include "TalkBallon.h"
#include "TalkWindow.h"
#include "SelectWindow.h"

bool CTutorial::m_bTutorialEnd = false;

CTutorial::CTutorial(LPDIRECT3DDEVICE9 pGraphicDev) :
	CNpc(pGraphicDev)
{
}

CTutorial::~CTutorial()
{
	Release();
}

HRESULT CTutorial::Initialize(const VEC3& vPos, CPlayer* pPlayer)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_wstrCurStateKey = L"BkeinIdleDD";
	SetStateKey();
	SetTrakingPos();
	SetTrakingDir();
	SetWeakList();

	m_pPlayer = pPlayer;

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);
	m_pInfo->m_vPos = vPos;
	//m_pInfo->m_vPos.z += 100.f;

	FAILED_CHECK_RETURN(InitTalkBalloon(), E_FAIL);
	FAILED_CHECK_RETURN(InitTalkWindow(), E_FAIL);
	FAILED_CHECK_RETURN(InitSelectWindow(), E_FAIL);

	if (m_bTutorialEnd)
	{
		m_eEvent = E_TIP;
		m_pTalkWindow->SetTalkEnd(true);

		m_pInfo->m_vPos = m_vTrakingPos[5];
		m_DIR[Engine::CUR] = Engine::RR;
		m_eState[Engine::CUR] = T_STATE::IDLE;
	}

	m_eState[Engine::PRE] = IDLE;
	m_eState[Engine::CUR] = IDLE;

	m_DIR[Engine::PRE] = Engine::DD;
	m_DIR[Engine::CUR] = Engine::DD;

	return S_OK;
}

HRESULT CTutorial::AddComponent()
{
	m_pTexAni = CTexAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrCurStateKey), L"Buffer_Tutorial");
	NULL_CHECK_RETURN(m_pTexAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pTexAni);

	m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(m_pInfo, E_FAIL);
	m_mapComponent.emplace(L"Transform", m_pInfo);

	return S_OK;
}

void CTutorial::Update()
{
	m_vPlayerPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;

	CheckEvent();

	CGameObject::Update();
	SetTransform();

	SceneChange();

	m_pTalkBalloon->SetPos(m_pInfo->m_vPos);
	m_pTalkBalloon->Update();
	m_pTalkWindow->Update();
	m_pSelectWindow->Update();
}

void CTutorial::Render()
{
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pTexAni->Render();
	m_pGraphicDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

	m_pTalkBalloon->Render();
	m_pTalkWindow->Render();
	m_pSelectWindow->Render();
}

void CTutorial::Release()
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

	Engine::Safe_Delete(m_pTalkBalloon);		// 말풍선
	Engine::Safe_Delete(m_pTalkWindow);			// 대화창
	Engine::Safe_Delete(m_pSelectWindow);		// 선택창
}

void CTutorial::SetStateKey()
{
	m_wstrStateKey[IDLE] = L"BkeinIdle";
	m_wstrStateKey[WALK] = L"BkeinWalk";
	m_wstrStateKey[ATTACK] = L"BkeinAttack";
}

void CTutorial::SetTrakingPos()
{
	m_vTrakingPos[0] = { 110.f, 0.1f, 63.5f };
	m_vTrakingPos[1] = { 125.f, 0.1f, 63.5f };
	m_vTrakingPos[2] = { 125.f, 0.1f, 70.f };
	m_vTrakingPos[3] = { 110.f, 0.1f, 70.f };
	m_vTrakingPos[4] = { 110.f, 0.1f, 76.f };
	m_vTrakingPos[5] = { 107.f, 0.1f, 76.f };
}

void CTutorial::SetTrakingDir()
{
	m_eEventDir[0] = Engine::RR;
	m_eEventDir[1] = Engine::UU;
	m_eEventDir[2] = Engine::LL;
	m_eEventDir[3] = Engine::UU;
	m_eEventDir[4] = Engine::RR;
}

void CTutorial::SetWeakList()
{
	m_wstrWeak[0] = L"물컹물컹한 녀석은 심장병이 있다더라";
	m_wstrWeak[1] = L"눈에는 눈이지";
	m_wstrWeak[2] = L"앉은뱅이 녀석은 등에 구멍이 났다는데..";
	m_wstrWeak[3] = L"돌덩이 녀석은 배탈이 났다는데?";
	m_wstrWeak[4] = L"설인 녀석, 엉덩이가 빨개";
	m_wstrWeak[5] = L"얼음 타입에는 불타입인데 말이야.";
}

void CTutorial::ShowTalkBalloon()
{
	if (m_bBalloon)
	{
		m_pTalkBalloon->SetOnOff(true);
		m_fBalloonCool += m_pTimeMgr->GetTime();

		if (m_fBalloonCool > m_fBalloonCoolTime)
		{
			m_pTalkBalloon->SetOnOff(false);
			m_fBalloonCool = 0.f;
			m_bBalloon = false;
		}
	}
	else
	{
		m_fBalloonRestTime += m_pTimeMgr->GetTime();

		if (m_fBalloonRestTime > m_fBalloonRestCoolTime)
		{
			m_pTalkBalloon->SetOnOff(true);
			m_fBalloonRestTime = 0.f;
			m_bBalloon = true;
		}
	}
}

HRESULT CTutorial::InitTalkBalloon()
{
	VEC3 vSize = { 1.5f, 1.f, 1.2f };
	m_pTalkBalloon = CTalkBallon::Create(m_pGraphicDev, m_pInfo->m_vPos, vSize ,0);
	NULL_CHECK_RETURN(m_pTalkBalloon, E_FAIL);
	m_pTalkBalloon->SetOnOff(false);

	return S_OK;
}

HRESULT CTutorial::InitTalkWindow()
{
	m_pTalkWindow = CTalkWindow::Create(m_pGraphicDev, L"잠깐. 거기 너 이리 좀 와봐. \n그래. 지금 갸웃거리는 너 말이야.\n(Space키를 누르면 대사가 빨리 넘어갑니다.)\n(Space키를 누르면 다음 대사로 넘어갑니다.)");
	NULL_CHECK_RETURN(m_pTalkWindow, E_FAIL);
	m_pTalkWindow->SetOnOff(false);

	return S_OK;
}

HRESULT CTutorial::InitSelectWindow()
{
	m_pSelectWindow = CSelectWindow::Create(m_pGraphicDev, L"네.", L"아뇨.", L"누구세요?\n결정도 Space키 입니다.");
	NULL_CHECK_RETURN(m_pSelectWindow, E_FAIL);
	m_pSelectWindow->SetOnOff(false);
	
	return S_OK;
}

void CTutorial::SceneChange()
{
	if (m_eState[Engine::PRE] != m_eState[Engine::CUR])
	{
		m_wstrCurStateKey = m_wstrStateKey[m_eState[Engine::CUR]];
		if (m_DIR[Engine::CUR] % 2 != 0 && m_DIR[Engine::CUR] != 0)
			m_wstrCurStateKey = m_wstrCurStateKey + g_wstrDir[m_DIR[Engine::CUR] + 1];

		else
			m_wstrCurStateKey = m_wstrCurStateKey + g_wstrDir[m_DIR[Engine::CUR]];

		m_eState[Engine::PRE] = m_eState[Engine::CUR];
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrCurStateKey));
	}

	if (m_DIR[Engine::PRE] != m_DIR[Engine::CUR])
	{
		m_wstrCurStateKey.pop_back();
		m_wstrCurStateKey.pop_back();

		if (m_DIR[Engine::CUR] % 2 != 0 && m_DIR[Engine::CUR] != 0)
		{
			if (m_DIR[Engine::CUR] == Engine::DIRECTION::UU)
				m_wstrCurStateKey += L"UU";
			else
			{
				m_wstrCurStateKey = m_wstrCurStateKey + g_wstrDir[m_DIR[Engine::CUR] + 1];
				m_pInfo->m_vScale = { -1.f, 1.f, 1.f };
			}
		}
		else
		{
			m_wstrCurStateKey = m_wstrCurStateKey + g_wstrDir[m_DIR[Engine::CUR]];
			m_pInfo->m_vScale = { 1.f, 1.f, 1.f };
		}
		m_DIR[Engine::PRE] = m_DIR[Engine::CUR];
		m_pTexAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrCurStateKey));
	}
}

void CTutorial::CheckEvent()
{
	switch (m_eEvent)
	{
	case CTutorial::E_NO:
		Event_No();
		break;
	case CTutorial::E_MEAT:
		Event_Meat();
		break;
	case CTutorial::E_ATTACK:
		Event_Attack();
		break;
	case CTutorial::E_WALK1:
		Event_Walk1();
		break;
	case CTutorial::E_ROLL:
		Event_Roll();
		break;
	case CTutorial::E_WALK2:
		Event_Walk2();
		break;
	case CTutorial::E_WALK3:
		Event_Walk3();
		break;
	case CTutorial::E_RUN:
		Event_Run();
		break;
	case CTutorial::E_WALK4:
		Evnet_Walk4();
		break;
	case CTutorial::E_WALK5:
		Event_Walk5();
		break;
	case CTutorial::E_BYE:
		Event_Bye();
		break;
	case CTutorial::E_WALK6:
		Event_Walk6();
		break;
	case CTutorial::E_TIP:
		Event_Tip();
		break;
	}
}

void CTutorial::Event_No()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 3.f)									// 가까이 왔을때 말이 걸린다
	{
		m_pTalkBalloon->SetOnOff(false);
		SetDir(SetAngle(m_vPlayerPos));

		m_pTalkWindow->SetOnOff(true);
		m_eEvent = E_MEAT;
		m_iEvent_TalkCnt = 0;
	}
	else
		TalkAfter(fDis);
}

void CTutorial::Event_Meat()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 5.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkWindow->SetTalk(L"이 게임은 친절하지 않으니,\n이 친절한 브케인님께서 가르쳐주지.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetTalk(L"준비는 됐겠지?");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					g_SystemMode = true;
					m_pTalkWindow->SetOnOff(false);
					m_pSelectWindow->SetOnOff(true);
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 3)
				{
					g_SystemMode = false;
					int iIndex = m_pSelectWindow->GetSelectIndex();
					m_pSelectWindow->SetOnOff(false);
					SetDir(SetAngle(m_vPlayerPos));

					if (iIndex == 0)
					{
						m_eState[Engine::CUR] = T_STATE::IDLE;
						m_pTalkWindow->SetTalk(L"좋아!\n그럼 바로 시작하자. 다시 말을 걸어줘.");
						m_pTalkWindow->SetOnOff(true);
						m_pTalkWindow->SetTalkEnd(false);
						++m_iEvent_TalkCnt;
					}
					else if (iIndex == 1)
					{
						m_eState[Engine::CUR] = T_STATE::ATTACK;
						m_pTalkWindow->SetTalk(L"나가.");
						m_pTalkWindow->SetOnOff(true);
						--m_iEvent_TalkCnt;
					}
					else if (iIndex == 2)
					{
						m_pTalkWindow->SetTalk(L"브케인.");
						m_pTalkWindow->SetOnOff(true);
						--m_iEvent_TalkCnt;
					}
				}
				else if (m_iEvent_TalkCnt == 4)
				{
					m_eState[Engine::CUR] = T_STATE::IDLE;
					m_pTalkWindow->SetTalkEnd(true);
					m_pTalkWindow->SetOnOff(false);
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 5)
				{
					m_pTalkWindow->SetTalkEnd(false);
					m_pTalkWindow->SetOnOff(true);
					m_pTalkWindow->SetTalk(L"공격하는 방법을 알려줄께. 공격은 'C'키를\n 누르고 있으면 화살을 장전하고 차지를 해. \n차지 시간에 따라 날아가는 화살의 거리가\n 조금 달라져.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 6)
				{
					m_pTalkWindow->SetTalk(L"마지막으로 키에서 손을 때면 날아가지.");
					m_iEvent_TalkCnt = 0;
					m_eEvent = E_EVENT::E_ATTACK;
				}
			}
		}
	}
}

void CTutorial::Event_Attack()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	CheckAttat();

	if (fDis < 5.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkWindow->SetTalk(L"가르쳐줬으니, 가서 한번 쏴 보고 와.\n난 맞지 않으니 쏠 생각은 하지 않는게\n좋을거야.하하");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetOnOff(false);
					m_pTalkWindow->SetTalkEnd(true);
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					SetDir(SetAngle(m_vPlayerPos));
					m_pTalkWindow->SetOnOff(true);
					if (m_bAttackEvent)
					{
						m_pTalkWindow->SetTalk(L"이제 화살을 쏜 상태에서\n'C'키를 다시 꾹 누르고 있으면 화살이\n 돌아올거야 화살을 회수하고 따라와.");
						++m_iEvent_TalkCnt;
					}
					else
					{
						m_pTalkWindow->SetTalk(L"한번 쏴보고 오라니까.");
						m_iEvent_TalkCnt = 1;
					}
				}
				else if (m_iEvent_TalkCnt == 3)
				{
					m_pTalkWindow->SetOnOff(false);
					m_eEvent = E_EVENT::E_WALK1;
					m_iEvent_TalkCnt = 0;
				}
			}
		}
	}
}

void CTutorial::Event_Walk1()
{
	VEC3 vDir = m_vTrakingPos[0] - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	SetDir(SetAngle(m_vTrakingPos[0]));
	float fLength = D3DXVec3Length(&vDir);

	D3DXVec3Normalize(&vDir, &vDir);

	m_pInfo->m_vPos += vDir * m_pTimeMgr->GetTime() * 3.f;
	m_pInfo->m_vPos.y = 0.1f;

	if (fLength < 0.1f)
		m_eEvent = E_EVENT::E_ROLL;
}

void CTutorial::Event_Roll()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);
	
	CheckRoll();
	if (fDis < 5.f)									// 가까이 왔을때 말을 건다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					SetDir(SetAngle(m_vPlayerPos));
					m_pTalkWindow->SetTalk(L"자자. 이제 구르기를 해보자고,\n'X'키를 누르면 바라보고 있는 방향으로\n 구를 수 있어. 해보고 말을 걸어줘");
					m_pTalkWindow->SetOnOff(true);
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetOnOff(false);
					m_pTalkWindow->SetTalkEnd(true);
					++m_iEvent_TalkCnt;
				}
				else if(m_iEvent_TalkCnt == 2)
				{
					SetDir(SetAngle(m_vPlayerPos));
					m_pTalkWindow->SetOnOff(true);
					if (m_bRollEvent)
					{
						m_pTalkWindow->SetTalk(L"너 정말 잘 구르는구나! 콩벌레인줄 알았네.");
						++m_iEvent_TalkCnt;
					}
					else
					{
						m_pTalkWindow->SetTalk(L"굴러보라니까");
						m_iEvent_TalkCnt = 1;
					}
				}
				else if (m_iEvent_TalkCnt == 3)
				{
					m_pTalkWindow->SetTalk(L"다음으로 넘어가자");
					++m_iEvent_TalkCnt;
					m_eState[Engine::CUR] = T_STATE::WALK;
				}
				else if(m_iEvent_TalkCnt == 4)
				{
					m_pTalkWindow->SetOnOff(false);
					m_pTalkWindow->SetTalkEnd(true);
					m_eEvent = E_EVENT::E_WALK2;
					m_iEvent_TalkCnt = 0;
					m_DIR[Engine::CUR] = Engine::RR;
				}
			}
		}
	}
}

void CTutorial::Event_Walk2()
{
	VEC3 vDir = m_vTrakingPos[1] - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	m_DIR[Engine::CUR] = Engine::RR;
	float fLength = D3DXVec3Length(&vDir);

	D3DXVec3Normalize(&vDir, &vDir);

	m_pInfo->m_vPos += vDir * m_pTimeMgr->GetTime() * 3.f;
	m_pInfo->m_vPos.y = 0.1f;

	m_pTalkBalloon->SetOnOff(false);
	if (fLength < 0.1f)
		m_eEvent = E_EVENT::E_WALK3;
}

void CTutorial::Event_Walk3()
{
	VEC3 vDir = m_vTrakingPos[2] - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	SetDir(SetAngle(m_vTrakingPos[2]));
	float fLength = D3DXVec3Length(&vDir);

	D3DXVec3Normalize(&vDir, &vDir);

	m_pInfo->m_vPos += vDir * m_pTimeMgr->GetTime() * 3.f;
	m_pInfo->m_vPos.y = 0.1f;

	m_pTalkBalloon->SetOnOff(false);
	if (fLength < 0.1f)
	{
		m_eEvent = E_EVENT::E_RUN;
		m_eState[Engine::CUR] = T_STATE::IDLE;
		m_DIR[Engine::CUR] = Engine::LL;
	}
}

void CTutorial::Event_Run()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	CheckRun();
	if (fDis < 5.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					SetDir(SetAngle(m_vPlayerPos));
					m_pTalkWindow->SetTalk(L"이번에는 뛰는 방법을 가르쳐주지.\n그런데, 이게 유용하진 않아.\n알아만 두라고");
					m_pTalkWindow->SetOnOff(true);
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetTalk(L"구르기 후에도 계속 'X'키를 누르고 있으면\n 케릭터가 뛰어다녀.\n한번 해봐");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					m_pTalkWindow->SetOnOff(false);
					m_pTalkWindow->SetTalkEnd(true);
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 3)
				{
					SetDir(SetAngle(m_vPlayerPos));
					m_pTalkWindow->SetOnOff(true);
					if (m_bRunEvent)
					{
						m_pTalkWindow->SetTalk(L"이제 내가 알려줄 수 있는건 전부 알려줬어.");
						++m_iEvent_TalkCnt;
					}
					else
					{
						m_pTalkWindow->SetTalk(L"뛰어.");
						m_iEvent_TalkCnt = 2;
					}
				}
				else if (m_iEvent_TalkCnt == 4)
				{
					m_pTalkWindow->SetTalk(L"나머지는 너의 손가락에 달려있어.");
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 5)
				{
					m_pTalkWindow->SetOnOff(false);
					m_pTalkWindow->SetTalkEnd(true);
					m_eEvent = E_EVENT::E_WALK4;
					m_eState[Engine::CUR] = T_STATE::WALK;
					m_iEvent_TalkCnt = 0;
				}
			}
		}
	}
}

void CTutorial::Evnet_Walk4()
{
	VEC3 vDir = m_vTrakingPos[3] - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	SetDir(SetAngle(m_vTrakingPos[3]));
	float fLength = D3DXVec3Length(&vDir);

	D3DXVec3Normalize(&vDir, &vDir);

	m_pInfo->m_vPos += vDir * m_pTimeMgr->GetTime() * 3.f;
	m_pInfo->m_vPos.y = 0.1f;

	m_pTalkBalloon->SetOnOff(false);
	if (fLength < 0.1f)
	{
		m_eEvent = E_EVENT::E_WALK5;
		m_DIR[Engine::CUR] = Engine::UU;
	}
}

void CTutorial::Event_Walk5()
{
	VEC3 vDir = m_vTrakingPos[4] - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	SetDir(SetAngle(m_vTrakingPos[4]));
	float fLength = D3DXVec3Length(&vDir);

	D3DXVec3Normalize(&vDir, &vDir);

	m_pInfo->m_vPos += vDir * m_pTimeMgr->GetTime() * 3.f;
	m_pInfo->m_vPos.y = 0.1f;

	m_pTalkBalloon->SetOnOff(false);
	if (fLength < 0.1f)
	{
		m_eEvent = E_EVENT::E_BYE;
		SetDir(SetAngle(m_vPlayerPos));
		m_eState[Engine::CUR] = T_STATE::IDLE;
		m_eState[Engine::PRE] = T_STATE::WALK;
	}
}

void CTutorial::Event_Bye()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 5.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					m_pTalkWindow->SetTalk(L"찾아오면 몬스터 놈들의 정보를 알려줄께. 잘가");
					m_pTalkWindow->SetOnOff(true);
					SetDir(SetAngle(m_vPlayerPos));

					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_eEvent = E_EVENT::E_WALK6;
					m_eState[Engine::CUR] = T_STATE::WALK;
					m_DIR[Engine::CUR] = Engine::LL;
					m_pTalkWindow->SetOnOff(false);
					m_iEvent_TalkCnt = 0;
				}
			}
		}
	}
}

void CTutorial::Event_Walk6()
{
	VEC3 vDir = m_vTrakingPos[5] - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	SetDir(SetAngle(m_vTrakingPos[5]));
	float fLength = D3DXVec3Length(&vDir);

	D3DXVec3Normalize(&vDir, &vDir);

	m_pInfo->m_vPos += vDir * m_pTimeMgr->GetTime() * 3.f;
	m_pInfo->m_vPos.y = 0.1f;

	m_pTalkBalloon->SetOnOff(false);
	if (fLength < 0.1f)
	{
		m_eEvent = E_EVENT::E_TIP;
		m_DIR[Engine::CUR] = Engine::RR;
		m_eState[Engine::CUR] = T_STATE::IDLE;
		m_bTutorialEnd = true;
		m_pTalkWindow->SetTalkEnd(true);
	}
}

void CTutorial::Event_Tip()
{
	VEC3 vDir = m_vPlayerPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;
	float fDis = D3DXVec3Length(&vDir);

	if (fDis < 5.f)									// 가까이 왔을때 말이 걸린다
	{
		if (m_pTalkWindow->GetTalkEnd())
		{
			if (m_pKeyMgr->KeyDown(KEY_SPACE))
			{
				if (m_iEvent_TalkCnt == 0)
				{
					SetDir(SetAngle(m_vPlayerPos));

					m_pTalkWindow->SetOnOff(true);
					m_pTalkWindow->SetTalkEnd(false);
					m_pTalkWindow->SetTalk(L"누구의 약점을 알려줄까나...");
					++m_iEvent_TalkCnt;

					m_iWeak = GetRandom<int>(0, 5);
				}
				else if (m_iEvent_TalkCnt == 1)
				{
					m_pTalkWindow->SetTalk(m_wstrWeak[m_iWeak]);
					++m_iEvent_TalkCnt;
				}
				else if (m_iEvent_TalkCnt == 2)
				{
					m_iEvent_TalkCnt = 0;
					m_pTalkWindow->SetOnOff(false);
					m_pTalkWindow->SetTalkEnd(true);
				}
			}
		}
	}
	else
		TalkAfter(fDis);
}

float CTutorial::SetAngle(const VEC3& vPos)
{
	VEC3 vDir = vPos - m_pInfo->m_vPos;		// 가까이 왔는지 검사
	vDir.y = 0.f;

	float fAngle = 0.f;
	D3DXVec3Normalize(&vDir, &vDir);
	fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vDir, &g_vLook)));

	if (vPos.x > m_pInfo->m_vPos.x)
		fAngle *= -1.f;

	return fAngle;
}

void CTutorial::SetDir(const float& fAngle)
{
	if (-22.5f <= fAngle	&& fAngle <= 22.5f)			m_DIR[Engine::CUR] = Engine::DIRECTION::UU;
	else if (22.5f <= fAngle	&& fAngle <= 67.5f)			m_DIR[Engine::CUR] = Engine::DIRECTION::UL;
	else if (67.5f <= fAngle	&& fAngle <= 112.5f)		m_DIR[Engine::CUR] = Engine::DIRECTION::LL;
	else if (112.5f <= fAngle	&& fAngle <= 157.5f)		m_DIR[Engine::CUR] = Engine::DIRECTION::DL;
	else if (-157.5f <= fAngle	&& fAngle <= -112.5f)		m_DIR[Engine::CUR] = Engine::DIRECTION::DR;
	else if (-112.5f <= fAngle  && fAngle <= -67.5f)		m_DIR[Engine::CUR] = Engine::DIRECTION::RR;
	else if (-67.5 <= fAngle	&& fAngle <= -22.5f)		m_DIR[Engine::CUR] = Engine::DIRECTION::UR;
	else													m_DIR[Engine::CUR] = Engine::DIRECTION::DD;
}

void CTutorial::TalkAfter(const float& fDis)
{
	if (m_pTalkWindow->GetOnOff())
	{
		if (fDis > 8.f)
		{
			m_pTalkWindow->SetOnOff(false);
			m_pTalkWindow->SetTalkEnd(true);
			m_eState[Engine::CUR] = T_STATE::IDLE;
		}
	}
	ShowTalkBalloon();
}

void CTutorial::CheckAttat()
{
	if ((m_eEvent == E_ATTACK) && (m_iEvent_TalkCnt == 2))
	{
		if (m_iAttackEventCnt == 0)
		{
			if (m_pKeyMgr->KeyPressing(KEY_C))
				++m_iAttackEventCnt;
		}
		else if (m_iAttackEventCnt == 1)
		{
			if (m_pKeyMgr->KeyUp(KEY_C))
				m_bAttackEvent = true;
		}

	}
}

void CTutorial::CheckRoll()
{
	if ((m_eEvent == E_EVENT::E_ROLL) && (m_iEvent_TalkCnt == 2))
	{
		if (m_pKeyMgr->KeyPressing(KEY_X))
			m_bRollEvent = true;
	}
}

void CTutorial::CheckRun()
{
	if ((m_eEvent == E_EVENT::E_RUN) && ((m_iEvent_TalkCnt == 2) || (m_iEvent_TalkCnt == 3)))
	{
		if (m_pPlayer->Get_PlayerState() == P_RUN)
			m_bRunEvent = true;
	}
}

void CTutorial::SetTransform()
{
	const D3DXMATRIX* pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);
	const D3DXMATRIX* pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pTexAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);
}

CTutorial* CTutorial::Creare(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, CPlayer* pPlayer)
{
	CTutorial* pInstance = new CTutorial(pGraphicDev);
	if (FAILED(pInstance->Initialize(vPos, pPlayer)))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}