#ifndef Tutorial_h__
#define Tutorial_h__



#include "Npc.h"
class CPlayerObserver;
class CPlayer;
class CTutorial : public CNpc
{
	enum T_STATE { IDLE, WALK, ATTACK, S_END };
	enum E_EVENT { E_NO, E_MEAT, E_ATTACK, E_WALK1, E_ROLL, E_WALK2, E_WALK3, E_RUN, E_WALK4,
					E_WALK5, E_BYE, E_WALK6, E_TIP, E_END };
private:
	explicit CTutorial(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CTutorial();

private:
	virtual HRESULT Initialize(const VEC3& vPos, CPlayer* pPlayer);
	virtual HRESULT AddComponent();
	virtual void Release();

	virtual HRESULT InitTalkBalloon()		override;
	virtual HRESULT InitTalkWindow()		override;
	HRESULT			InitSelectWindow();

private:
	void	SetTransform();
	void	SetStateKey();
	void	SetTrakingPos();
	void	SetTrakingDir();

	void	SetWeakList();

	void	ShowTalkBalloon();

	void	SceneChange();

	void	CheckEvent();

	void	Event_No();
	void	Event_Meat();
	void	Event_Attack();
	void	Event_Walk1();
	void	Event_Roll();
	void	Event_Walk2();
	void	Event_Walk3();
	void	Event_Run();
	void	Evnet_Walk4();
	void	Event_Walk5();
	void	Event_Bye();
	void	Event_Walk6();
	void	Event_Tip();

	float	SetAngle(const VEC3& vPos);
	void	SetDir(const float& fAngle);

	void	TalkAfter(const float& fDis);

	void	CheckAttat();
	void	CheckRoll();
	void	CheckRun();

public:
	virtual void Update();
	virtual void Render();

private:
	wstring					m_wstrStateKey[S_END];
	wstring					m_wstrCurStateKey;

	T_STATE					m_eState[Engine::END_COMPARE];

	VEC3					m_vTrakingPos[6];
	Engine::DIRECTION		m_eEventDir[5];

	CPlayer*				m_pPlayer;
	CPlayerObserver*		m_pPlayerObserver = nullptr;

	E_EVENT					m_eEvent = E_NO;

	VEC3					m_vPlayerPos;

	int						m_iEvent_TalkCnt = 0;

	bool					m_bBalloon = true;
	const float				m_fBalloonCoolTime = 3.f;
	float					m_fBalloonCool = 0.f;
	const float				m_fBalloonRestCoolTime = 2.f;
	float					m_fBalloonRestTime = 0.f;
	
	bool					m_bAttackEvent = false;
	int						m_iAttackEventCnt = 0;

	bool					m_bRollEvent = false;
	bool					m_bRunEvent = false;

	int						m_iWeak = 0;
	wstring					m_wstrWeak[6];

public:
	static CTutorial*	Creare(LPDIRECT3DDEVICE9 pGraphicDev, const VEC3& vPos, CPlayer* pPlayer);
	static bool	m_bTutorialEnd;
};

#endif // !Tutorial_h__