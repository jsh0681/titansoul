#ifndef CrossEffect_h__
#define CrossEffect_h__


#include "Effect.h"

class CCrossEffect : public CEffect
{
private:
	explicit CCrossEffect(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CCrossEffect();
	virtual void Release();
public:
	virtual void Render();
	virtual void SetTransform(const D3DXMATRIX* pMatWorld, const D3DXMATRIX* pMatView, const D3DXMATRIX* pMatProj)		override;

private:
	HRESULT	Initialize(EFFECTINFO* pEffectInfo);


public:
	static CCrossEffect* Create(LPDIRECT3DDEVICE9 pGraphicDev, EFFECTINFO* pEffectInfo);
};
#endif // RcEffect_h__
