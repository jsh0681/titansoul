#include "stdafx.h"
#include "BackGround.h"

#include "Export_Function.h"
#include "Include.h"

#include "CameraObserver.h"


CBackGround::CBackGround(LPDIRECT3DDEVICE9 pGraphicDev)
	: Engine::CGameObject(pGraphicDev)
	, m_pResourcesMgr(Engine::Get_ResourceMgr())
	, m_pTimeMgr(Engine::Get_TimeMgr()),
	m_pInfoSubject(Engine::Get_InfoSubject())
{
	
}

CBackGround::~CBackGround()
{
	Release();
}

void CBackGround::Update(void)
{
	CGameObject::Update();

	m_fFrameCnt += m_fFrameMax * m_pTimeMgr->GetTime() * 0.5f;
	
	if (m_fFrameCnt > m_fFrameMax)
		m_fFrameCnt = 0.f;

	SetTransform();
}

void CBackGround::Render(void)
{
	m_pResourcesMgr->ClientToEngine(Engine::RESOURCE_STATIC, L"Buffer_BackGround", m_pConvertVertex);

	m_pTextureCom->Render((WORD)m_fFrameCnt);
	m_pBufferCom->Render();
}

HRESULT CBackGround::AddComponent(void)
{
	Engine::CComponent*		pComponent = nullptr;

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Buffer_BackGround");
	m_pBufferCom = dynamic_cast<Engine::CVIBuffer*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Buffer", pComponent);

	pComponent = m_pResourcesMgr->Clone(Engine::RESOURCE_STATIC, L"Texture_Logo");
	m_pTextureCom = dynamic_cast<Engine::CTexture*>(pComponent);
	NULL_CHECK_RETURN(m_pBufferCom, E_FAIL);
	m_mapComponent.emplace(L"Texture", pComponent);

	// Transform
	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	return S_OK;
}

HRESULT CBackGround::Initialize(void)
{
	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_dwVtxCnt = 4;

	m_pVertex = new Engine::VTXTEX[m_dwVtxCnt];
	m_pConvertVertex = new Engine::VTXTEX[m_dwVtxCnt];

	m_pResourcesMgr->EngineToClient(Engine::RESOURCE_STATIC, L"Buffer_BackGround", m_pVertex);

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);

	m_pInfo->m_vScale = { 400.f,300.f,1.f };
	m_pInfo->m_vPos = { 0.f,0.f,0.f };

	return S_OK;
}

void CBackGround::Release(void)
{
	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	Engine::Safe_Delete_Array(m_pVertex);
	Engine::Safe_Delete_Array(m_pConvertVertex);
}

void CBackGround::SetTransform()
{
	D3DXMATRIX matView;
	const D3DXMATRIX* matOrtho = m_pCameraObserver->GetOrtho();

	D3DXMatrixIdentity(&matView);

	for (size_t i = 0; i < m_dwVtxCnt; ++i)
	{
		m_pConvertVertex[i] = m_pVertex[i];

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &m_pInfo->m_matWorld);
		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, &matView);
		if (m_pConvertVertex[i].vPos.z < 1.f)
			m_pConvertVertex[i].vPos.z = 1.f;

		Engine::CPipeline::MyTransformCoord(&m_pConvertVertex[i].vPos, &m_pConvertVertex[i].vPos, matOrtho);
	}
}

CBackGround* CBackGround::Create(LPDIRECT3DDEVICE9 pGraphicDev)
{
	CBackGround*	pInstance = new CBackGround(pGraphicDev);

	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);

	return pInstance;
}

