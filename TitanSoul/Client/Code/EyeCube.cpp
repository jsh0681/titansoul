#include "stdafx.h"
#include "EyeCube.h"
#include "Export_Function.h"
#include "Include.h"
#include"CubeAni.h"
#include "EyeCubeRaser.h"
#include<cmath>
#include"SphereColBox.h"
#include"Arrow.h"
#include"StaticCamera2D.h"


CEyeCube::CEyeCube(LPDIRECT3DDEVICE9 pGraphicDev)
	:Engine::CGameObject(pGraphicDev)
{

}

CEyeCube::~CEyeCube(void)
{
	Release();
}

const float & CEyeCube::GetRadius()
{

	return m_pSphereBox->GetRadius();
}

HRESULT CEyeCube::Initialize()
{
	m_pResourcesMgr = Engine::Get_ResourceMgr();
	m_pInfoSubject = Engine::Get_InfoSubject();
	m_pManagement = Engine::Get_Management();
	m_pCollisionMgr = CCollisionMgr::GetInstance();

	m_pCameraObserver = CCameraObserver::Create();
	m_pInfoSubject->Subscribe(m_pCameraObserver);

	m_pPlayerObserver = CPlayerObserver::Create();
	m_pInfoSubject->Subscribe(m_pPlayerObserver);

	m_wstrStateKey = L"BossEyeCubeSleep";

	FAILED_CHECK_RETURN(AddComponent(), E_FAIL);


	m_pCollisionWorld = new D3DXMATRIX;
	D3DXMatrixIdentity(m_pCollisionWorld);
	m_pSphereBox = CSphereColBox::Create(m_pGraphicDev, m_pCollisionWorld, 1.f, true);


	return S_OK;
}

HRESULT CEyeCube::AddComponent(void)
{
	Engine::CComponent* pComponent = nullptr;

	m_pCubeAni = CCubeAni::Create(m_pGraphicDev, CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey), L"Buffer_EyeCube");
	NULL_CHECK_RETURN(m_pCubeAni, E_FAIL);
	m_mapComponent.emplace(L"Texture", m_pCubeAni);

	pComponent = m_pInfo = Engine::CTransform::Create(g_vLook);
	NULL_CHECK_RETURN(pComponent, E_FAIL);
	m_mapComponent.emplace(L"Transform", pComponent);

	m_pInfo->m_vPos = D3DXVECTOR3(30.48f, 2.18f, 40.66f);

	m_pVertex = m_pCubeAni->GetVertex();
	m_pConvertVertex = m_pCubeAni->GetConvertVertext();

	m_pMatWorld = &(m_pInfo->m_matWorld);
	m_pMatView = const_cast<D3DXMATRIX*>(m_pCameraObserver->GetView());
	m_pMatProj = const_cast<D3DXMATRIX*>(m_pCameraObserver->GetProj());

	return S_OK;
}


void CEyeCube::Release()
{
	Engine::Safe_Delete(m_pCollisionWorld);
	Engine::Safe_Delete(m_pSphereBox);
	Engine::Safe_Delete(m_pRaser);

	m_pInfoSubject->UnSubscribe(m_pCameraObserver);
	Engine::Safe_Delete(m_pCameraObserver);

	m_pInfoSubject->UnSubscribe(m_pPlayerObserver);
	Engine::Safe_Delete(m_pPlayerObserver);

}

void CEyeCube::SetTransform()
{
	const D3DXMATRIX*		pMatView = m_pCameraObserver->GetView();
	NULL_CHECK(pMatView);

	const D3DXMATRIX*		pMatProj = m_pCameraObserver->GetProj();
	NULL_CHECK(pMatProj);

	m_pCubeAni->SetTransform(&m_pInfo->m_matWorld, pMatView, pMatProj);

}

void CEyeCube::Update()
{
	float fTime = Engine::Get_TimeMgr()->GetTime();
	m_iIndex = m_pCubeAni->GetIndex();
	if (m_iIndex == 12)
		CSoundMgr::GetInstance()->PlaySoundw(L"Boss_EyeCube_Charge.ogg", CSoundMgr::SOUND_BOSS);


	if (12 <= m_iIndex && m_iIndex <= 24)
		HitWeakness();
	
	if (m_pRaser != nullptr)
	{
		m_pRaser->SetStartPos(m_vPlayerPos);
		m_pRaser->Update();
	}

	D3DXMatrixIdentity(&m_pInfo->m_matWorld);

	vRight = D3DXVECTOR3(1.f, 0.f, 0.f);
	vUp = D3DXVECTOR3(0.f, 1.f, 0.f);
	vLook = D3DXVECTOR3(0.f, 0.f, 1.f);
	vPos = D3DXVECTOR3(0.f, 0.f, 0.f);

	CMathMgr::MyRotationX(&vRight, &vRight, m_pInfo->m_fAngle[Engine::ANGLE_X]);
	CMathMgr::MyRotationX(&vUp, &vUp, m_pInfo->m_fAngle[Engine::ANGLE_X]);
	CMathMgr::MyRotationX(&vLook, &vLook, m_pInfo->m_fAngle[Engine::ANGLE_X]);

	CMathMgr::MyRotationY(&vRight, &vRight, m_pInfo->m_fAngle[Engine::ANGLE_Y]);
	CMathMgr::MyRotationY(&vUp, &vUp, m_pInfo->m_fAngle[Engine::ANGLE_Y]);
	CMathMgr::MyRotationY(&vLook, &vLook, m_pInfo->m_fAngle[Engine::ANGLE_Y]);

	CMathMgr::MyRotationZ(&vRight, &vRight, m_pInfo->m_fAngle[Engine::ANGLE_Z]);
	CMathMgr::MyRotationZ(&vUp, &vUp, m_pInfo->m_fAngle[Engine::ANGLE_Z]);
	CMathMgr::MyRotationZ(&vLook, &vLook, m_pInfo->m_fAngle[Engine::ANGLE_Z]);

	vPos = m_pInfo->m_vPos;

	
	StateChange();
	Engine::CGameObject::Update();

	Move(fTime);

	SetDirection();
	SetTransform();
}


void CEyeCube::Render()
{
	m_pCubeAni->Render();
	m_pSphereBox->Render();
	if (m_pRaser != nullptr)
		m_pRaser->Render();

}


void CEyeCube::SetDirection(void)
{
	D3DXVec3TransformNormal(&m_pInfo->m_vDir, &g_vLook, &m_pInfo->m_matWorld);
	D3DXVec3Normalize(&m_pInfo->m_vDir, &m_pInfo->m_vDir);

}

void CEyeCube::StateChange()
{
	if (m_bWake)
	{
		m_bCalculate = true;
		m_wstrStateKey = L"BossEyeCubeAttack";
		m_pCubeAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
		m_bWake = false;
		m_bAttack = true;
	}
	
	if (m_bDead)
	{
		m_bAttack = false;
		m_bCalculate = false;	
		m_bMoveUpZ = false;
		m_bMoveDownZ = false;
		m_bMoveRightX = false;
		m_bMoveLeftX = false;
		m_wstrStateKey = L"BossEyeCubeDead";
		m_pCubeAni->SetVecTexInfo(CTextureInfoMgr::GetInstance()->GetTexInfoDataVec(m_wstrStateKey));
	}

	if (!m_bRaserOn)
	{
		if (14 == m_iIndex)
		{
			if ((m_pInfo->m_vPos.x - TILE_SIZE*2 - 2.03f < m_vPlayerPos.x&&m_vPlayerPos.x < m_pInfo->m_vPos.x - 2.03f)
				&& (m_pInfo->m_vPos.z - 2.03f < m_vPlayerPos.z&&m_vPlayerPos.z < m_pInfo->m_vPos.z + 2.03f))
			{
				m_pRaser = CEyeCubeRaser::Create(m_pGraphicDev, m_pInfo->m_vPos);
				NULL_CHECK(m_pRaser);
				m_bCurseOn = true;
			}
			else if ((m_pInfo->m_vPos.x + 2.03f < m_vPlayerPos.x&&m_vPlayerPos.x < m_pInfo->m_vPos.x + TILE_SIZE*2 + 2.03f)
				&& (m_pInfo->m_vPos.z - 2.03f < m_vPlayerPos.z&&m_vPlayerPos.z < m_pInfo->m_vPos.z + 2.03f))
			{
				m_pRaser = CEyeCubeRaser::Create(m_pGraphicDev, m_pInfo->m_vPos);
				NULL_CHECK(m_pRaser);
				m_bCurseOn = true;
			}
			else if ((m_pInfo->m_vPos.x - 2.03f < m_vPlayerPos.x&&m_vPlayerPos.x < m_pInfo->m_vPos.x + 2.03f)
				&& (m_pInfo->m_vPos.z - 2.03f  < m_vPlayerPos.z&&m_vPlayerPos.z < m_pInfo->m_vPos.z - TILE_SIZE*2 - 2.03f))
			{
				m_pRaser = CEyeCubeRaser::Create(m_pGraphicDev, m_pInfo->m_vPos);
				NULL_CHECK(m_pRaser);
				m_bCurseOn = true;
			}
			else if ((m_pInfo->m_vPos.x - 2.03f < m_vPlayerPos.x&&m_vPlayerPos.x < m_pInfo->m_vPos.x + 2.03f)
				&& (m_pInfo->m_vPos.z + 2.03f < m_vPlayerPos.z&&m_vPlayerPos.z < m_pInfo->m_vPos.z + TILE_SIZE*2 + 2.03f))
			{
				m_pRaser = CEyeCubeRaser::Create(m_pGraphicDev, m_pInfo->m_vPos);
				NULL_CHECK(m_pRaser);
				m_bCurseOn = true;
			}
			m_bRaserOn = true;
		}
	}
	else
	{
		if (m_bCurseOn)
		{
			m_fCurseTime += Engine::Get_TimeMgr()->GetInstance()->GetTime();
			if (m_fCurseTime > 1.825f&&m_pRaser != nullptr)
			{
				Engine::Safe_Delete(m_pRaser);
				m_fCurseTime = Engine::Get_TimeMgr()->GetInstance()->GetTime();
				m_bCurseOn = false;
			}
		}
		else
			m_bRaserOn = false;
	}
	if (m_bCurseOn)
	{
		dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(0.1f, 2.0f, 2.5f);
	}
}

void CEyeCube::SetRotateAngle()
{
	m_pInfo->m_fAngle[Engine::ANGLE_X] = D3DXToRadian(m_fAngleX);
	m_pInfo->m_fAngle[Engine::ANGLE_Z] = D3DXToRadian(m_fAngleZ);
}

void CEyeCube::SpreadPlayer(D3DXVECTOR3 vPlayerPos, D3DXVECTOR3 vEyeCubePos, float fRange)
{
	if ((vEyeCubePos.x - fRange < vPlayerPos.x) && (vPlayerPos.x < vEyeCubePos.x + fRange)
		&& (vEyeCubePos.z - fRange < vPlayerPos.z) && (vPlayerPos.z < vEyeCubePos.z + fRange))
	{
		m_bPlayerDead = true;
	}
	else
		m_bPlayerDead = false;
}

CEyeCube* CEyeCube::Create(LPDIRECT3DDEVICE9 pGraphicDev, D3DXVECTOR3 vStartPos)
{
	CEyeCube* pInstance = new CEyeCube(pGraphicDev);
	if (FAILED(pInstance->Initialize()))
		Engine::Safe_Delete(pInstance);
	//pInstance->SetStartPos(vStartPos);

	return pInstance;
}

void CEyeCube::ReadjustmentAxis()
{
	for (int i = 0; i < 8; i++)
	{
		m_pVertex[i].vPos = m_pConvertVertex[i].vPos - m_pInfo->m_vPos;
		m_pVertex[i].vPos.x = floor(m_pVertex[i].vPos.x + 0.5f);
		m_pVertex[i].vPos.y = floor(m_pVertex[i].vPos.y + 0.5f);
		m_pVertex[i].vPos.z = floor(m_pVertex[i].vPos.z + 0.5f);
	}
}
void CEyeCube::PlayerInTile()
{
	m_vPlayerPos = m_pPlayerObserver->GetPlayerTransform()->m_vPos;//한번만 좌표를 받고 잠궈버림 한바퀴 돌면 다시 좌표 받음
	for (int i = 0; i < 8; ++i)
	{
		float fStartX = 16.27f + (TILE_SIZE*i);
		float fEndX = 16.27f + (TILE_SIZE*(i + 1));

		float fMaxZ = 46.75f - (TILE_SIZE*i);
		float fMinZ = 46.75f - (TILE_SIZE*(i + 1));

		if (fStartX < m_vPlayerPos.x&&m_vPlayerPos.x < fEndX)
		{
			m_vCalPlayerPos.x = fStartX + 2.03f;
		}
		if (fMinZ < m_vPlayerPos.z&&m_vPlayerPos.z < fMaxZ)
		{
			m_vCalPlayerPos.z = fMaxZ - 2.03f;
		}
		m_vCalPlayerPos.y = 0.1f;
	}
}

void CEyeCube::Move(float fDeltaTime)
{
	//1.03f; 4.f 88f
	PlayerInTile();

	if (m_bCalculate)//연산을 해라 가던 방향을 바꿀수있게 해줘라 
	{
		m_bMoveUpZ = false;
		m_bMoveDownZ = false;
		m_bMoveRightX = false;
		m_bMoveLeftX = false;
		m_bCalculate = false;

		m_fAngleZ = 0.f;
		m_fAngleX = 0.f;

		float fX = fabs(m_vCalPlayerPos.x - m_pInfo->m_vPos.x);
		float fZ = fabs(m_vCalPlayerPos.z - m_pInfo->m_vPos.z);


		if (fX < fZ)// x가 더 
		{
			if (m_vCalPlayerPos.z > m_pInfo->m_vPos.z)//나보다 위에있으니까 위로 따라가자 
			{
				m_bMoveUpZ = true;
			}
			else
			{
				m_bMoveDownZ = true;
			}

		}
		else if (fX > fZ)//차이가 더 큰 방향으로 이동 해야함   z가 더가까우면 z축회전
		{
			if (m_vCalPlayerPos.x > m_pInfo->m_vPos.x)//나보다 오른쪽 에있으니까 오른쪽 ++로 따라가자 
			{
				m_bMoveRightX = true;
			}
			else
			{
				m_bMoveLeftX = true;
			}
		}
	}
	else
	{
		if (m_bMoveUpZ)
		{
			fRotateTime += Engine::Get_TimeMgr()->GetTime();

			if (fRotateTime < EYECUBE_MOVEING_TIME) //딱 한블럭 만 갈거야
			{
				if (fRotateTime >= EYECUBE_MOVEING_TIME - 0.01f)
				{
					SpreadPlayer(m_vCalPlayerPos, m_pInfo->m_vPos, 0.2f);
				}
				m_pInfo->m_vPos.z += EYECUBE_MOVEING_VALUE*fDeltaTime;
				m_fAngleX += EYECUBE_ROTATING_ANGLE*fDeltaTime;
			}
			else
				RaserPattern();
		}

		else if (m_bMoveDownZ)
		{
			fRotateTime += Engine::Get_TimeMgr()->GetTime();
			if (fRotateTime < EYECUBE_MOVEING_TIME) //딱 한블럭 만 갈거야
			{
				if (fRotateTime >= EYECUBE_MOVEING_TIME - 0.01f)
				{
					SpreadPlayer(m_vCalPlayerPos, m_pInfo->m_vPos, 0.2f);
				}
				m_pInfo->m_vPos.z -= EYECUBE_MOVEING_VALUE*fDeltaTime;
				m_fAngleX -= EYECUBE_ROTATING_ANGLE*fDeltaTime;
			}
			else
				RaserPattern();
		}

		else if (m_bMoveRightX)
		{
			fRotateTime += Engine::Get_TimeMgr()->GetTime();
			if (fRotateTime < EYECUBE_MOVEING_TIME) //딱 한블럭 만 갈거야
			{
				if (fRotateTime >= EYECUBE_MOVEING_TIME - 0.01f)
				{
					SpreadPlayer(m_vCalPlayerPos, m_pInfo->m_vPos, 0.2f);
				}
				m_pInfo->m_vPos.x += EYECUBE_MOVEING_VALUE*fDeltaTime;
				m_fAngleZ -= EYECUBE_ROTATING_ANGLE*fDeltaTime;

			}
			else
				RaserPattern();
		}

		else if (m_bMoveLeftX)
		{
			fRotateTime += Engine::Get_TimeMgr()->GetTime();

			if (fRotateTime < EYECUBE_MOVEING_TIME) //딱 한블럭 만 갈거야
			{
				if (fRotateTime >= EYECUBE_MOVEING_TIME - 0.01f)
				{
					SpreadPlayer(m_vCalPlayerPos, m_pInfo->m_vPos, 0.2f);
				}
				m_pInfo->m_vPos.x -= EYECUBE_MOVEING_VALUE*fDeltaTime;
				m_fAngleZ += EYECUBE_ROTATING_ANGLE*fDeltaTime;
			}
			else
				RaserPattern();
		}
	}
	SetRotateAngle();
}

void CEyeCube::RaserPattern()
{
	if (iRollingCount == 3)//3번구르고 4번째에 레이저 발사
	{
		m_fRaserTime += Engine::Get_TimeMgr()->GetTime();
		if (m_fRaserTime <= EYECUBE_RASER_TIME)
		{
			if (m_fRaserTime < 0.05f)
			{
				CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BOSS);
				CSoundMgr::GetInstance()->PlaySoundw(L"Boss_EyeCube_Laser.ogg", CSoundMgr::SOUND_BOSS);
			}
		}
		else
		{
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BOSS);
			CSoundMgr::GetInstance()->PlaySoundw(L"Boss_EyeCube_Move.ogg", CSoundMgr::SOUND_BOSS);
			ReadjustmentAxis();
			dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(0.1f, 1.0f, 5.5f);
			m_fRaserTime = Engine::Get_TimeMgr()->GetTime();
			fRotateTime = Engine::Get_TimeMgr()->GetTime();
			iRollingCount = 0;
			m_bCalculate = true;
		}
	}
	else
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"Boss_EyeCube_Move.ogg", CSoundMgr::SOUND_BOSS);
		dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(0.1f, 1.0f, 5.5f);
		ReadjustmentAxis();
		fRotateTime = Engine::Get_TimeMgr()->GetTime();
		iRollingCount += 1;
		m_bCalculate = true;
	}
}

void CEyeCube::HitWeakness()
{

	float fU, fV, fDist;
	m_vRayPos = m_pArrow->Get_Trnasform()->m_vPos;
	m_vRayDir = m_vRayPos - m_vPlayerPos;

	D3DXVec3Normalize(&m_vRayDir, &m_vRayDir);

	if (D3DXIntersectTri(&m_pConvertVertex[4].vPos,
		&m_pConvertVertex[5].vPos,
		&m_pConvertVertex[1].vPos,
		&m_vPlayerPos, &m_vRayDir, &fU, &fV, &fDist))
	{
		if (m_pInfo->m_vPos.x - 2.1f < m_pArrow->Get_Trnasform()->m_vPos.x&&m_pArrow->Get_Trnasform()->m_vPos.x < m_pInfo->m_vPos.x + 2.1f
			&&m_pInfo->m_vPos.y - 2.1f < m_pArrow->Get_Trnasform()->m_vPos.y&&m_pArrow->Get_Trnasform()->m_vPos.y < m_pInfo->m_vPos.y + 2.1f
			&&m_pInfo->m_vPos.z - 2.1f < m_pArrow->Get_Trnasform()->m_vPos.z&&m_pArrow->Get_Trnasform()->m_vPos.z < m_pInfo->m_vPos.z + 2.1f)
		{
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
			m_bDead = true;
			g_BattleMode = false;
			dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(0.5f, 1.0f, 3.5f);
			m_pArrow->StopArrow();
			return;
		}
	}

	// 왼쪽 아래
	if (D3DXIntersectTri(&m_pConvertVertex[4].vPos,
		&m_pConvertVertex[1].vPos,
		&m_pConvertVertex[0].vPos,
		&m_vRayPos, &m_vRayDir, &fU, &fV, &fDist))
	{

		if (m_pInfo->m_vPos.x - 2.1f < m_pArrow->Get_Trnasform()->m_vPos.x&&m_pArrow->Get_Trnasform()->m_vPos.x < m_pInfo->m_vPos.x + 2.1f
			&&m_pInfo->m_vPos.y - 2.1f < m_pArrow->Get_Trnasform()->m_vPos.y&&m_pArrow->Get_Trnasform()->m_vPos.y < m_pInfo->m_vPos.y + 2.1f
			&&m_pInfo->m_vPos.z - 2.1f < m_pArrow->Get_Trnasform()->m_vPos.z&&m_pArrow->Get_Trnasform()->m_vPos.z < m_pInfo->m_vPos.z + 2.1f)
		{
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::SOUND_BGM);
			m_bDead = true;
			g_BattleMode = false;
			dynamic_cast<CStaticCamera2D*>(m_pCamera)->ShakingStart(0.5f, 1.0f, 3.5f);
			m_pArrow->StopArrow();
			return;
		}
	}
}
