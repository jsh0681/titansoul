#ifndef __ParticleSystemH__
#define __ParticleSystemH__

#include"Engine_Include.h"
#include "MathMgr.h"

namespace Engine
{
	class CGraphicDev;
	class CInfoSubject;
}


#include "../../Client/Code/CameraObserver.h"


BEGIN(Engine)
class ENGINE_DLL ParticleSystem
{
public:
	ParticleSystem();
	virtual ~ParticleSystem();

	virtual bool Initialize(IDirect3DDevice9* device, wchar_t* texFileName);
	virtual void Reset();


	virtual void ResetParticle(Engine::tagAttribute* attribute) = 0;
	virtual void AddParticle();

	virtual void Update(float timeDelta) = 0;

	virtual void PreRender();
	virtual void Render();
	virtual void PostRender();

	bool isEmpty();
	bool isDead();

protected:
	virtual void removeDeadParticles();

protected:
	Engine::CGraphicDev*	 m_pGraphicDev = nullptr;
	IDirect3DDevice9*		 m_pDevice;

	D3DXVECTOR3             m_vOriginPos; // 시스템 내에서 파티클이 시작되는 원 위치

	tagBoundingBox	m_BoundingBox; // 경계 상자 파티클이 존재할 수 있는 영역

	float                   m_fEmitRate;   // 시스템에 새로운 파티클이 추가되는 비율  보통은 초당 파티클 수로 기록한다 

	float                   m_fSize;       // 시스템 내 모든 파티클의 크기
	IDirect3DTexture9*      m_pTexture;
	IDirect3DVertexBuffer9* m_pVB;
	std::list<tagAttribute>    _particles;
	int                     m_iMaxParticles; // 주어진 시간 동안 시스템이 가질 수 있는 최대 파티클의 수


	Engine::CInfoSubject*			m_pInfoSubject = nullptr;
	CCameraObserver*				m_pCameraObserver = nullptr;


	//
	// Following three data elements used for Rendering the p-system efficiently
	//

	DWORD m_pVBSize;      // 버텍스 버퍼가 보관할 수 있는 파티클의 수
	DWORD m_pVBOffset;    // offset in vb to lock   
	DWORD m_pVBBatchSize; // number of vertices to lock starting at m_pVBOffset
};
END
#endif // __ParticleSystemH__
