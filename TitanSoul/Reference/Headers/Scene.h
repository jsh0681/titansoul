#ifndef Scene_h__
#define Scene_h__

#include "Layer.h"

BEGIN(Engine)

class ENGINE_DLL CScene
{
protected:
	explicit CScene(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CScene(void);

public:
	const VTXTEX* GetTerrainVertex(const WORD& LayerID,	const wstring& wstrObjKey);

public:
	virtual HRESULT	Initialize(void);
	virtual void	Update(void);
	virtual void	Render(void);
	virtual void	Release(void);

	void		ObjListClear(WORD _wLayerKey, wstring _ObjListKey);
	void		ObjListAdd(WORD _wLayerKey, wstring _ObjListKey, CGameObject* _pObject);

	void		SetBlood(const bool& bResult) { m_bBlood = bResult; };
	virtual		void BloodMode();
protected:
	LPDIRECT3DDEVICE9				m_pGraphicDev;
	unordered_map<WORD, CLayer*>	m_mapLayer;
	unordered_map<WORD, CLayer*>	m_mapRenderLayer[RENDER_END];

	bool							m_bBlood = FALSE; //블러드효과
};

END
#endif // Scene_h__
