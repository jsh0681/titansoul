#ifndef Renderer_h__
#define Renderer_h__

#include "Scene.h"

BEGIN(Engine)

class ENGINE_DLL CRenderer
{
private:
	CRenderer(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	~CRenderer(void);

public:
	void		SetScene(CScene* pScene);
	HRESULT		Initialize(void);
	void		Render(float fTime);

public:
	LPD3DXFONT GetFont()		{ return m_pFont; }

private:
	LPDIRECT3DDEVICE9		m_pGraphicDev;
	CScene*					m_pScene;
	ID3DXFont*				m_pD3DXFont = nullptr;
	WORD					m_wFrameCnt = 0;
	TCHAR					m_szFPS[128];
	float					m_fTime = 0.f;

	LPD3DXFONT				m_pFont;

public:
	static		CRenderer*		Create(LPDIRECT3DDEVICE9 pGraphicDev);

};

END
#endif // Renderer_h__
