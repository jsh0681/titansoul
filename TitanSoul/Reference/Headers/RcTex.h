#ifndef RcTex_h__
#define RcTex_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CRcTex : public CVIBuffer
{
private:
	explicit CRcTex(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CRcTex(void);

public:
	virtual void	Release(void) override;
	virtual void	Render(void);
	HRESULT	CreateBuffer(void);
	HRESULT	CreateBuffer(const WORD& wCX, const WORD& wCY);

public:
	static CRcTex*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
	static CRcTex*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const WORD& wCX, const WORD& wCY);

};


END
#endif // RcTex_h__
