#ifndef MathMgr_h__
#define MathMgr_h__

#include "Engine_Include.h"

class ENGINE_DLL CMathMgr
{
public:
	CMathMgr(void);
	~CMathMgr(void);

public:
	static void MyRotationX(D3DXVECTOR3* pOut, D3DXVECTOR3* pIn, float fAngle);
	static void MyRotationY(D3DXVECTOR3* pOut, D3DXVECTOR3* pIn, float fAngle);
	static void MyRotationZ(D3DXVECTOR3* pOut, D3DXVECTOR3* pIn, float fAngle);

	static float MyVec3Dot(D3DXVECTOR3* _pV1, D3DXVECTOR3* _pV2);
	static void MyVec3Cross(D3DXVECTOR3* pVOutput, D3DXVECTOR3* _pV1, D3DXVECTOR3* _pV2);
	static float MyVec3Length(D3DXVECTOR3* _pInput);
	static void MyVec3Normalize(D3DXVECTOR3* _pOutput, D3DXVECTOR3* _pInput);
	static DWORD FtoDw(float f);
	static float GetRandomFloat(float lowBound, float highBound);
	static void GetRandomVector(D3DXVECTOR3* out, D3DXVECTOR3* min, D3DXVECTOR3* max);

};


#endif // MathMgr_h__
