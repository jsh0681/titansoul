#ifndef PlaneTex_h__
#define PlaneTex_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CPlaneTex : public CVIBuffer
{
private:
	explicit CPlaneTex(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CPlaneTex(void);

public:
	virtual void	Release(void) override;
	virtual void	Render(void);
	HRESULT	CreateBuffer();


public:
	static CPlaneTex*		Create(LPDIRECT3DDEVICE9 pGraphicDev, float _f0X, float _f0Z, float _f1X, float _f1Z
															, float _f2X, float _f2Z, float _f3X, float _f3Z, const int iOption);
	int m_iOpt;
public:
	static float m_f0X;
	static float m_f0Z;
	static float m_f1X;
	static float m_f1Z;
	static float m_f2X;
	static float m_f2Z;
	static float m_f3X;
	static float m_f3Z;
	static int m_iOption;

};


END
#endif // PlaneTex_h__
