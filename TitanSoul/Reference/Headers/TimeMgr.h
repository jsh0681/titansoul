#ifndef TimeMgr_h__
#define TimeMgr_h__

#include "Engine_Include.h"

BEGIN(Engine)

class ENGINE_DLL CTimeMgr
{
public:
	DECLARE_SINGLETON(CTimeMgr)

public:

private:
	CTimeMgr(void);
	~CTimeMgr(void);

private:
	LARGE_INTEGER		m_FrameTime;
	LARGE_INTEGER		m_FixTime;
	LARGE_INTEGER		m_LastTime;
	LARGE_INTEGER		m_CpuTick;

	float		m_fTime;
	float		m_fSlowMotion = 1.f;
	float		m_fSlowTurm = 5.f;
	bool		m_bSlowMode = FALSE;
public:
	float GetTime(void);

public:
	void InitTime(void);
	void SetTime(void);
	void SlowModeStart(const float& fTime, const float& fSpd);


};

END


#endif // TimeMgr_h__

