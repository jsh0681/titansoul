#ifndef RcCol_h__
#define RcCol_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CRcCol : public CVIBuffer
{
private:
	explicit CRcCol(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CRcCol(void);

public:
	virtual void	Release(void) override;
	virtual void	Render(void);
	HRESULT	CreateBuffer(void);

public:
	static CRcCol*		Create(LPDIRECT3DDEVICE9 pGraphicDev);

};


END
#endif // RcCol_h__
