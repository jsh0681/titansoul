#ifndef Texture_h__
#define Texture_h__

#include "Resources.h"

BEGIN(Engine)

class ENGINE_DLL CTexture : public CResources
{
private:
	explicit CTexture(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CTexture(void);

private:
	HRESULT		LoadTexture(TEXTYPE eTexType, 
							const wstring& wstrFilePath, 
							const WORD& wCnt);

public:
	void	Render() {}
	void	Render(const DWORD& iIndex);

private:
	vector<IDirect3DBaseTexture9*>			m_vecTexture;
	DWORD									m_dwContainerSize;

public:
	static CTexture*	Create(LPDIRECT3DDEVICE9 pGraphicDev, 
								TEXTYPE eTexType, 
								const wstring& wstsrFilePath, 
								const WORD& wCnt);
	virtual	CResources*		Clone(void);
	void	Release(void);

};

END
#endif // Texture_h__
