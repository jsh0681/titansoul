#ifndef Engine_struct_h__
#define Engine_struct_h__

namespace Engine
{
	typedef	struct tagVertexColor
	{
		D3DXVECTOR3		vPos;
		DWORD			dwColor;

	}VTXCOL;

	const DWORD		VTXFVF_COL = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX0;

	typedef	struct tagVertexTexture
	{
		D3DXVECTOR3		vPos;
		D3DXVECTOR2		vTex;

	}VTXTEX;

	const DWORD		VTXFVF_TEX = D3DFVF_XYZ | D3DFVF_TEX1;

	typedef	struct tagVertexCube
	{
		D3DXVECTOR3		vPos;
		D3DXVECTOR3		vTex;

	}VTXCUBE;

	const DWORD		VTXFVF_CUBE = D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE3(0);
	
	typedef struct tagIndex16
	{
		WORD	_0, _1, _2;

	}INDEX16;

	typedef struct tagIndex32
	{
		DWORD	_0, _1, _2;

	}INDEX32;

	typedef struct tagInfo
	{
		D3DXVECTOR3		vPos;
		D3DXVECTOR3		vLook;
		D3DXVECTOR3		vDir;

		D3DXMATRIX		matWorld;

	}INFO;

	typedef struct tagParticle
	{
		D3DXVECTOR3 _position;
		D3DCOLOR    _color;
		static const DWORD FVF;
	}PARTICLE;
	const DWORD FVF_PARTICLE = D3DFVF_XYZ | D3DFVF_DIFFUSE;

	typedef struct tagAttribute
	{
		tagAttribute()
		{
			_lifeTime = 0.0f;
			_age = 0.0f;
			_isAlive = true;
		}

		D3DXVECTOR3 _position;     //월드 스페이스 내의 파티클 위치 
		D3DXVECTOR3 _velocity;     //파티클의 속도， 보통은 초당 이동 단위로 기록한다，
		D3DXVECTOR3 _acceleration; //파티클의 가속 보통은 초당 이동 단위로 기록한다 
		float       _lifeTime;     // 파티클이 소멸할 때까지 유지되는 시간 
		float       _age;          // 파티클의 현재 나이  
		D3DXCOLOR   _color;        // 파티클의 컬러가 시간의 흐름에 따라 퇴색하는 방법   
		D3DXCOLOR   _colorFade;    // 파티클이 생존한 경우 True , 소멸한 경우 False	
		bool        _isAlive;
	}ATTRIBUTE;

	typedef struct tagBoundingBox
	{
		tagBoundingBox::tagBoundingBox()
		{
			_min.x = FLT_MAX;
			_min.y = FLT_MAX;
			_min.z = FLT_MAX;

			_max.x = -FLT_MAX;
			_max.y = -FLT_MAX;
			_max.z = -FLT_MAX;
		}


		bool isPointInside(D3DXVECTOR3& p)
		{
			if (p.x >= _min.x && p.y >= _min.y && p.z >= _min.z &&
				p.x <= _max.x && p.y <= _max.y && p.z <= _max.z)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		D3DXVECTOR3 _min;
		D3DXVECTOR3 _max;
	}BOUNDINGBOX;
}
#endif // Engine_struct_h__
