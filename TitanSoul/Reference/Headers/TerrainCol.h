#ifndef TerrainCol_h__
#define TerrainCol_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CTerrainCol : public CVIBuffer
{
private:
	explicit CTerrainCol(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CTerrainCol(void);

public:
	virtual void	Release(void) override;
	virtual void	Render(void);
	HRESULT	CreateBuffer(const WORD& wCntX, const WORD& wCntZ, const WORD& wItv);

public:
	static CTerrainCol*		Create(LPDIRECT3DDEVICE9 pGraphicDev, 
		const WORD& wCntX, const WORD& wCntZ, const WORD& wItv);

};


END
#endif // TerrainCol_h__
