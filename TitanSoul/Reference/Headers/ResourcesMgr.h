#ifndef ResourcesMgr_h__
#define ResourcesMgr_h__

#include "TriCol.h"
#include "RcCol.h"
#include "RcTex.h"
#include "TerrainCol.h"
#include "TerrainTex.h"
#include "CubeTex.h"
#include "PlaneTex.h"
#include "RcTexZ.h"
#include "CrossTex.h"

#include "Texture.h"

#include "Component.h"

BEGIN(Engine)

class ENGINE_DLL CResourcesMgr
{
	DECLARE_SINGLETON(CResourcesMgr)

private:
	CResourcesMgr(void);
	~CResourcesMgr(void);

public:
	void	ClientToEngine(RESOURCETYPE eResourceType, const wstring& wstrResourceKey, void* pVertex);
	void	EngineToClient(RESOURCETYPE eResourceType, const wstring& wstrResourceKey, void* pVertex);

public:
	CComponent*		Clone(RESOURCETYPE eResourceType, 
						  const wstring& wstrResourceKey);

	HRESULT		AddBuffer(LPDIRECT3DDEVICE9 pGraphicDev, 
						RESOURCETYPE eResourceType, 
						BUFFERTYPE eBufferType, 
						const wstring& wstrResourceKey, 
						const WORD& wCntX = 0, 
						const WORD& wCntZ = 0, 
						const WORD& wItv = 1);

	HRESULT		AddTexture(LPDIRECT3DDEVICE9 pGraphicDev,
							RESOURCETYPE eResourceType,
							TEXTYPE	 eTexType, 
							const wstring& wstrResourceKey, 
							const wstring& wstrFilePath, 
							const WORD& wCnt);

	void		Render(const wstring& wstrResourceKey);
	void		Render(const wstring& wstrResourceKey, const DWORD& iIndex);
	void		ResetDynamic(void);

private:
	void		Release(void);

private:
	CResources*							m_pResources;
	map<wstring, CResources*>			m_mapResources[RESOURCE_END];
	
};
END
#endif // ResourcesMgr_h__
