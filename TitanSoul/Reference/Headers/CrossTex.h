#ifndef CrossTex_h__
#define CrossTex_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CCrossTex : public CVIBuffer
{
private:
	explicit CCrossTex(LPDIRECT3DDEVICE9 pGraphicDev);
public:
	virtual ~CCrossTex(void);

public:
	virtual void	Release(void) override;
	virtual void	Render(void);
	HRESULT	CreateBuffer(void);
	HRESULT	CreateBuffer(const WORD& wCX, const WORD& wCZ);

public:
	static CCrossTex*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
	static CCrossTex*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const WORD& wCX, const WORD& wCZ);

};


END
#endif // CrossTex_h__
