#ifndef InfoSubject_h__
#define InfoSubject_h__

#include "Subject.h"

typedef list<void*>				DATALIST;
typedef map<int, list<void*>>	MAPDATALIST;

BEGIN(Engine)

class ENGINE_DLL CInfoSubject : public CSubject
{
	DECLARE_SINGLETON(CInfoSubject)

private:
	CInfoSubject(void);
	virtual ~CInfoSubject(void);

public:
	DATALIST*	GetDataList(int iFlag);

public:
	void		AddData(int iFlag, void* pData);
	void		RemoveData(int iFlag, void* pData);

private:
	void	Release(void);

private:
	MAPDATALIST		m_MapDataList;


};

END
#endif // InfoSubject_h__
