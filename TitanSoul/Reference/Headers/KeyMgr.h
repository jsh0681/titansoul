#ifndef KeyMgr_h__
#define KeyMgr_h__
#include "Engine_Include.h"
BEGIN(Engine)

class ENGINE_DLL CKeyMgr
{
	DECLARE_SINGLETON(CKeyMgr)

private:
	CKeyMgr();
public:
	~CKeyMgr();

public:
	void KeyCheck();
	bool KeyUp(DWORD dwKey);
	bool KeyDown(DWORD dwKey);
	bool KeyPressing(DWORD dwKey);
	bool KeyCombine(DWORD dwFirst, DWORD dwSecond);

private:
	DWORD m_dwKey;
	DWORD m_dwKeyPressed;
	DWORD m_dwKeyDown;
};

END
#endif // GraphicDev_h__


