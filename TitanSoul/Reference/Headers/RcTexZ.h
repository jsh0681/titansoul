#ifndef RcTexZ_h__
#define RcTexZ_h__

#include "VIBuffer.h"

BEGIN(Engine)

class ENGINE_DLL CRcTexZ : public CVIBuffer
{
private:
	explicit CRcTexZ(LPDIRECT3DDEVICE9 pGraphicDev);

public:
	virtual ~CRcTexZ();

public:
	virtual void	Release()	override;
	virtual void	Render()	override;

private:
	HRESULT			CreateBuffer();
	HRESULT			CreateBuffer(const WORD& wCntX, const WORD& wCntZ);

public:
	static CRcTexZ*		Create(LPDIRECT3DDEVICE9 pGraphicDev);
	static CRcTexZ*		Create(LPDIRECT3DDEVICE9 pGraphicDev, const WORD& wCntX, const WORD& wCntZ);
};

END

#endif // !RcTexZ_h__