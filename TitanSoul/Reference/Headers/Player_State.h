#ifndef Player_State_h__
#define Player_State_h__

#include "Engine_Include.h"

BEGIN(Engine)
class ENGINE_DLL CPlayer_State
{
protected:
	CPlayer_State();
public:
	virtual ~CPlayer_State();
public:
	//플레이어 상태 : 대기, 이동, 달리기, 구르기, 공격, 화살회수, 죽음, 수영, 오르기(덩굴), 세레모니(보스잡고), 일어서기(시작지점)
	//세레모니와 보스 (끝 2개)는 8가지 방향이 아님.
	enum STATE_P { P_IDLE, P_MOVE, P_RUN, P_ROLL, P_ATTACK, P_RECOVERY, P_DIE, P_SWIM, P_CLIMB, P_DANCE, P_RELEVER};
	enum DIRECTION { DD, DL, LL, UL, UU, UR, RR, DR, END_DIR};
	virtual HRESULT Initialize(void) = 0;
	virtual void Update(void) = 0;
	virtual void Release(void) = 0;

protected: //변수는 나중에 추가해보고...
	
};


END
#endif // Player_State_h__